class HospitalReview < ApplicationRecord
  belongs_to :user
  belongs_to :hospital

  scope :approved, -> { where(approved: true) }

end
