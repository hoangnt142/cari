class Job < ApplicationRecord

  enum salary_type: {hour: 1, day: 2, month: 3, year: 4}

  belongs_to :hospital
  belongs_to :website

  has_many :job_search_keys
  has_many :search_keys, through: :job_search_keys

  has_many :qualifications, through: :job_search_keys, source: :search_key, class_name: 'Qualification'
  has_many :job_types, through: :job_search_keys, source: :search_key, class_name: 'JobType'
  has_many :job_details, through: :job_search_keys, source: :search_key, class_name: 'JobDetail'
  has_many :job_contracts, through: :job_search_keys, source: :search_key, class_name: 'JobContract'

  has_one :cache_job_search

  # scope :job_types, -> { JobType.joins(:jobs).where(jobs: {id: ids}) }
  # scope :job_details, -> { JobDetail.joins(:jobs).where(jobs: {id: ids}) }
  # scope :qualifications, -> { Qualification.joins(:jobs).where(jobs: {id: ids}) }


  def create_cache!
    bits = nil
    search_key_texts = []
    if self.job_search_keys
      ids = self.job_search_keys.collect{ |jsk| jsk.search_key_id }
      # Include hospital_types into cache_job_searches
      ids_hospital_types = self.hospital.hospital_hospital_types.collect{ |hht| hht.search_key_id }

      bits = SearchKey.ids_to_bits(ids+ids_hospital_types)

      search_key_texts = self.job_search_keys.collect{ |jsk| jsk.search_key.name + jsk.search_key.match_text }
    end


    hospital = self.hospital

    fulltext = (search_key_texts + [
                  hospital.name,
                  hospital.prefecture.name,
                  hospital.city.name,
                  hospital.address,
                  hospital.standard,
                  hospital.karte,
                  hospital.features,
                  self.salary_detail,
                  self.allowance,
                  self.work_time
                ].compact || []).join(' ')

    create_cache_job_search!(
      hospital_id: self.hospital_id,
      job_id: self.id,
      region_id: self.hospital.prefecture.region_id,
      prefecture_id: self.hospital.prefecture_id,
      city_id: self.hospital.city_id,
      cache_job_search_keys_bits: bits,
      cache_fulltext: fulltext
    )
  end

  def salary_formated
    ActionController::Base.helpers.number_with_delimiter(salary)
  end

  def salary_type_converted
    salary_type ? I18n.t("salary_type.#{salary_type}") : ''
  end

  def rss_description
    [ self.hospital.name,
      self.hospital.work_location,
      self.hospital.access,
      self.qualifications.collect{ |q| q.name }.join(','),
      (salary_formated.nil? ||
       salary_formated == 0 ||
       salary_formated.length == 0) ? '給与 非公開情報' :
        "#{salary_type_converted}#{salary_formated}"
    ].compact.join(' / ')
  end

end
