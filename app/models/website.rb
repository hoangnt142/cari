class Website < ApplicationRecord

  has_many :websites_hospitals

  class << self
    def kango_oshigoto
      find(1)
    end

    def kango_mynavi
      find(2)
    end

    def nursejinzaibank
      find(3)
    end

    def hellowork
      find(4)
    end
  end

  def redirect_link_url
    "/redirect-links/#{id}"
  end

end
