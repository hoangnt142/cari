class Page < ApplicationRecord
  enum publish_status: [:unpublished, :published, :deleted]

  belongs_to :pageable, polymorphic: true, optional: true
  belongs_to :image, -> { where(category_type: Image.category_types[:other]) }, dependent: :destroy, optional: true
  belongs_to :cta, optional: true
  has_many :page_views
  validates :url, :title, presence: true
  validates :url, uniqueness: true

  accepts_nested_attributes_for :image, reject_if: lambda { |c| c[:image].blank? && c[:alt].blank? }

  scope :filled, -> { where(filled: true) }
  scope :unfilled, -> { where(filled: false) }

  # def to_json
  #   json = {}
  #   attributes.each do |attr, value|
  #     json[attr] = value || ''
  #   end
  #   { page: json }
  # end

  before_validation :set_url
  after_update :update_child_url, if: lambda { |page| page.saved_change_to_url? && page.pageable_type == 'Category'  }

  def status
    case
    when unpublished? then "非公開"
    when published? then "公開済み"
    when deleted? then "削除"
    else ""
    end
  end

  def used_content
    limit_characters = 40
    %w(use_content_top use_content use_content_bottom).each do |content|
      if eval(content)
        strip_tags_content = ActionController::Base.helpers.strip_tags(eval(content.gsub('use_', '')).to_s).truncate(limit_characters, omission: '・・・')
        return strip_tags_content if strip_tags_content.present?
      end
    end
    ActionController::Base.helpers.strip_tags(content.to_s).truncate(limit_characters, omission: '・・・')
  end

  def page_date_formatted
    (page_date || created_at).strftime('%Y年%m月%d日')
  end

  def published_at_formatted
    publish_at.try(:strftime, '%Y年%m月%d日')
  end

  def self.not_found
    find_by(id: 16)
  end

  def not_found?
    id == 16
  end

  private

  def update_child_url
    child_categories = pageable.children
    articles = pageable.articles

    child_categories.each do |category|
      page = category.page
      page_url = page.url
      page_url.gsub!(url_before_last_save, url)
      page.update_columns(url: page_url)
    end

    articles.each do |article|
      page = article.page
      page_url = page.url
      page_url.gsub!(url_before_last_save, url)
      page.update_columns(url: page_url)
    end
  end

  def set_url
    slug = url_format(url_slug)
    self.url_slug = slug
    if !%w(Article Category).include?(pageable_type)
      if url_slug.blank?
        self.url_slug = url_format(url)
      else
        self.url = slug
      end
    end
  end

end
