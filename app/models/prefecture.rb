class Prefecture < ApplicationRecord
  has_many :areas, -> { where parent_id: nil }
  has_many :cities
  belongs_to :region
end
