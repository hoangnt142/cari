class JobSearchKey < ApplicationRecord
  belongs_to :job
  belongs_to :search_key
end
