class Area < ApplicationRecord

  belongs_to :prefecture
  has_many :children, class_name: 'Area', foreign_key: 'parent_id'
  belongs_to :parent, class_name: 'Area', foreign_key: 'parent_id'

  scope :area1, -> { where parent_id: nil }

end
