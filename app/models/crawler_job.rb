class CrawlerJob < ApplicationRecord
  self.table_name = 'jobs'
  establish_connection :crawler_db

#  belongs_to :hospital, optional: true
  has_many :crawler_job_search_keys, foreign_key: 'job_id', primary_key: 'id'
end
