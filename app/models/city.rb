class City < ApplicationRecord
  belongs_to :area1, optional: true, foreign_key: :area1_id, class_name: 'Area'
  belongs_to :area2, optional: true, foreign_key: :area2_id, class_name: 'Area'
  belongs_to :area3, optional: true, foreign_key: :area3_id, class_name: 'Area'
end
