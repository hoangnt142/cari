require 'shell'
class Hospital < ApplicationRecord

  has_many :hospital_reviews
  has_many :hospital_hospital_types
  has_many :hospital_types, through: :hospital_hospital_types, foreign_key: 'search_key_id'
  has_one :page, as: :pageable
  has_many :jobs
  belongs_to :prefecture
  belongs_to :city

  accepts_nested_attributes_for :page

  after_save do |record|
    if record.page.nil?

#      a = record.name.underscore.gsub(/[ 　]+/, '-').gsub(/[\:\.\&\?\=\\\/]/,'')
#      sh = Shell.new
#      roman = sh.transact { system("echo", a) | system("kakasi", "-Ja", "-Ha", "-Ka", "-Ea", "-i", "utf-8", "-o", "utf-8") }
#      roman = roman.to_s.sub(/\n/,'')

## hospital name is duplicated in same prefecture.

      url = "/#{record.prefecture.name_roman}/hospital/#{record.id}"
      record.create_page!(url: url,
                          name: record.name,
                          title: record.name,
                          publish_status: 1)
    end
  end

  def max_job_created
    (jobs.pluck(:created_at).max || created_at).strftime("%Y年%m月%d日")
  end

  def breadcrumb
    [
      { title: '看護師求人サイトCarriee', url: '/' },
      { title: prefecture.name, url: "/#{prefecture.name_roman}" }
    ]
  end

  def work_location
    "#{prefecture.name}#{city.name}#{address}"
  end

  class << self
    def count_reviews_and_calculate_point
      Hospital.find_each do |hospital|
        reviews = hospital.hospital_reviews.approved
        next if reviews.size == 0

        hospital.review_count =  reviews.size

        point_recommends = 0
        point_career = 0
        point_environment = 0

        reviews.each do |r|
          point_recommends += r.point_recommends
          point_career += r.point_career
          point_environment += r.point_environment
        end

        hospital.average_point = (point_recommends + point_career + point_environment) / (reviews.size*3)
        hospital.save!
      end
    end
  end
end
