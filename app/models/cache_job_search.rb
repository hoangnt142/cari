class CacheJobSearch < ApplicationRecord

  belongs_to :hospital
  belongs_to :region
  belongs_to :city
  # belongs_to :job


  class << self

    def total_job_count
      joins(:hospital).where(hospitals: { deleted: false }).count
    end

    def set_condition(params)
      # adding where conditions
      model = self

      if params[:regionId].present?
        model = model.where(region_id: params[:regionId])
      end

      field = 'prefectureId'
      if (value = params[field])
        model = model.where(prefecture_id: value)
      end


      field = 'areaIds'
      if params[field]
        city_ids = get_city_ids(params)
        if city_ids.present?
          model = model.where(city_id: city_ids)
        else
          # No result
          model = model.where('1=0')
        end
      end

      all_search_key_ids = [] #only show jobs are related to these search keys

      ['hospitalTypeIds',
       'jobContractIds',
       'jobDetailIds',
       'jobTypeIds',
       'qualificationIds',
      ].each do |id_type|

        collected_ids = []
        if (value = params[id_type]) && value.length > 0
          ids = value =~ /,/ ? value.split(',') : [value]
          collected_ids += ids
        end
#        logger.debug "collected_ids: #{collected_ids}"
        search_key_ids = collected_ids

        if search_key_ids.length > 0
          all_search_key_ids += search_key_ids if id_type != 'hospitalTypeIds'
          bits = SearchKey.ids_to_bits(search_key_ids)
          model = model.where("cache_job_search_keys_bits & :bits", bits: bits)
        end
      end

      field = 'text'
      if (value = params[field])
        sanitized_words = ActiveRecord::Base.connection.quote(value)
        match_phrase = "MATCH(cache_fulltext) AGAINST(#{sanitized_words})"
        model = model.where(match_phrase)
        order = match_phrase
      else
        order = { created_at: :desc }
      end

      # order
      model = model.order(order)

      model
    end

    def all_search_key_ids(params)
      all_search_key_ids = [] #only show jobs are related to these search keys

      ['hospitalTypeIds',
       'jobContractIds',
       'jobDetailIds',
       'jobTypeIds',
       'qualificationIds',
      ].each do |id_type|

        collected_ids = []
        if (value = params[id_type]) && value.length > 0
          ids = value =~ /,/ ? value.split(',') : [value]
          collected_ids += ids
        end
        search_key_ids = collected_ids

        if search_key_ids.length > 0
          all_search_key_ids += search_key_ids if id_type != 'hospitalTypeIds'
        end
      end
      all_search_key_ids
    end

    def refresh
      begin
        ActiveRecord::Base.transaction do
          CacheJobSearch.delete_all

          Job.find_each do |job|
            job.create_cache!
          end
        end
      rescue ActiveRecord::Rollback => e
        logger.error "Error occurred #{e}"
      end
    end


    private
    def get_city_ids(params)
      return nil unless params[:areaIds].present?

      ids = (params[:areaIds] =~ /,/ ? params[:areaIds].split(',') : [params[:areaIds]]).collect{ |str| str.to_i }

      prefecture_id = params['prefectureId']
      city_ids1 = city_ids2 = city_ids3 = []
      if prefecture_id.to_i == 13
        # For Tokyo(id=13)
        # Case1: area1 is selected, and none of area2 is selected, show all of area1
        # Case2: area1 is selected, and some of area2 is selected, and none of area3 is selected, show area2
        # Case3: area1 is selected, and some of area2 is selected, and some of area3 is selected, show area3

        area3s = Area.select('id,parent_id').where(prefecture_id: prefecture_id).where(level: 3).where(id: ids)
        area3_ids = area3s.pluck(:id)
        ids_to_be_removed_as_area2 = area3s.pluck(:parent_id)

        area2s = Area.select('id,parent_id').where(prefecture_id: prefecture_id).where(level: 2).where(id: ids)
        area2_ids = area2s.pluck(:id).reject{ |id| ids_to_be_removed_as_area2.include?(id) }
        ids_to_be_removed_as_area1 = area2s.pluck(:parent_id)

        area1s = Area.select('id,parent_id').where(prefecture_id: prefecture_id).where(level: 1).where(id: ids)
        area1_ids = area1s.pluck(:id).reject{ |id| ids_to_be_removed_as_area1.include?(id) }


        logger.debug "area1_ids: #{area1_ids}, area2_ids: #{area2_ids}, area3_ids: #{area3_ids}"
        city_ids1 = City.where("area1_id": area1_ids).pluck(:id) if area1_ids.length > 0
        city_ids2 = City.where("area2_id": area2_ids).pluck(:id) if area2_ids.length > 0
        city_ids3 = City.where("area3_id": area3_ids).pluck(:id) if area3_ids.length > 0
      else
        # For other prefectures
        # Case1: area1 is selected, and none of area2 is selected, show all of area1
        # Case2: area1 is selected, and some of area2 is selected, show only the area2

        area2s = Area.select('id,parent_id').where(prefecture: prefecture_id).where(level: 2).where(id: ids)
        area2_ids = area2s.pluck(:id)
        ids_to_be_removed_as_area1 = area2s.pluck(:parent_id)

        area1s = Area.select('id,parent_id').where(prefecture_id: prefecture_id).where(level: 1).where(id: ids)
        area1_ids = area1s.pluck(:id).reject{ |id| ids_to_be_removed_as_area1.include?(id) }

        city_ids1 = City.where("area1_id": area1_ids).pluck(:id) if area1_ids.length > 0
        city_ids2 = City.where("area2_id": area2_ids).pluck(:id) if area2_ids.length > 0
      end

      (city_ids1 + city_ids2 + city_ids3).uniq

    end

  end

end
