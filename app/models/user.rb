class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable

  enum role: [:admin, :general]

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :omniauthable,
         :omniauth_providers => [:facebook]

  has_many :hospital_reviews
  has_many :user_favorite_hospitals
  has_many :user_history_hospitals

  def self.admin_authenticate(email, password)
    user = admin.find_by_email(email)
    return nil unless user
    user if user.valid_password?(password)
  end

end
