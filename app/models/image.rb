class Image < ApplicationRecord

  has_attached_file :image,
                    url: "/upload_images/:class/:id/:style_:filename",
                    styles: lambda { |a| a.instance.image_styles }

  enum category_type: [:library, :other]
  has_one :page
  validates :category_type, :width, :height, presence: true, if: lambda { |a| a.library? }

  validates_attachment  :image,
                        presence: true,
                        content_type: { content_type: ["image/jpeg",
                                                      "image/jpg",
                                                      "image/gif",
                                                      "image/png"]},
                        size: { in: 0..5.megabytes }

  def image_styles
    {standard: "420x280#", thumb: "315x210#", large: "600x365#"}
  end

  def preview
    image.present? ? image.url(:large) : ''
  end

  def as_json(options = {})
    h = super(options)
    h[:image_url] = {
      thumb: image.url(:thumb),
      standard: image.url(:standard),
      large: image.url(:large)
    }
    h
  end
end
