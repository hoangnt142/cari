class Article < ApplicationRecord
  include Pageable
  belongs_to :category, optional: true
  belongs_to :search_key, optional: true
  has_many :page_views, through: :page

  scope :ranking_all_time, -> (this, limit=5) {
    published.joins(:page_views).select("articles.*, SUM(page_views.page_view_count) as total_view_count")
    .where(category_id: this.category_id).where.not(id: this.id)
    .group(:id).order("total_view_count DESC").limit(limit)
  }

  after_create :init_page_views

  def short_content
    ActionController::Base.helpers.strip_tags(content.to_s).truncate(100, omission: '')
  end

  def search_key_articles
    Article.where.not(id: id, search_key: nil)
      .published.where(search_key: search_key)
      .order("RAND()").limit(3)
      .as_json(
        methods: [
          :image, :url, :title
        ]
      )
  end

  def next
    Article.published.where("pages.publish_at > ?", publish_at).last.as_json(
        methods: [
          :image, :url, :title
        ]
      )
  end

  def prev
    Article.published.where("pages.publish_at < ?", publish_at).first.as_json(
        methods: [
          :image, :url, :title
        ]
      )
  end

  def breadcrumb
    result = []
    if category.present?
      result << {title: category.title, url: category.url}
    end
    result << {title: title, url: url}
    result
  end

  private

  def init_page_views
    page.page_views.create(year_month: Time.now, page_view_count: 0)
  end
end
