class CrawlerHospitalVersion < ApplicationRecord
  self.table_name = 'hospital_versions'
  establish_connection :crawler_db

#  belongs_to :hospital, optional: true
  has_many :crawler_hospital_version_hospital_types, foreign_key: 'hospital_version_id', primary_key: 'id'
end
