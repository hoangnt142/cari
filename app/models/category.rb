class Category < ApplicationRecord
  include Pageable
  has_many :children, class_name: "Category", foreign_key: :parent_id
  belongs_to :parent, class_name: "Category", foreign_key: :parent_id, optional: true

  def articles
    category_ids = children.pluck(:id) << id
    Article.published.includes(:category).where(category_id: category_ids).order("pages.publish_at DESC")
  end

  def breadcrumb
    result = []
    if parent.present? && parent.published?
      result << {title: parent.title, url: parent.url}
    end
    result << {title: title, url: url}
    result
  end
end