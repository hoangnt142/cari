class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  def url_format(url)
    return '' if url.blank?
    url.to_s.first == '/' ? url : ('/'+url.to_s)
  end

end
