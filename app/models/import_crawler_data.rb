class ImportCrawlerData
  class << self
    def execute(specified_range_to = nil)

      begin
        batch_started_at = Time.now
        range_to = specified_range_to || batch_started_at

        count = ImportLog.count
        range_from =
          if count == 0
            CrawlerHospitalVersion.minimum(:created_at) - 1.second
          else
            ImportLog.last.range_to
          end


        # begin transaction
        ActiveRecord::Base.transaction do
          CrawlerHospitalVersion.where('created_at > :range_from AND created_at <= :range_to',
                                       range_from: range_from, range_to: range_to
                                      ).each{ |chv|

            # insert or update hospitals
            # chv.hospital_id should be absolutely same with the hospitals.id.
            found = Hospital.exists?(chv.hospital_id)
            if !found
              attrs = chv.attributes.dup
              attrs['id'] = attrs['hospital_id']
              attrs['title'] = ''
              %w(hospital_id created_at updated_at).each{ |field|
                attrs.delete(field)
              }

              hospital = Hospital.create!(attrs)
            else
              attrs = chv.attributes.dup
              %w(id hospital_id created_at updated_at).each{ |field|
                attrs.delete(field)
              }
              hospital = Hospital.find(chv.hospital_id)
              attrs.each{ |key, value| hospital[key] = value }
              hospital.save!
            end

            # insert hospital_hospital_types

            # before inserting, delete all existing records
            if hospital.hospital_hospital_types.length > 0
              ids = hospital.hospital_hospital_types.collect{ |hht| hht.id }
              HospitalHospitalType.where(id: ids).destroy_all
            end

            chv.crawler_hospital_version_hospital_types.each{ |chvht|
              hospital.hospital_hospital_types.create!(
                search_key_id: chvht.search_key_id
              )
            }


            # Delete existing jobs of the hospital added from the website which is included in jobs
            # before inserting the new jobs.
            website_ids = CrawlerJob.where(hospital_id: hospital.id).collect{ |cj| cj.website_id }
            if website_ids.length > 0
              hospital.jobs.where(website_id: website_ids).destroy_all
            end

          }


          # insert jobs
          CrawlerJob.where('created_at > :range_from AND created_at <= :range_to',
                           range_from: range_from, range_to: range_to
                          ).each{ |cj|

            attrs = cj.attributes.dup
            %w(id hospital_id created_at updated_at).each { |field|
              attrs.delete(field)
            }
            hospital = Hospital.find(cj.hospital_id)

            # Insert on jobs
            job = hospital.jobs.create!(attrs)

            # insert on job_search_keys
            cj.crawler_job_search_keys.each{ |cjsk|
              job.job_search_keys.create!(
                search_key_id: cjsk.search_key_id
              )
            }
          }

          # count totals
          ## prefecture
          Prefecture.all.each{ |prefecture|
            prefecture.total_jobs = CacheJobSearch.where(prefecture_id: prefecture.id).count
            prefecture.save!
          }
          ## area
          ## TODO...



          # Refresh cache_job_searches
          CacheJobSearch.refresh


          # Save import log
          il = ImportLog.create!(
            batch_started_at: batch_started_at,
            batch_ended_at: Time.now,
            range_from: range_from,
            range_to: range_to
          )
          Rails.logger.info "Finished: #{il.inspect}"
        end

      rescue ActiveRecord::Rollback => e
        logger.error "Error occurred #{e}"
      end

    end
  end
end
