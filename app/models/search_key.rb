class SearchKey < ApplicationRecord
  has_many :job_search_keys
  has_many :jobs, through: :job_search_keys

  class << self
    # `cache_jobs_search_keys_bits` is calculated by the following equation
    def ids_to_bits(*params)
      if params.length == 1 && params[0].is_a?(String)
        # String
        ids = [params]
      elsif params.length == 1 && params[0].is_a?(Array)
        ids = params[0]
      else
        ids = params
      end
      logger.debug "ids: #{ids}"

      sum = ids.inject(0) { |sum, id|
        logger.debug "sum: #{sum}, id: #{id}"
        sum = sum + 2**(id.to_i - 1)
      }
      sum
    end
  end
end
