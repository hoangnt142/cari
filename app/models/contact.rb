class Contact < ApplicationRecord
	enum contact_type: [
      :opinions,
      :about_site,
      :request_for_improvement,
      :ad,
      :other,
       ]

  validates_format_of :email, :with => Devise::email_regexp
  validates :content, :contact_type, :name, :company, presence: true

  after_save :send_to_admin

  def send_to_admin
    AdminMailer.send_contact_to_admin(self).deliver
  end

end
