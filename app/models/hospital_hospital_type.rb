class HospitalHospitalType < ApplicationRecord
  belongs_to :hospital
#  belongs_to :hospital_type, -> { where(type: 'HospitalType') }, foreign_key: 'search_key_id', class_name: 'SearchKey'
  belongs_to :hospital_type, foreign_key: 'search_key_id', class_name: 'SearchKey'

  before_save do |record|
    raise "#{record.hospital_type.type} can't be added." if record.hospital_type.type != 'HospitalType'
  end
end
