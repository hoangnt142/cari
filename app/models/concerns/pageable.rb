module Pageable
  extend ActiveSupport::Concern
  included do

    has_one :page, as: :pageable, dependent: :destroy

    scope :published, -> { includes(page: :image).where(pages: {publish_status: Page.publish_statuses[:published]}) }

    scope :roots, ->(this = nil) { includes(:page).where.not( pages: { url_slug: nil }, id: this ) }

    accepts_nested_attributes_for :page

    before_validation :set_url

    def set_url
      slug = url_format(page.url_slug)
      page.url_slug = slug

      if try(:parent_id) #for child categories
        page.url = parent.url + slug
      elsif is_a?(Category)
        page.url = slug
      end

      if try(:category_id) #for article
        page.url = category.url + slug
      elsif is_a?(Article)
        page.url = slug
      end
    end

    def increase_view
      page_view = page.page_views.where("DATE(page_views.year_month) = ?", Date.today).first_or_create(year_month: Time.now)
      page_view.update(page_view_count: page_view.page_view_count+1)
    end

  end

  delegate :url_slug, :url, :title, :published?, :image, :publish_at, :content, :cta, to: :page
  
end