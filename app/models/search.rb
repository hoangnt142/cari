class Search < ApplicationRecord

  has_one :page, as: :pageable
  belongs_to :prefecture, optional: true
  belongs_to :area, optional: true
  belongs_to :qualification, optional: true
  belongs_to :job_contract, optional: true
  belongs_to :hospital_type, optional: true
  belongs_to :job_type, optional: true
  belongs_to :job_detail, optional: true

  class << self
    def hospital_count(condition=nil)
      # When counting for /search page, returns all as setting no condition.
      model = condition ? CacheJobSearch.set_condition(condition) : CacheJobSearch
      sql0 = model
               .select('hospital_id')
               .group('hospital_id')
               .joins(:hospital)
               .where(hospitals: { deleted: false })
               .to_sql
      sql =<<-SQL
    SELECT COUNT(*) AS cnt FROM (#{sql0}) a
    SQL

      record = ActiveRecord::Base.connection.execute(sql)
      cnt = nil
      record.each do |r|
        cnt = r[0]
      end
      cnt
    end

    # def find_by_params(params)
    #   model = self
    #   [
    #     "prefectureId",
    #     "areaIds",
    #     "qualificationIds",
    #     "jobTypeIds",
    #     "jobDetailIds",
    #     "jobContractIds",
    #     "hospitalTypeIds",
    #   ].each do |camel_field|

    #     field = camel_field.singularize.underscore
    #     value = params[camel_field]
    #     logger.debug "#{field}: #{value}"

    #     if value
    #       condition = {}
    #       condition[field.to_sym] = value
    #       model = model.where(condition)
    #     end
    #   end

    #   record = model.take

    #   logger.debug record.try(:id)
    #   record
    # end
  end

  def hospital_count
    condition = map_to_js_condition()
    self.class.hospital_count(condition)
  end

  def job_count
    condition = map_to_js_condition()
    model = CacheJobSearch.set_condition(condition)
    total = model.joins(:hospital).where(hospitals: { deleted: false }).count
    total
  end

  private
  def map_to_js_condition
    condition = {}
    mapping = {
      prefecture_id: 'prefectureIds',
    }
    mapping.each do |key, value|
      unless self[key].nil?
        condition[value] = self[key]
      end
    end
    mapping = {
      hospital_type_id: 'hospitalTypeIds',
      job_contract_id: 'jobContractIds',
      job_detail_id: 'jobDetailIds',
      job_type_id: 'jobTypeIds',
      qualification_id: 'qualificationIds'
    }
    mapping.each do |key, value|
      unless self[key].nil?
        condition[value] = self[key].to_s
      end
    end

    if self.area_id
      case self.area.level
      when 1
        condition[:area1Ids] = self.area_id
      when 2
        condition[:area2Ids] = self.area_id
      when 3
        condition[:area3Ids] = self.area_id
      end
    end

    condition
  end

end
