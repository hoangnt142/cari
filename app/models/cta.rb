class Cta < ApplicationRecord
  self.table_name = 'ctas'
  validates :title, :content, presence: true

  def as_json(options = {})
    h = super(options)
    h[:content_with_out_tag] = ActionController::Base.helpers.strip_tags(content.to_s).truncate(40, omission: '・・・')
    h
  end
end
