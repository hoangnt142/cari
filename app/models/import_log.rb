class ImportLog < ApplicationRecord

  class << self
    def created_at_str
      (self.last.try(:created_at) || Time.now).strftime("%m月%d日更新")
    end
  end
end
