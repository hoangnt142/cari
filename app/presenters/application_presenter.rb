class ApplicationPresenter < SimpleDelegator
  include Rails.application.routes.url_helpers
  
  def initialize(model)
    @model = model
    super(model)
  end

  private

  attr_reader :model
end
