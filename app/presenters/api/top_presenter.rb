class Api::TopPresenter < ApplicationPresenter

  def jobs
    model.jobs.map{ |job| Api::JobPresenter.new(job) } 
  end

end