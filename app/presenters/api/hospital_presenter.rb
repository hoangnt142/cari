class Api::HospitalPresenter < ApplicationPresenter

  def work_location
    "#{model.prefecture.name}#{model.city.name}#{model.address}"
  end

  def max_job_created
    (model.jobs.pluck(:created_at).max || model.created_at).strftime("%Y年%m月%d日")
  end

  def search_keys
    model.hospital_types.pluck(:name).collect{|key| key}.join(" | ")
  end

  def url
    model.page.url
  end
end