class Api::PagePresenter < ApplicationPresenter

  # const page2OrMore = pageNumber !== null && pageNumber > 1
  # const pageTitle = page.seo_title || seoTitle
  # const title = (pageTitle ? `${pageTitle} | Carriee（キャリー）` : '') + (page2OrMore ? ` - ${pageNumber}ページ目`: '')
  # const description = !page2OrMore ? (page.seo_description || seoDescription) : ''
  # const keywords = !page2OrMore ? (page.seo_keywords || seoKeywords) : ''

  @@site_title = 'Carriee（キャリー）'

  def seo_title
    if model.id == 1 # Top page
      replace_placeholder(model.seo_title)
    elsif model.pageable_type == 'Hospital'
      # hospital pages
      hospital = model.pageable
      "#{hospital.name} #{hospital.prefecture.name}の看護師求人情報 | #{@@site_title}"
    else
      # others
      "#{replace_placeholder(model.seo_title)} | #{@@site_title}"
    end
  end

  def seo_description
    if model.id == 501 # Search page
      replace_placeholder(model.seo_description)
    elsif model.pageable_type == 'Search'
      pageable = model.pageable

      description = ''
      word = ''
      if pageable.prefecture
        # 1. prefecture
        prefecture_and_area_name = pageable.prefecture.name

        if pageable.area
          # 1-a. area
          prefecture_and_area_name += pageable.area.name
        end

        if pageable.qualification
          # 1-b. qualification
          word = pageable.qualification.name
          description = "#{prefecture_and_area_name}の#{word}求人を%{count}件掲載中！《%{import_log_created_at}更新》Carriee（キャリー）は人気の転職エージェントの看護師求人やハローワークの求人をまとめて検索することが出来ます。"

        elsif(!pageable.job_contract &&
              !pageable.hospital_type &&
              !pageable.job_type &&
              !pageable.job_detail)

          description = "#{prefecture_and_area_name}の看護師求人・転職を%{count}件掲載中！《%{import_log_created_at}更新》Carriee（キャリー）は人気の転職エージェントの看護師求人やハローワークの求人をまとめて検索することが出来ます。"
        else
          # 1-c. job_contract
          # 1-d. hospital_type
          # 1-e. job_type
          # 1-f. job_detail
          if pageable.job_contract
            word = pageable.job_contract.name
          elsif pageable.hospital_type
            word = pageable.hospital_type.name
          elsif pageable.job_type
            word = pageable.job_type.name
          elsif pageable.job_detail
            word = pageable.job_detail.name
          end

          description = "#{prefecture_and_area_name}で#{word}の看護師求人を%{count}件掲載中！《%{import_log_created_at}更新》Carriee（キャリー）は人気の転職エージェントの看護師求人やハローワークの求人をまとめて検索することが出来ます。"
        end
      else
        if pageable.qualification
          word = pageable.qualification.name
          description = "Carriee（キャリー）で#{word}の求人を%{count}件掲載中！《%{import_log_created_at}更新》人気の転職エージェントの看護師求人やハローワークの求人をまとめて検索することが出来ます。"
        else
          if pageable.job_contract
            word = pageable.job_contract.name
          elsif pageable.hospital_type
            word = pageable.hospital_type.name
          elsif pageable.job_type
            word = pageable.job_type.name
          elsif pageable.job_detail
            word = pageable.job_detail.name
          end

          description = "Carriee（キャリー）で#{word}の看護師求人を%{count}件掲載中！《%{import_log_created_at}更新》人気の転職エージェントの看護師求人やハローワークの求人をまとめて検索することが出来ます。"
        end
      end

      replace_placeholder(description)
    else
      replace_placeholder(model.seo_description)
    end
  end

  def seo_keywords
    model.seo_keywords
  end

  private
  def replace_placeholder(text)
    return nil if text.nil?

    count = ''
    if pageable.is_a?(Search)
      count = pageable.job_count
    end

    text = text % { import_log_created_at: ImportLog.created_at_str, count: count }
  end
end
