class Api::JobPresenter < ApplicationPresenter

  def hospital
    Api::HospitalPresenter.new(Hospital.find_by(id: model.hospital_id))
  end

end