module Api::PagesHelper
  def articles_json_for_list(articles)
    articles.as_json(
      methods: [:image, :title, :short_content, :url, :publish_at],
      include: [
        category: {
          methods: [:title, :url]
        }
      ]
    )
  end
end
