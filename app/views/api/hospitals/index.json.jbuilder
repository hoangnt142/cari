json.hospitals @hospitals, partial: 'api/hospitals/hospital', as: :hospital
json.total_pages @total_pages
json.current_page @current_page
json.total_count @total_count
json.last_date @last_date
json.pagination_range @pagination_range