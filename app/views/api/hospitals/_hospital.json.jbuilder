json.(hospital,
  :id,
  :name,
  :title,
  :access,
  :number_of_beds,
  :number_of_beds_text,
  :medical_subjects,
  :work_location,
  :average_point,
  :review_count,
  :hospital_types,
  :breadcrumb,
  :max_job_created,
  :latitude,
  :longitude
)

json.url hospital.page.url
json.prefecture hospital.prefecture

jobs = []
job_details = []

hospital.jobs.each do |job|

  search_keys = job.search_keys

  if !@all_search_key_ids.try(:any?) || (search_keys.pluck(:id) & @all_search_key_ids.try(:map, &:to_i)).length > 0
    job_details += search_keys.select { |key| key.type == 'JobDetail' }
    qualifications = search_keys.select { |key| key.type == 'Qualification' }
    contracts = search_keys.select { |key| key.type == 'JobContract' }
    types = search_keys.select { |key| key.type == 'JobType' }
    website = job.website

    jobs << {
      id: job.id,
      original_url: job.original_url,
      job_type_text: job.job_type_text,
      salary_formated: job.salary_formated,
      salary_detail: job.salary_detail,
      salary_type: job.salary_type_converted,
      allowance: job.allowance,
      work_time: job.work_time,
      qualifications: qualifications,
      contracts: contracts,
      types: types,
      website_id: website.id,
      website_name: website.name,
      website_redirect_url: website.redirect_url,
      website_redirect_link_url: website.redirect_link_url,
    }
  end
end

json.jobs jobs

json.jobs_details job_details.uniq
