json.pages @pages, partial: 'page', as: :page
json.totalPages @pages.total_pages
json.currentPage @pages.current_page