json.page do
  json.partial! 'api/admin/pages/page', page: @page
end