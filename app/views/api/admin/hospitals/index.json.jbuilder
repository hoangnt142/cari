json.hospitals @hospitals, partial: 'hospital', as: :hospital
json.totalPages @hospitals.total_pages
json.currentPage @hospitals.current_page