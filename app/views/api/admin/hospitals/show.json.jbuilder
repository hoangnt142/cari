json.hospital do
  json.partial! 'hospital', hospital: @hospital
end