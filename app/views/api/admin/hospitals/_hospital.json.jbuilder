json.(hospital,
  :id,
  :name,
  :access
)
json.page do
  json.partial! 'api/admin/pages/page', page: hospital.page
  json.alertSuccess flash[:alertSuccess]
end
flash[:alertSuccess] = nil