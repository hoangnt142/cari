json.(@top_data,
  :job_count,
  :import_log_created_at,
  :popular_areas
)

json.page ({
  id: @top_data.page.id,
  seo_title: @top_data.page.seo_title,
  seo_description: @top_data.page.seo_description,
  seo_keywords: @top_data.page.seo_keywords,
  seo_robot_noindex: @top_data.page.seo_robot_noindex,
  seo_robot_nofollow: @top_data.page.seo_robot_nofollow,
  content_bottom: @top_data.page.content_bottom
})

jobs = []

jobs_details = []

@top_data.jobs.each do |job|

  search_keys = job.search_keys

  qualifications = search_keys.select { |key| key.type == 'Qualification' }
  contracts = search_keys.select { |key| key.type == 'JobContract' }
  types = search_keys.select { |key| key.type == 'JobType' }
  job_details = search_keys.select { |key| key.type == 'JobDetail' }
  jobs_details += job_details

  hospital = job.hospital

  hospital = {
    name: hospital.name,
    work_location: hospital.work_location,
    access: hospital.access,
    url: hospital.page.url,
    total_jobs: hospital.jobs.count
  }

  jobs << {
    id: job.id,
    salary_formated: job.salary_formated,
    allowance: job.allowance,
    work_time: job.work_time,
    hospital: hospital,
    contracts: contracts,
    qualifications: qualifications,
    job_details: job_details,
    types: types,
    salary_type: job.salary_type ? I18n.t("salary_type.#{job.salary_type}") : ''
  }
end

json.jobs jobs
json.top_jobs_details jobs_details.uniq
json.top_links @top_data.top_links
json.hospitals @top_data.hospitals, partial: 'api/hospitals/hospital', as: :hospital