json.page ({
             id: @page.id,
             seo_title: @page.seo_title,
             seo_description: @page.seo_description,
             seo_keywords: @page.seo_keywords,
             seo_robot_noindex: @page.seo_robot_noindex,
             seo_robot_nofollow: @page.seo_robot_nofollow,
             pageable_id: @page.pageable_id,
             pageable_type: @page.pageable_type,
             name: @page.name,
             title: @page.title,
             url: @page.url,
             url_slug: @page.url_slug,
             publish_at: @page.publish_at,
             page_date: @page.page_date,
             created_at: @page.created_at,
             updated_at: @page.updated_at
})

json.pageable do
  json.(@pageable, *@pageable.attribute_names)

  json.prefecture @pageable.prefecture
  json.area @pageable.area
  json.qualification @pageable.qualification
  json.job_contract @pageable.job_contract
  json.hospital_type @pageable.hospital_type
  json.job_type @pageable.job_type
  json.job_detail @pageable.job_detail

end
