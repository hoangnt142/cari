json.page ({
             id: @page.id,
             seo_title: @page.seo_title,
             seo_description: @page.seo_description,
             seo_keywords: @page.seo_keywords,
             seo_robot_noindex: @page.seo_robot_noindex,
             seo_robot_nofollow: @page.seo_robot_nofollow,
             pageable_id: @page.pageable_id,
             pageable_type: @page.pageable_type,
             name: @page.name,
             title: @page.title,
             url: @page.url,
             content_bottom: @page.content_bottom,
             url_slug: @page.url_slug,
             publish_at: @page.publish_at,
             page_date: @page.page_date,
             created_at: @page.created_at,
             updated_at: @page.updated_at
           })

#json.page @page
json.pageable @pageable, partial: 'api/hospitals/hospital', as: :hospital
