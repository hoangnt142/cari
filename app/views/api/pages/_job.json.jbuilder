json.(job,
  :id,
  :original_url,
  :salary_formated,
  :salary_detail,
  :allowance,
  :work_time,
  :qualifications,
  :contracts,
  :types
)

if job[:hospital]
  hospital = job.try(:hospital)
  json.hospital do 
    json.name hospital.name
    json.work_location hospital.work_location
    json.access hospital.access
    json.url hospital.page.url
  end
end