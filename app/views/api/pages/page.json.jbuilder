json.page(@page,
  :id,
  :seo_title,
  :seo_description,
  :seo_keywords,
  :seo_robot_noindex,
  :seo_robot_nofollow,
  :pageable_type,
  :name,
  :title,
  :content_top,
  :content,
  :url,
  :cta,
  :url_slug,
  :publish_at,
  :page_date,
  :created_at,
  :updated_at
)

json.pageable @page.pageable || nil
