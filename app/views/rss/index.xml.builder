#encoding: UTF-8
xml.instruct! :xml, :version => "1.0"
xml.rss :version => "2.0" do
  xml.channel do
    xml.title "#{@top_page_title}"
    xml.link "#{root_url}"
    xml.description "#{@top_page_description}"
    xml.language "ja-JP"
		xml.copyright "peko Inc. Copyright All Rights Reserved."
		xml.pubDate @pub_date.to_s
		xml.lastBuildDate @last_build_date.to_s

    @jobs.each do |job|
      xml.item do
        xml.title job.hospital.page.title
        xml.description job.rss_description
        xml.pubDate job.created_at.to_s
        xml.link "#{request.protocol}#{request.host}#{([80,443].include?(request.port) ? '' : ":#{request.port.to_s}")}#{job.hospital.page.url}"
#        xml.guid url_for(hospital.page.url)
      end
    end
  end
end
