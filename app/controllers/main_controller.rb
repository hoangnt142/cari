class MainController < ApplicationController

  include UrlMatches

  def index

    @page = get_page(request.path)

    if @page.not_found?
      # Page not found.
      # Real users doesn't get the 404 code, however, when requesting the URL directly returnes the code.
      # It is needed for crawler like googlebot.
      # @page = OpenStruct.new(seo_title: 'ページが見つかりませんでした', seo_robot_noindex: false, seo_robot_nofollow: false)
      render 'react/index', status: 404
    else
      set_meta_info()
      render 'react/index'
    end
  end


  private
  def set_meta_info
    @seo_title = @page.seo_title
    @seo_description = @page.seo_description
    @seo_keywords = @page.seo_keywords
    @seo_robots = "#{@page.seo_robot_noindex ? 'noindex' : 'index'},#{@page.seo_robot_nofollow ? 'nofollow' : 'follow'}"
    @link_rel_canonical0 = @link_rel_canonical = with_protocol_and_port(@page.url)
    @link_rel_prev = nil
    @link_rel_next = nil

    page_number = self.class.match_page(request.path)


    # When the page consists of multiple page, it needs <link rel /> .
    case
    when @page.pageable_type == 'Search'
      # case 1. searches in hospital page (Api:HospitalsController)
      search = @page.pageable
      if search.nil? || search.hospital_count > 0 # /search has no record on searches table(pageable_id=nil).
        has_prev_page, has_next_page = has_prev_next_pages_in_hospitals(search, page_number)
        @link_rel_prev, @link_rel_next = link_rel_prev_next(has_prev_page, has_next_page, page_number)
      else
        # When there is no result, set robots as noindex,follow
        @seo_robots = 'noindex,follow'
      end
    when @page.pageable_type == 'Hospital'
      # hospital = @page.pageable
      # area = hospital.city.area3 || hospital.city.area2 || hospital.city.area1
      # search = Search.where(
      #   prefecture_id: hospital.prefecture_id,
      #   area_id: area.id
      # ).take

      # # NOTE:
      # #   When the information is not filled(pages.filled=0),
      # #   the canonical URL of the Hospital page is set to the area page where the hospital belongs to.
      # # TODO: After publish_status will be worked properly, set the condition
      # # page = Page.where(pageable_id: search.id, pageable_type: 'Hospital', filled: false).published.take

      # if !@page.filled && search.page.published?
      #   @link_rel_canonical = with_protocol_and_port(search.page.url)
      # end
    end

    # After page2,
    # - title should have page number
    # - description and keywords should be empty
    # - canonical url needs page_number
    page2_or_more = page_number && page_number > 1
    if page2_or_more
      @seo_title = "#{@seo_title} - #{page_number}ページ目"
      @seo_description = ''
      @seo_keywords = ''
      @link_rel_canonical = url_with_page_number(page_number)
    end
  end

  # page 1 should not have link prev
  # and final page should not have link next
  # others should have the both
  def has_prev_next_pages_in_hospitals(search, page_number)
    count = (search || Search).hospital_count
    total_pages =
      (count / Api::HospitalsController::LIMIT_PER_PAGE) +
      (count % Api::HospitalsController::LIMIT_PER_PAGE == 0 ? 0 : 1)

    has_prev_page = (page_number || 1) > 1
    has_next_page = total_pages > (page_number || 1)
    logger.debug "has_prev_page: #{has_prev_page}, has_next_page: #{has_next_page}, total_pages: #{total_pages}"
    return has_prev_page, has_next_page
  end

  def link_rel_prev_next(has_prev_page, has_next_page, page_number)
    page_number = 1 if page_number.nil?
    link_rel_prev = link_rel_next = nil
    if has_prev_page && page_number > 2
      link_rel_prev = url_with_page_number(page_number - 1)
    elsif has_prev_page
      link_rel_prev = url_with_page_number
    end
    if has_next_page
      link_rel_next = url_with_page_number(page_number + 1)
    end

    return link_rel_prev, link_rel_next
  end

  def url_with_page_number(page_number=nil)
    if page_number.nil?
      "#{@link_rel_canonical0}"
    else
      "#{@link_rel_canonical0}/page/#{page_number}"
    end
  end

  def with_protocol_and_port(path)
    "#{request.protocol}#{request.host}" +
      ([80,443].include?(request.port) ? '' : ":#{request.port.to_s}") +
      (path == '/' ? '' : "#{path}")
  end
end
