class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  skip_before_action :verify_authenticity_token
  before_action :preload
  protected

  def current_page
    regexp_match_page = /\/page\/([0-9]+)\/?$/
    params[:path].scan(regexp_match_page).first.try(:first).to_i
  end

  def preload
    @config = SystemConfig.first
  end

  def present(model, presenter_class=nil)
    klass = presenter_class || "#{model.class}Presenter".constantize
    if model.respond_to?(:map)
      model.map do |item|
        klass.new(item)
      end
    else
      klass.new(model)
    end
  end
end
