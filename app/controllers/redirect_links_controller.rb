class RedirectLinksController < ApplicationController
  layout false

  def index
    website = Website.find(params[:website_id])
    @redirect_url = website.redirect_url
    @title = website.name
  end

end
