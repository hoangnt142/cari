require 'uri'
module UrlMatches
  extend ActiveSupport::Concern

  class_methods do
    @@regexp_match_page = /\/page\/([0-9]+)\/?$/

    def match_page(url)
      if url =~ @@regexp_match_page
        page_number = url.scan(@@regexp_match_page)[0][0]
        page_number.to_i
      else
        nil
      end
    end
  end

  included do

    # Some sub URLs are related to a main page record.

    # 1. URL of hospital
    # /hospital1/repute-list -> /hospital1
    # /hospital1/repute-list/page/2 -> /hospital1
    #
    # 2. URL having pagination
    # /blog -> /blog
    # /blog/pages/2 -> /blog
    # /blog/category1 -> /blog/category1
    # /blog/category1/page/2 -> /blog/category1
    def get_page(path)
      page = nil

      path = "/#{path}" unless path =~ /^\//

      match_repute_list = /\/repute-list\/?$/

      url = path
      url = URI.unescape(url)


      # Remove page number from the url since the pages table doesn't contain the page numbers.
      if self.class.match_page(url)
        # The case having page in url
        url = url.sub(@@regexp_match_page, '')
        logger.debug "Mathing the URL having pagination: #{url}"

        if url =~ match_repute_list
          url = url.sub(match_repute_list, '')
          logger.debug "Mathing the URL of Hopital: #{url}"
        end
      elsif url =~ match_repute_list
        # The case not having page in url
        url = url.sub(match_repute_list, '')
        logger.debug "Mathing the URL of Hopital: #{url}"
      end

      page = Page.where(url: url).take

      if page.try(:predefined) && page.try(:pageable).nil? && ([1,501,10].exclude?(page.id)) #hide such pages for this release
        return Page.not_found
      end

      if page.nil? || !page.published?
        return Page.not_found
      else
        logger.debug page.inspect
        page_presenter = Api::PagePresenter.new(page)
        page_presenter
      end
    end

  end
end
