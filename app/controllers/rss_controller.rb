class RssController < ApplicationController

  def index

    page_presenter = Api::PagePresenter.new(Page.find(1))
    @top_page_title = page_presenter.seo_title
    @top_page_description = page_presenter.seo_description

    # Set pudDate as  6:00 on every day
    @pub_date = ImportLog.last.created_at
    @last_build_date = ImportLog.last.created_at
    @jobs = Job.includes(:hospital).where(hospitals: { deleted: false }).order(created_at: :desc).limit(30)

    respond_to do |format|
      format.xml { render layout: false }
    end
  end
end
