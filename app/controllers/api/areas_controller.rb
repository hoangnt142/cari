class Api::AreasController < ApplicationController

  def index
    areas = Area.area1.where( prefecture_id: params[:prefecture_id])
    render json: { areas: areas }
  end

  def with_ancestors
    area = Area.find(params[:id])
    areas = []
    areas << area
    parent = area.parent
    if parent
      while parent
        logger.debug parent.inspect
        areas << parent
        parent = parent.parent
      end
    end
    areas.reverse!
    render json: { areas: areas }
  end

  def area123_by_prefecture_ids
    unless params[:prefecture_ids]
      render json: { error_message: 'prefecture_ids is needed' }
      return
    end
    unless params[:prefecture_ids] =~ /\A\[[0-9,]+\]\Z/
      render json: { error_message: 'wrong format for prefecture_ids' }
      return
    end


    # area1
    prefecture_ids = eval(params[:prefecture_ids])
    area1_list_by_prefecture = prefecture_ids.collect do |prefecture_id|
      Area.area1.where(prefecture_id: prefecture_id).select('id,prefecture_id,name,total_jobs')
    end

    data_area1s = area1_list_by_prefecture.collect do |area1_list|
      {
        prefecture: Prefecture.where(id: area1_list.first.prefecture_id).select('id,name,total_jobs').take,
        area1s: area1_list
      }
    end
    data_area1s.compact!


    # area2

    data_area2s = []
    data_area3s = []
    area1_list_by_prefecture.each do |area1_list|
      area2_list_by_area1 = area1_list.collect do |area1|
        area1.children.select('id,parent_id,name,total_jobs')
      end

      data_area2s += area2_list_by_area1.collect do |area2_list|
        area2_list.length == 0 ? nil :
        {
          prefecture: Prefecture.where(id: area2_list.first.parent.prefecture_id).select('id,name,total_jobs').take,
          area1: Area.where(id: area2_list.first.parent_id).select('id,parent_id,name,total_jobs').take,
          area2s: area2_list
        }
      end
      data_area2s.compact!


      # area3

      area2_list_by_area1.each do |area2_list|
        area3_list_by_area2 = area2_list.collect do |area2|
          area2.children.select('id,parent_id,name,total_jobs')
        end

        data_area3s += area3_list_by_area2.collect do |area3_list|
          area3_list.length == 0 ? nil :
            {
              prefecture: Prefecture.where(id: area3_list.first.parent.parent.prefecture_id).select('id,name,total_jobs').take,
              area2: Area.where(id: area3_list.first.parent_id).select('id,parent_id,name,total_jobs').take,
              area1: Area.where(id: area3_list.first.parent.parent_id).select('id,parent_id,name,total_jobs').take,
              area3s: area3_list
            }
        end
        data_area3s.compact!
      end
    end

    data = {
      area1s: data_area1s,
      area2s: data_area2s,
      area3s: data_area3s
    }

    render json: data
  end

end
