class Api::PrefecturesController < ApplicationController

  def index
    @prefectures = Prefecture.all
    @regions = Region.all
    render json: { prefectures: @prefectures, regions: @regions }
  end

  def show
    prefecture = Prefecture.find(params[:id])
    render json: { prefecture: prefecture }
  end

end
