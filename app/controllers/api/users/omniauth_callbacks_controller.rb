class Api::Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController

  def facebook
    if user_signed_in?
      return redirect_to root_path
    end

    if request.env["omniauth.auth"].info.email.blank?
      check_failed = session[:failed_fb].to_i
      if check_failed > 1
        flash[:notice] = "Can't register via this Facebook account without email."
        session.delete(:failed_fb)
        return redirect_to new_user_session_path #user_my_page_path
      else
        session[:failed_fb] = check_failed + 1
      end
      return redirect_to user_facebook_omniauth_authorize_path(auth_type: 'rerequest', scope: 'email')
    end

    auth = request.env['omniauth.auth']
    @user = User.where(provider: auth.provider, uid: auth.uid).first

    if @user.blank?

      # Register this user directly from facebook information
      email = auth.info.email
      name = email.split('@')[0]
      dv_password = Devise.friendly_token[0,20]
      user_info = {
          nickname: name,
          email: email,
          password: dv_password,
          password_confirmation: dv_password,
          provider: auth.provider,
          uid: auth.uid
      }

      if auth.extra.raw_info.gender.present?
        user_info[:gender] = auth.extra.raw_info.gender
      end

      if auth.extra.raw_info.birthday.present? && auth.extra.raw_info.birthday.length == 10
        user_info[:birth] = Date.strptime auth.extra.raw_info.birthday.to_s, '%m/%d/%Y' rescue nil
      end

      if auth.info.location.present?
        city = auth.info.location.split(',')[0]
        pref = Prefecture.where("name LIKE CONCAT('%',?,'%')", city).first
        if pref.present?
          user_info[:prefecture_id] = pref.id
        end
      end

      user = User.new(user_info)

      if user.save
        flash[:oauth] = I18n.t('oauth.message')
        sign_in user, event: :authentication
        redirect_to root_path
      else
        flash[:notice] = I18n.t('oauth.error')
        redirect_to root_path
      end

    else
      # Sign in this FB user:
      sign_in @user, event: :authentication
      redirect_to root_path
    end
  end


end