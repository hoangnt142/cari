# frozen_string_literal: true

class Api::Users::SessionsController < Devise::SessionsController
  skip_before_action :require_no_authentication
  # before_action :configure_sign_in_params, only: [:create]

  # GET /resource/sign_in
  def new
    super do |user|
      render json: { user: nil }
      break
    end
  end

  # POST /resource/sign_in
  def create
    super do |user|
      # Get and set only the length of the records without getting all fields.
#      size = user.user_favorite_hosiptals.size
#      user.user_favorite_hosiptals_size = size
#      size = user.user_history_hosiptals.size
#      user.user_history_hosiptals_size = size
      size = user.hospital_reviews.size
      user.hospital_reviews_size = size

      render json: { user: resource }
      break
    end
  end

  # DELETE /resource/sign_out
  def destroy
    super
  end

  # protected

  def failed
    render json: {}, status: 401
  end

  def auth_options
    super.merge(recall: "#{controller_path}#failed")
  end

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.permit(:sign_in, keys: [:attribute])
  # end
end
