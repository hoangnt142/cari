# frozen_string_literal: true

class Api::Users::RegistrationsController < Devise::RegistrationsController
  skip_before_action :require_no_authentication
  # before_action :configure_sign_up_params, only: [:create]
  # before_action :configure_account_update_params, only: [:update]

  # GET /resource/sign_up
  def new
    super
  end

  def create
    super do |user|
      if user.id.present?
        return render json: {ok: true}
      end
    end
  end

  def check_email_exist
    emailExisted =  User.find_by(email: params[:email]).present?
    puts "emailExisted #{emailExisted}"
    render json: {emailExisted: emailExisted}
  end

  private

  def sign_up_params
    params.permit(:email, :password, :password_confirmation,
                  :gender,
                  :nickname,
                  :birth,
                  :prefecture_id,
                  :city_id,
                  :favorite_prefecture_id1,
                  :favorite_city_id1,
                  :favorite_prefecture_id2,
                  :favorite_city_id2
                ).merge(password_confirmation: params[:password])
  end
end
