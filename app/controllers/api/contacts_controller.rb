class Api::ContactsController < ApplicationController

  def new
  	contact_types = Contact.contact_types.keys.map { |key| {key: key, value: key, text: t("contact.contact_type.#{key}")} }
  	render json: {
  		contact_types: contact_types
  	}
  end

  def create
    contact = Contact.create(contact_params)
    render json: {
      error: contact.errors.full_messages.length > 0,
      errors: contact.errors
    }
  end

  private

  def contact_params
    params.require(:contact).permit(:need_reply, :contact_type, :company, :name, :email, :content)
  end
end
