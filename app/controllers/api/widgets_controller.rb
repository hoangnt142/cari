class Api::WidgetsController < ApplicationController

  def index
    page = params[:page]
    widgets = []
    case page
    when 'top'
      widgets << {
        title: '雇用形態で探す',
        items: JobContract.all
      }
      widgets << {
        title: '保有資格で探す',
        items: Qualification.all
      }
      widgets << {
        title: '仕事内容で探す',
        items: JobType.all
      }
      widgets << {
        title: '病院・施設で探す',
        items: HospitalType.all
      }
    end
    render json: {widgets: widgets}
  end

end
