class Api::SearchKeysController < ApplicationController

  def index
    class_name = (
      case params[:type].try(:camelcase)
      when 'Qualification'
        Qualification
      when 'JobContract'
        JobContract
      when 'HospitalType'
        HospitalType
      when 'JobType'
        JobType
      when 'JobDetail'
        JobDetail
      else
        SearchKey
      end
    )

    if class_name != SearchKey
      records = class_name.all
      json = {}
      json[class_name.name.underscore.to_sym] = records
    else
      # when no type is specified, return all types
      json = {
        qualifications: Qualification.all,
        job_contracts: JobContract.all,
        hospital_types: HospitalType.all,
        job_types: JobType.all,
        job_details: JobDetail.all
      }
    end

    render json: json
  end

end
