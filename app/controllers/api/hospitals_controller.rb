class Api::HospitalsController < ApplicationController

  LIMIT_PER_PAGE = 25

  def index
    @current_page = params[:page].present? ? params[:page] : 1

    model = CacheJobSearch.set_condition(params)
    @all_search_key_ids = CacheJobSearch.all_search_key_ids(params)
    hospital_ids = Kaminari.paginate_array(model
                     .select('hospital_id')
                     .group('hospital_id')
                     .joins(:hospital)
                     .where(hospitals: { deleted: false })
                     .collect{ |record| record.hospital_id })
                     .page(@current_page).per(LIMIT_PER_PAGE)

    @hospitals = Hospital.includes(:page, :hospital_types, :jobs)
                  .where(id: hospital_ids)

    # search = Search.find_by_params(params)
    # if search
    #   @page = search.page
    #   @pageable = search
    # end

    @total_pages = hospital_ids.total_pages
    @total_count = hospital_ids.total_count
    @last_date = @hospitals.last.try(:created_at).try(:strftime, "%m月%d日更新")
    @pagination_range = pagination_range(hospital_ids, LIMIT_PER_PAGE)
  end

  def favorite_jobs
    job_ids = params[:job_ids]

    @current_page = params[:page].present? ? params[:page] : 1
    @hospitals = job_ids.length > 0 ?
      Hospital.includes(:page, :hospital_types, :jobs)
              .where(deleted: false, jobs: {id: job_ids})
              .order("field(jobs.id, #{job_ids.reverse.join(',')})")
              .page(@current_page).per(LIMIT_PER_PAGE)
      : Array.new
    @total_count = @hospitals.try(:total_count)
    @total_pages = @hospitals.try(:total_pages)
    @last_date = @hospitals.last.try(:created_at).try(:strftime, "%m月%d日更新")
    render :index
  end

  # "Related" means the conditions below has to be same with the specified hospital
  # - prefecture and area
  # - hospital_type
  # #- job_type
  def related_jobs
    hospital_id = params[:hospital_id]
    hospital = Hospital.find(hospital_id)

    city_ids = nil
    if hospital.city.area3
      city_ids = City.where(area3_id: hospital.city.area3.id).select(:id)
    elsif hospital.city.area2
      city_ids = City.where(area2_id: hospital.city.area2.id).select(:id)
    elsif hospital.city.area1
      city_ids = City.where(area1_id: hospital.city.area1.id).select(:id)
    end

    search_key_ids = nil
    if hospital.hospital_hospital_types && hospital.hospital_hospital_types.size > 0
      search_key_ids = hospital.hospital_hospital_types.collect{ |hht| hht.search_key_id }
    end

    # Shown radonmly
    @hospitals =
      Hospital.includes(:hospital_hospital_types)
        .where(hospital_hospital_types: { search_key_id: search_key_ids })
        .where(city_id: city_ids)
        .order('RAND()')
        .limit(5)
  end

  private

  def pagination_range(hospital_ids, limit_per_page)
    size = hospital_ids.size
    range = []
    if @current_page.to_i <= 1
      range = [1, size]
    else
      range = [(@current_page.to_i-1)*limit_per_page + 1, (@current_page.to_i-1)*limit_per_page + size]
    end
    range
  end

end
