class Api::CitiesController < ApplicationController

  def index
    cities = City.where( prefecture_id: params[:prefecture_id])
    render json: { cities: cities }
  end

  def show
    city = City.find(params[:id])
    render json: { city: city }
  end
end
