class Api::PagesController < ApplicationController
  include Api::PagesHelper
  include UrlMatches
  ARTICLE_PER_PAGE = 12

  def index
    @page = get_page(params[:path])
    if @page.nil? || @page.not_found?
      # It's not the status code of HTTP
      render json: { page: @page, code: 404 }
    elsif @page.id == 1 # /
      @top_data = top_page_data
      render 'api/pages/top'
    elsif @page.id == 501 # /search
      render 'api/pages/search_top'
    elsif @page.id == 10 # /blog
      render_blog_page
    else
      # set additional information
#      case page.pageable.class
#      when SearchKey
#      end
      pageable = @page.pageable
      case pageable.try(:class).try(:name)
      when 'Hospital'
        @pageable = present(pageable, Api::HospitalPresenter)
        @all_search_key_ids = []
        render 'api/pages/hospital'
      when 'Category'
        render_category
      when 'Article'
        render_article
      when 'Search'
        @pageable = pageable
        render 'api/pages/search'
      else
        render 'api/pages/page'
      end
    end
  end

  def show
    page = Page.where(url: "/#{params[:id]}")
    render json: { page: Api::PagePresenter.new(page) }
  end

  private

  def render_category
    category = @page.pageable
    articles = category.articles.published.order("pages.publish_at DESC").page(current_page).per(ARTICLE_PER_PAGE)
    render json: {
      page: @page,
      total_pages: articles.total_pages,
      current_page: current_page,
      category: category.as_json().merge(
        articles: articles_json_for_list(articles),
        breadcrumb: category.breadcrumb
      )
    }
  end

  def render_article
    article = @page.pageable
    article.increase_view
    popular_articles = Article.ranking_all_time(article)
    render json: {
      page: @page,
      popular_articles: popular_articles.as_json(
        only: [:id],
        methods: [:total_view_count, :title, :url, :image]
      ),
      article: article.as_json(
        methods: [:search_key_articles, :next, :prev, :image, :cta, :content, :breadcrumb],
        include: [
          category: {
            methods: [:page]
          }
        ]
      )
    }
  end

  def render_blog_page
    articles = Article.includes(category: :page)
                .where(categories: {pages: {publish_status: Page.publish_statuses[:published]}}) #articles belongs to published category
                .published
                .order("pages.publish_at DESC").page(current_page).per(ARTICLE_PER_PAGE)
    render json: {
      page: @page,
      blog_articles: articles_json_for_list(articles),
      totalPages: articles.total_pages,
      currentPage: articles.current_page
    }
  end

  def top_page_data
    OpenStruct.new(
      page: @page,
      job_count: ActionController::Base.helpers.number_with_delimiter(CacheJobSearch.total_job_count),
      import_log_created_at: ImportLog.created_at_str,
      top_links: [
        {title: '人気の市区町村から探す', links: Area.where(id: [655, 718, 851, 112, 171, 77, 58, 91, 655, 563, 567, 500, 417, 301]).map { |area| {url: "/#{area.prefecture.name_roman}/#{area.name_roman}", name: area.name}}},
        {title: 'こだわりから看護師求人を探す', links: JobDetail.all.map {|jd| {url: "/#{jd.name_roman}", name: jd.name} } }
      ],
      jobs: Job.order(created_at: :DESC).limit(4),
      hospitals: Hospital.includes(:jobs).where(jobs: {id: Job.order(created_at: :DESC).limit(4).pluck(:id)}).uniq
    )
  end
end
