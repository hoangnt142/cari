class Api::Admin::ArticlesController < Api::Admin::BaseController
  def index
    articles = get_pageables(Article)
    render json: {
      articles: articles.as_json(
        only: [:id, :category_id],
        include: [
          page: {
            methods: [:used_content, :status]
          }
        ]
      ),
      totalPages: articles.total_pages,
      currentPage: articles.current_page
    }
  end

  def new
    render json: {
      article: {page:{image:{}}},
      article_categories: article_categories,
      ctas: Cta.all,
      search_keys: search_keys
    }
  end

  def show
    article = Article.find_by(id: params[:id])
    render json: {
      ctas: Cta.all,
      search_keys: search_keys,
      article: article.as_json(
        include: [
          page: {
            include: [{
              image: {
                methods: [:preview]
              }
            }]
          }
        ]
      ).merge(alertSuccess: flash[:alertSucess]),
      article_categories: article_categories
    }
    flash[:alertSucess] = nil
  end

  def create
    Article.transaction do
      article = Article.new(article_params)
      if article.save(article_params)
        flash[:alertSucess] = '更新しました。'
        render json: {article: article}
      else
        render json: { errors: article.errors }
      end
    end
  end

  def update
    Article.transaction do
      article = Article.find(params[:id])
      if article.update(article_params)
        flash[:alertSucess] = '更新しました。'
        render json: {
          article: article.as_json(
            include: [
              page: {
                include: [{
                  image: {
                    methods: [:preview]
                  }
                }]
              }
            ]
          ).merge(alertSuccess: flash[:alertSucess])
        }
      else
        render json: { errors: article.errors }
      end
    end
    flash[:alertSucess] = nil
  end

  private

  def search_keys
    [{key: 0, value: 0, text: '-- Select Search Key --'}] + SearchKey.all.map {|key| {key: key.id, value: key.id, text: key.name}}
  end

  def article_params
    article = params.require(:article)
    article.permit(
      :category_id,
      :search_key_id
    ).merge(
      page_attributes: page_params(:article),
      category_id: article[:category_id].to_i == 0 ? nil : article[:category_id],
      search_key_id: article[:search_key_id].to_i == 0 ? nil : article[:search_key_id]
    )
  end
end