class Api::Admin::HospitalsController < Api::Admin::BaseController
  def index
    @hospitals = get_hospitals
    render json: {
      hospitals: @hospitals.as_json(
        only: [:id, :name],
        include: [
          page: {
            methods: [:used_content]
          }
        ]
      ),
      totalPages: @hospitals.total_pages,
      currentPage: @hospitals.current_page
    }
  end

  def show
    @hospital = Hospital.find_by(id: params[:id])
  end

  def create
  end

  def update
    @hospital = Hospital.find(params[:id])
    if @hospital.update(hospital_params)
      flash[:alertSucess] = '更新しました。'
      render :show
    else
      render json: { errors: @hospital.errors }
    end
  end

  private

  def hospital_params
    params.require(:hospital).permit(
      :name,
      :title,
      :deleted
    ).merge(
      deleted: page_params[:publish_status] == 'deleted',
      page_attributes: page_params
    )
  end

  def page_params
    params.require(:hospital).require(:page).permit(
      :id,
      :content,
      :seo_title,
      :seo_description,
      :seo_keywords,
      :seo_robot_noindex,
      :seo_robot_nofollow,
      :content_top,
      :content,
      :filled,
      :content_bottom,
      :publish_status,
      :publish_at
    )
  end

  def get_hospitals
    limit_per_page = 20
    hospitals = Hospital.includes(:page)
    status = params[:status].to_s.downcase
    search = params[:search].to_s.strip
    if ['unpublished', 'published', 'deleted'].index(status)
      hospitals = hospitals.where(pages: {publish_status: Page.publish_statuses[status]})
    end
    if status == 'filled'
      hospitals = hospitals.where(pages: {filled: true})
    end
    if status == 'unfilled'
      hospitals = hospitals.where(pages: {filled: false})
    end
    if !search.blank?
      hospitals = hospitals.where("`hospitals`.`name` LIKE :search", search: "%#{search}%")
    end
    hospitals.order(created_at: :DESC).page(params[:page_id]).per(limit_per_page)
  end
end