class Api::Admin::ImagesController < Api::Admin::BaseController
  def index
    @images = get_images
    render json: {images: @images.as_json}
  end

  def create
    image = Image.library.new(image_params)
    if image.save
      images = get_images
      render json: {images: images.as_json}
    else
      render json: {errors: image.errors}
    end
  end

  def update
    image = Image.find(params[:id])
    image.assign_attributes(image_params)
    image.save
    render json: {errors: image.errors}
  end

  private

  def get_images
    search = params[:search].to_s.strip
    if search.present?
      Image.library.where("title LIKE :search OR caption LIKE :search OR alt LIKE :search OR description LIKE :search", search: "%#{search}%").order(created_at: :DESC).page(params[:page]).per(30)
    else
      Image.library.order(created_at: :DESC).page(params[:page]).per(30)
    end
  end

  def image_params
    if params[:image].present?
      params.permit(
        :title,
        :caption,
        :alt,
        :width,
        :height,
        :image,
        :description
      )
    else
      params.permit(
        :width,
        :height
      )
    end
  end
end