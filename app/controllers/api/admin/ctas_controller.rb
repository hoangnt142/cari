class Api::Admin::CtasController < Api::Admin::BaseController
  def index
    render json: {ctas: Cta.all.as_json}
  end

  def create
    cta = Cta.create(cta_params)
    flash[:alertSuccess] = '登録しました。'
    render_basic_response(cta)
  end

  def update
    cta = Cta.find_by(id: params[:cta][:id])
    cta.update(cta_params)
    flash[:alertSuccess] = '更新しました。'
    render_basic_response(cta)
  end

  def show
    cta = Cta.find_by(id: params[:id])
    render_basic_response(cta)
  end

  def destroy
    cta = Cta.find_by(id: params[:id])
    cta.destroy
    render json: {ctas: Cta.all.as_json}
  end

  private

  def cta_params
    params.require(:cta).permit(:title, :content)
  end
end