require 'w3c_validators'
class Api::Admin::SystemConfigsController < Api::Admin::BaseController
  include W3CValidators
  def show
    render json: {systemConfig: SystemConfig.first}
  end

  def create
    config = SystemConfig.first
    css_custom = params[:system_config][:css_custom]
    validator = CSSValidator.new
    results = validator.validate_text(css_custom)
    if results.errors.length > 0
      flash[:error] = "Systax error at line #{results.errors.first.line}"
      render json: {systemConfig: SystemConfig.first.as_json.merge(
        css_custom: css_custom,
        error: flash[:error]
      )}
    else
      config.update_columns(css_custom: css_custom)
      flash[:alertSuccess] = "更新しました。"
      render json: {systemConfig: SystemConfig.first.as_json.merge(
        alertSuccess: flash[:alertSuccess]
      )}
    end
    flash[:error] = nil
    flash[:alertSuccess] = nil
  end
end
