class Api::Admin::CategoriesController < Api::Admin::BaseController
  def index
    categories = get_pageables(Category)
    render json: {
      categories: categories.as_json(
        only: [:id, :name],
        include: [
          page: {
            methods: [:used_content, :status]
          }
        ]
      ),
      totalPages: categories.total_pages,
      currentPage: categories.current_page
    }
  end

  def new
    render json: {
      category: {page:{image:{}}},
      parent_categories: parent_categories
    }
  end

  def show
    category = Category.find_by(id: params[:id])
    render json: {
      category: category.as_json(
        include: [
          page: {
            include: [{
              image: {
                methods: [:preview]
              }
            }]
          }
        ]
      ).merge(alertSuccess: flash[:alertSucess]),
      parent_categories: parent_categories(params[:id])
    }
    flash[:alertSucess] = nil
  end

  def create
    Category.transaction do
      category = Category.new(category_params)
      if category.save(category_params)
        flash[:alertSucess] = '更新しました。'
        render json: {category: category}
      else
        render json: { errors: category.errors }
      end
    end
  end

  def update
    Category.transaction do
      category = Category.find(params[:id])
      if category.update(category_params)
        flash[:alertSucess] = '更新しました。'
        render json: {
          category: category.as_json(
            include: [
              page: {
                include: [{
                  image: {
                    methods: [:preview]
                  }
                }]
              }
            ]
          ).merge(alertSuccess: flash[:alertSucess])
        }
      else
        render json: { errors: category.errors }
      end
    end
    flash[:alertSucess] = nil
  end

  private

  def category_params
    params.require(:category).permit(
      :parent_id
    ).merge(
      page_attributes: page_params(:category),
      parent_id: params[:category][:parent_id].to_i == 0 ? nil : params[:category][:parent_id]
    )
  end
end