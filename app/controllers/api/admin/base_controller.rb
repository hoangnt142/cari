class Api::Admin::BaseController < ApplicationController
  before_action :restrict_admin_users

  def parse_image_data(img_params)
    begin
      image_file = nil
      if img_params[:img_data]
        image_file = Paperclip.io_adapters.for(img_params[:img_data])
        image_file.original_filename = img_params[:title]
        image_file.content_type = img_params[:img_type]
      end
      image_file
    rescue => e
      raise e.message
    end
  end

  def render_basic_response(model)
    render json: {
      error: model.errors.full_messages.length > 0,
      errors: model.errors,
      model: model.as_json.merge(alertSuccess: flash[:alertSuccess])
    }
  end

  def image_params
    page = params[:page] || params[params.keys.first.to_sym][:page]
    image = parse_image_data(page[:image])
    if image.present?
      page.require(:image).permit(
        :id,
        :alt,
        :width,
        :height,
        :title
      ).merge(image: image)
    elsif page[:image].present?
      page.require(:image).permit(
        :id,
        :alt
      )
    else
      {}
    end
  end

  def parent_categories(id=nil)
    [{key: 0, value: 0, text: '-- 選択して下さい --', url: ''}]+Category.published.roots(id).map {|category| {key: category.id, value: category.id, text: category.page.title, url: category.page.url}}
  end

  def get_pageables(pageable)
    limit_per_page = 20
    pageables = pageable.includes(:page)
    status = params[:status].to_s.downcase
    search = params[:search].to_s.strip
    if ['unpublished', 'published', 'deleted'].index(status)
      pageables = pageables.where(pages: {publish_status: Page.publish_statuses[status]})
    end
    if status == 'filled'
      pageables = pageables.where(pages: {filled: true})
    end
    if status == 'unfilled'
      pageables = pageables.where(pages: {filled: false})
    end
    pageables.order(created_at: :DESC).page(params[:page_id]).per(limit_per_page)
  end

  def page_params(pageable)
    page = params.require(pageable).require(:page)
    page.permit(
      :id,
      :title,
      :url_slug,
      :content,
      :seo_title,
      :seo_description,
      :seo_keywords,
      :seo_robot_noindex,
      :seo_robot_nofollow,
      :content_top,
      :content,
      :filled,
      :content_bottom,
      :publish_status,
      :publish_at,
      :cta_id
    ).merge(
      publish_at: page[:publish_at] || Time.now,
      image_attributes: image_params,
      seo_title: page[:seo_title].present? ? page[:seo_title] : page[:title]
    )
  end

  def article_categories
    [{key: 0, value: 0, text: '-- 選択して下さい --', url: ''}]+Category.published.map {|category| {key: category.id, value: category.id, text: category.page.title, url: category.page.url}}
  end

  private

  def restrict_admin_users
    unless current_user.try(:admin?)
      render json: {isNotAdmin: true}
    end
  end
end
