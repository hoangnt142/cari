class Api::Admin::SessionsController < Devise::SessionsController
  skip_before_action :require_no_authentication
  layout "admin"

  def create
    user = current_user
    if !user || sign_in_params[:email].present?
      user = User.admin_authenticate(sign_in_params[:email], sign_in_params[:password])
      unless user
        sign_out user
        return render json: {ok: false, isNotAdmin: false} unless user
      end
      sign_in user
    end
    render json: {ok: true, user: user}
  end

  def destroy
    sign_out current_user
    render json: {isNotAdmin: true}
  end

end
