class Api::Admin::PagesController < Api::Admin::BaseController
  def index
    @pages = get_pages
    render json: {
      pages: @pages.as_json(
        methods: [:used_content, :page_date_formatted, :published_at_formatted]
      ),
      totalPages: @pages.total_pages,
      currentPage: @pages.current_page
    }
  end

  def new
    render json: {
      ctas: Cta.all,
      page: {}
    }
  end

  def show
    @page = Page.find_by(id: params[:id])
    render json: {
      ctas: Cta.all,
      page: @page.as_json(
        include: [{
          image: {
            methods: [:preview]
          }
        }]
      ).merge(alertSuccess: flash[:success])
    }
  end

  def create
    ActiveRecord::Base.transaction do
      page = Page.new(page_params)
      if page.save
        flash[:success] = '登録しました。'
        page_content_replace_blob_url(page)
        render json: { errors: {}, page: page }
      else
        puts page.errors.as_json
        render json: { errors: page.errors, page: {}}
      end
    end
  end

  def update
    ActiveRecord::Base.transaction do
      page = Page.find_by(id: params[:id])
      page.assign_attributes(page_params)

      if page.save
        page_content_replace_blob_url(page)
        render json: {
          errors: {},
          page: page.as_json(
            include: [{
              image: {
                methods: [:preview]
              }
            }]
          ).merge(alertSuccess: '更新しました。')
        }
      else
        puts page.errors.full_messages
        render json: { errors: page.errors, page: {} }
      end
    end
  end

  def destroy
    page = Page.where(predefined: false).find_by(id: params[:id])
    page.destroy
    @pages = get_pages
    render :index
  end

  private

  def page_content_replace_blob_url(page)
    uploaded_image_url = page.try(:image).try(:image).try(:url, :large)
    page.update(
      content: remove_blob(page.content, uploaded_image_url),
      content_top: remove_blob(page.content_top, uploaded_image_url)
    )
  end

  def remove_blob(content, uploaded_image_url)
    if content.present?
      doc = Nokogiri::HTML(content)
      doc.css('img').each do |img|
        if img[:src].index('blob:http').present?
          content.gsub!(img[:src], uploaded_image_url)
        end
      end
      return content
    end
    content
  end

  def get_pages
    limit_per_page = 20
    pages = Page.where(pageable: nil).where(predefined: params[:predefined].to_i).order(created_at: :DESC)
    status = params[:status].to_s.downcase
    search = params[:search].to_s.strip
    if ['unpublished', 'published', 'deleted', 'filled', 'unfilled'].index(status)
      pages = eval("pages.#{status}")
    end
    if search
      pages = pages.where("`pages`.`title` LIKE :search OR `pages`.`name` LIKE :search", search: "%#{search}%")
    end
    pages.page(params[:page_id]).per(limit_per_page)
  end


  def page_params
    page = params[:page]
    page.permit(
      :name,
      :url_slug,
      :url,
      :seo_title,
      :seo_keywords,
      :seo_description,
      :seo_robot_noindex,
      :seo_robot_nofollow,
      :title,
      :content_top,
      :content,
      :content_bottom,
      :page_date,
      :cta_id,
      :publish_at,
      :filled,
      :publish_status,
      :use_redirect,
      :redirect_url
    ).merge(
      seo_title: page[:seo_title].present? ? page[:seo_title] : page[:title],
      publish_at: page[:publish_at] || Time.now,
      url: page[:url].try(:first).to_s == '/' || page[:url].blank? ? page[:url] : '/'+page[:url].to_s,
      image_attributes: image_params
    )
  end
end