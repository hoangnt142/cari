export const keepOnPage = function(e) {
  let message = '変更内容が保存されませんが、よろしいですか?';
  e.returnValue = message;
  return message;
}

export const websiteUrl = (basedUrl=null) => {
  let arr = window.location.href.split("/")
  return (arr[0] + "//" + arr[2] + (basedUrl || ''))
}

export const urlParamsToObject = (string) => {
  if (!string) {
    return {}
  }
  let params = string.substring(1)
  return JSON.parse('{"' + decodeURI(params).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}')
}

export const savedJobIds = (type, removeOld=false) => {
  if (type == 'history') {
    let jobs = JSON.parse(localStorage.getItem('history')) || []
    let deletionJob = []
    if (removeOld) {
      let currentDate = Date.parse(new Date())
      jobs.map((job, index) => {
        if (currentDate - Date.parse(job.createdAt) >= 2592000000 ) {// different 1 month
          deletionJob.push(job.jobId)
        }
      })
    }
    jobs = jobs.filter(job => !deletionJob.includes(job.jobId))
    localStorage.setItem('history', JSON.stringify(jobs));
    return jobs.map(o => o['jobId'])
  } else {
    return (JSON.parse(localStorage.getItem(type)) || [])
  }
  
}

export const toggleSavedJob = (type, jobId) => {
  let jobIds = savedJobIds(type)
  let jobIdIndex = jobIds.indexOf(jobId)
  if (type == 'history') {
    let jobs = JSON.parse(localStorage.getItem('history')) || []
    jobs.push({jobId: jobId, createdAt: new Date()})
    localStorage.setItem('history', JSON.stringify(jobs));
  } else {
    if ( jobIdIndex > -1) {
      jobIds.splice(jobIdIndex, 1)
    } else {
      jobIds.push(jobId)
    }
    localStorage.setItem(type, JSON.stringify(jobIds));
    return
  }
}

