import React, { PureComponent } from 'react'
import TinyMCE from 'react-tinymce'
import TinyMCEConfig from './TinyMCEConfig'
import ImageLibrary from '../../bundles/AdminApp/containers/ImageLibraryContainer'

export default class Editor extends PureComponent {

  constructor(props) {
    super(props);
    this.state = { openImageLibrary: false };
  }

  openImageLibrary = () => {
    this.setState({ openImageLibrary: true });
  }

  handleOpenLibrary = () => {
    this.setState({ openImageLibrary: !this.state.openImageLibrary });
  }

  getContent = () => {
    let editor = tinymce.EditorManager.get(this.props.name);
    let value_text = editor.getContent();
    if (value_text.length == 0) {
      return ''
    } else {
      return editor.getContent({ format: 'raw' })
    }
  }

  insertImage = (image = {}) => {
    let style = ''
    let alt = ''
    let title = ''
    if (image.align == 'center') {
      style = `style="display: block; margin-left: auto; margin-right: auto"`
    } else if (image.align == 'right') {
      style = `style="display: block; margin-left: auto"`
    }
    if (image.alt) { alt = `alt=${image.alt}` }
    if (image.title) { title = `title=${image.title}` }
    let content = `<img ${style} src=${image.image_url.large} width=${image.width} height=${image.height} ${alt} ${title} />`
    
    if (image.linkTo) {
      content = `<a href=${image.linkTo}>${content}</a>`
    }
    tinymce.activeEditor.execCommand('mceInsertContent', false, content)
    this.setState({ openImageLibrary: false })
  }

  render() {
    console.log('Editor rendered')
    return (
      <div className='field'>
        <label style={{textTransform: 'capitalize'}}>{this.props.label}</label>
        {this.state.openImageLibrary && (
          <ImageLibrary insertImage={this.insertImage} isOpen={this.state.openImageLibrary} handleOpenLibrary={this.handleOpenLibrary} popup />
        )}
        <TinyMCEConfig {...this.props} openImageLibrary={this.handleOpenLibrary} />
      </div>
    );
  }
}