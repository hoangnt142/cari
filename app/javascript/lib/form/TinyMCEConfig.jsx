import React from 'react';
import TinyMCE from 'react-tinymce';
import { keepOnPage } from '../function';

export default class TinyMCEConfig extends React.PureComponent {

  constructor(props) {
    super(props);
  }

  render() {
    const { name, value, insertEyeCatchImage, openImageLibrary } = this.props
    let minHeight = this.props.minHeight || 400
    return (
      <div>
        <TinyMCE
          id={name}
          content={value}
          config={{
                  fontsize_formats: "8pt 9pt 10pt 11pt 12pt 13pt 14pt 15pt 16pt 17pt 18pt 19pt 20pt 21pt 22pt 23pt 24pt 25pt 26pt 27pt 28pt 29pt 30pt 31pt 32pt 33pt 34pt 35pt 36pt",
                  theme: "modern",
                  language_url: "/langs/ja.js",
                  theme_advanced_fonts: "Andale Mono=andale mono,times;",
                  plugins: 'advlist autolink lists link anchor image charmap preview hr pagebreak searchreplace visualblocks visualchars code fullscreen insertdatetime media nonbreaking save table contextmenu directionality emoticons paste textcolor colorpicker textpattern imagetools codesample',
                  toolbar1: "undo redo | bold italic underline blockquote | cut copy paste pastetext | alignleft aligncenter alignright alignjustify | bullist numlist | table | outdent indent",
                  toolbar2: "strikethrough | charmap emoticons insertdatetime nonbreaking codesample | hr pagebreak | subscript superscript | visualblocks visualchars | ltr rtl | removeformat",
                  toolbar3: "formatselect styleselect | fontselect | fontsizeselect",
                  toolbar4: "link unlink anchor | addimage insertfile | addad | forecolor backcolor | preview code fullscreen searchreplace | nonQuote addPoint addCaution addButton addConversation insertEyeCatchImage",
                  min_height: minHeight,
                  width: 840,
                  convert_urls: false,
                  content_css: "/editor.css",
                  init_instance_callback: function (editor) {
                    editor.on('KeyDown', function (e) {
                      if(e.keyCode == 27) {
                        const editor = tinyMCE.activeEditor
                        const dom = editor.dom
                        const parentBlock = tinyMCE.activeEditor.selection.getSelectedBlocks()[0].parentNode
                        const containerBlock = parentBlock.parentNode.nodeName == 'BODY' ? dom.getParent(parentBlock, dom.isBlock) : dom.getParent(parentBlock.parentNode, dom.isBlock)
                        let newBlock = tinyMCE.activeEditor.dom.create('p')
                        newBlock.innerHTML = '<br data-mce-bogus="1">';
                        dom.insertAfter(newBlock, containerBlock)
                        let rng = dom.createRng();
                        newBlock.normalize();
                        rng.setStart(newBlock, 0);
                        rng.setEnd(newBlock, 0);
                        editor.selection.setRng(rng);
                      }
                    });
                  },
                  setup: (t) => {
                    t.addButton("insertEyeCatchImage", {
                      text: 'アイキャッチ',
                      onclick: () => {
                        insertEyeCatchImage(t)
                      }
                    }),
                    t.addButton("addimage", {
                      text:!1,
                      icon:"image",
                      onclick: () => {
                        openImageLibrary();
                      }
                    }),
                    t.addButton("nonQuote", {
                      text:!1,
                      icon:"code",
                      onclick: () => {
                        t.insertContent('<div class="p-content non-quote"><p>' + t.selection.getContent() + '</p></div>');
                      }
                    }),
                    t.addButton("addPoint", {
                      text:'ADDポイント',
                      onclick: () => {
                        t.insertContent('<div class="editor-point"><div class="p-title"><p>ポイント</p></div><div class="p-content"><p>' + t.selection.getContent() +'</p></div></div>');
                      }
                    }),
                    t.addButton("addCaution", {
                      text:'ADD注意',
                      onclick: () => {
                        t.insertContent('<div class="editor-caution"><div class="c-title"><p>注意</p></div><div class="p-content"><p>' + t.selection.getContent() +'</p></div></div>');
                      }
                    }),
                    t.addButton("addButton", {
                      text:'ADDボタン',
                      onclick: function() {
                        t.windowManager.open({
                          title: 'Add ボタン',
                          width: 420,
                          height: 170,
                          body: [
                            {type: 'textbox', name: 'content', label: 'Content'},
                            {type: 'textbox', name: 'url', label: 'URL'},
                            {type: 'checkbox', name: 'target_blank', label: 'Open new tab'},
                            {
                              type   : 'listbox',
                              name   : 'color',
                              label  : 'Color',
                              values : [
                                { text: 'Green', value: 'green', selected: true },
                                { text: 'Pink', value: 'pink' }
                              ]
                            }
                          ],
                          onsubmit: function(e) {
                            t.insertContent(`<div style="width: 100%;text-align: center;margin: 2rem 0"><p><a href=${e.data.url} ${e.data.target_blank ? 'target="_blank"':''}><button class="ui ${e.data.color} button massive">${e.data.content}</button></a></p></div>`);
                          }
                        });
                      }
                    }),
                    t.addButton("addConversation", {
                      text:'ADD Conversation',
                      onclick: function() {
                        t.windowManager.open({
                          title: 'Add Conversation',
                          width: 420,
                          height: 70,
                          body: [
                            {
                              type   : 'listbox',
                              name   : 'position',
                              label  : 'Position (left, right)',
                              values : [
                                { text: 'Left', value: 'left', selected: true },
                                { text: 'Right', value: 'right' }
                              ]
                            }
                          ],
                          onsubmit: function(e) {
                            t.insertContent(`<div class="ca-conversation ${e.data.position}"><div class="co-icon"><div class="con-circel"></div><div>のんぴー</div></div><div class="co-content"><p>${t.selection.getContent()}</p></div></div></div>`);
                          }
                        });
                      }
                    })
                  }
                }}
          />
      </div>
    );
  }
}