import React from 'react';


export const breadCrumbList = (list) => {

  const itemListElements = list.map((l, i) => (`{
    "@type": "ListItem",
    "position": ${i+1},
    "item": {
      "@id": "${l.url}",
      "name": "${l.title}"
    }
  }`));

    return (
      <script type="application/ld+json">
        {`{
               "@context": "http://schema.org/",
               "@type": "BreadcrumbList",
               "itemListElement": [
                 ${itemListElements.join()}
               ]
            }`}
      </script>
    );
}
