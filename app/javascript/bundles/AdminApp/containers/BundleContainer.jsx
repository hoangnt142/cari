import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import Admin from '../components/Admin'

const mapStateToProps = (state) => ({
  isNotAdmin: state.adminApi.isNotAdmin
})

export default withRouter(connect(mapStateToProps)(Admin))