import { connect } from 'react-redux'
import Ctas from '../components/Ctas'
import * as actions from '../actions/adminCtaActions'

const mapStateToProps = (state) => ({
  ctas: state.adminApi.ctas,
  totalPages: state.adminApi.totalPages,
  currentPage: state.adminApi.currentPage,
  fetching: state.adminApi.fetching
})

export default connect(mapStateToProps, actions)(Ctas)
