import { connect } from 'react-redux'
import CategoryForm from '../components/CategoryForm'
import * as actions from '../actions/adminCategoryActions'

const mapStateToProps = (state) => ({
  category: state.adminCategory.category,
  parentCategories: state.adminApi.parent_categories,
  errors: state.adminCategory.errors,
  changed: state.adminCategory.changed,
  fetching: state.adminApi.fetching
})

export default connect(mapStateToProps, actions)(CategoryForm)
