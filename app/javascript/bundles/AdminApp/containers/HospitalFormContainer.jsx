import { connect } from 'react-redux'
import HospitalForm from '../components/HospitalForm'
import * as actions from '../actions/adminHospitalActions'

const mapStateToProps = (state) => ({
  hospital: state.adminHospital.hospital,
  errors: state.adminHospital.errors,
  changed: state.adminHospital.changed,
  fetching: state.adminApi.fetching
})

export default connect(mapStateToProps, actions)(HospitalForm)
