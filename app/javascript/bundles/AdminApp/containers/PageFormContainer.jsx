import { connect } from 'react-redux'
import PageForm from '../components/PageForm'
import * as actions from '../actions/adminPageActions'

const mapStateToProps = (state) => ({
  page: state.adminPage.page,
  errors: state.adminPage.errors,
  changed: !state.adminPage.changed,
  ctas: state.adminApi.ctas,
})

export default connect(mapStateToProps, actions)(PageForm)
