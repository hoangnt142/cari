import { connect } from 'react-redux'
import Pages from '../components/Pages'
import * as actions from '../actions/adminPageActions'

const mapStateToProps = (state) => ({
  pages: state.adminApi.pages,
  totalPages: state.adminApi.totalPages,
  currentPage: state.adminApi.currentPage,
  isNotAdmin: state.adminApi.isNotAdmin,
  fetching: state.adminApi.fetching,
  changed: state.adminPage.changed
})

export default connect(mapStateToProps, actions)(Pages)
