import { connect } from 'react-redux'
import Categories from '../components/Categories'
import * as actions from '../actions/adminCategoryActions'

const mapStateToProps = (state) => ({
  adminApi: state.adminApi
})

export default connect(mapStateToProps, actions)(Categories)
