import { connect } from 'react-redux'
import { withRouter, Route, Redirect } from 'react-router-dom'

const PrivateRoute = ({ component: Component, isNotAdmin, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      isNotAdmin ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{
            pathname: "/admin/login",
            state: { from: props.location }
          }}
        />
      )
    }
  />
)
PrivateRoute.propTypes = {
  component: PropTypes.func.isRequired
 }
const mapStateToProps = (state) => ({
  isNotAdmin: state.adminApi.isNotAdmin
})

export default withRouter(connect(mapStateToProps)(PrivateRoute))