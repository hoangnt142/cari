import { connect } from 'react-redux'
import Login from '../components/Login'
import * as actions from '../actions/loginActions'
import { reset } from '../actions/adminApiActions'

const mapStateToProps = (state) => ({
  admin: state.login.admin,
  adminApi: state.adminApi,
  loggedInFailed: state.login.loggedInFailed
})

export default connect(mapStateToProps, Object.assign(actions, { reset }))(Login)
