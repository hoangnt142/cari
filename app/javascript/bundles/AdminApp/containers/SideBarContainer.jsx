import { connect } from 'react-redux';
import SideBar from '../components/SideBar';
import { logout } from '../actions/loginActions'

const mapStateToProps = (state) => ({
  loggedInFailed: state.login.loggedInFailed
});

export default connect(mapStateToProps, { logout })(SideBar);
