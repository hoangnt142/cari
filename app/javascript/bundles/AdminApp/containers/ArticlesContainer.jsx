import { connect } from 'react-redux'
import Articles from '../components/Articles'
import * as actions from '../actions/adminArticleActions'

const mapStateToProps = (state) => ({
  adminApi: state.adminApi
})

export default connect(mapStateToProps, actions)(Articles)
