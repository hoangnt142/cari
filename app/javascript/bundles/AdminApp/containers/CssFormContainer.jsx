import { connect } from 'react-redux'
import CssForm from '../components/CssForm'
import * as actions from '../actions/adminSystemConfigActions'

const mapStateToProps = (state) => ({
  systemConfig: state.adminSystemConfig.systemConfig,
  changed: state.adminSystemConfig.changed,
  adminApi: state.adminApi
})

export default connect(mapStateToProps, actions)(CssForm)
