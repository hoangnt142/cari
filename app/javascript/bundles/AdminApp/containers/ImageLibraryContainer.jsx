import { connect } from 'react-redux'
import ImageLibrary from '../components/ImageLibrary'
import * as actions from '../actions/adminImageActions'

const mapStateToProps = (state) => ({
  images: state.adminApi.images,
  submmitedAt: state.adminImage.submmitedAt,
  errors: state.adminImage.errors
})

export default connect(mapStateToProps, actions)(ImageLibrary)
