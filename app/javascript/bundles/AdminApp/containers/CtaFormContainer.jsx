import { connect } from 'react-redux'
import CtaForm from '../components/CtaForm'
import * as actions from '../actions/adminCtaActions'

const mapStateToProps = (state) => ({
  cta: state.adminCta.cta,
  errors: state.adminCta.errors,
  changed: state.adminCta.changed,
  fetching: state.adminApi.fetching
})

export default connect(mapStateToProps, actions)(CtaForm)
