import { connect } from 'react-redux'
import Hospitals from '../components/Hospitals'
import * as actions from '../actions/adminHospitalActions'

const mapStateToProps = (state) => ({
  adminApi: state.adminApi
})

export default connect(mapStateToProps, actions)(Hospitals)
