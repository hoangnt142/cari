import { connect } from 'react-redux'
import ArticleForm from '../components/ArticleForm'
import * as actions from '../actions/adminArticleActions'

const mapStateToProps = (state) => ({
  article: state.adminArticle.article,
  article_categories: state.adminApi.article_categories,
  search_keys: state.adminApi.search_keys,
  errors: state.adminArticle.errors,
  ctas: state.adminApi.ctas,
  changed: state.adminArticle.changed,
  fetching: state.adminApi.fetching
})

export default connect(mapStateToProps, actions)(ArticleForm)
