import { createStore, applyMiddleware } from 'redux'
import reducers from '../reducers'
import { createLogger } from 'redux-logger'
import thunkMiddleware from 'redux-thunk'


const middleware = [thunkMiddleware]
// Include logger only in development
if(process.env.NODE_ENV != 'production') {
  const logger = createLogger({
    level: 'log'
  })
  middleware.push(logger)
}

const configureStore = (railsProps) => {
  return createStore(reducers, railsProps, applyMiddleware(...middleware))
}

export default configureStore
