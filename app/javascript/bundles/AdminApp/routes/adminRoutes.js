import React from 'react'
import { Switch, Route, Redirect, BrowserRouter as Router } from 'react-router-dom'
import { Sidebar } from 'semantic-ui-react'

import {
  AdminSideBar,
  Login,
  Jobs,
  Bundle
} from '../containers';


const asyncComponent = (imp, props={}) =>
  <Bundle
    load={() => imp()}>
    {Comp => <Comp {...props} />}
  </Bundle>
;

const AsynPages = (props) => {
  return asyncComponent(() => import('../containers/PagesContainer'), props)
}

const AsynPageForm = (props) => {
  return asyncComponent(() => import('../containers/PageFormContainer'), props)
}

const AsynHospitals = (props) => {
  return asyncComponent(() => import('../containers/HospitalsContainer'), props)
}
const AsynHospitalForm = (props) => {
  return asyncComponent(() => import('../containers/HospitalFormContainer'), props)
}

const AsynCategories = (props) => {
  return asyncComponent(() => import('../containers/CategoriesContainer'), props)
}
const AsynCategoryForm = (props) => {
  return asyncComponent(() => import('../containers/CategoryFormContainer'), props)
}

const AsynArticles = (props) => {
  return asyncComponent(() => import('../containers/ArticlesContainer'), props)
}

const AsynArticleForm = (props) => {
  return asyncComponent(() => import('../containers/ArticleFormContainer'), props)
}

const AsynCtas = (props) => {
  return asyncComponent(() => import('../containers/CtasContainer'), props)
}

const AsynCtaForm = (props) => {
  return asyncComponent(() => import('../containers/CtaFormContainer'), props)
}

const AsynCssForm = (props) => {
  return asyncComponent(() => import('../containers/CssFormContainer'), props)
}

export default () => {
  let context = null
  const getContext = () => context

  return (
    <Sidebar.Pushable>
      <Route path="/admin" component={AdminSideBar} />
      <Route path='/admin/login' component={Login} />
      <Sidebar.Pusher style={{width: 'calc(100% - 260px)', backgroundColor: '#fff', height: '100%', overflowY: 'scroll'}}>
        <Switch>
          <Route exact path='/admin' component={AsynPages} />
          <Route path='/admin/pages/:id' component={AsynPageForm} />
          <Route exact path='/admin/predefined-pages' component={AsynPages} />
          <Route path='/admin/predefined-pages/:id' component={AsynPageForm} />

          <Route exact path='/admin/hospitals' component={AsynHospitals} />
          <Route path='/admin/hospitals/:id' component={AsynHospitalForm} />

          <Route exact path='/admin/ctas' component={AsynCtas} />
          <Route path='/admin/ctas/:id' component={AsynCtaForm} />

          <Route exact path='/admin/categories' component={AsynCategories} />
          <Route path='/admin/categories/:id' component={AsynCategoryForm} />

          <Route exact path='/admin/articles' component={AsynArticles} />
          <Route path='/admin/articles/:id' component={AsynArticleForm} />

          <Route path='/admin/css/' component={AsynCssForm} />
        </Switch>
      </Sidebar.Pusher>
    </Sidebar.Pushable>
  )
}