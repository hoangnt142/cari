import React, { PureComponent } from 'react'
import { Provider } from 'react-redux'
import { BrowserRouter as Router } from 'react-router-dom'

import { login } from '../actions/loginActions'
import routes from '../routes/adminRoutes'
import configureStore from '../store/store'

import '../../../app/assets/stylesheets/admin.scss'
import '../../../../../semantic/dist/semantic.min.css'

export default class AdminApp extends PureComponent {

  constructor(props) {
    super(props)
    this.store = configureStore(this.props)
  }

  render() {
    return (
      <Provider store={this.store}>
        <Router>
          {routes()}
        </Router>
      </Provider>
    )
  }
}
