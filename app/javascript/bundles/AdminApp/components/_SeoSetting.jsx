import React, { Component } from 'react'

import { Segment, Header, Form, Input, TextArea, Radio} from 'semantic-ui-react'

export default class SeoSetting extends Component {

  render() {
    const { page, forPageable, onChange } = this.props
    const prefix = forPageable ? 'page.' : ''
    return (
      <div className='ui segments input-group'>
        <Segment>
          <Header>SEO setting</Header>
        </Segment>
        <Segment>
          <Form.Field control={Input} label='SEO Title' placeholder='SEO Title' name={prefix+'seo_title'} value={page.seo_title || ''} onChange={onChange} />
          <Form.Field control={TextArea} label='SEO Description' placeholder='SEO Description' name={prefix+'seo_description'} value={page.seo_description || ''} onChange={onChange} />
          <Form.Field control={Input} label='SEO Keywords' placeholder='SEO Keywords' name={prefix+'seo_keywords'} value={page.seo_keywords || ''} onChange={onChange} />
          <div className='inline fields'>
            <label>Robot Index</label>
            <Form.Field>
              <Radio label='No Index' name={prefix+'seo_robot_noindex'} checked={page.seo_robot_noindex} onChange={(e, data) => onChange({target: {name: data.name, value: data.checked}})} />
              <Radio label='Index' name={prefix+'seo_robot_noindex'} checked={!page.seo_robot_noindex} onChange={(e, data) => onChange({target: {name: data.name, value: !data.checked}})} />
            </Form.Field>
          </div>
          <div className='inline fields'>
            <label>Robot Follow</label>
            <Form.Field>
              <Radio label='No Follow' name={prefix+'seo_robot_nofollow'} checked={page.seo_robot_nofollow} onChange={(e, data) => onChange({target: {name: data.name, value: data.checked}})} />
              <Radio label='Follow' name={prefix+'seo_robot_nofollow'} checked={!page.seo_robot_nofollow} onChange={(e, data) => onChange({target: {name: data.name, value: !data.checked}})} />
            </Form.Field>
          </div>
        </Segment>
      </div>
    )
  }
}
