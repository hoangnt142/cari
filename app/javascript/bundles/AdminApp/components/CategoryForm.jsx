import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'
import { Grid, Segment, Table, Header, Button, Form, Input, TextArea, Radio, Message, Icon, Label, Select } from 'semantic-ui-react'

import SeoSetting from './_SeoSetting'
import PageContent from './_PageContent'
import PageCommonForm from './_PageCommonForm'
import Loading from './_Loading'

export default class CategoryForm extends Component {

  constructor(props) {
    super(props);
    this.action = { new: props.match.params.id == 'new' }

    const { getCategory } = this.props

    getCategory(props.match.params.id)

    this.onChange = this.onChange.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
  }

  onChange = (e) => {
    const field = e.target.name
    const value = e.target.value
    this.props.onChange(field, value)
  }

  onSubmit = () => {
    const that = this
    const page = this.props.category.page;
    ['content', 'content_top', 'content_bottom'].map(content => {
      if (eval(`page.use_${content}`)) {
        that.props.onChange('page.'+content, eval(`that.pageContents.page_${content}.getContent()`))
      }
    })
    this.props.onSubmit(this.props.category)
  }

  componentWillUnmount() {
    const { reset } = this.props
    reset()
  }

  render() {
    const { category, parentCategories, errors, changed } = this.props
    let page = category.page
    if (!page.use_content) {page.use_content = true}
    if (this.action.new && category.id) {
      return <Redirect to={`/admin/categories/${category.id}`} />
    }

    if (!this.action.new && !page.id) {
      return <Loading />
    }

    let basedUrl = ''
    if (category.parent_id) {
      try {
        basedUrl = parentCategories.find(parent => parent.key == category.parent_id).url
      } catch (err) {
        basedUrl = page.url.replace(page.url_slug, '')
      }
    }

    return (
      <Form>
        <Grid columns='equal' textAlign='left' stackable>
          <Grid.Row>
            <Grid.Column width={16}>
              <div className='ui segments'>
                <PageCommonForm
                  flagDisable={true}
                  forPageable
                  page={page}
                  action={this.action}
                  onChange={this.onChange}
                  onSubmit={this.onSubmit}
                  title={page.title}
                  enablePageDate={false}
                />
                <Segment>
                  <div className='ui segments input-group'>
                    <Segment>
                      <Header>Category Contents</Header>
                    </Segment>
                    <Segment>
                      <Form.Field>
                        <label>Parent Category</label>
                        <Select
                          value={category.parent_id || 0}
                          onChange={(e, data) => this.onChange({target: {name: 'parent_id', value: data.value}})}
                          options={parentCategories}
                        />
                      </Form.Field>
                      <Form.Field>
                        <label>Title</label>
                        <Form.Input value={page.title || ''} name='page.title' onChange={this.onChange} error={errors['page.title'] !== undefined} />
                        {errors['page.title'] && <Label basic color='red' pointing>{errors['page.title']}</Label>}
                      </Form.Field>
                      <PageContent
                        eyeCatchImage
                        forPageable
                        basedUrl={basedUrl}
                        ref={(pageContents) => this.pageContents = pageContents}
                        error={errors['page.url']}
                        errors={errors}
                        page={page}
                        changed={changed}
                        onChange={this.onChange}
                      />
                    </Segment>
                  </div>
                  <SeoSetting page={page} onChange={this.onChange} forPageable/>
                </Segment>
              </div>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Form>
    )
  }
}
