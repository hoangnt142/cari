import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'
import {
  Grid,
  Segment,
  Loader,
  Dimmer,
  Table,
  Header,
  Button,
  Form,
  Radio,
  Message,
  Icon,
  Label,
  Select } from 'semantic-ui-react'

import SeoSetting from './_SeoSetting'
import PageContent from './_PageContent'
import PageCommonForm from './_PageCommonForm'
import Loading from './_Loading'

export default class ArticleForm extends Component {

  constructor(props) {
    super(props);
    this.action = { new: props.match.params.id == 'new' }

    const { getArticle } = this.props

    getArticle(props.match.params.id)

    this.onChange = this.onChange.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
  }

  onChange = (e) => {
    const field = e.target.name
    const value = e.target.value
    this.props.onChange(field, value)
  }

  onSubmit = () => {
    const that = this
    const page = this.props.article.page;
    ['content', 'content_top', 'content_bottom'].map(content => {
      if (eval(`page.use_${content}`)) {
        that.props.onChange('page.'+content, eval(`that.pageContents.page_${content}.getContent()`))
      }
    })
    this.props.onSubmit(this.props.article)
  }

  componentWillUnmount() {
    const { reset } = this.props
    reset()
  }

  render() {
    const { article, article_categories, search_keys, errors, changed, ctas } = this.props
    let page = article.page
    page.use_content = true
    if (this.action.new && article.id) {
      return <Redirect to={`/admin/articles/${article.id}`} />
    }

    if (!this.action.new && !page.id) {
      return <Loading />
    }

    let basedUrl = ''
    if (article.category_id) {
      try {
        basedUrl = article_categories.find(category => category.key == article.category_id).url
      } catch (error) {
        basedUrl = page.url.replace(page.url_slug, '')
      }
    }

    return (
      <Form>
        <Grid columns='equal' textAlign='left' stackable>
          <Grid.Row>
            <Grid.Column width={16}>
              <div className='ui segments'>
                <PageCommonForm
                  flagDisable={true}
                  forPageable
                  page={page}
                  action={this.action}
                  onChange={this.onChange}
                  onSubmit={this.onSubmit}
                  title={page.title}
                  enablePageDate={false}
                />
                <Segment>
                  <div className='ui segments input-group'>
                    <Segment>
                      <Header>Article Contents</Header>
                    </Segment>
                    <Segment>
                      <Form.Field>
                        <label>Category</label>
                        <Select
                          value={article.category_id || 0}
                          onChange={(e, data) => this.onChange({target: {name: 'category_id', value: data.value}})}
                          options={article_categories}
                        />
                      </Form.Field>
                      <Form.Field>
                        <label>Title</label>
                        <Form.Input value={page.title || ''} name='page.title' onChange={this.onChange} error={errors['page.title'] !== undefined} />
                        {errors['page.title'] && <Label basic color='red' pointing>{errors['page.title']}</Label>}
                      </Form.Field>
                      <PageContent
                        eyeCatchImage
                        forPageable
                        basedUrl={basedUrl}
                        ref={(pageContents) => this.pageContents = pageContents}
                        error={errors['page.url']}
                        errors={errors}
                        page={page}
                        changed={changed}
                        onChange={this.onChange}
                        ctas={ctas}
                      />
                      <Form.Field>
                        <label>関連記事</label>
                        <Select
                          value={article.search_key_id || 0}
                          onChange={(e, data) => this.onChange({target: {name: 'search_key_id', value: data.value}})}
                          options={search_keys}
                        />
                      </Form.Field>
                    </Segment>
                  </div>
                  <SeoSetting page={page} onChange={this.onChange} forPageable/>
                </Segment>
              </div>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Form>
    )
  }
}
