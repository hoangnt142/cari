import React, { Component } from 'react';
import { Redirect } from 'react-router-dom'

class Admin extends Component {
  state = {
    // short for "module" but that's a keyword in js, so "mod"
    mod: null
  }

  componentWillMount() {
    this.load(this.props)
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.load !== this.props.load) {
      this.load(nextProps)
    }
  }

  load(props) {
//    console.log('load is called', props);
    this.setState({
      mod: null
    });
//    props.load((mod) => {
    props.load().then(mod => {
//      console.log('mod: ', mod);
      this.setState({
        // handle both es imports and cjs
        mod: mod.default ? mod.default : mod
      });
    })
  }

  render() {
    const { isNotAdmin } = this.props
    if (isNotAdmin) {
      return <Redirect to={'/admin/login'} />
    }
    return this.state.mod ? this.props.children(this.state.mod) : null
  }
}

export default Admin
