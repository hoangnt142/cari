import React, { PureComponent, PropTypes } from 'react'
import { Link } from 'react-router-dom'
import { Grid, Segment, Table, Header, Button, Menu, Dropdown, Input, Icon, Dimmer} from 'semantic-ui-react'
import { urlParamsToObject } from 'lib/function'
import { objectToParameters } from '../../App/lib/url'

import Pagination from './_Pagination'
import ListHeader from './_ListHeader'

export default class Cta extends PureComponent {

  constructor(props) {
    super(props);
    const { getCtas } = this.props
    getCtas()
  }

  onDelete = (cta_id) => {
    if (confirm('本当に削除してもよろしいですか?')) {
      this.props.onDelete(cta_id, this.props.location.search)
    }
  }

  render() {
    const { ctas, totalPages, currentPage, onDelete, fetching, location, history } = this.props

    return (
      <Grid columns='equal' textAlign='left' stackable>
        <Grid.Column width={16}>
          <div className='ui segments'>
            <ListHeader title='CTA' hideFilter buttonName='Add CTA'/>
            <Dimmer.Dimmable as={Segment} blurring dimmed={fetching}>
              <Table celled>
                <Table.Header>
                  <Table.Row>
                    <Table.HeaderCell>名称</Table.HeaderCell>
                    <Table.HeaderCell>Content</Table.HeaderCell>
                    <Table.HeaderCell textAlign='center' style={{width: '80px'}}>操作</Table.HeaderCell>
                  </Table.Row>
                </Table.Header>

                <Table.Body>
                  {ctas.map(cta => 
                    <Table.Row key={cta.id}>
                      <Table.Cell>{cta.title}</Table.Cell>
                      <Table.Cell>{cta.content_with_out_tag}</Table.Cell>
                      <Table.Cell>
                        <Link to={'/admin/ctas/'+cta.id}>
                          <Button color='blue' size='mini' style={{margin: 0}}>編集</Button>
                        </Link>
                        <Button color='red' onClick={ () => this.onDelete(cta.id) } size='mini' style={{margin: '5px 0 0 0'}}>削除</Button>
                      </Table.Cell>
                    </Table.Row>
                  )}
                </Table.Body>

              </Table>
            </Dimmer.Dimmable>
          </div>
        </Grid.Column>
      </Grid>
    )
  }
}
