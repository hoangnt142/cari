import React, { Component } from 'react'
import { Form, Icon, Image as SemanticImage, Input} from 'semantic-ui-react'

export default class PageContent extends Component {

  onChange = (e) => {
    let file = e.target.files[0]
    let reader = new FileReader();
    let img = new Image()
    const { prefix, onChange, page, setEditorContent } = this.props
    if (file) {
      const preview = URL.createObjectURL(file)
      reader.onloadend = () => {
        img.src = reader.result
        file.id = page.image.id
        file.img_data = reader.result
        file.title = file.name
        file.alt = page.image.alt || ''
        file.img_type = file.type
        file.preview = preview
        img.onload = function(){
          file.width = img.width;
          file.height = img.height
        }
        if (setEditorContent) {setEditorContent(preview)} //get editor content before rerendering
        onChange({target: {name: prefix+'image', value: file}})
      }
      reader.readAsDataURL(file)
    }
  }

  selectFile = () => {
    this.fileInput.click()
  }

  render() {
    const { onChange, page, prefix } = this.props
    return (
      <Form.Group>
        <Form.Field>
          <label>アイキャッチ</label>
          <div className='eyeCatch' style={page.image.preview ? {border: 'none'} : {}} onClick={this.selectFile}>
            { page.image && page.image.preview ? <img src={page.image.preview} /> : null }
            <Icon name='plus' size='large' />
          </div>
          <input type='file' onChange={this.onChange} ref={(file) => this.fileInput = file} style={{display: 'none'}}/>
        </Form.Field>
        <Form.Field style={{width: '100%'}}>
          <label>イメージalt</label>
          <Input disabled={!page.image.preview} name={prefix+'image.alt'} value={page.image.alt || ''} onChange={onChange}/>
        </Form.Field>
      </Form.Group>
    )
  }
}
