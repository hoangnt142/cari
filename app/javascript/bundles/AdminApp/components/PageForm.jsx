import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'
import DateTimeInput from './_DateTimeInput'
import { Grid, Segment, Table, Header, Button, Form, Input, TextArea, Radio, Message, Icon, Label, Select } from 'semantic-ui-react'

import PageContent from './_PageContent'
import SeoSetting from './_SeoSetting'
import PageCommonForm from './_PageCommonForm'
import Loading from './_Loading'

class PageForm extends Component {

  constructor(props) {
    super(props);
    this.action = { new: props.match.params.id == 'new' }

    const { getPage } = this.props
    getPage(props.match.params.id)

    this.onChange = this.onChange.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
    this.setEditorContent = this.setEditorContent.bind(this)
  }

  onChange = (e) => {
    const field = e.target.name
    const value = e.target.value
    this.props.onChange(field, value)
  }

  onSubmit = () => {
    const page = this.props.page;
    this.setEditorContent()

    this.props.onSubmit(this.props.page)
  }

  setEditorContent = (eyeCatchUrl=null) => {
    const that = this;
    ['content', 'content_top', 'content_bottom'].map(content => {
      if (eval(`that.pageContents.page_${content}`)) {
        let editorContent = eval(`that.pageContents.page_${content}.getContent()`)
        if (eyeCatchUrl) {
          that.getImageSrces(editorContent).map(src => {
            editorContent = editorContent.split(src).join(eyeCatchUrl)
          })
        }
        that.props.onChange(content, editorContent)
      }
    })
  }

  getImageSrces = (string) => {
    let imageSrces = []
    let elem = document.createElement("div")
    elem.innerHTML = string
    const images = elem.getElementsByClassName("eye-catch-image");
    for(var i=0; i < images.length; i++) {
      imageSrces.push(images[i].getAttribute('src'))
    }
    return imageSrces
  }

  componentWillUnmount() {
    const { reset } = this.props
    reset()
  }

  render() {
    const { page, errors, changed, ctas } = this.props
    const predefined = page.predefined

    if (!this.action.new && !page.id) {
      return <Loading />
    }

    if (this.action.new && page.id) {
      return <Redirect to={`/admin/pages/${page.id}`} />
    }

    return (
      <Form>
        <Grid columns='equal' textAlign='left' stackable>
          <Grid.Row>
            <Grid.Column width={16}>
              <div className='ui segments'>
                <PageCommonForm
                  page={page}
                  action={this.action}
                  onChange={this.onChange}
                  onSubmit={this.onSubmit}
                />
                <Segment>
                  <div className='ui segments input-group'>
                    <Segment>
                      <Header>Page Contents</Header>
                    </Segment>
                    <Segment>
                      <Form.Field>
                        <label>Title</label>
                        <Form.Input value={page.title || ''} name='title' onChange={this.onChange} error={errors.title !== undefined} />
                        {errors.title && <Label basic color='red' pointing>{errors.title}</Label>}
                      </Form.Field>
                      <PageContent
                        eyeCatchImage={!predefined}
                        setEditorContent={this.setEditorContent}
                        ref={(pageContents) => this.pageContents = pageContents}
                        page={page}
                        onChange={this.onChange}
                        error={errors.url}
                        changed={changed}
                        ctas={ctas}
                      />
                    </Segment>
                  </div>
                  <SeoSetting page={page} onChange={this.onChange} />
                </Segment>
              </div>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Form>
    )
  }
}

export default PageForm
