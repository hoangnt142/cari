import React, { PureComponent, PropTypes } from 'react'
import { Link, Redirect } from 'react-router-dom'
import { Grid, Segment, Table, Header, Button, Menu, Dropdown, Input, Icon, Dimmer} from 'semantic-ui-react'
import { urlParamsToObject } from 'lib/function'
import Pagination from './_Pagination'
import ListHeader from './_ListHeader'

class Pages extends PureComponent {

  constructor(props) {
    super(props);
    console.log(props)
    const { getPages } = this.props
    const predefined = props.location.pathname === "/admin"? '0':'1';
    const params = props.location.search? props.location.search + '&predefined=' + predefined : '?predefined=' + predefined
    getPages(params)
    this.onSearch = this.onSearch.bind(this)
    this.searchParams = this.searchParams.bind(this)
    this.onDelete = this.onDelete.bind(this)
  }

  onSearch = () => {
    this.props.history.push({
      pathname: this.props.location.pathname,
      search: "?search="+this.searchInput.value
    })
  }

  onDelete = (page_id) => {
    if (confirm('本当に削除してもよろしいですか?')) {
      this.props.onDelete(page_id)
    }
  }

  searchParams = () => {
    return urlParamsToObject(this.props.location.search)
  }

  render() {
    const { pages, totalPages, currentPage, onDelete, isNotAdmin, fetching, changed } = this.props
    if (isNotAdmin) {
      return <Redirect to='/admin/login' />
    }

    const path = this.props.location.pathname === "/admin"? '':'predefined-pages';

    return (
      <Grid columns='equal' textAlign='left' stackable>
        <Grid.Column width={16}>
          <div className='ui segments'>
            <ListHeader title={path? 'Predefined Page List' : 'Page List'} buttonName='Add Page' newPath='/admin/pages/new' hideAction={path == 'predefined-pages'}/>
            <Dimmer.Dimmable as={Segment} blurring dimmed={fetching}>
              <Table celled>
                <Table.Header>
                  <Table.Row>
                    <Table.HeaderCell>タイトル</Table.HeaderCell>
                    <Table.HeaderCell style={{width: '128px'}}>記事日付</Table.HeaderCell>
                    <Table.HeaderCell>Content</Table.HeaderCell>
                    <Table.HeaderCell>SEO description</Table.HeaderCell>
                    <Table.HeaderCell style={{width: '128px'}}>公開</Table.HeaderCell>
                    <Table.HeaderCell style={{width: '116px'}}>Robots</Table.HeaderCell>
                    <Table.HeaderCell textAlign='center' style={{width: '79px'}}>入力済み<br />フラグ</Table.HeaderCell>
                    <Table.HeaderCell textAlign='center' style={{width: '80px'}}>操作</Table.HeaderCell>
                  </Table.Row>
                </Table.Header>

                <Table.Body>
                  {pages.map(page =>
                    <Table.Row key={page.id}>
                      <Table.Cell>{page.title}</Table.Cell>
                      <Table.Cell>{page.page_date_formatted}</Table.Cell>
                      <Table.Cell>{page.used_content}</Table.Cell>
                      <Table.Cell>{page.seo_description}</Table.Cell>
                      <Table.Cell textAlign='center'>{page.status} {page.published_at_formatted && page.publish_status == 'published' && <div>{page.published_at_formatted}</div>}</Table.Cell>
                      <Table.Cell>
                        <div>INDEX: {page.seo_robot_noindex ? 'OFF' : 'ON'}</div>
                        <div>FOLLOW: {page.seo_robot_nofollow ? 'OFF' : 'ON'}</div>
                      </Table.Cell>
                      <Table.Cell textAlign='center'>
                        <Icon fitted name='check circle outline' size='large' style={{fontSize: '30px', color: page.filled ? '#8ac339' : '#ccc'}} />
                      </Table.Cell>
                      <Table.Cell>
                        <Link to={path?'/admin/predefined-pages/' + page.id:'/admin/pages/' + page.id}>
                          <Button style={{margin: 0}} color='blue' size='mini'>編集</Button>
                        </Link>
                        {!path && (
                          <Button color='red' onClick={ () => this.onDelete(page.id) } size='mini' style={{margin: '5px 0 0 0'}}>削除</Button>
                        )}
                      </Table.Cell>
                    </Table.Row>
                  )}
                </Table.Body>
              </Table>
            </Dimmer.Dimmable>
            {totalPages > 1 &&
              <div className='carrieePagination' style={{padding: 0, margin: '0.5rem 0px 1rem 0px'}}>
                <Pagination totalPages={totalPages} currentPage={currentPage} />
              </div>
            }
          </div>
        </Grid.Column>
      </Grid>
    )
  }
}

export default Pages
