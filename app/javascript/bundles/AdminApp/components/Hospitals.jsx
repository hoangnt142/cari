import React, { PureComponent, PropTypes } from 'react'
import { Link, Redirect } from 'react-router-dom'
import { Grid, Segment, Table, Header, Button, Menu, Dropdown, Input, Icon, Dimmer} from 'semantic-ui-react'
import { urlParamsToObject } from 'lib/function'
import { objectToParameters } from '../../App/lib/url'

import Pagination from './_Pagination'
import ListHeader from './_ListHeader'

export default class Hospitals extends PureComponent {

  constructor(props) {
    super(props);
    const { getHospitals } = this.props

    let params = urlParamsToObject(props.location.search)
    if (!params.status) {
      params.status = 'published'
    }
    getHospitals(`?${objectToParameters(params)}`)
  }

  render() {
    const { adminApi, location, history } = this.props
    const { hospitals, totalPages, currentPage, onDelete, fetching, isNotAdmin } = adminApi
    if (isNotAdmin) {
      return <Redirect to='/admin/login' />
    }
    return (
      <Grid columns='equal' textAlign='left' stackable>
        <Grid.Column width={16}>
          <div className='ui segments'>
            <ListHeader title='Hospital List' hideAction defaultStatus='published' />
            <Dimmer.Dimmable as={Segment} blurring dimmed={fetching}>
              <Table celled>
                <Table.Header>
                  <Table.Row>
                    <Table.HeaderCell>名称</Table.HeaderCell>
                    <Table.HeaderCell>Content</Table.HeaderCell>
                    <Table.HeaderCell>SEO description</Table.HeaderCell>
                    <Table.HeaderCell style={{width: '128px'}}>公開</Table.HeaderCell>
                    <Table.HeaderCell style={{width: '116px'}}>Robots</Table.HeaderCell>
                    <Table.HeaderCell textAlign='center' style={{width: '79px'}}>入力済み<br />フラグ</Table.HeaderCell>
                    <Table.HeaderCell textAlign='center' style={{width: '80px'}}>操作</Table.HeaderCell>
                  </Table.Row>
                </Table.Header>

                <Table.Body>
                  {hospitals.map(hospital => {
                    const page = hospital.page
                    return (
                      <Table.Row key={hospital.id}>
                        <Table.Cell>{hospital.name}</Table.Cell>
                        <Table.Cell>{page.used_content}</Table.Cell>
                        <Table.Cell>{page.seo_description}</Table.Cell>
                        <Table.Cell>{page.status}</Table.Cell>
                        <Table.Cell>
                          <div>INDEX: {page.seo_robot_noindex ? 'OFF' : 'ON'}</div>
                          <div>FOLLOW: {page.seo_robot_nofollow ? 'OFF' : 'ON'}</div>
                        </Table.Cell>
                        <Table.Cell textAlign='center'>
                          <Icon fitted name='check circle outline' size='large' style={{fontSize: '30px', color: page.filled ? '#8ac339' : '#ccc'}} />
                      </Table.Cell>
                        <Table.Cell>
                          <Link to={'/admin/hospitals/'+hospital.id}>
                            <Button color='blue' size='mini' style={{margin: 0}}>編集</Button>
                          </Link>
                        </Table.Cell>
                      </Table.Row>
                    )}
                  )}
                </Table.Body>

              </Table>
            </Dimmer.Dimmable>
            <div className='carrieePagination' style={{padding: 0, margin: '0.5rem 0px 1rem 0px'}}>
              <Pagination totalPages={totalPages} currentPage={currentPage} />
            </div>
          </div>
        </Grid.Column>
      </Grid>
    )
  }
}
