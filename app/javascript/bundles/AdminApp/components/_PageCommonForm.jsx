import React, { Component } from 'react'
import DateTimeInput from './_DateTimeInput'
import { Segment, Icon, Label, Select, Button, Message, Grid } from 'semantic-ui-react'
import PropType from 'prop-types'

const statuses = [
  { key: 'published', text: '公開済み', value: 'published' },
  { key: 'unpublished', text: '非公開', value: 'unpublished' },
  { key: 'deleted', text: '削除', value: 'deleted' }
]

class Notice extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = {};
    const self = this
    setTimeout(() => {
      self.fadeIn()
    }, 50);
  }

  fadeIn = (ms=500) => {
    let elem = document.getElementById('successAlert')

    if (ms) {
      let opacity = 0;
      const timer = setInterval(() => {
        opacity += 50 / ms;
        if (opacity >= 1) {
          clearInterval(timer);
          opacity = 1;
        }
        elem.style.opacity = opacity;
      }, 50);
    } else {
      elem.style.opacity = 1;
    }
  }

  render() {
    const { close, message } = this.props
    return (
      <div id='successAlert' onClick={() => close()}>
        <div
          style={{
            backgroundColor: "rgb(94, 160, 0)",
            color: "white",
            padding: 10,
            display: "flex",
            width: 300,
            margin: 10,
            fontWeight: 'bold'
          }}
        >
          <Icon name='checkmark' />
          <span style={{ flex: "2 1 0%" }}>{message}</span>
          <Icon onClick={() => close()} style={{cursor: 'pointer'}} name='close' />
        </div>
      </div>
    )
  }
}

export default class PageCommonForm extends Component {

  static defaultProps = {
    enablePageDate: true
  };

  constructor(props, context) {
    super(props)
  }

  closeAlert = () => {
    const { forPageable, onChange } = this.props
    const prefix = forPageable ? 'page.' : ''
    this.props.onChange({target: {name: prefix+'alertSuccess', value: false}})
  }

  render() {
    const {
      action,
      page,
      forPageable,
      onChange,
      onSubmit,
      title,
      enablePageDate,
      onlySubmit,
      flagDisable
    } = this.props
    console.log(this.context)
    if (onlySubmit) {
      return (
        <React.Fragment>
          {page.alertSuccess && <Notice close={this.closeAlert} message={page.alertSuccess} /> }
          <Segment style={{
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center'
          }}>
            <div style={{fontSize: '1.4rem', fontWeight: 'bold'}}>{title}</div>
            <div style={{display: 'flex', alignItems: 'center'}}>
              <Button color='blue' onClick={onSubmit}>
                <Icon name='save' />
                {action.new ? '保存' : '更新'}
              </Button>
            </div>
          </Segment>

        </React.Fragment>
      )
    }
    const prefix = forPageable ? 'page.' : ''
    return (
      <React.Fragment>
        {page.alertSuccess && <Notice close={this.closeAlert} message={page.alertSuccess} /> }
        <Segment style={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center'
        }}>
          <div style={{fontSize: '1.4rem', fontWeight: 'bold'}}>{title || page.title || 'New Page'}</div>
          <div style={{display: 'flex', alignItems: 'center'}}>
            {!flagDisable &&
              <div style={{marginRight: '1rem', fontSize: '20px'}}>
                <label style={{marginRight: '1rem', fontWeight: 'bold', fontSize: '1rem'}}>入力済みフラグ</label>
                <Icon
                  onClick={() => onChange({target: {name: prefix+'filled', value: !page.filled}})}
                  name='check circle outline'
                  size='large'
                  style={{cursor: 'pointer', color: page.filled ? '#8ac339' : '#ccc'}} />
              </div>
            }
            <Button color='blue' onClick={onSubmit}>
              <Icon name='save' />
              {action.new ? '保存' : '更新'}
            </Button>
          </div>
        </Segment>

        {!page.predefined &&
          <Segment>
            <Grid columns='equal'>
              <Grid.Column computer={6} mobile={16}>
                {enablePageDate &&
                  <DateTimeInput inline label='Page Date' name={prefix+'page_date'} value={page.page_date || new Date()} onChange={onChange} />
                }
              </Grid.Column>

              <Grid.Column computer={5} mobile={16}>
                <DateTimeInput inline label='Published At' name={prefix+'publish_at'} value={page.publish_at} onChange={onChange}/>
              </Grid.Column>

              <Grid.Column computer={5} mobile={16}>
                <div style={{display: 'flex', alignItems: 'center', justifyContent: 'flex-end'}}>
                  <label style={{marginRight: '1rem', fontWeight: 'bold'}}>Status</label>
                  <Select
                    value={page.publish_status || 'unpublished'}
                    name={prefix+'publish_status'}
                    onChange={(e, data) => onChange({target: {name: data.name, value: data.value}})}
                    options={statuses}
                  />
                </div>
              </Grid.Column>
            </Grid>
          </Segment>
        }
      </React.Fragment>
    )
  }
}
