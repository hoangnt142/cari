import React, { PureComponent } from 'react'
import { Link } from 'react-router-dom'
import { Grid, Segment, Header, Button, Dropdown, Icon} from 'semantic-ui-react'
import { urlParamsToObject } from 'lib/function'
import PropTypes from 'prop-types'

import { objectToParameters } from '../../App/lib/url'

const statuses = [
  { key: 'all', text: 'すべて', value: 'all' },
  { key: 'published', text: '公開済み', value: 'published' },
  { key: 'unpublished', text: '非公開', value: 'unpublished' },
  { key: 'deleted', text: '削除', value: 'deleted' },
  { key: 'filled', text: '入力済みフラグ', value: 'filled' },
  { key: 'unfilled', text: '未入力フラグ', value: 'unfilled' }
]

export default class ListHeader extends PureComponent {

  static propTypes = {
    buttonName: PropTypes.string.isRequired
  }

  static defaultProps = {
    defaultStatus: 'all'
  }

  static contextTypes = {
    router: PropTypes.object.isRequired
  }

  constructor(props, context) {
    super(props);
    this.state = {
      searchText: urlParamsToObject(context.router.route.location.search).search || ''
    }
    this.onSearch = this.onSearch.bind(this)
  }

  onChangeSearch = (e) => {
    this.setState({
      searchText: e.target.value
    })
  }

  onSearch = (param, value) => {
    const { router } = this.context
    let params = urlParamsToObject(router.route.location.search)
    if (param == 'status') {
      params.status = value
    } else if (param == 'search') {
      params.search = this.state.searchText
    }
    params.page_id = ''
    router.history.push({
      pathname: router.route.location.pathname,
      search: `?${objectToParameters(params)}`
    })
  }

  render() {
    const location = this.context.router.route.location
    const { searchText } = this.state
    const searchParams = urlParamsToObject(this.context.router.route.location.search)
    const { newPath, buttonName, hideAction, defaultStatus, hideFilter } = this.props

    return (
      <Segment>
        <Grid>
          <Grid.Column computer={7} mobile={16} style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center'}}>
            <Header style={{margin: 0}}>
              {this.props.title}
            </Header>
            {!hideFilter &&
              <Dropdown
                options={statuses}
                selection
                onChange={(e, data) => this.onSearch('status', data.value)}
                value={searchParams.status || defaultStatus}
              />
            }
          </Grid.Column>

          <Grid.Column computer={6} mobile={16}>
            <div className="ui small action input" style={{width: '100%'}}>
              <input
                type="text"
                placeholder="Search..."
                value={searchText}
                onChange={(e) => this.onChangeSearch(e)}
                onKeyPress={(e) => {if (e.key == 'Enter') { this.onSearch('search') } }}
              />
              <button className="ui icon button" role="button" onClick={() => this.onSearch('search')}><Icon name="search" /></button>
            </div>
          </Grid.Column>
          {!hideAction &&
            <Grid.Column computer={3} mobile={16} textAlign='right'>
              <Button as={Link} color='blue' size='small' to={newPath || `${location.pathname}/new`}>
                <Icon name='add' />
                {buttonName}
              </Button>
            </Grid.Column>
          }
        </Grid>
      </Segment>
    )
  }
}
