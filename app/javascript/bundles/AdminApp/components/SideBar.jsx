import React, { PropTypes } from 'react'
import { Link } from 'react-router-dom'
import { Sidebar, Segment, Button, Menu, Image, Icon, Header } from 'semantic-ui-react'
import { Redirect } from 'react-router-dom'

class SideBar extends React.PureComponent {

  render() {
    const { location, logout, isNotAdmin } = this.props

    return (
      <Sidebar as={Menu} animation='slide along' visible={true} inverted vertical style={{width: 260, backgroundColor: 'rgb(27, 28, 29)'}}>
        <Menu.Item className="item header ui" style={{display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
          Carriee Admin
          <a href='javascript:;' onClick={logout}>
            <Icon name='log out' size='large' />
          </a>
        </Menu.Item>
        <Menu.Item as={Link} to='/admin' active={location.pathname == '/admin' || location.pathname.indexOf('/admin/pages/') > -1}>
          Pages
        </Menu.Item>
        <Menu.Item as={Link} to='/admin/predefined-pages' active={location.pathname.indexOf('/admin/predefined-pages') > -1}>
          Predefined Pages
        </Menu.Item>
        <Menu.Item as={Link} to='/admin/hospitals' active={location.pathname.indexOf('/admin/hospitals') > -1}>
          Hospitals
        </Menu.Item>
        <Menu.Item as={Link} to='/admin/ctas' active={location.pathname.indexOf('/admin/ctas') > -1}>
          CTA
        </Menu.Item>
        <Menu.Item as={Link} to='/admin/categories' active={location.pathname.indexOf('/admin/categories') > -1}>
          Categories
        </Menu.Item>
        <Menu.Item as={Link} to='/admin/articles' active={location.pathname.indexOf('/admin/articles') > -1}>
          Articles
        </Menu.Item>
        <Menu.Item as={Link} to='/admin/css' active={location.pathname.indexOf('/admin/css') > -1}>
          CSS Customization
        </Menu.Item>
      </Sidebar>
    )
  }
}

export default SideBar
