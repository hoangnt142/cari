import React, { PureComponent } from 'react'
import { Form, Input, Label, Icon, Select } from 'semantic-ui-react'

import { websiteUrl } from 'lib/function'
import Editor from '../../../lib/form/Editor'
import EyeCatchImage from './EyeCatchImage'

export default class PageContent extends PureComponent {

  static defaultProps = {
    eyeCatchImage: false
  }

  constructor(props) {
    super(props);
    this.state = {
      copied: false
    }
    this.onCopy = this.onCopy.bind(this)
    this.getPageContent = this.getPageContent.bind(this)
  }

  onCopy = () => {
    let that = this
    that.urlInput.select()
    document.execCommand('copy')
    that.setState({ copied: true })
    setInterval(() => {
      that.setState({ copied: false })
    }, 5000)
  }

  getPageContent = (content, editor) => {
    eval(`this.page_${content} = editor`)
  }

  insertEyeCatchImage = (editor) => {
    const { page } = this.props
    const eyeCatchImage = page.image ? {src: page.image.preview, alt: page.image.alt || ''} : {}
    if (eyeCatchImage.src) {
      editor.insertContent(`<img class="eye-catch-image" style="display: block; margin-left: auto; margin-right: auto" alt="${eyeCatchImage.alt}" src=${eyeCatchImage.src} />`);
    } else {
      alert('Eye catch image is empty')
    }
  }

  render() {
    const {
      error,
      errors,
      page,
      forPageable,
      onChange,
      setEditorContent,
      eyeCatchImage,
      changed,
      ctas,
      basedUrl
    } = this.props
    const prefix = forPageable ? 'page.' : ''
    const urlReadOnly = this.props.urlReadOnly || page.predefined
    const showContentEditor = !page.use_content && !page.user_content_top && !page.use_content_bottom && !page.predefined // https://childfoot.slack.com/archives/GAQ1TPXB8/p1527140710000258

    const pageContents = ['content_top', 'content', 'content_bottom']
    const ctaSelections = ctas ? ctas.map(cta => (
      {key: cta.id, value: cta.id, text: cta.title}
    )) : []
    const url = page.url_slug || page.url || ''
    return (
      <React.Fragment>
        <Form.Field>
          <label>URL</label>
          <div className="ui action labeled input">
            <div className="ui label label">{websiteUrl(basedUrl)}</div>
            <input type="text" name={prefix+'url_slug'} value={url} onChange={onChange} disabled={urlReadOnly}/>
            <input
              style={{position: 'absolute', zIndex: '-1' }}
              ref={(urlInput) => this.urlInput = urlInput}
              type="text" value={websiteUrl(basedUrl) + url}
              onChange={onChange} />
            <button className="ui blue icon right labeled button" role="button" onClick={this.onCopy}>
              <i aria-hidden="true" className="copy icon"></i>
              {this.state.copied ? 'Copied' : 'Copy'} 
            </button>
          </div>
          {error && <Label basic color='red' pointing>{error}</Label>}
        </Form.Field>
        {eyeCatchImage &&
          <React.Fragment>
            <EyeCatchImage
              prefix={prefix}
              onChange={onChange}
              setEditorContent={setEditorContent}
              page={page}
              changed={changed}
            />
            {errors && errors['page.image.image'] && <Label basic color='red' pointing>{errors['page.image.image']}</Label>}
          </React.Fragment>
        }

        {pageContents.map((content, index) =>
          eval(`page.use_${content}`) &&
          <Editor
            key={index}
            name={content}
            label={content.split('_').join(' ')}
            value={eval(`page.${content}`) || ''}
            ref={(editor) => this.getPageContent(content, editor)}
          />
        )}
        {showContentEditor &&
          <React.Fragment>
            {['content_top', 'content'].map((content, i) =>
              <Editor
                key={i}
                name={content}
                label={content.split('_').join(' ')}
                insertEyeCatchImage={this.insertEyeCatchImage}
                value={eval(`page.${content}`) || ''}
                ref={(editor) => this.getPageContent(content, editor)}
              />
            )}
          </React.Fragment>
        }
        {ctas &&
          <Form.Field>
            <label style={{marginRight: '1rem', fontWeight: 'bold'}}>CTA</label>
            <Select
              value={page.cta_id || 0}
              name={prefix+'cta_id'}
              onChange={(e, data) => onChange({target: {name: data.name, value: data.value}})}
              options={[{key: 0, value: 0, text: '-- 選択して下さい --'}].concat(ctaSelections)}
            />
          </Form.Field>
        }
      </React.Fragment>
    )
  }
}
