import React, { PureComponent, PropTypes } from 'react'
import { Link } from 'react-router-dom'
import { Grid, Segment, Table, Header, Button, Menu, Dropdown, Input, Icon, Dimmer} from 'semantic-ui-react'
import { urlParamsToObject } from 'lib/function'
import { objectToParameters } from '../../App/lib/url'

import Pagination from './_Pagination'
import ListHeader from './_ListHeader'

export default class Articles extends PureComponent {

  constructor(props) {
    super(props);
    const { getArticles } = this.props

    let params = urlParamsToObject(props.location.search)
    if (!params.status) {
      params.status = 'published'
    }
    getArticles(`?${objectToParameters(params)}`)
  }

  onDelete = (articleId) => {
    if (confirm('本当に削除してもよろしいですか?')) {
      this.props.onDelete(articleId)
    }
  }

  render() {
    const { onDelete, location, history, adminApi } = this.props
    const { isNotAdmin, articles, totalPages, currentPage, fetching } = adminApi
    if (isNotAdmin) {
      return <Redirect to='/admin/login' />
    }
    return (
      <Grid columns='equal' textAlign='left' stackable>
        <Grid.Column width={16}>
          <div className='ui segments'>
            <ListHeader title='Article List' defaultStatus='published' buttonName='Add Article'/>
            <Dimmer.Dimmable as={Segment} blurring dimmed={fetching}>
              <Table celled>
                <Table.Header>
                  <Table.Row>
                    <Table.HeaderCell>名称</Table.HeaderCell>
                    <Table.HeaderCell>SEO description</Table.HeaderCell>
                    <Table.HeaderCell style={{width: '128px'}}>公開</Table.HeaderCell>
                    <Table.HeaderCell style={{width: '116px'}}>Robots</Table.HeaderCell>
                    <Table.HeaderCell textAlign='center' style={{width: '80px'}}>操作</Table.HeaderCell>
                  </Table.Row>
                </Table.Header>

                <Table.Body>
                  {articles.map(article => {
                    const page = article.page
                    return (
                      <Table.Row key={article.id}>
                        <Table.Cell>{page.title}</Table.Cell>
                        <Table.Cell>{page.seo_description}</Table.Cell>
                        <Table.Cell>{page.status}</Table.Cell>
                        <Table.Cell>
                          <div>INDEX: {page.seo_robot_noindex ? 'OFF' : 'ON'}</div>
                          <div>FOLLOW: {page.seo_robot_nofollow ? 'OFF' : 'ON'}</div>
                        </Table.Cell>
                        <Table.Cell>
                          <Link to={'/admin/articles/'+article.id}>
                            <Button color='blue' size='mini' style={{margin: 0}}>編集</Button>
                          </Link>
                          <Button color='red' onClick={ () => this.onDelete(article.id) } size='mini' style={{margin: '5px 0 0 0'}}>削除</Button>
                        </Table.Cell>
                      </Table.Row>
                    )}
                  )}
                </Table.Body>
              </Table>
            </Dimmer.Dimmable>
            {totalPages > 1 &&
              <div className='carrieePagination' style={{padding: 0, margin: '0.5rem 0px 1rem 0px'}}>
                <Pagination totalPages={totalPages} currentPage={currentPage} />
              </div>
            }
          </div>
        </Grid.Column>
      </Grid>
    )
  }
}
