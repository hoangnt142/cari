import React, { PureComponent } from 'react'
import ImageLibraryPopup from './_ImageLibraryPopup'

export default class ImageLibrary extends PureComponent {
  constructor(props) {
    super(props)
    this.props.getImages()
  }

  render() {
    const { popup } = this.props
    if (popup) {
      return(
        <ImageLibraryPopup {...this.props} />
      )
    }
  }
}