import React, { PureComponent } from 'react';
import { Button, Header, Image as SemanticImage, Modal, Grid, Form, Select, Input, Tab, Label, Responsive } from 'semantic-ui-react'

class ImageForm extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      image: props.image
    }
    this.onSubmit = this.onSubmit.bind(this)
  }

  onSubmit = () => {
    this.props.onSubmit(this.props.image)
  }

  handleNewImageChange(e) {
    e.preventDefault();
    let reader = new FileReader();
    let file = e.target.files[0];
    let img = new Image()
    reader.onloadend = () => {
      img.src = reader.result;
      let width, height;
      img.onload = function(){
        width = img.width;
        height = img.height
      };
      setTimeout(function(){
        const newImage = {
          width: width,
          height: height,
          title: this.props.image.title || file.name.split(".")[0],
          image: file
        }
        this.props.onSetNewImage(newImage)
      }.bind(this), 100);
    }
    reader.readAsDataURL(file)
  }

  render() {
    const { newAction, onCreate, onChange, errors } = this.props
    const image = this.props.image

    return (
      <Tab.Pane style={{padding: '10px'}}>
        <Form>
          <Form.Field>
            <label>タイトル</label>
            <Form.Input value={image.title || ''} name='title' onChange={(e, data) => onChange(data)}/>
          </Form.Field>
          <Form.Field>
            <label>キャプション</label>
            <Form.Input value={image.caption || ''} name='caption' onChange={(e, data) => onChange(data)}/>
          </Form.Field>
          <Form.Field>
            <label>ALT</label>
            <Form.Input value={image.alt || ''} name='alt' onChange={(e, data) => onChange(data)}/>
          </Form.Field>
          <Form.Field>
            <label>詳細</label>
            <Form.Input value={image.description || ''} name='description' onChange={(e, data) => onChange(data)}/>
          </Form.Field>
          <Form.Group widths='equal'>
            <Form.Field style={{width: '50%'}}>
              <label>Width</label>
              <Input placeholder='Width' type='number' name='width' value={image.width || ''} onChange={(e, data) => onChange(data)}/>
              {newAction && errors.width && <Label basic color='red' pointing>{errors.width[0]}</Label>}
            </Form.Field>
            <Form.Field style={{width: '50%'}}>
              <label>Height</label>
              <Input placeholder='Height' type='number' name='height' value={image.height || ''} onChange={(e, data) => onChange(data)}/>
              {newAction && errors.height && <Label basic color='red' pointing>{errors.height[0]}</Label>}
            </Form.Field>
          </Form.Group>

          {!newAction &&
            <React.Fragment>
              <Form.Field>
                <label>リンク先</label>
                <Form.Input value={image.linkTo || ''} name='linkTo' onChange={(e, data) => onChange(data)}/>
              </Form.Field>
              <Form.Field>
                <label>配置</label>
                <Select
                  value={image.align || 'none'}
                  name='align'
                  options={[
                    {key: 0, value: 'none', text: '無し'},
                    {key: 1, value: 'left', text: '右'},
                    {key: 2, value: 'right', text: '左'},
                    {key: 3, value: 'center', text: '中央'}
                  ]}
                  onChange={(e, data) => onChange(data)}
                />
              </Form.Field>
            </React.Fragment>
          }
          {newAction &&
            <React.Fragment>
              <Form.Field>
                <label>イメージ選択</label>
                <Input type='file' onChange={ (e) => this.handleNewImageChange(e) } />
                {errors.image && <Label basic color='red' pointing>{errors.image[0]}</Label>}
              </Form.Field>
              <Button type='submit' color='blue' onClick={this.onSubmit} >アップロード</Button>
            </React.Fragment>
          }
        </Form>
      </Tab.Pane>
    )
  }
}

export default class EditorLibraryPopup extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      activeTabIndex: 0,
      selectedImage: {},
      newImage: {}
    }
    this.onSelect = this.onSelect.bind(this)
  }

  onSelect = (image) => {
    this.setState({
      selectedImage: image,
      activeTabIndex: 0
    })
  }

  onChangeSelectedImage = (data) => {
    let selectedImage = this.state.selectedImage
    eval(`selectedImage.${data.name} = data.value`)
    this.setState({
      selectedImage: selectedImage,
      updated: !this.state.updated
    })
  }

  onChangeNewImage = (data) => {
    let newImage = this.state.newImage
    eval(`newImage.${data.name} = data.value`)
    this.setState({
      newImage: newImage,
      updated: !this.state.updated
    })
  }

  onSetNewImage = (newImage) => {
    this.setState({
      newImage: newImage,
      updated: !this.state.updated
    })
  }

  handleChangeTab = (e, data) => {
    this.setState({
      activeTabIndex: data.activeIndex
    })
  }

  componentWillReceiveProps(nextProps) { // Select the image that has been uploaded
    const submmitedAt = this.props.submmitedAt
    const nextsubmmitedAt = nextProps.submmitedAt
    if (submmitedAt != nextsubmmitedAt) {
      this.onSelect(nextProps.images[0])
    }
  }

  onSearch = () => {
    this.onSelect({})
    this.props.getImages('?search='+this.searchInput.value)
  }

  render() {
    const { activeTabIndex, selectedImage, newImage } = this.state
    const { isOpen, insertImage, handleOpenLibrary, images } = this.props

    const panes = (props) => {
      return [
        { menuItem: '選択', render: () => <ImageForm newAction={false} {...props} image={selectedImage} onChange={this.onChangeSelectedImage} updated={this.state.updated}/> },
        { menuItem: 'アップロード', render: () => <ImageForm newAction={true} {...props} image={newImage} onChange={this.onChangeNewImage} onSetNewImage={this.onSetNewImage} updated={this.state.updated}/> }
      ]
    }

    const insertImageToEditor = () => {
      insertImage(selectedImage)
      this.props.onSubmit(selectedImage) // Save image info
    }

    return(
      <Modal open={isOpen} size='fullscreen'>
        <Modal.Header style={{padding: 0}}>
          <Grid verticalAlign='middle' padded >
            <Grid.Column computer={10} mobile={16}>
              Select a Photo
            </Grid.Column>
            <Grid.Column computer={6} mobile={16}>
              <div className="ui mini icon input" style={{width: '100%'}}>
                <input type="text" placeholder="Search..." ref={(input) => this.searchInput = input} onKeyPress={(e) => { if (e.key == 'Enter') { this.onSearch() } }}/>
                <i aria-hidden="true" className="search circular link icon" onClick={() => this.onSearch()}></i>
              </div>
            </Grid.Column>
          </Grid>
        </Modal.Header>
        <Modal.Content style={{height: window.innerHeight-200, padding: '10px'}}>
          <Grid className='imageLibrary' padded>
            <Grid.Column computer={12} mobile={16} style={{height: window.innerHeight-220, overflowY: 'scroll', paddingBottom: '20px'}}>
              <Grid>
                {images && images.length == 0 &&
                  <Grid.Column width={16} textAlign='center'>
                    <span className='ui text red'>There is 0 image</span>
                  </Grid.Column>
                }
                {images && images.map((image, i) =>
                  <Grid.Column computer={4} mobile={8} key={image.id} style={{padding: '0 10px 10px 0'}}>
                    <SemanticImage
                      src={image.image_url.thumb}
                      style={selectedImage.id == image.id ? {border: '2px solid red', width: '100%'} : {border: '2px solid #ccc', width: '100%'}}
                      onClick={() => this.onSelect(image)} />
                  </Grid.Column>
                )}
              </Grid>
            </Grid.Column>
            <Grid.Column computer={4} mobile={16} style={{height: window.innerHeight-220, overflowY: 'scroll', padding: '0 0 10px 10px'}}>
              <Tab panes={panes(this.props)} activeIndex={activeTabIndex} onTabChange={this.handleChangeTab}/>
            </Grid.Column>
          </Grid>
        </Modal.Content>
        <Modal.Actions>
          <Button color='blue' disabled={selectedImage.id == undefined} onClick={() => insertImageToEditor()}>Insert</Button>
          <Button color='red' onClick={() => handleOpenLibrary()}>Cancel</Button>
        </Modal.Actions>
      </Modal>
    )
  }
}