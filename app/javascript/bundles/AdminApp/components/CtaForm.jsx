import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'
import { Grid, Segment, Header, Button, Form, Input, Label } from 'semantic-ui-react'

import Editor from '../../../lib/form/Editor'
import PageCommonForm from './_PageCommonForm'

export default class CtaForm extends Component {

  constructor(props) {
    super(props);
    this.action = { new: props.match.params.id == 'new' }

    const { getCta } = this.props
    if (!this.action.new) {
      getCta(props.match.params.id)
    }

    this.onChange = this.onChange.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
    this.setEditorContent = this.setEditorContent.bind(this)
  }

  onChange = (e) => {
    const field = e.target.name
    const value = e.target.value
    this.props.onChange(field, value)
  }

  onSubmit = () => {
    this.setEditorContent()
    this.props.onSubmit(this.props.cta)
  }

  setEditorContent() {
    this.props.onChange('content', this.content.getContent())
  }

  componentWillUnmount() {
    const { reset } = this.props
    reset()
  }

  render() {
    const { cta, errors, changed } = this.props

    if (!cta.id && !this.action.new)  {
      return null
    }

    if (this.action.new && cta.id) {
      return <Redirect to={`/admin/ctas/${cta.id}`} />
    }

    return (
      <Form>
        <Grid columns='equal' textAlign='left' stackable>
          <Grid.Row>
            <Grid.Column width={16}>
              <div className='ui segments'>
                <PageCommonForm
                  onlySubmit
                  page={cta}
                  action={this.action}
                  onSubmit={this.onSubmit}
                  onChange={this.onChange}
                  title={cta.title || 'New CTA'}
                />
                <Segment>
                  <div className='ui segments input-group'>
                    <Segment>
                      <Header>CTA Contents</Header>
                    </Segment>
                    <Segment>
                      <Form.Field>
                        <label>Title</label>
                        <Form.Input value={cta.title || ''} name='title' onChange={this.onChange} error={errors.title !== undefined} />
                        {errors.title && <Label basic color='red' pointing>{errors.title}</Label>}
                      </Form.Field>
                      <Form.Field>
                        <Editor
                          name='content'
                          label='Content'
                          value={cta.content}
                          ref={(editor) => this.content = editor}
                        />
                        {errors.content && <Label basic color='red' pointing>{errors.content}</Label>}
                      </Form.Field>
                    </Segment>
                  </div>
                </Segment>
              </div>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Form>
    )
  }
}
