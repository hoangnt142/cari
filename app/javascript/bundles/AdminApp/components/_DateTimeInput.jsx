import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import DatePicker from 'react-datepicker'
import 'moment/locale/ja'
import moment from 'moment'
import 'react-datepicker/src/stylesheets/datepicker-cssmodules'

class DatetimeInput extends PureComponent {

  static propTypes = {
    inline: PropTypes.bool
  }

  render() {
    const { label, inline, name, disabled, onChange, value} = this.props

    const style = inline ? {display: 'flex', alignItems: 'center', justifyContent: 'flex-end'} : {marginBottom: '1rem'} 

    return (
      <div style={style}>
        {label && <label style={{marginRight: '1rem', fontWeight: 'bold'}}>{this.props.label}</label>}
        <DatePicker
          dateFormat="YYYY年MM月DD日"
          selected={moment(value)}
          minDate={moment()}
          onChange={(value) => !disabled ? onChange({target: {name: name, value: value}}) : null}
          shouldCloseOnSelect={true}/>
      </div>
    )
  }
}

export default DatetimeInput;
