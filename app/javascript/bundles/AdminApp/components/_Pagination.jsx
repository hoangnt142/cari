import React, { PureComponent } from 'react'
import { Icon, Pagination, Responsive } from 'semantic-ui-react'
import { urlParamsToObject } from 'lib/function'
import { objectToParameters } from '../../App/lib/url'
import PropType from 'prop-types'

export default class CarrieePagination extends PureComponent {

  state = { isBreak: window.innerWidth < 991 }

  constructor(props, context) {
    super(props)
    this.onPageChange = this.onPageChange.bind(this)
  }

  handleOnUpdate = () => {
    let windowWidth = window.innerWidth
    this.setState({
      isBreak: windowWidth < 991
    })
  }

  onPageChange = (data) => {
    const { router } = this.context
    let params = urlParamsToObject(router.route.location.search)
    params.page_id = data.activePage
    router.history.push({
      pathname: router.route.location.pathname,
      search: `?${objectToParameters(params)}`
    })
  }

  render() {
    const { totalPages, currentPage} = this.props
    const { isBreak } = this.state

    const {firstItem, lastItem, prevItem, nextItem} = isBreak ?
      {
        firstItem: <Icon name='angle double left' size='large' style={{margin: 0}} />,
        lastItem: <Icon name='angle double right' size='large' style={{margin: 0}} />,
        prevItem: <Icon name='angle left' size='large' style={{margin: 0}} />,
        nextItem: <Icon name='angle right' size='large' style={{margin: 0}} />
      }
      :
      { firstItem: <div className='ui text blue'><Icon name='chevron left' style={{margin: 0}}/>最初</div>,
        lastItem: <div className='ui text blue'>最後<Icon name='chevron right' style={{margin: 0}}/></div>,
        prevItem: <div><Icon name='chevron left' style={{margin: 0}}/>前</div>,
        nextItem: <div>次<Icon name='chevron right' style={{margin: 0}}/></div>
      }

    return (
      <Responsive
        onUpdate={this.handleOnUpdate}
        as={Pagination}
        onPageChange={(e, data) => this.onPageChange(data)}
        activePage={currentPage}
        ellipsisItem={{ content: <Icon name='ellipsis horizontal' />, icon: true }}
        firstItem={{content: firstItem }}
        lastItem={{content: lastItem }}
        prevItem={{content: prevItem }}
        nextItem={{content: nextItem }}
        totalPages={totalPages}
      />
    )
  }
}

CarrieePagination.contextTypes = {
  router: PropType.object.isRequired,
}