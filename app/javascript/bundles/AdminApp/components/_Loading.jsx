import React, { PureComponent } from 'react'
import { Loader, Dimmer, Segment} from 'semantic-ui-react'

export default class Loading extends PureComponent {
  render() {
    return (
      <Segment style={{ height: '100%' }}>
        <Dimmer active inverted>
          <Loader />
        </Dimmer>
      </Segment>
    )
  }
}
