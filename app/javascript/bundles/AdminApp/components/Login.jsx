import React, { PropTypes } from 'react'

import { Redirect } from 'react-router-dom'
import { Container, Grid, Header, Form, Segment, Icon, Button, Message } from 'semantic-ui-react'

class Login extends React.PureComponent {

  constructor(props) {
    super(props);
    const { reset } = this.props
    reset()
  }

  state = {
    email: '',
    password: '',
    redirectToReferrer: false
  }

  onChange = (e) => {
    const field = e.target.name
    const value = e.target.value
    eval(`this.setState({
      ${field}: value
    })`)
  }

  componentWillMount() {
    if (!this.props.adminApi.isNotAdmin) {
      this.setState({
        redirectToReferrer: !this.state.redirectToReferrer
      })
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.admin.email) {
      this.setState({
        redirectToReferrer: !this.state.redirectToReferrer
      })
    }
  }

  render() {
    const { email, password } = this.state
    const { redirectToReferrer } = this.state
    const { from } = this.props.location.state || { from: { pathname: "/admin" } };
    if (redirectToReferrer) {
      return <Redirect to={from} />
    }

    return (
      <Grid centered verticalAlign='middle' style={{height: '100%', backgroundColor: '#eee', position: 'relative', zIndex: 1 }}>
        <Grid.Column style={{maxWidth: '450px', backgroundColor: '#fff', marginTop: '-200px'}}>
          <Header as='h2' color='blue' textAlign='center'>
            Carriee Admin Login
          </Header>
          <Form size='large'>
            <Segment basic>
              <Form.Field>
                <Form.Input icon='user' iconPosition='left' placeholder='E-mail address' name='email' value={email} onChange={(e) => this.onChange(e)} />
              </Form.Field>
              <Form.Field>
                <Form.Input icon='lock' type='password' iconPosition='left' placeholder='Password' name='password' value={password} onChange={(e) => this.onChange(e)} />
              </Form.Field>
              {this.props.loggedInFailed && <Message color='red'>Wrong email or password</Message>}
              <Form.Field>
                <Button size='large' fluid color='blue' onClick={() => this.props.login(email, password)}>Login</Button>
              </Form.Field>
            </Segment>
          </Form>
        </Grid.Column>
      </Grid>
    )
  }
}

export default Login
