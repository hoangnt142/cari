import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'
import { Grid, Segment, Table, Header, Button, Form, Input, TextArea, Radio, Message, Icon, Label, Select } from 'semantic-ui-react'

import SeoSetting from './_SeoSetting'
import PageContent from './_PageContent'
import PageCommonForm from './_PageCommonForm'
import Loading from './_Loading'

export default class HospitalForm extends Component {

  constructor(props) {
    super(props);
    this.action = { new: props.match.params.id == 'new' }

    const { getHospital } = this.props
    if (!this.action.new) {
      getHospital(props.match.params.id)
    }

    this.onChange = this.onChange.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
  }

  onChange = (e) => {
    const field = e.target.name
    const value = e.target.value
    this.props.onChange(field, value)
  }

  onSubmit = () => {
    const that = this
    const page = this.props.hospital.page;
    ['content', 'content_top', 'content_bottom'].map(content => {
      if (eval(`page.use_${content}`)) {
        that.props.onChange('page.'+content, eval(`that.pageContents.page_${content}.getContent()`))
      }
    })
    this.props.onSubmit(this.props.hospital)
  }

  componentWillUnmount() {
    const { reset } = this.props
    reset()
  }

  render() {
    const { hospital, errors, changed, ctas } = this.props
    const page = hospital.page

    if (!page.id) {
      return <Loading />
    }

    return (
      <Form>
        <Grid columns='equal' textAlign='left' stackable>
          <Grid.Row>
            <Grid.Column width={16}>
              <div className='ui segments'>
                <PageCommonForm
                  forPageable
                  page={page}
                  action={this.action}
                  onChange={this.onChange}
                  onSubmit={this.onSubmit}
                  title={hospital.name}
                  enablePageDate={false}
                />
                <Segment>
                  <div className='ui segments input-group'>
                    <Segment>
                      <Header>Hospital Contents</Header>
                    </Segment>
                    <Segment>
                      <Form.Field>
                        <label>Name</label>
                        <Form.Input value={hospital.name || ''} name='name' onChange={this.onChange} error={errors.name !== undefined} />
                        {errors.name && <Label basic color='red' pointing>{errors.name}</Label>}
                      </Form.Field>
                      <PageContent
                        urlReadOnly={true}
                        forPageable
                        ref={(pageContents) => this.pageContents = pageContents}
                        error={errors.url}
                        page={page}
                        onChange={this.onChange}
                      />
                    </Segment>
                  </div>
                  <SeoSetting page={page} onChange={this.onChange} forPageable/>
                </Segment>
              </div>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Form>
    )
  }
}
