import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'
import { Grid, Segment, Header, Form, Label } from 'semantic-ui-react'
import {UnControlled as CodeMirror} from 'react-codemirror2'
import PageCommonForm from './_PageCommonForm'

export default class CssForm extends Component {

  constructor(props) {
    super(props);
    this.action = { new: false }
    const { getSystemConfig } = this.props
    getSystemConfig()
    this.cssContent = ''
  }

  onSubmit = () => {
    this.props.onChange('css_custom', this.cssContent)
    this.props.onSubmit(this.props.systemConfig)
  }

  closeNotice = () => {
    this.props.onChange('alertSuccess', false)
  }

  render() {
    const { systemConfig, errors, changed, adminApi } = this.props
    const { isNotAdmin } = adminApi
    if (isNotAdmin) {
      return <Redirect to='/admin/login' />
    }
    if (!systemConfig) {
      return null
    }
    return (
      <Form>
        <Grid columns='equal' textAlign='left' stackable>
          <Grid.Row>
            <Grid.Column width={16}>
              <div className='ui segments'>
                <PageCommonForm
                  onlySubmit
                  page={systemConfig}
                  action={this.action}
                  onSubmit={this.onSubmit}
                  onChange={this.closeNotice}
                  title={'CSS customization'}
                />

                  <Segment>
                    <CodeMirror
                      value={systemConfig.css_custom}
                      options={{
                        mode: 'xml',
                        theme: 'material',
                        lineNumbers: true
                      }}
                      onChange={(editor, data, value) => {
                        this.cssContent = value
                      }}
                    />
                    {systemConfig.error && <label className='ui text red'>{systemConfig.error}</label>}
                  </Segment>

              </div>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Form>
    )
  }
}
