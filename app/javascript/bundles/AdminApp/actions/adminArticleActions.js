export const actions = {
  GET_ARTICLE: 'GET_ARTICLE',
  ARTICLE_ON_CHANGE: 'ARTICLE_ON_CHANGE',
  ARTICLE_ON_CHANGE_PAGE: 'ARTICLE_ON_CHANGE_PAGE',
  ARTICLE_SUBMITTED: 'ARTICLE_SUBMITTED',
  ARTICLE_RESET: 'ARTICLE_RESET',
  ARTICLE_SUBMIT_FAILED: 'ARTICLE_SUBMIT_FAILED'
}

import { apiGet } from './adminApiActions'

export const reset = () => {
  return {
    type: actions.ARTICLE_RESET
  }
}

const parseArticle = (article) => {
  if (!article.page || !article.page.image) {
    article = Object.assign({},
      article,
      {page: Object.assign({}, article.page,
        {
          image: {},
        }
      )}
    )
  }
  if (article.alertSuccess) {
    article = Object.assign({},
      article,
      {page: Object.assign({}, article.page,
        {
          alertSuccess: article.alertSuccess,
        }
      )}
    )
  }
  return article
}

export const onSubmit = (article) => {
  const data = { article: article }
  const url = article.id ? '/api/admin/articles/'+article.id : '/api/admin/articles'

  const params = {
    method: article.id ? 'PUT' : 'POST',
    mode: 'same-origin',
    credentials: 'same-origin',
    headers: new Headers({
      "Content-Type": "application/json"
    }),
    body: JSON.stringify(data)
  }

  return dispatch => {
    return fetch(url, params)
      .then(response => {
        response.json()
          .then(json => {
            const errors = json.errors || {}
            if (Object.keys(errors).length > 0) {
              dispatch({
                type: actions.ARTICLE_SUBMIT_FAILED,
                errors
              })
            } else {
              const article = parseArticle(json.article)
              dispatch({
                type: actions.ARTICLE_SUBMITTED,
                article
              })
            }
          })
          .catch(error => {
            console.error(error)
          })
        
      })
      .catch(error => {
        console.error(error)
      })
  }
}

export const onChange = (name, value) => (dispatch, getState) => {
  dispatch({
    type: actions.ARTICLE_ON_CHANGE,
    name,
    value
  })
}

export const getArticles = (query) => (dispatch, getState) => {
  dispatch(apiGet(`/api/admin/articles${query}`)).then(() => {
  })
}

export const getArticle = (article_id) => (dispatch, getState) => {
  dispatch(apiGet(`/api/admin/articles/${article_id}`)).then(() => {
    const article = parseArticle(getState().adminApi.article)
    dispatch({
      type: actions.GET_ARTICLE,
      article: article
    })
  })
}