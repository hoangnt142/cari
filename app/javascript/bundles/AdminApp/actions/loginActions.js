export const actions = {
  ADMIN_LOGGED_IN: "ADMIN_LOGGED_IN",
  ADMIN_LOGGED_OUT: "ADMIN_LOGGED_OUT",
  ADMIN_LOGGED_IN_FAILED: "ADMIN_LOGGED_IN_FAILED"
}

import { apiGet } from './adminApiActions'

const loggedIn = (admin) => {
  return {
    type: actions.ADMIN_LOGGED_IN,
    admin
  }
}

const loggedInFailed = () => {
  return {
    type: actions.ADMIN_LOGGED_IN_FAILED,
  }
}

export const logout = () => (dispatch, getState) => {
  dispatch(apiGet('/api/admin/sign-out')).then(() => {
    dispatch({
      type: actions.ADMIN_LOGGED_OUT
    })
  })
}

export const login = (email, password) => {
  const headers = new Headers({
    "Content-Type": "application/json"
  })
  const data = { user: { email, password } }
  const params = {
    method: 'POST',
    mode: 'same-origin',
    credentials: 'same-origin',
    headers: headers,
    body: JSON.stringify(data)
  }
  const url = '/api/admin/sign-in'
  return dispatch => {
    return fetch(url, params)
      .then(response => {
        if(!response.ok) {
          dispatch(loggedInFailed())
        }
        else {
          response.json()
                  .then(json => {
                    console.log('json', json)
                    if(json.user) {
                      dispatch(loggedIn(json.user))
                    }
                    else {
                      dispatch(loggedInFailed())
                    }
                  })
                  .catch(error => {
                    console.error(error)
                    dispatch(loggedInFailed())
                  })
        }
      })
      .catch(error => {
        console.error(error)
        dispatch(loggedInFailed())
      })
  }
}