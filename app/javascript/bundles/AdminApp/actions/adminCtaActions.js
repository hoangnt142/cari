export const actions = {
  GET_CTA: 'GET_CTA',
  CTA_ON_CHANGE: 'CTA_ON_CHANGE',
  CTA_ON_CHANGE_PAGE: 'CTA_ON_CHANGE_PAGE',
  CTA_SUBMITTED: 'CTA_SUBMITTED',
  CTA_RESET: 'CTA_RESET',
  CTA_SUBMIT_FAILED: 'CTA_SUBMIT_FAILED'
}

import { apiGet } from './adminApiActions'
import { actions as apiActions } from './adminApiActions'

export const reset = () => {
  return {
    type: actions.CTA_RESET
  }
}

export const onDelete = (ctaId) => {
  const url = '/api/admin/ctas/'+ctaId

  const params = {
    method: 'DELETE',
    mode: 'same-origin',
    credentials: 'same-origin',
    headers: new Headers({
      "Content-Type": "application/json"
    })
  }

  return dispatch => {
    return fetch(url, params)
      .then(response => {
        response.json()
          .then(json => {
            dispatch({
              type: apiActions.API_RECEIVED,
              json
            })
          })
      })
  }
}

export const onSubmit = (cta) => {
  const data = { cta: cta }
  const url = cta.id ? '/api/admin/ctas/'+cta.id : '/api/admin/ctas'

  const params = {
    method: cta.id ? 'PUT' : 'POST',
    mode: 'same-origin',
    credentials: 'same-origin',
    headers: new Headers({
      "Content-Type": "application/json"
    }),
    body: JSON.stringify(data)
  }
  return dispatch => {
    return fetch(url, params)
      .then(response => {
        response.json()
          .then(json => {
            const errors = json.errors || {}
            if (Object.keys(errors).length > 0) {
              dispatch({
                type: actions.CTA_SUBMIT_FAILED,
                errors
              })
            } else {
              let cta = json.model
              dispatch({
                type: actions.CTA_SUBMITTED,
                cta: cta
              })
            }
          })
          .catch(error => {
            console.error(error)
          })
        
      })
      .catch(error => {
        console.error(error)
      })
  }
}

export const onChange = (name, value) => (dispatch, getState) => {
  dispatch({
    type: actions.CTA_ON_CHANGE,
    name,
    value
  })
}

export const getCtas = () => (dispatch, getState) => {
  dispatch(apiGet('/api/admin/ctas')).then(() => {
  })
}

export const getCta = (cta_id) => (dispatch, getState) => {
  dispatch(apiGet(`/api/admin/ctas/${cta_id}`)).then(() => {
    dispatch({
      type: actions.GET_CTA,
      cta: getState().adminApi.model
    })
  })
}