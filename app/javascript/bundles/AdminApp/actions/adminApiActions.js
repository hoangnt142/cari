export const actions = {
  API_RECEIVED: 'API_RECEIVED',
  API_FAILED: 'API_FAILED',
  API_RESET: 'API_RESET',
  API_REQUEST_BEGIN: 'API_REQUEST_BEGIN',
  API_REQUEST_END: 'API_REQUEST_END'
}

export const reset = () => {
  return {
    type: actions.API_RESET
  }
}

export const receive = (json) => (dispatch, getState) => {
  dispatch({
    type: actions.API_RECEIVED,
    json
  })
  dispatch(apiEnd())
}

export const requestFailed = (url) => (dispatch, getState) => {
  dispatch(apiEnd())
  dispatch({
    type: actions.API_FAILED,
    url
  })
}

export const resetSearchResult = () => ({
  type: RESET_SEARCH_RESULT
})

const apiBegin = () => {
  return {
    type: actions.API_REQUEST_BEGIN
  }
}

const apiEnd = () => {
  return {
    type: actions.API_REQUEST_END
  }
}

export const apiGet = (url) => {
  const defaultInit = {
    method: 'GET',
    mode: 'same-origin',
    credentials: 'same-origin'
  }
  const mergedInit = Object.assign({}, defaultInit)
  return dispatch => {
    dispatch(apiBegin())
    return fetch(url, mergedInit)
      .then(response => {
        if(!response.ok) {
          throw `Fetch was failed: ${response.code}`
        }
        return response.json()
                       .then(json => {
                         console.log('json', json)
                         dispatch(receive(json))
                       })
                       .catch(error => {
                         console.error(error)
                         dispatch(requestFailed(url))
                       })
      })
      .catch(error => {
        console.error(error)
        dispatch(requestFailed(url))
      })
  }
}
