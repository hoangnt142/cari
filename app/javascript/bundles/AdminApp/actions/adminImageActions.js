export const actions = {
  IMAGE_SUBMITTED: 'IMAGE_SUBMITTED',
  IMAGE_SUBMIT_FAILED: 'IMAGE_SUBMIT_FAILED'
}

import { actions as apiActions } from './adminApiActions'

import { apiGet } from './adminApiActions'

export const onChange = (name, value) => (dispatch, getState) => {
  dispatch({
    type: actions.IMAGE_ON_CHANGE,
    name,
    value
  })
}

export const onSubmit = (image) => {
  const url = image.id ? '/api/admin/images/'+image.id : '/api/admin/images'
  let formData = new FormData();
  for (var k in image) {
    if (image.hasOwnProperty(k)) {
      formData.append(k, image[k])
    }
  }
  const params = {
    method: image.id ? 'PUT' : 'POST',
    mode: 'same-origin',
    credentials: 'same-origin',
    body: formData
  }

  return dispatch => {
    return fetch(url, params)
      .then(response => {
        response.json()
          .then(json => {
            const errors = json.errors || {}
            if (Object.keys(errors).length > 0) {
              dispatch({
                type: actions.IMAGE_SUBMIT_FAILED,
                errors
              })
            } else {
              dispatch({
                type: apiActions.API_RECEIVED,
                json
              })
              dispatch({
                type: actions.IMAGE_SUBMITTED
              })
            }
          })
          .catch(error => {
            console.error(error)
          })
        
      })
      .catch(error => {
        console.error(error)
      })
  }
}

export const getImages = (query='') => (dispatch, getState) => {
  dispatch(apiGet(`/api/admin/images${query}`)).then(() => {
  })
}