export const actions = {
  GET_SYSTEM_CONFIG: 'GET_SYSTEM_CONFIG',
  SYSTEM_CONFIG_ON_CHANGE: 'SYSTEM_CONFIG_ON_CHANGE',
  SYSTEM_CONFIG_SUBMITTED: 'SYSTEM_CONFIG_SUBMITTED',
  SYSTEM_CONFIG_RESET: 'SYSTEM_CONFIG_RESET',
  SYSTEM_CONFIG_SUBMIT_FAILED: 'SYSTEM_CONFIG_SUBMIT_FAILED'
}

import { apiGet } from './adminApiActions'

export const reset = () => {
  return {
    type: actions.SYSTEM_CONFIG_RESET
  }
}

export const onSubmit = (system_config) => {
  const data = { system_config: system_config }
  const url = '/api/admin/system_configs'

  const params = {
    method: 'POST',
    mode: 'same-origin',
    credentials: 'same-origin',
    headers: new Headers({
      "Content-Type": "application/json"
    }),
    body: JSON.stringify(data)
  }

  return dispatch => {
    return fetch(url, params)
      .then(response => {
        response.json()
          .then(json => {
            const errors = json.errors || {}
            const systemConfig = json.systemConfig
            if (Object.keys(errors).length > 0) {
              dispatch({
                type: actions.SYSTEM_CONFIG_SUBMIT_FAILED,
                errors
              })
            } else {
              dispatch({
                type: actions.SYSTEM_CONFIG_SUBMITTED,
                systemConfig
              })
            }
          })
          .catch(error => {
            console.error(error)
          })
        
      })
      .catch(error => {
        console.error(error)
      })
  }
}

export const onChange = (name, value) => (dispatch, getState) => {
  dispatch({
    type: actions.SYSTEM_CONFIG_ON_CHANGE,
    name,
    value
  })
}

export const getSystemConfig = () => (dispatch, getState) => {
  dispatch(apiGet('/api/admin/system_configs/1')).then(() => {
    dispatch({
      type: actions.GET_SYSTEM_CONFIG,
      systemConfig: getState().adminApi.systemConfig
    })
  })
}