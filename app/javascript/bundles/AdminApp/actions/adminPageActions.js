export const actions = {
  GET_PAGE: 'GET_PAGE',
  ON_CHANGE: 'ON_CHANGE',
  SUBMITTED: 'SUBMITTED',
  PAGE_RESET: 'PAGE_RESET',
  SUBMIT_FAILED: 'SUBMIT_FAILED'
}
import { apiGet } from './adminApiActions'
import { actions as apiActions } from './adminApiActions'
export const reset = () => {
  return {
    type: actions.PAGE_RESET
  }
}

export const onDelete = (pageId) => {
  const url = '/api/admin/pages/'+pageId

  const params = {
    method: 'DELETE',
    mode: 'same-origin',
    credentials: 'same-origin',
    headers: new Headers({
      "Content-Type": "application/json"
    })
  }

  return dispatch => {
    return fetch(url, params)
      .then(response => {
        response.json()
          .then(json => {
            dispatch({
              type: apiActions.API_RECEIVED,
              json
            })
          })
      })
  }
}

export const onSubmit = (page) => {
  const data = { page: page }
  const url = page.id ? '/api/admin/pages/'+page.id : '/api/admin/pages'

  const params = {
    method: page.id ? 'PUT' : 'POST',
    mode: 'same-origin',
    credentials: 'same-origin',
    headers: new Headers({
      "Content-Type": "application/json"
    }),
    body: JSON.stringify(data)
  }

  return dispatch => {
    return fetch(url, params)
      .then(response => {
        response.json()
          .then(json => {
            const errors = json.errors
            let page = json.page
            if (!page.image) {
              page = Object.assign({}, page, {image: {}})
            }
            if (Object.keys(errors).length > 0) {
              dispatch({
                type: actions.SUBMIT_FAILED,
                errors
              })
            } else {
              dispatch({
                type: actions.SUBMITTED,
                page
              })
            }
          })
          .catch(error => {
            console.error(error)
          })
      })
      .catch(error => {
        console.error(error)
      })
  }
}

export const onChange = (name, value) => (dispatch, getState) => {
  dispatch({
    type: actions.ON_CHANGE,
    name,
    value
  })
}

export const getPages = (query) => (dispatch, getState) => {
  dispatch(apiGet(`/api/admin/pages${query}`)).then(() => {
  })
}

export const getPage = (page_id) => (dispatch, getState) => {
  dispatch(apiGet(`/api/admin/pages/${page_id}`)).then(() => {
    let page = getState().adminApi.page
    if (!page.image) {
      page = Object.assign({}, page, {image: {}})
    }
    dispatch({
      type: actions.GET_PAGE,
      page: page
    })
  })
}
