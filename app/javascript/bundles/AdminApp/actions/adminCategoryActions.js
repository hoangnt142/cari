export const actions = {
  GET_CATEGORY: 'GET_CATEGORY',
  CATEGORY_ON_CHANGE: 'CATEGORY_ON_CHANGE',
  CATEGORY_ON_CHANGE_PAGE: 'CATEGORY_ON_CHANGE_PAGE',
  CATEGORY_SUBMITTED: 'CATEGORY_SUBMITTED',
  CATEGORY_RESET: 'CATEGORY_RESET',
  CATEGORY_SUBMIT_FAILED: 'CATEGORY_SUBMIT_FAILED'
}

import { apiGet } from './adminApiActions'

export const reset = () => {
  return {
    type: actions.CATEGORY_RESET
  }
}

const parseCategory = (category) => {
  if (!category.page || !category.page.image) {
    category = Object.assign({},
      category,
      {page: Object.assign({}, category.page,
        {
          image: {},
        }
      )}
    )
  }
  if (category.alertSuccess) {
    category = Object.assign({},
      category,
      {page: Object.assign({}, category.page,
        {
          alertSuccess: category.alertSuccess,
        }
      )}
    )
  }
  return category
}

export const onSubmit = (category) => {
  const data = { category: category }
  const url = category.id ? '/api/admin/categories/'+category.id : '/api/admin/categories'

  const params = {
    method: category.id ? 'PUT' : 'POST',
    mode: 'same-origin',
    credentials: 'same-origin',
    headers: new Headers({
      "Content-Type": "application/json"
    }),
    body: JSON.stringify(data)
  }

  return dispatch => {
    return fetch(url, params)
      .then(response => {
        response.json()
          .then(json => {
            const errors = json.errors || {}
            if (Object.keys(errors).length > 0) {
              dispatch({
                type: actions.CATEGORY_SUBMIT_FAILED,
                errors
              })
            } else {
              const category = parseCategory(json.category)
              dispatch({
                type: actions.CATEGORY_SUBMITTED,
                category
              })
            }
          })
          .catch(error => {
            console.error(error)
          })
        
      })
      .catch(error => {
        console.error(error)
      })
  }
}

export const onChange = (name, value) => (dispatch, getState) => {
  dispatch({
    type: actions.CATEGORY_ON_CHANGE,
    name,
    value
  })
}

export const getCategories = (query) => (dispatch, getState) => {
  dispatch(apiGet(`/api/admin/categories${query}`)).then(() => {
  })
}

export const getCategory = (category_id) => (dispatch, getState) => {
  dispatch(apiGet(`/api/admin/categories/${category_id}`)).then(() => {
    const category = parseCategory(getState().adminApi.category)
    dispatch({
      type: actions.GET_CATEGORY,
      category: category
    })
  })
}