export const actions = {
  GET_HOSPITAL: 'GET_HOSPITAL',
  HOSPITAL_ON_CHANGE: 'HOSPITAL_ON_CHANGE',
  HOSPITAL_ON_CHANGE_PAGE: 'HOSPITAL_ON_CHANGE_PAGE',
  HOSPITAL_SUBMITTED: 'HOSPITAL_SUBMITTED',
  HOSPITAL_RESET: 'HOSPITAL_RESET',
  HOSPITAL_SUBMIT_FAILED: 'HOSPITAL_SUBMIT_FAILED'
}

import { apiGet } from './adminApiActions'

export const reset = () => {
  return {
    type: actions.HOSPITAL_RESET
  }
}

export const onSubmit = (hospital) => {
  const data = { hospital: hospital }
  const url = hospital.id ? '/api/admin/hospitals/'+hospital.id : '/api/admin/hospitals'

  const params = {
    method: hospital.id ? 'PUT' : 'POST',
    mode: 'same-origin',
    credentials: 'same-origin',
    headers: new Headers({
      "Content-Type": "application/json"
    }),
    body: JSON.stringify(data)
  }

  return dispatch => {
    return fetch(url, params)
      .then(response => {
        response.json()
          .then(json => {
            const errors = json.errors || {}
            const hospital = json.hospital
            if (Object.keys(errors).length > 0) {
              dispatch({
                type: actions.HOSPITAL_SUBMIT_FAILED,
                errors
              })
            } else {
              dispatch({
                type: actions.HOSPITAL_SUBMITTED,
                hospital
              })
            }
          })
          .catch(error => {
            console.error(error)
          })
        
      })
      .catch(error => {
        console.error(error)
      })
  }
}

export const onChange = (name, value) => (dispatch, getState) => {
  dispatch({
    type: actions.HOSPITAL_ON_CHANGE,
    name,
    value
  })
}

export const getHospitals = (query) => (dispatch, getState) => {
  dispatch(apiGet(`/api/admin/hospitals${query}`)).then(() => {
  })
}

export const getHospital = (hospital_id) => (dispatch, getState) => {
  dispatch(apiGet(`/api/admin/hospitals/${hospital_id}`)).then(() => {
    dispatch({
      type: actions.GET_HOSPITAL,
      hospital: getState().adminApi.hospital
    })
  })
}