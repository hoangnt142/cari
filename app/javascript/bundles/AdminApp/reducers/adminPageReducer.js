import { actions } from '../actions/adminPageActions'

const initialState = {
  page: {
    image: {}
  },
  errors: {},
  changed: false
}

const adminPageReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.GET_PAGE:
      return Object.assign({}, state, {
        page: action.page
      })
    case actions.PAGE_DELETED:
      return Object.assign({}, state, {
        changed: !state.changed
      })
    case actions.ON_CHANGE:
      let page = state.page
      eval(`page.${action.name} = action.value`)
      return Object.assign({}, state, {
        page: page,
        changed: !state.changed
      })
    case actions.SUBMITTED:
      return Object.assign({}, state, {
        page: action.page
      })
    case actions.SUBMIT_FAILED:
      return Object.assign({}, state, {
        errors: action.errors
      })
    case actions.PAGE_RESET:
      return Object.assign({}, initialState, {
        page: {image:{}}
      })
    default:
      return state
  }
}

export default adminPageReducer
