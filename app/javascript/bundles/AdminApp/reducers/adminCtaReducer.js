import { actions } from '../actions/adminCtaActions'
const initialState = {
  cta: {},
  errors: {},
  changed: false
}

const adminctaReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.CTA_SUBMITTED:
      return Object.assign({}, state, {
        cta: action.cta
      })
    case actions.CTA_SUBMIT_FAILED:
      return Object.assign({}, state, {
        errors: action.errors
      })
    case actions.GET_CTA:
      return Object.assign({}, state, {
        cta: action.cta
      })
    case actions.CTA_ON_CHANGE:
      let cta = state.cta
      eval(`cta.${action.name} = action.value`)
      return Object.assign({}, state, {
        cta: cta,
        changed: !state.changed
      })
    case actions.CTA_RESET:
      return Object.assign({}, initialState, {
        cta: {}
      })
    default:
      return state
  }
}

export default adminctaReducer
