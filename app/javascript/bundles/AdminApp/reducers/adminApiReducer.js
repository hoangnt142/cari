import { actions } from '../actions/adminApiActions'

const initialState = {
  hospitals: [],
  pages: [],
  categories: [],
  article_categories: [],
  search_keys: [],
  articles: [],
  parent_categories: [],
  ctas: [],
  totalPages: 1,
  currentPage: 1,
  fetching: false
}

const adminApiReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.API_REQUEST_BEGIN:
      return Object.assign({}, state, {fetching: true})
    case actions.API_REQUEST_END:
      return Object.assign({}, state, {fetching: false})
    case actions.API_RECEIVED:
      const receivedParams = action.json
      return Object.assign({}, state, receivedParams)
    case actions.API_RESET:
      return Object.assign({}, initialState)
    default:
      return state
  }
}

export default adminApiReducer
