import { actions } from '../actions/adminCategoryActions'
const initialState = {
  category: {
    page: {image: {}}
  },
  errors: {page: {}},
  changed: false
}

const adminCategoryReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.CATEGORY_SUBMITTED:
      return Object.assign({}, state, {
        category: action.category
      })
    case actions.CATEGORY_SUBMIT_FAILED:
      return Object.assign({}, state, {
        errors: action.errors
      })
    case actions.GET_CATEGORY:
      return Object.assign({}, state, {
        category: action.category
      })
    case actions.CATEGORY_ON_CHANGE:
      let category = state.category
      eval(`category.${action.name} = action.value`)
      return Object.assign({}, state, {
        category: category,
        changed: !state.changed
      })
    case actions.CATEGORY_RESET:
      return Object.assign({}, initialState)
    default:
      return state
  }
}

export default adminCategoryReducer
