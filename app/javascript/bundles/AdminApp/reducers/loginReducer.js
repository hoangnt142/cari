import { actions } from '../actions/loginActions'

const initialState = {
  admin: {},
  loggedInFailed: false
}

const loginReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.ADMIN_LOGGED_IN:
      return Object.assign({}, state, {
        admin: action.admin
      })
    case actions.ADMIN_LOGGED_IN_FAILED:
      return Object.assign({}, state, {
        loggedInFailed: true,
        admin: {}
      })
    case actions.ADMIN_LOGGED_OUT:
      return Object.assign({}, state, {
        admin: {}
      })
    default:
      return state
  }
}

export default loginReducer
