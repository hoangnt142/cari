import { actions } from '../actions/adminArticleActions'
const initialState = {
  article: {
    page: {image: {}}
  },
  errors: {page: {}},
  changed: false
}

const adminArticleReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.ARTICLE_SUBMITTED:
      return Object.assign({}, state, {
        article: action.article
      })
    case actions.ARTICLE_SUBMIT_FAILED:
      return Object.assign({}, state, {
        errors: action.errors
      })
    case actions.GET_ARTICLE:
      return Object.assign({}, state, {
        article: action.article
      })
    case actions.ARTICLE_ON_CHANGE:
      let article = state.article
      eval(`article.${action.name} = action.value`)
      return Object.assign({}, state, {
        article: article,
        changed: !state.changed
      })
    case actions.ARTICLE_RESET:
      return Object.assign({}, initialState)
    default:
      return state
  }
}

export default adminArticleReducer
