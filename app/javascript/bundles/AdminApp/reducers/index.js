import { combineReducers } from 'redux'

import loginReducer from './loginReducer'
import adminPageReducer from './adminPageReducer'
import adminApiReducer from './adminApiReducer'
import adminHospitalReducer from './adminHospitalReducer'
import adminCategoryReducer from './adminCategoryReducer'
import adminArticleReducer from './adminArticleReducer'
import adminImageReducer from './adminImageReducer'
import adminCtaReducer from './adminCtaReducer'
import adminSystemConfigReducer from './adminSystemConfigReducer'

const reducers = combineReducers({
  'login': loginReducer,
  'adminPage': adminPageReducer,
  'adminApi': adminApiReducer,
  'adminHospital': adminHospitalReducer,
  'adminCategory': adminCategoryReducer,
  'adminArticle': adminArticleReducer,
  'adminImage': adminImageReducer,
  'adminCta': adminCtaReducer,
  'adminSystemConfig': adminSystemConfigReducer
})

export default reducers