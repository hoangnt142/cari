import { actions } from '../actions/adminHospitalActions'
const initialState = {
  hospital: {
    page: {}
  },
  errors: {},
  changed: false
}

const adminHospitalReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.HOSPITAL_SUBMITTED:
      return Object.assign({}, state, {
        hospital: action.hospital
      })
    case actions.HOSPITAL_SUBMIT_FAILED:
      return Object.assign({}, state, {
        errors: action.errors
      })
    case actions.GET_HOSPITAL:
      return Object.assign({}, state, {
        hospital: action.hospital
      })
    case actions.HOSPITAL_ON_CHANGE:
      let hospital = state.hospital
      eval(`hospital.${action.name} = action.value`)
      return Object.assign({}, state, {
        hospital: hospital,
        changed: !state.changed
      })
    case actions.HOSPITAL_RESET:
      return Object.assign({}, initialState, {
        hospital: {
          page: {}
        }
      })
    default:
      return state
  }
}

export default adminHospitalReducer
