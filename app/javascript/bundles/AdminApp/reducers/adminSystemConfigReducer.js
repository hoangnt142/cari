import { actions } from '../actions/adminSystemConfigActions'
const initialState = {
  systemConfig: {},
  changed: false
}

const adminSystemConfigReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.SYSTEM_CONFIG_SUBMITTED:
      return Object.assign({}, state, {
        systemConfig: action.systemConfig
      })
    case actions.SYSTEM_CONFIG_SUBMIT_FAILED:
      return Object.assign({}, state, {
        errors: action.errors
      })
    case actions.GET_SYSTEM_CONFIG:
      return Object.assign({}, state, {
        systemConfig: action.systemConfig
      })
    case actions.SYSTEM_CONFIG_ON_CHANGE:
      let systemConfig = state.systemConfig
      eval(`systemConfig.${action.name} = action.value`)
      return Object.assign({}, state, {
        systemConfig: systemConfig,
        changed: !state.changed
      })
    case actions.SYSTEM_CONFIG_RESET:
      return Object.assign({}, initialState, {
        systemConfig: {}
      })
    default:
      return state
  }
}

export default adminSystemConfigReducer
