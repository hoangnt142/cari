import { actions } from '../actions/adminImageActions'
const initialState = {
  image: {},
  submmitedAt: new Date(),
  errors: {}
}

const adminImageReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.IMAGE_SUBMIT_FAILED:
      return Object.assign({}, state, {
        errors: action.errors
      })
    case actions.IMAGE_SUBMITTED:
      return Object.assign({}, state, {
        submmitedAt: new Date(),
        errors: {}
      })
    default:
      return state
  }
}

export default adminImageReducer
