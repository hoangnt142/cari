import { MYPAGE_MY_REPUTES_RESET } from '../constants/constants'

export const reset = () => {
  return {
    type: MYPAGE_MY_REPUTES_RESET,
  }
}
