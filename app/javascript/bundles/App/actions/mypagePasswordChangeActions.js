import { MYPAGE_PASSWORD_CHANGE_RESET } from '../constants/constants'

export const reset = () => {
  return {
    type: MYPAGE_PASSWORD_CHANGE_RESET,
  }
}
