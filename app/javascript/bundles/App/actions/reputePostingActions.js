import { REPUTE_POSTING_RESET } from '../constants/constants'

export const reset = () => {
  return {
    type: REPUTE_POSTING_RESET,
  }
}
