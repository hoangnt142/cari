import {
  HOSPITAL_PAGE_RELATED_JOBS_SEARCHING,
  HOSPITAL_PAGE_RELATED_JOBS_SET,
  HOSPITAL_PAGE_RELATED_JOBS_FAILED,
  HOSPITAL_PAGE_RESET
} from '../constants/constants'


export const init = (hospital_id) => (dispatch,getState) => {

  dispatch({
    type: HOSPITAL_PAGE_RELATED_JOBS_SEARCHING
  })

  fetch('/api/cache/hospitals/related-jobs',{
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      hospital_id
    })
  }).then(response => {
    if(!response.ok) {
      throw `Fetch was failed: ${response.code}`
    }

    return response.json()
  }).then(json => {
    const {
      hospitals
    } = json

    dispatch({
      type: HOSPITAL_PAGE_RELATED_JOBS_SET,
      relatedHospitals: hospitals
    })

  }).catch(error => {
    dispatch({
      type: HOSPITAL_PAGE_RELATED_JOBS_FAILED,
    })
  })

}

export const reset = () => {
  return {
    type: HOSPITAL_PAGE_RESET,
  }
}
