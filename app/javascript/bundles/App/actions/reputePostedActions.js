import { REPUTE_POSTED_RESET } from '../constants/constants'

export const reset = () => {
  return {
    type: REPUTE_POSTED_RESET,
  }
}
