import { MYPAGE_JOBS_RESET } from '../constants/constants'

export const reset = () => {
  return {
    type: MYPAGE_JOBS_RESET,
  }
}
