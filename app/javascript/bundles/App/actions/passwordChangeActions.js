import { PASSWORD_CHANGE_RESET } from '../constants/constants'

export const reset = () => {
  return {
    type: PASSWORD_CHANGE_RESET,
  }
}
