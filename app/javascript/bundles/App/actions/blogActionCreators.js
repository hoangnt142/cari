import { BLOG_RESET } from '../constants/constants';

/*
export const onClickSample1 = (text) => {
  return {
    type: ACTION_SAMPLE1,
    text
  };
};
*/

export const reset = () => {
  return {
    type: BLOG_RESET,
  };
};
