import { NOT_FOUND_PAGE_RESET } from '../constants/constants'

export const reset = () => {
  return {
    type: NOT_FOUND_PAGE_RESET,
  }
}
