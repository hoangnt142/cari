import { PASSWORD_GENERATE_RESET } from '../constants/constants'

export const reset = () => {
  return {
    type: PASSWORD_GENERATE_RESET,
  }
}
