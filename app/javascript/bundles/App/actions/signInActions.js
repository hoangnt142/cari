import { SIGN_IN_RESET, SIGN_IN_ON_CHANGE, SIGN_IN_CLICKED } from '../constants/constants'

export const reset = () => {
  return {
    type: SIGN_IN_RESET,
  }
}

export const onChange = (field, value) => {
  return {
    type: SIGN_IN_ON_CHANGE,
    field,
    value
  }
}

export const signInClick = () => {
  return {
    type: SIGN_IN_CLICKED,
  }
}
