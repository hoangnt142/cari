import {
  MY_JOBS_RESET,
  MY_JOB_GET_HOSPITALS
} from '../constants/constants'

import { savedJobIds } from 'lib/function'
import { post } from './apiActions'

export const reset = () => {
  return {
    type: MY_JOBS_RESET,
  }
}

export const getHospitals = () => (dispatch, getState) => {
  const job_ids = JSON.stringify({job_ids: savedJobIds('favorite')})
  const url = '/api/cache/hospitals/favorite-jobs'
  dispatch(post(url, job_ids)).then(() => {
    dispatch({
      type: MY_JOB_GET_HOSPITALS,
      hospitals: getState().api.hospitals
    })
  })
}