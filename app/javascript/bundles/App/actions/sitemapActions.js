import { SITEMAP_RESET } from '../constants/constants'

export const reset = () => {
  return {
    type: SITEMAP_RESET,
  }
}
