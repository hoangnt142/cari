import { API_RESET_CATEGORY } from '../constants/constants'

export const reset = () => (dispatch) => {
  dispatch({
    type: API_RESET_CATEGORY,
  })
}
