import { AGENTS_RESET } from '../constants/constants'

export const reset = () => {
  return {
    type: AGENTS_RESET,
  }
}
