import { MYPAGE_RESET } from '../constants/constants'

export const reset = () => {
  return {
    type: MYPAGE_RESET,
  }
}
