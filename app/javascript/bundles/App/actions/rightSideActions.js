import { RIGHT_SIDE_INITIALIZE } from '../constants/constants'

export const initialize = () => (dispatch, getState) => {

  dispatch({
    type: RIGHT_SIDE_INITIALIZE
  })
}