import { INFORMATION_RESET } from '../constants/constants'

export const reset = () => {
  return {
    type: INFORMATION_RESET,
  }
}
