import { CONTACT_SENT_RESET } from '../constants/constants'

export const reset = () => {
  return {
    type: CONTACT_SENT_RESET,
  }
}
