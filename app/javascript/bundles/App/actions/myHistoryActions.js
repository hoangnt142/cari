import {
  MY_HISTORY_RESET,
  MY_HISTORY_GET_HOSPITALS
} from '../constants/constants'

import { savedJobIds } from 'lib/function'
import { post } from './apiActions'

export const reset = () => {
  return {
    type: MY_HISTORY_RESET,
  }
}

export const getHospitals = () => (dispatch, getState) => {
  const job_ids = savedJobIds('history', true)
  const url = '/api/cache/hospitals/favorite-jobs'
  dispatch(post(url, JSON.stringify({job_ids: job_ids}))).then(() => {
    dispatch({
      type: MY_HISTORY_GET_HOSPITALS,
      hospitals: getState().api.hospitals
    })
  })
}
