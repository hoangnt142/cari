import { API_RESET_ARTICLE } from '../constants/constants'

export const reset = () => (dispatch) => {
  dispatch({
    type: API_RESET_ARTICLE,
  })
}
