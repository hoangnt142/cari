import {
  LOGGING,
  LOGGED_IN,
  LOGGED_OUT,
  LOGGED_IN_FAILED,
  SIGNING,
  SIGN_UP_SUCCEEDED,
  SIGN_UP_FAILED,
} from '../constants/constants'


const logging = () => {
  return {
    type: LOGGING,
  }
}

const loggedIn = (user) => {
  return {
    type: LOGGED_IN,
    user
  }
}

const loggedOut = (user) => {
  return {
    type: LOGGED_OUT,
    user
  }
}

const loggedInFailed = () => {
  return {
    type: LOGGED_IN_FAILED,
  }
}

export const logout = () => {
  const params = {
    method: 'DELETE',
    mode: 'same-origin',
    credentials: 'same-origin'
  }
  const url = '/api/uncache/users/sign-out'
  return dispatch => {
    return fetch(url, params)
      .then(response => {
        dispatch(loggedOut())
      })
  }
}

export const login = (email, password) => dispatch => {

  dispatch(loggedInFailed())

  // TODO: Commented to hide the URLs before releasing the features of signing in
  /* const headers = new Headers({
   *   "Content-Type": "application/json"
   * });
   * const data = { user: { email, password } }
   * const params = {
   *   method: 'POST',
   *   mode: 'same-origin',
   *   credentials: 'same-origin',
   *   headers: headers,
   *   body: JSON.stringify(data)
   * }
   * const url = '/api/uncache/users/sign-in'
   * return dispatch => {
   *   dispatch(logging())
   *   return fetch(url, params)
   *     .then(response => {
   *       if(!response.ok) {
   *         dispatch(loggedInFailed())
   *       }
   *       else {
   *         response.json()
   *                 .then(json => {
   *                   console.log('json', json)
   *                   if(json.user) {
   *                     dispatch(loggedIn(json.user))
   *                   }
   *                   else {
   *                     dispatch(loggedInFailed())
   *                   }
   *                 })
   *                 .catch(error => {
   *                   console.error(error)
   *                   dispatch(loggedInFailed())
   *                 })
   *       }
   *     })
   *     .catch(error => {
   *       console.error(error)
   *       dispatch(loggedInFailed())
   *     })
   * } */
}

export const signUpSucceeded = () => {
  return {
    type: SIGN_UP_SUCCEEDED
  }
}
export const signUpFailed = () => {
  return {
    type: SIGN_UP_FAILED
  }
}

export const signUp = (signUpParams) => {
  return dispatch => {

    dispatch({
      type: SIGNING
    })

    const data = {}
    const fields = [
      'nickname',
      'email',
      'password',
      'gender',
      'qualification1',
      'qualification2',
      'qualification3',
      'qualification4',
      'qualification5',
      'prefectureId',
      'cityId',
      'birth',
      'favoritePrefectureId1',
      'favoriteCityId1',
      'favoritePrefectureId2',
      'favoriteCityId2',
    ]

    fields.forEach(f => data[f] = signUpParams[f])

    const headers = new Headers({
      "Content-Type": "application/json"
    });

    const options = {
      method: 'POST',
      mode: 'same-origin',
      credentials: 'same-origin',
      body: JSON.stringify(data),
      headers: headers
    }

    fetch('api/uncache/users', options).then(response => {
      if(!response.ok) {
        dispatch(signUpFailed())
      }

      response.json().then(json => {
        console.log(json)
        if(json) {
          dispatch(signUpSucceeded())
        }
        else {
          dispatch(signUpFailed())
        }
      })
    })
  }
}
