import {
  SIGN_UP_RESET,
  SIGN_UP_ON_CHANGE,
  SIGN_UP_ON_CHANGE_SLIDE,
  SIGN_UP_ON_CHANGE_PREFECTURE,
  SIGN_UP_ON_CHANGE_BIRTH,
  SIGN_UP_ON_CHANGE_FAVORITE_PREFECTURE,
  SIGN_UP_ON_CHANGE_CITY
} from '../constants/constants'
import { fetch } from './apiActions'
import 'whatwg-fetch'

export const reset = () => {
  return {
    type: SIGN_UP_RESET,
  }
}

export const onChange = (field, value) => {

  return dispatch => {
    dispatch({
      type: SIGN_UP_ON_CHANGE,
      field,
      value
    })

    if(field == 'email') {
      // Check duplicate email when email is valid
      const regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if (regex.test(value)) {
        dispatch(fetch(`/api/uncache/check-email-exist?email=${value}`))
      }
    }
  }
}

export const onChangeSlide = (currentSlide) => {
  return {
    type: SIGN_UP_ON_CHANGE_SLIDE,
    currentSlide
  }
}

export const onChangePrefecture = (prefectureId, prefectureName) => {
  return dispatch => {
    dispatch({
      type: SIGN_UP_ON_CHANGE_PREFECTURE,
      prefectureId,
      prefectureName
    })
    console.log(prefectureId, prefectureName)
    dispatch(fetch(`/api/cache/cities?prefecture_id=${prefectureId}`))
  }
}

export const onChangeCity = (cityData) => {
  return Object.assign(cityData, {
    type: SIGN_UP_ON_CHANGE_CITY,
  })
}

export const onChangeFavoritePrefecture = (number, prefectureId, prefectureName) => {
  return dispatch => {
    dispatch({
      type: SIGN_UP_ON_CHANGE_FAVORITE_PREFECTURE,
      number,
      prefectureId,
      prefectureName
    })

    if(prefectureId && prefectureId > 0) {
      dispatch(fetch(`/api/cache/cities?prefecture_id=${prefectureId}`, null, `city${number}`))
    }
  }
}

export const onChangeBirth = (field, value) => {
  return {
    type: SIGN_UP_ON_CHANGE_BIRTH,
    field: field,
    value: value
  }
}

export const fetchPrefectures = () => {
  return dispatch => {
    dispatch(fetch('/api/cache/prefectures'))
  }
}