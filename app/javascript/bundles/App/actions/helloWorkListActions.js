import { HELLO_WORK_LIST_RESET } from '../constants/constants'

export const reset = () => {
  return {
    type: HELLO_WORK_LIST_RESET,
  }
}
