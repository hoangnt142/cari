import {
  SEARCH_MODAL_OPEN,
  SEARCH_MODAL_CLOSE,
  SEARCH_MODAL_CHANGE_MODAL,
  SEARCH_MODAL_OPEN_CHILD_MODAL,
  SEARCH_MODAL_CLOSE_CHILD_MODAL,

  SEARCH_MODAL_INIT,
  SEARCH_MODAL_RESET,
  SEARCH_MODAL_ON_CHANGE_CONDITION,
  SEARCH_MODAL_ON_CHANGE_ALL_CONDITION,
  SEARCH_MODAL_DELETE_CONDITION,
  SEARCH_MODAL_BEFORE_SEARCHING,
  SEARCH_MODAL_SEARCHED,
  SEARCH_MODAL_SEARCHING_FAILED,
  SEARCH_MODAL_ON_CHANGE_PAGE,
  SEARCH_MODAL_ON_CHANGE_TEXT,

  SEARCH_MODAL_ON_CHANGE_REGION,
  SEARCH_MODAL_ON_CHANGE_PREFECTURE,
  SEARCH_MODAL_ON_CHANGE_AREA,
  SEARCH_MODAL_AREA1_IN_TOKYO,
  SEARCH_MODAL_REMOVE_CONDITION_EXCEPT_TEXT,
  SEARCH_MODAL_SAVE_TO_BACK_STATE,
  SEARCH_MODAL_ON_CLICK_SEARCH
} from '../constants/constants'
import { objectToParameters } from '../lib/url'
import { fetch as apiFetch } from './apiActions'
import { scroller } from 'react-scroll'
import history from '../lib/history'


export const openSearchModal = (searchType, positionOrArea=null, backModal=false) => (dispatch, getState) => {

  // When backModal is true, state will be copied back to search with the new state
  if(backModal) {
    dispatch({
      type: SEARCH_MODAL_SAVE_TO_BACK_STATE
    })
    dispatch(search())
  }


  dispatch({
    type: SEARCH_MODAL_OPEN,
    searchType
  })

  // Scroll the modal window by the position
  if(searchType == 'detail') {
    if(positionOrArea) {
      setTimeout(() => {
        console.log('scroll to', positionOrArea)
        scroller.scrollTo(positionOrArea, {
          containerId: 'scroll-container',
          smooth: true,
          duration: 500,
          offset: -110
        })
      }, 500)
    }
  }
  else if(searchType == 'area') {
    if(positionOrArea) {
      dispatch(onChangeRegion(positionOrArea))
    }
  }
}

export const closeSearchModal = () => ({
  type: SEARCH_MODAL_CLOSE
})

export const changeModal = (searchType) => ({
  type: SEARCH_MODAL_CHANGE_MODAL,
  searchType
})

export const openChildModal = (searchType) => ({
  type: SEARCH_MODAL_OPEN_CHILD_MODAL,
  searchType
})

export const closeChildModal = (searchType) => ({
  type: SEARCH_MODAL_CLOSE_CHILD_MODAL
})

export const onClickSearch = () => (dispatch,getState) => {
  dispatch({
    type: SEARCH_MODAL_ON_CLICK_SEARCH
  })

  history.push('/search')
}



export const init = (initialCondition=null) => (dispatch, getState) => {

  if(initialCondition) {
    dispatch(onChangeAllCondition(initialCondition))
  }

  const state = getState()
  const condition = state.searchModal.condition
  //dispatch(beginFetch())

  let promise1, promise2, promise3, allPromises, hospitalsPromise

  hospitalsPromise = dispatch(search())

  if (!state.api.qualifications) {
    promise1 = dispatch(apiFetch('/api/cache/search_keys'))
  }

  if (!state.api.prefectures) {
    promise2 = dispatch(apiFetch('/api/cache/prefectures'))
  }

  if(condition.areaId) {
    promise3 = dispatch(apiFetch(`/api/cache/areas/${condition.areaId}/with_ancestors`))
    allPromises = Promise.all([promise1, promise2, promise3, hospitalsPromise])
  }
  else {
    allPromises = Promise.all([promise1, promise2, hospitalsPromise])
  }



  allPromises.then(() => {

    const {
      regions,
      prefectures,
      areas,
      qualifications,
      job_contracts,
      hospital_types,
      job_types,
      job_details,
      hospitals
    } = getState().api

    dispatch({
      type: SEARCH_MODAL_INIT,
      condition,
      regions,
      prefectures,
      areas,
      qualifications,
      jobContracts: job_contracts,
      hospitalTypes: hospital_types,
      jobTypes: job_types,
      jobDetails: job_details,
      hospitals: hospitals || []
    })
  })
}

export const reset = () => (dispatch, getState) => {
  dispatch({
    type: SEARCH_MODAL_RESET
  })
  dispatch(search())
}

export const onChangePage = (page) => (dispatch, getState) => {
  dispatch({
    type: SEARCH_MODAL_ON_CHANGE_PAGE,
    page
  })
  dispatch(search())
}

export const deleteCondition = (field) => (dispatch, getState) => {
  dispatch({
    type: SEARCH_MODAL_DELETE_CONDITION,
    field
  })
  dispatch(search())
}

export const onChangeJobContract = (e, data) => (dispatch, getState) => {
  dispatch(onChangeCondition('jobContractIds', data))
  dispatch(search())
}
export const onChangeQualification = (e, data) => (dispatch, getState) => {
  dispatch(onChangeCondition('qualificationIds', data))
  dispatch(search())
}
export const onChangeHospitalType = (e, data) => (dispatch, getState) => {
  dispatch(onChangeCondition('hospitalTypeIds', data))
  dispatch(search())
}
export const onChangeJobType = (e, data) => (dispatch, getState) => {
  dispatch(onChangeCondition('jobTypeIds', data))
  dispatch(search())
}
export const onChangeJobDetail = (e, data) => (dispatch, getState) => {
  dispatch(onChangeCondition('jobDetailIds', data))
  dispatch(search())
}



const onChangeCondition = (field, data) => (dispatch, getState) => {
  dispatch({
    type: SEARCH_MODAL_ON_CHANGE_CONDITION,
    field,
    data
  })
}

export const onChangeAllCondition = (condition) => (dispatch, getState) => {
  dispatch({
    type: SEARCH_MODAL_ON_CHANGE_ALL_CONDITION,
    condition
  })
}

export const onChangeRegion = (regionId=null) => (dispatch, getState) => {
  dispatch({
    type: SEARCH_MODAL_ON_CHANGE_REGION,
    regionId,
  })
  dispatch(search())
}

export const onChangePrefecture = (prefectureId=null) => (dispatch, getState) => {
  if(prefectureId) {
    dispatch(apiFetch(`/api/cache/areas/area123_by_prefecture_ids?prefecture_ids=[${prefectureId}]`)).then(() => {
      const state = getState()

      const area1s = state.api.area1s[0].area1s
      const area2s = []
      const area3s = []
      state.api.area2s.forEach(a => {
        area2s.push(...a.area2s)
      })
      state.api.area3s.forEach(a => {
        area3s.push(...a.area3s)
      })

      dispatch({
        type: SEARCH_MODAL_ON_CHANGE_PREFECTURE,
        prefectureId,
        area1s,
        area2s,
        area3s,
      })
      dispatch(search())
    })
  }
  else {
    dispatch({
      type: SEARCH_MODAL_ON_CHANGE_PREFECTURE,
      prefectureId: null,
    })
    dispatch(search())
  }
}

export const area1InTokyo = () => (dispatch, getState) => {
  dispatch({
    type: SEARCH_MODAL_AREA1_IN_TOKYO
  })
  dispatch(search())
}

export const onChangeArea = (areaId, withChildren=false) => (dispatch, getState) => {
  const state = getState()
  const {
    areaIds,
    prefectureId
  } = state.searchModal.condition
  const {
    area1s,
    area2s,
    area3s,
  } = state.searchModal

  let children = []
  let level = 3
  if(area1s.find(a => a.id == areaId)) {
    level = 1
    children = area2s.filter(a => a.parent_id == areaId)
  }
  else if(area2s.find(a => a.id == areaId)) {
    level = 2
    children = area3s.filter(a => a.parent_id == areaId)
  }


  // Checking child elements should be done only when it's not Tokyo or area2 in Tokyo
  const checkChildren = withChildren && (prefectureId != 13 || level == 2)
  //console.log('checkChildren', checkChildren)
  // Checked areas has to be reset when checking area1 in Tokyo
  const resetAreasInTokyo = prefectureId == 13 && level == 1

  const index = areaIds.findIndex(id => id == areaId)
  let _areaIds = areaIds.slice()
  if(resetAreasInTokyo) {
    _areaIds = []
  }

  if(index == -1) {
    // Check
    if(checkChildren) {
      _areaIds.push(areaId, ...(children.map(c => c.id)))
    }
    else {
      _areaIds.push(areaId)
    }
    // Delete duplicated elements
    _areaIds = [...new Set(_areaIds)]
  }
  else {
    // Uncheck
    _areaIds.splice(index, 1)
    children.forEach(c => {
      const j = _areaIds.findIndex(id => id == c.id)
      if(j >= 0) {
        _areaIds.splice(j, 1)
      }
    })
  }

  dispatch({
    type: SEARCH_MODAL_ON_CHANGE_AREA,
    areaIds: _areaIds
  })
  dispatch(search())
}

export const onChangeText = (text) => {
  return {
    type: SEARCH_MODAL_ON_CHANGE_TEXT,
    text
  }
}



export const search = () => (dispatch, getState) => {
  console.log('searching..')
  const state = getState()
  const condition = Object.assign({}, state.searchModal.condition)
  const parameters = objectToParameters(condition)

  dispatch({
    type: SEARCH_MODAL_BEFORE_SEARCHING
  })

  const options = {
    method: 'GET',
    mode: 'same-origin',
    credentials: 'same-origin'
  }
  fetch(`/api/cache/hospitals?${parameters}`, options).then(response => {
    if(!response.ok) {
      throw `Error from the server (${response.status})`
    }

    return response.json()
  }).then(json => {
    const state = getState()
    const {
      hospitals,
      total_pages,
      current_page,
      total_count,
      last_date,
      pagination_range
    } = json

    dispatch({
      type: SEARCH_MODAL_SEARCHED,
      freeTextTitle: state.searchModal.condition.text,
      hospitals,
      totalPages: total_pages,
      currentPage: current_page,
      totalCount: total_count,
      lastDate: last_date,
      paginationRange: pagination_range,
      condition
    })
  }).catch(error => {
    console.log(error)
    dispatch({
      type: SEARCH_MODAL_SEARCHING_FAILED
    })
  })
}

export const searchByText = () => (dispatch, getState) => {
  // 1. go to the /search page
  console.log('move to /search')
  history.push('/search')
  scrollTo(0,0)

  dispatch({
    type: SEARCH_MODAL_REMOVE_CONDITION_EXCEPT_TEXT,
  })

  // 2. Search
  dispatch(search())
}
