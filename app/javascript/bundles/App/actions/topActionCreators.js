import {
  TOP_RESET,
  TOP_SET_VISIBLE,
  TOP_INITIALIZE
} from '../constants/constants';

import { fetch } from './apiActions'
import { pluck } from '../lib/semantic'
import { reset as resetSearchModal } from './searchModalActions'

/*
export const onClickSample1 = (text) => {
  return {
    type: ACTION_SAMPLE1,
    text
  };
};
*/

export const initialize = () => (dispatch, getState) => {

  let state = getState()
  let allPromises, prefectures, widgets

  if (!state.api.regions) {
    prefectures = dispatch(fetch('/api/cache/prefectures'))
  }

  if (!state.api.widgets) {
    widgets = dispatch(fetch('/api/cache/widgets?page=top'))
  }

  allPromises = Promise.all([prefectures, widgets])

  allPromises.then(() => {
    const state = getState()
    const {
      regions,
      widgets,
      prefectures
    } = state.api

    let regionsWithPrefectures = prefecturesGroup(regions, prefectures)

    dispatch({
      type: TOP_INITIALIZE,
      regionsWithPrefectures,
      widgets
    })
  })
}

const prefecturesGroup = (regions, prefectures) => {
  let region12, region3, region4, region5, region67, region89

  region12 = {
    label: pluck(regions.filter(region => [1,2].includes(region.id)), 'name').join('・'),
    prefectures: prefectures.filter(prefecture => [1,2].includes(prefecture.region_id))
  }

  region3 = {
    label: regions.find(region => region.id == 3).name,
    prefectures: prefectures.filter(prefecture => prefecture.region_id == 3)
  }

  region4 = {
    label: regions.find(region => region.id == 4).name,
    prefectures: prefectures.filter(prefecture => prefecture.region_id == 4)
  }

  region5 = {
    label: regions.find(region => region.id == 5).name,
    prefectures: prefectures.filter(prefecture => prefecture.region_id == 5)
  }

  region67 = {
    label: pluck(regions.filter(region => [6,7].includes(region.id)), 'name').join('・'),
    prefectures: prefectures.filter(prefecture => [6,7].includes(prefecture.region_id))
  }

  region89 = {
    label: pluck(regions.filter(region => [8,9].includes(region.id)), 'name').join('・'),
    prefectures: prefectures.filter(prefecture => [8,9].includes(prefecture.region_id))
  }

  return [region12, region3, region4, region5, region67, region89]
}

export const reset = () => (dispatch, getState) => {
  dispatch(resetSearchModal())
  dispatch({
    type: TOP_RESET,
  })
}


export const setVisible = (visible) => {
  return {
    type: TOP_SET_VISIBLE,
    visible: visible
  }
}
