import { REPUTES_RESET } from '../constants/constants'

export const reset = () => {
  return {
    type: REPUTES_RESET,
  }
}
