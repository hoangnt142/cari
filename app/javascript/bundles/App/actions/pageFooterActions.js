import {
  PAGE_FOOTER_SEARCH_KEYS_FETCHING,
  PAGE_FOOTER_SEARCH_KEYS_FETCHED,
  PAGE_FOOTER_SEARCH_KEYS_SET,
  PAGE_FOOTER_SEARCH_KEYS_FETCH_FAILED,

  PAGE_FOOTER_PREFECTURES_FETCHING,
  PAGE_FOOTER_PREFECTURES_FETCHED,
  PAGE_FOOTER_PREFECTURES_SET,
  PAGE_FOOTER_PREFECTURES_FETCH_FAILED
} from '../constants/constants'


const fetchSearchKeys = (dispatch) => {
  dispatch({
    type: PAGE_FOOTER_SEARCH_KEYS_FETCHING,
  })

  fetch('/api/cache/search_keys').then(response => {
    dispatch({
      type: PAGE_FOOTER_SEARCH_KEYS_FETCHED,
    })

    if(!response.ok) {
      throw `Fetch was failed: ${response.code}`
    }

    return response.json()
  }).then(json => {
    const {
      qualifications,
      job_contracts,
      hospital_types,
      job_types,
      job_details
    } = json

    dispatch({
      type: PAGE_FOOTER_SEARCH_KEYS_SET,
      qualifications,
      job_contracts,
      hospital_types,
      job_types,
      job_details
    })

  }).catch(error => {
    dispatch({
      type: PAGE_FOOTER_SEARCH_KEYS_FETCH_FAILED,
    })
  })
}

const fetchPrefectures = (dispatch) => {
  dispatch({
    type: PAGE_FOOTER_PREFECTURES_FETCHING,
  })

  fetch('/api/cache/prefectures').then(response => {
    dispatch({
      type: PAGE_FOOTER_PREFECTURES_FETCHED,
    })

    if(!response.ok) {
      throw `Fetch was failed: ${response.code}`
    }

    return response.json()
  }).then(json => {
    const {
      prefectures,
      regions
    } = json

    dispatch({
      type: PAGE_FOOTER_PREFECTURES_SET,
      prefectures,
      regions
    })

  }).catch(error => {
    dispatch({
      type: PAGE_FOOTER_PREFECTURES_FETCH_FAILED,
    })
  })
}

export const init = () => (dispatch, getState) => {
  fetchSearchKeys(dispatch)
  fetchPrefectures(dispatch)
}
