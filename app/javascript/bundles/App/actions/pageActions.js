import { PAGE_RESET } from '../constants/constants'

export const reset = () => {
  return {
    type: PAGE_RESET,
  }
}
