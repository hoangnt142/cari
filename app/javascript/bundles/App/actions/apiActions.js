import {
  API_BEGIN,
  API_RECEIVED,
  API_FAILED,
  UPDATE_PAGE,
  API_RESET_AREAS,
  RESET_SEARCH_RESULT
} from '../constants/constants'

export const request = (url, requestName) => ({
  type: API_BEGIN,
  url,
  requestName
})
export const receive = (url, json, requestName) => ({
  type: API_RECEIVED,
  url,
  json,
  requestName
})
export const requestFailed = (url, requestName) => ({
  type: API_FAILED,
  url,
  requestName
})

export const resetSearchResult = () => ({
  type: RESET_SEARCH_RESULT
})

export const post = (url, body) => {
  const _fetch = window.fetch
  const defaultInit = {
    method: 'POST',
    mode: 'same-origin',
    credentials: 'same-origin',
    headers: new Headers({
      "Content-Type": "application/json"
    }),
    body: body
  }
  return dispatch => {
    return _fetch(url, defaultInit)
      .then(response => {
        if(!response.ok) {
          throw `Fetch was failed: ${response.code}`
        }
        return response.json()
                       .then(json => {
                         dispatch(receive(url, json))
                       })
                       .catch(error => {
                         dispatch(requestFailed(url))
                       })
      })
      .catch(error => {
        console.error(error)
      })
  }
}

export const fetch = (url, init, requestName=null) => {
  const _fetch = window.fetch
  const defaultInit = {
    method: 'GET',
    mode: 'same-origin',
    credentials: 'same-origin'
  }
  const mergedInit = Object.assign({}, defaultInit, init ? init : {})
  console.log(`fetching.. (${requestName || url})`)
  return dispatch => {
    dispatch(request(url, requestName))
    return _fetch(url, mergedInit)
      .then(response => {
        if(!response.ok) {
          throw `Fetch was failed: ${response.code}`
        }
        return response.json()
                       .then(json => {
                         console.log('json', json)
                         dispatch(receive(url, json, requestName))
                       })
                       .catch(error => {
                         console.error(error)
                         dispatch(requestFailed(url, requestName))
                       })
      })
      .catch(error => {
        console.error(error)
        dispatch(requestFailed(url, requestName))
      })
  }
}

export const fetchPage = () => {
  const path = window.location.href.replace(/^https?:\/\/[a-z0-9:\._-]+/, '')
  const url = `/api/cache/pages${path}`
  return fetch(url)
}

export const updatePage = (page) => ({
  type: UPDATE_PAGE,
  page,
})

export const resetAreas = () => ({
  type: API_RESET_AREAS
})
