import { MYPAGE_SETTINGS_RESET } from '../constants/constants'

export const reset = () => {
  return {
    type: MYPAGE_SETTINGS_RESET,
  }
}
