import { CONTACT_RESET } from '../constants/constants'

export const reset = () => {
  return {
    type: CONTACT_RESET,
  }
}
