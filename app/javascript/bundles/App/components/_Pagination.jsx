import React, { PureComponent } from 'react'
import { Icon, Pagination, Responsive } from 'semantic-ui-react'

import PropType from 'prop-types'

export default class CarrieePagination extends PureComponent {

  state = { isBreak: window.innerWidth < 991 }

  constructor(props, context) {
    super(props)
    this.onPageChange = this.onPageChange.bind(this)
  }

  onPageChange = (data) => {
    if (this.props.handleReset) {
      this.props.handleReset()
    }
    setTimeout(() => {
      let url_page = ''
      if(data.activePage > 1) {
        url_page = `/page/${data.activePage}`
      }

      // First page should not have /page
      this.context.router.history.push(`${this.context.router.route.match.url.split('/page/')[0]}${url_page}`)
    }, 0);
  }

  handleOnUpdate = () => {
    let windowWidth = window.innerWidth
    this.setState({
      isBreak: windowWidth < 991
    })
  }

  render() {
    const { totalPages, currentPage} = this.props
    const { isBreak } = this.state

    const {firstItem, lastItem, prevItem, nextItem} = isBreak ?
      {
        firstItem: <Icon name='angle double left' size='large' style={{margin: 0}} />,
        lastItem: <Icon name='angle double right' size='large' style={{margin: 0}} />,
        prevItem: <Icon name='angle left' size='large' style={{margin: 0}} />,
        nextItem: <Icon name='angle right' size='large' style={{margin: 0}} />
      }
      :
      { firstItem: <div className='ui text blue'><Icon name='chevron left' style={{margin: 0}}/>最初</div>,
        lastItem: <div className='ui text blue'>最後<Icon name='chevron right' style={{margin: 0}}/></div>,
        prevItem: <div><Icon name='chevron left' style={{margin: 0}}/>前</div>,
        nextItem: <div>次<Icon name='chevron right' style={{margin: 0}}/></div>
      }

    return (
      <Responsive
        onUpdate={this.handleOnUpdate}
        as={Pagination}
        onPageChange={(e, data) => this.onPageChange(data)}
        activePage={currentPage}
        ellipsisItem={{ content: <Icon name='ellipsis horizontal' />, icon: true }}
        firstItem={{content: firstItem }}
        lastItem={{content: lastItem }}
        prevItem={{content: prevItem }}
        nextItem={{content: nextItem }}
        totalPages={totalPages}
      />
    )
  }
}

CarrieePagination.contextTypes = {
  router: PropType.object.isRequired,
}
