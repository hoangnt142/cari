import React, { PureComponent } from 'react'
import PropType from 'prop-types'
import _ from 'lodash'
import GroupTitle from './_GroupTitle'

import {
  Image, Icon,
  Segment,
  Header,
  Form, Message, Grid,
  Responsive,
  Dropdown
} from 'semantic-ui-react'
import { Button as OriginalButton } from 'semantic-ui-react'
import SignUpSlideStep from './SignUpSlideStep'

const Button = (props) => <OriginalButton {...Object.assign({ style: { height: '5em'}}, props)} />


export default class SignUpSlide3 extends PureComponent {

  next = () => {
    const {
      next,
      nickname,
      birth,
      prefectureId,
      cityId
    } = this.props

    if(!nickname || nickname.length == 0) {
      alert('ニックネームを入力してください。')
    }
    else if(!birth) {
      alert('生年月日を入力してください。')
    }
    else if(!prefectureId || !cityId) {
      alert('住所を選択してください。')
    }
    else {
      next()
    }
  }

  render() {

    const {
      prefectures,
      cities,
      cityIsLoading,

      onChange,
      onChangePrefecture,
      onChangeBirth,
      onChangeCity,

      previous,
      nickname,
      birthYear,
      birthMonth,
      birthDay,
      prefectureId,
      cityId
    } = this.props


    // Gotten asynchrnously
    const prefectureOptions = prefectures ? prefectures.map(p => ({ text: p.name, value: p.id })) : null
    const cityOptions = cities ? cities.map(p => ({ text: p.name, value: p.id })) : null

    console.log('rendering')
    return (
      <div>
        <Segment style={{ paddingTop: '50px', paddingBottom: '50px' }}>

          <SignUpSlideStep step='2' />

          <Form>
            <GroupTitle as='h3' title='あなたのプロフィールを教えてください' required />
            <Grid>
              <Grid.Row>
                <Grid.Column width={10}>
                  <div style={{ fontWeight: 'bold' }} className='ui text pink large'>Carrieeで使われるニックネーム</div>
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column width={10}>
                  <Form.Input placeholder='ニックネーム(3文字以上)'
                              fluid
                              value={nickname}
                              onChange={(e, data) => onChange('nickname', data.value)} />
                </Grid.Column>
              </Grid.Row>

              <Grid.Row>
                <Grid.Column width={10}>
                  <div style={{ fontWeight: 'bold' }} className='ui text pink large'>あなたの生年月日</div>
                </Grid.Column>
              </Grid.Row>

              <Grid.Row>
                <Grid.Column width={4}>
                  <Form.Field inline>
                    <Dropdown compact
                              style={{ width: '70%' }}
                              selection
                              placeholder='Year'
                              value={birthYear}
                              onChange={(e, data) => onChangeBirth('year', data.value)}
                              options={_.range(1910, ((new Date).getFullYear() - 16)).map(n => ({ text: n, value: n }))} />
                    <label>年</label>
                  </Form.Field>
                </Grid.Column>
                <Grid.Column width={3}>
                  <Form.Field inline>
                    <Dropdown compact
                              style={{ width: '70%' }}
                              selection
                              placeholder='Month'
                              value={birthMonth}
                              onChange={(e, data) => onChangeBirth('month', data.value)}
                              options={_.range(1, 13).map(n => ({ text: n, value: n }))} />
                    <label>月</label>
                  </Form.Field>
                </Grid.Column>
                <Grid.Column width={3}>
                  <Form.Field inline>
                    <Dropdown compact
                              style={{ width: '70%' }}
                              selection
                              placeholder='Day'
                              value={birthDay}
                              onChange={(e, data) => onChangeBirth('day', data.value)}
                              options={_.range(1, 32).map(n => ({ text: n, value: n }))} />
                    <label>日</label>
                  </Form.Field>
                </Grid.Column>
              </Grid.Row>

              <Grid.Row>
                <Grid.Column width={10}>
                  <div style={{ fontWeight: 'bold' }} className='ui text pink large'>現在お住まいの住所</div>
                </Grid.Column>
              </Grid.Row>

              <Grid.Row>
                <Grid.Column width={5}>
                  <Dropdown fluid
                            selection
                            loading={prefectureOptions == null}
                            options={prefectureOptions}
                            value={prefectureId}
                            onChange={(e, data) => {
                                const option = data.options.find(o => o.value == data.value)
                                onChangePrefecture(data.value, option.text)
                            }}
                            placeholder='都道府県を選択' />
                </Grid.Column>
                <Grid.Column width={5}>
                  <Dropdown fluid
                            selection
                            loading={cityIsLoading}
                            options={cityOptions}
                            value={cityId}
                            onChange={(e, data) => {
                                const option = data.options.find(o => o.value == data.value)
                                onChangeCity({ cityId: data.value, cityName: option.text })
                            }}
                            placeholder='市区町村を選択' />
                </Grid.Column>
              </Grid.Row>
            </Grid>


            <Grid columns={2} style={{ marginTop: '2rem' }}>
              <Grid.Row>
                <Grid.Column textAlign='right'>
                  <Button
                    color='light-grey'
                    size='large'
                    style={{ height: '4em', width: '70%' }}
                    onClick={previous}><Icon name='chevron left' color='pink' />前に戻る</Button>
                </Grid.Column>
                <Grid.Column textAlign='left'>
                  <Button
                    color='green'
                    size='large'
                    style={{ height: '4em', width: '70%' }}
                    onClick={this.next}>次に進む<Icon name='chevron right' color='white' /></Button>
                </Grid.Column>
              </Grid.Row>
            </Grid>

          </Form>
        </Segment>
      </div>
    )
  }
}
