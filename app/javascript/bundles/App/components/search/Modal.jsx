import React, { PureComponent } from 'react'
import PropType from 'prop-types'
import {
  Button,
  Dimmer,
  Grid,
  Icon,
  Loader,
  StyleSheet,
  Responsive,

  Label,
} from 'semantic-ui-react'
import ReactModal from 'react-modal'

import history from '../../lib/history'
import { AreaMobile, AreaPc } from '../../containers/search/AreaContainer'
import { DetailMobile, DetailPc } from '../../containers/search/DetailContainer'


export default class Modal extends PureComponent {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <div>
        <Responsive minWidth={992} as={ModalPc} {...this.props} />
        <Responsive maxWidth={991} as={ModalMobile} {...this.props} />
      </div>
    )
  }
}

class ModalBase extends PureComponent {
  constructor(props) {
    super(props)
    console.log('Modal constructor')
    props.init()
  }

  componentDidMount() {
    console.log('Modal did mount')
  }

  componentWillMount() {
    ReactModal.setAppElement('body')
  }

  goToSearch = () => {
    this.props.onClickSearch()
  }
}

class ModalPc extends ModalBase {

  render() {
    const {
      isOpen,
      fetching,

      searchType,
      childModal,

      totalCount,
      closeSearchModal,
      closeChildModal,
      changeModal,
    } = this.props
    console.log('searchType', searchType)
    console.log('childModal', childModal)
    return (
      <ReactModal
        isOpen={isOpen}
        contentLabel='Modal'
        style={stylesPc.modal}
        closeTimeoutMS={200}
      >
        <Dimmer.Dimmable as='div' dimmed={fetching} style={style.popupStyle}>

          <Dimmer simple inverted active={fetching}><Loader /></Dimmer>

          <SearchHeaderModal searchType={searchType}
                             closeModal={closeSearchModal}
          />

          <div style={{ marginBottom: '20px' }}>
            <SearchPc searchType={childModal || searchType} />
          </div>

          <SearchFooterModal count={totalCount}
                             search={this.goToSearch}
                             searchType={searchType}
                             childModal={childModal}
                             closeChildModal={closeChildModal}
                             changeModal={changeModal}
          />

        </Dimmer.Dimmable>
      </ReactModal>
    )
  }
}

class SearchHeaderModal extends PureComponent {

  render() {
    const { searchType, search, closeModal } = this.props
    let color, title, icon
    if(searchType == 'detail') {
      color = 'pink'
      title = '希望条件で探す'
    }
    else if(searchType == 'area') {
      color = 'green'
      title = 'エリアで探す'
    }

    return (
      <Grid padded verticalAlign='middle'>
        <Grid.Row color={color} style={stylesPc.header}>
          <Grid.Column width={6} style={stylesPc.headerIcon}>
            {icon ? (
               <Icon name={icon} size='big'/>
            ) : (
               <i className='areaIcon' />
            )}
            {title}
          </Grid.Column>
          <Grid.Column width={8}>
            {/*
                <Button style={{
                height: 55,
                backgroundColor: '#fff',
                color: '#f36580',
                width: 300,
                fontSize: '1.3rem'
                }} onClick={() => search()}><Icon name='search' />この条件で検索する</Button>
              */}
          </Grid.Column>
          <Grid.Column width={2}
                       textAlign='right'
                       style={stylesPc.headerCloseButton}
                       onClick={() => closeModal()}>
            <Icon name='close'
                  size='huge'
                  style={stylesPc.headerCloseIcon}
                  inverted />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    )
  }
}

class SearchFooterModal extends PureComponent {
  render() {
    const { count, search, searchType, childModal, closeChildModal, changeModal } = this.props
    return (
      <Grid padded
            verticalAlign='middle'
            style={stylesPc.footer}>
        <Grid.Column width={7} style={stylesPc.footerCount}>
          絞り込んだ場合の求人数 <span className='ui text pink' style={stylesPc.footerCountText}>{count}</span> 件
        </Grid.Column>

        <Grid.Column width={5}>
          {childModal ? (
             <Button color='green'
                     style={stylesPc.footerSearchButton}
                     onClick={closeChildModal}
               >決定</Button>
          ) : (
             <Button color='pink'
                     style={stylesPc.footerSearchButton}
                     onClick={search}>
               <Icon name='search' />この条件で検索する
             </Button>
          )}
        </Grid.Column>

        {searchType == 'area' && !childModal && (
           <Grid.Column width={4}>
             <Button color='grey'
                     style={stylesPc.footerSearchButton}
                     onClick={() => changeModal('detail')}
             >条件を追加する</Button>
           </Grid.Column>
        )}
      </Grid>
    )
  }
}


class ModalMobile extends ModalBase {
  render() {
    const {
      isOpen,
      fetching,

      searchType,
      childModal,

      totalCount,
      closeSearchModal,
      closeChildModal,
      changeModal,
    } = this.props

    let title = '希望条件で探す'
    let titleColor = 'blue'
    if(searchType == 'area') {
      title = 'エリアで探す'
      titleColor = 'green'
    }
    console.log('Modal is rendering')
    return (
      <ReactModal
        isOpen={isOpen}
        contentLabel='Modal'
        style={style.modal}
        closeTimeoutMS={200}
      >
        <div style={style.modalInner} >
          <Dimmer.Dimmable as='div' dimmed={fetching} style={style.popupStyle}>

            <Dimmer simple inverted active={fetching}><Loader /></Dimmer>

            {/*Modal Header*/}
            <Grid style={style.header}>
              <Grid.Row
                color={titleColor}
                verticalAlign='middle'
                style={style.headerRow}>
                <Icon name='star' />{title}
                <Icon name='close'
                      size='large'
                      style={{cursor: 'pointer', marginLeft: 'auto'}} onClick={closeSearchModal}/>
              </Grid.Row>
            </Grid>

            {/*Modal Content */}
            <div id='scroll-container' style={style.content}>
              <Search searchType={childModal || searchType} />
            </div>

            {/*Modal Footer*/}
            <Grid padded verticalAlign='middle' style={style.footer}>
              <Grid.Column width={6} style={style.footerTotal}>
                求人数
                <div style={style.footerNumber}>
                  <span className='ui text pink'
                        style={style.footerNumberText}
                  >{totalCount}</span>件
                </div>
              </Grid.Column>
              <Grid.Column width={10} style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', display: 'flex' }}>
                {searchType == 'area' && (
                   <Button color='blue'
                           style={style.footerAddButton}
                           onClick={() => changeModal('detail')}
                     >
                     条件追加
                   </Button>
                )}
                {childModal ? (
                   <Button color='green'
                           style={style.footerSearchButton}
                           onClick={closeChildModal}
                     >決定</Button>
                ) : (
                   <Button color='pink'
                           style={style.footerSearchButton}
                           onClick={this.goToSearch}
                     ><Icon name='search' />検索</Button>
                )}
              </Grid.Column>
            </Grid>
          </Dimmer.Dimmable>
        </div>
      </ReactModal>
    )
  }
}

const Search = (props) => {
  if(props.searchType == 'detail') {
    return <DetailMobile />
  }
  else if(props.searchType == 'area') {
    return <AreaMobile />
  }
  else {
    return null
  }
}


const SearchPc = (props) => {
  if(props.searchType == 'detail') {
    return <DetailPc />
  }
  else if(props.searchType == 'area') {
    return <AreaPc />
  }
  else {
    return null
  }
}

const style = {
  modal: {
    overlay: {
      zIndex: 1002,
    },
    content : {
      top: 0,
      left: 0,
      right: 'auto',
      bottom: 'auto',
      padding: 0,
      width: '100%',
      border: 'none',
      borderRadius: 0,
    }
  },
  modalInner: {
    /*    height: '82vh',*/
  },
  header: {
    margin: 0,
    height: '8vh',
  },
  headerRow: {
    fontSize: '1.4285rem',
    fontWeight: 'bold',
    paddingLeft: '1em',
    alignItems: 'center'
  },
  content: {
    /*    height: '56vh', overflow: 'scroll'*/
    height: '68vh', overflow: 'scroll'
  },
  footer: {
    /*
       height: 95,
     */
    height: '15vh',
    backgroundColor: '#eeeeee',
    borderBottomLeftRadius: '4px',
    borderBottomRightRadius: '4px',
    /*              position: 'fixed',
       bottom: 0,
       width: '100%'
     */
  },
  footerSearchButton: {
    flex: 1,
    height: 55,
    color: '#fff',
    /*    width: '100%', */
    padding: '5px'
  },
  footerAddButton: {
    flex: 1,
    height: 55,
    color: '#fff',
    padding: '5px'
  },
  footerTotal: {
    fontSize: '1.3rem',
    fontWeight: 'bold',
    color: '#000',
    fontSize: '1rem',
/*    paddingLeft: '0px',*/
    display: 'flex',
    alignItems: 'flex-begin'
  },
  footerNumber: {
    lineHeight: '40px'
  },
  footerNumberText: {
    fontSize: '30px'
  },
  popupStyle: {
    /*    position: 'fixed', */
    /*    top: '0', */
    zIndex: '10000',
    /*    background: '#fff',
       left: '0',*/
    /*    overflowY: 'scroll',
       width: '100%',
       height: '100%',
       paddingBottom: '95px',
       paddingTop: '65px'
     */
  }
}

const stylesPc = {
  modal: {
    overlay: {
      position: 'fixed',
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      backgroundColor: 'rgba(50, 50, 50, 0.75)'
    },
    content : {
      padding: 0,
      width: '900px',
      /*height: '812px',*/
      border: 'none',
      borderRadius: 0,
      margin: '10px auto 10px auto',
      bottom: 'auto',
    }
  },
  header: {
    height: '77px',
  },
  headerIcon: {
    fontSize: '1.3rem',
    fontWeight: 'bold',
    color: '#fff',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center'
  },
  headerCloseButton: {
    cursor: 'pointer'
  },
  headerCloseIcon: {
    fontSize: '2.5em'
  },
  footer: {
    marginTop: '10px',
    height: '95px',
    backgroundColor: '#eeeeee',
    borderBottomLeftRadius: '4px',
    borderBottomRightRadius: '4px',
    justifyContent: 'space-between'
  },
  footerCount: {
    fontSize: '1.3rem',
    fontWeight: 'bold',
    color: '#000'
  },
  footerCountText: {
    fontSize: '40px'
  },
  footerSearchButton: {
    height: 55,
    color: '#fff',
    width: '100%',
    fontSize: '1.3rem'
  },
  footerAddConditionButton: {
    height: 55,
    color: '#fff',
    width: '100%',
    fontSize: '1.3rem'
  },
}
