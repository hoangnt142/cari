import React, { PureComponent } from 'react'
import PropType from 'prop-types'
import {
  Icon,
  Checkbox,
  StyleSheet,
  Grid,
  Label,
  Button,

  Responsive,
  Header,
  List,
} from 'semantic-ui-react'

import { Element } from 'react-scroll'
import MobileBoxTitle from '../mobile/_BoxTitle'
import { selectedPrefecture, selectedAreaNames } from '../../lib/search'

class Detail extends PureComponent {

  constructor(props) {
    super(props)
    props.init()
  }

  openArea = () => this.props.openChildModal('area')
}


export class DetailPc extends Detail {
  render() {
    const {
      prefectures,
      area1s,
      area2s,
      area3s,
      condition,

      qualifications,
      jobContracts,
      hospitalTypes,
      jobTypes,
      jobDetails,
      hospitals,

      /* actions from searchModalActions */
      onChangeJobContract,
      onChangeQualification,
      onChangeHospitalType,
      onChangeJobType,
      onChangeJobDetail,
      onChangePage,
      openChildModal,
    } = this.props

    const prefecture = selectedPrefecture(prefectures, condition)
    const areaNames = selectedAreaNames(area1s, area2s, area3s, condition)

    return (
      <div style={{ padding: '0 2rem 2rem 2rem', height: '620px' }}>
        <Grid padded>
          <Grid.Row>
            <ShortBoxTitle title='路線またはエリア(都道府県)の設定' />
          </Grid.Row>
          <Grid.Row style={{ marginTop: 0, paddingTop: 0 }}>
            <Grid.Column width={16} style={{paddingLeft: 0, paddingRight: 0, paddingTop: 0}}>
              <div style={style.changeScreen}>
                <Label color='green' style={style.changeScreenItem}>
                  <i className='areaIcon' />
                  エリア
                </Label>
                {prefecture && areaNames.length == 0 ? (
                   prefecture.name
                ) : (
                   areaNames.length > 0 && areaNames.join('、')
                )}
                <Button style={style.changeScreenButton}
                        onClick={this.openArea}
                        color='grey'>選択</Button>
              </div>
            </Grid.Column>

          </Grid.Row>

          <SearchKeyPcRow searchKeys={jobContracts}
                          selectedIds={condition.jobContractIds}
                          title='雇用形態'
                          onChange={onChangeJobContract} />
          <SearchKeyPcRow searchKeys={qualifications}
                          selectedIds={condition.qualificationIds}
                          title='保有資格'
                          onChange={onChangeQualification} />
          <SearchKeyPcRow searchKeys={hospitalTypes}
                          selectedIds={condition.hospitalTypeIds}
                          title='病院・施設'
                          onChange={onChangeHospitalType} />
          <SearchKeyPcRow searchKeys={jobTypes}
                          selectedIds={condition.jobTypeIds}
                          title='仕事内容'
                          onChange={onChangeJobType} />
          <SearchKeyPcRow searchKeys={jobDetails}
                          selectedIds={condition.jobDetailIds}
                          title='こだわり条件'
                          onChange={onChangeJobDetail} />

        </Grid>
      </div>
    )
  }
}

export class DetailMobile extends Detail {
  render() {
    const {
      prefectures,
      area1s,
      area2s,
      area3s,
      condition,

      /* state of the searchModalReducer */
      qualifications,
      jobContracts,
      hospitalTypes,
      jobTypes,
      jobDetails,
      hospitals,

      /* actions from searchModalActions */
      onChangeJobContract,
      onChangeQualification,
      onChangeHospitalType,
      onChangeJobType,
      onChangeJobDetail,
      onChangePage,
      openChildModal,
    } = this.props

    const prefecture = selectedPrefecture(prefectures, condition)
    const areaNames = selectedAreaNames(area1s, area2s, area3s, condition)

    return (
      <Grid padded>
        <Grid.Row>
          <Grid.Column width={16}>
            <div style={style.changeScreen}>
              <Label color='green' style={style.changeScreenItem}>
                <i className='areaIcon' />
                エリア
              </Label>
              {prefecture && areaNames.length == 0 ? (
                 prefecture.name
              ) : (
                 areaNames.length > 0 && areaNames.join('、')
              )}
              <Button style={style.changeScreenButton}
                      onClick={this.openArea}
                      color='grey'>選択</Button>
            </div>

          </Grid.Column>
        </Grid.Row>

        <SearchKeyMobileRow searchKeys={jobContracts}
                            selectedIds={condition.jobContractIds}
                            title='雇用形態'
                            checkedMark='jobContract'
                            onChange={onChangeJobContract} />
        <SearchKeyMobileRow searchKeys={qualifications}
                            selectedIds={condition.qualificationIds}
                            title='保有資格'
                            checkedMark='qualification'
                            onChange={onChangeQualification} />
        <SearchKeyMobileRow searchKeys={hospitalTypes}
                            selectedIds={condition.hospitalTypeIds}
                            title='病院・施設'
                            checkedMark='hospitalType'
                            onChange={onChangeHospitalType} />
        <SearchKeyMobileRow searchKeys={jobTypes}
                            selectedIds={condition.jobTypeIds}
                            title='仕事内容'
                            checkedMark='jobType'
                            onChange={onChangeJobType} />
        <SearchKeyMobileRow searchKeys={jobDetails}
                            selectedIds={condition.jobDetailIds}
                            title='こだわり条件'
                            checkedMark='jobDetail'
                            onChange={onChangeJobDetail} />

      </Grid>
    )
  }
}




class SearchKeyPcRow extends PureComponent {

  checked = (field, value) => {
    return (this.props.selectedIds || []).indexOf(value) != -1
  }

  render() {
    const { searchKeys, title, checkedMark, onChange } = this.props
    return (
      <div style={{width: '100%', padding: 0}}>
        <Grid.Row>
          <ShortBoxTitle title={title} as='h3' />
        </Grid.Row>
        <Grid.Row style={{ paddingTop: '0.3em', paddingBottom: '0.3em' }}>
          {<List horizontal>
             {searchKeys && searchKeys.map(sk =>
               <List.Item key={sk.id} style={{lineHeight: '25px', marginLeft: 0, marginRight: '1rem'}}>
                 <Checkbox
                   label={sk.name}
                   onChange={onChange}
                   value={sk.id}
                   checked={this.checked(checkedMark, sk.id)}
                 />
               </List.Item>
             )}
           </List>
          }
        </Grid.Row>
      </div>
    )
  }
}
class SearchKeyMobileRow extends PureComponent {

  checked = (field, value) => {
    return (this.props.selectedIds || []).indexOf(value) != -1
  }

  render() {
    const { searchKeys, title, checkedMark, onChange } = this.props
    return (
      <Element name={checkedMark} style={{width: '100%', padding: 0}}>
        <Grid.Row>
          <MobileBoxTitle title={title} as='h3'/>
        </Grid.Row>
        <Grid.Row style={{ paddingTop: '0.3em', paddingBottom: '0.3em', paddingLeft: '0.4em', paddingRight: '0.4em' }}>
          {searchKeys && searchKeys.map(sk =>
            <Grid.Column key={sk.id} width={8} style={{lineHeight: '25px', marginLeft: 0, marginRight: '1rem'}}>
              <Checkbox
                label={sk.name}
                onChange={onChange}
                value={sk.id}
                checked={this.checked(checkedMark, sk.id)}
              />
            </Grid.Column>
          )}
          {searchKeys.length == 1 &&
           <Grid.Cell></Grid.Cell>
          }
        </Grid.Row>
      </Element>
    )
  }
}

class ShortBoxTitle extends PureComponent {
  render() {
    const { as, icon, top, title, leftColor, rightNode, style, iconStyle} = this.props
    return (
      <Grid padded verticalAlign='middle' style={{width: '100%'}}>
        <Grid.Row style={{paddingBottom: 0, paddingTop: '0.3em'}}>
          <Grid.Column width={rightNode ? 12 : 16} style={{paddingLeft: 0}}>
            <Header as={as || 'div'} style={Object.assign({display: 'flex'}, style || {} )}>
              {icon && icon != 'hospital' &&
               <Icon name={icon} size='small' />
              }
              {icon == 'hospital' &&
               <i className='hospitalIcon' style={iconStyle || {width: 40, minWidth: 40, height: 31}}/>
              }
              {title}
            </Header>
          </Grid.Column>
          <Responsive as={Grid.Column} minWidth={992} computer={4} textAlign='right' style={{paddingRight: 0}} >
            {rightNode}
          </Responsive>
        </Grid.Row>
        <Grid.Row style={{ paddingTop: '0.3em', paddingBottom: '0.3em' }}>
          <div style={{display: 'flex', width: '100%', height: '2px', flexDirection: 'row', fontWeight: 'bold'}}>
            <div style={{flex: 25, backgroundColor: leftColor || 'rgb(243,101,129)'}}></div>
            <div style={{flex: 75, backgroundColor: '#dcddde'}}></div>
          </div>
        </Grid.Row>
      </Grid>
    )
  }
}





const style = {
  changeScreen: {
    padding: '0 0.5rem 0 0',
    border: '1px solid #ccc',
    display: 'flex',
    alignItems: 'center',
    /*    height: '95px', */
    fontSize: '1em',
    marginBottom: '0.5rem'
  },
  changeScreenItem: {
    border: 'none',
    borderRadius: '0px',
    /*    height: '100%', */
    width: '90px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: '18px',
    marginRight: '1rem'

  },
  changeScreenButton: {
    marginLeft: 'auto',
    /*fontSize: '18px'*/
  },
}
