import React, { PureComponent } from 'react'
import PropType from 'prop-types'
import { Link } from 'react-router-dom'

import {
  Button,
  Image,
  Icon,
  Checkbox,
  Grid,
  Header,
  List,
  Responsive,
  Container,
} from 'semantic-ui-react'
import { selectedPrefecture, selectedAreaNames } from '../../lib/search'
import { numberOfConditions } from '../../lib/search'

export class ConditionPc extends PureComponent {

  checked = (field, value) => {
    try {
      return this.props[field].indexOf(value) != -1
    } catch (err) {
      return false
    }
  }

  onChangeArea = (area) => (e, data) => {
    this.props.onChangeArea(area, data.checked)
  }

  render () {
    console.log('render')
    const {
      regions,
      prefectures,
      area1s,
      area2s,
      area3s,

      qualifications,
      jobContracts,
      hospitalTypes,
      jobTypes,
      jobDetails,

      condition,

      openSearchModal,

      onChangeJobContract,
      onChangeQualification,
      onChangeHospitalType,
      onChangeJobType,
      onChangeJobDetail,
      onChangePage,
    } = this.props

    const prefecture = selectedPrefecture(prefectures, condition)
    const areaNames = selectedAreaNames(area1s, area2s, area3s, condition)

    return (
      <Grid divided='vertically' style={{fontWeight: 'bold', fontSize: '17px'}}>

        <Grid.Row style={{paddingBottom: 0, paddingTop: 0}}>
          <Grid.Column mobile={16} computer={3} style={stylePc.iconGroup}>
            <Icon name='marker' size='large' style={{color: '#c8c8c8'}}/>
            勤務地
          </Grid.Column>
          <Grid.Column mobile={16} computer={13}>

            <Button onClick={() => openSearchModal('area')} color='green' style={{marginRight: '2rem'}}>エリアで設定</Button>

            <List horizontal>
              <List.Item>
                {prefecture && areaNames.length == 0 && (
                   prefecture.name
                )}
                {areaNames.length > 0 && (
                   areaNames.join('、')
                )}
              </List.Item>
            </List>

            {/*
                <List horizontal>
                {prefecture && (
                <List.Item>
                <Checkbox label={prefecture.name}
                checked={prefecture.id == prefectureId}
                value={prefecture.id}
                onChange={onChangePrefecture}
                />
                </List.Item>
                )}
                {areaWithAncestors && areaWithAncestors.map((awa, i) =>
                <List.Item key={awa.id}>
                <Checkbox label={awa.name}
                checked={indexAreaChecked >= 0 && i <= indexAreaChecked}
                value={awa.id}
                onChange={this.onChangeArea(awa)}
                />
                </List.Item>
                )}
                </List>
              */}
          </Grid.Column>
        </Grid.Row>

        <Grid.Row style={stylePc.notPadding}>
          <Grid.Column computer={3} mobile={16} style={stylePc.iconGroup}>
            <i className='clockIcon' />
            雇用形態
          </Grid.Column>
          <SearchKeyPcRow searchKeys={jobContracts}
                          selectedIds={condition.jobContractIds}
                          title='雇用形態'
                          onChange={onChangeJobContract} />

        </Grid.Row>
        <Grid.Row style={stylePc.notPadding}>
          <Grid.Column computer={3} mobile={16} style={stylePc.iconGroup}>
            <i className='nurseIcon'/>
            保有資格
          </Grid.Column>
          <SearchKeyPcRow searchKeys={qualifications}
                          selectedIds={condition.qualificationIds}
                          title='保有資格'
                          onChange={onChangeQualification} />

        </Grid.Row>
        <Grid.Row style={stylePc.notPadding}>
          <Grid.Column computer={3} mobile={16} style={stylePc.iconGroup}>
            <Icon name='hospital' style={{color: '#c8c8c8', fontSize: '1.2rem'}}/>
            病院・施設
          </Grid.Column>
          <SearchKeyPcRow searchKeys={hospitalTypes}
                          selectedIds={condition.hospitalTypeIds}
                          title='病院・施設'
                          onChange={onChangeHospitalType} />

        </Grid.Row>
        <Grid.Row style={stylePc.notPadding}>
          <Grid.Column computer={3} mobile={16} style={stylePc.iconGroup}>
            <i className='jobDescriptionIcon' />
            仕事内容
          </Grid.Column>
          <SearchKeyPcRow searchKeys={jobTypes}
                          selectedIds={condition.jobTypeIds}
                          title='仕事内容'
                          onChange={onChangeJobType} />

        </Grid.Row>
        <Grid.Row>
          <Grid.Column computer={8} mobile={12}>
          </Grid.Column>
          <Grid.Column computer={2} mobile={4} style={{padding: 0}}>
          </Grid.Column>
          <Responsive as={Grid.Column} width={6} minWidth={992} textAlign='right'>
            <Button onClick={() => openSearchModal('detail')}
                    color='pink'
                    size='large'>
              <Icon name='star'/>
              希望条件で探す
            </Button>
          </Responsive>
        </Grid.Row>
      </Grid>
    )
  }
}

class SearchKeyPcRow extends PureComponent {

  checked = (field, value) => {
    return (this.props.selectedIds || []).indexOf(value) != -1
  }

  render() {
    const { searchKeys, title, checkedMark, onChange } = this.props
    return (
      <Grid.Column computer={13} mobile={16}>
        {<List horizontal>
          {searchKeys && searchKeys.map(sk =>
            <List.Item key={sk.id} style={{lineHeight: '25px', marginLeft: 0, marginRight: '1rem'}}>
              <Checkbox
                label={sk.name}
                onChange={onChange}
                value={sk.id}
                checked={this.checked(checkedMark, sk.id)}
              />
            </List.Item>
          )}
        </List>
        }
      </Grid.Column>
    )
  }
}



export class ConditionMobile extends PureComponent {

  constructor(props) {
    super(props)
    this.state = {
      isOpen: false
    }
  }

  reset = () => { this.props.reset() }
  openModalWithPosition = (position) => { this.props.openSearchModal('detail', position) }

  render () {
    const {
      prefectures,
      area1s,
      area2s,
      area3s,

      qualifications,
      jobContracts,
      hospitalTypes,
      jobTypes,
      jobDetails,
      condition,
      deleteCondition,
      openSearchModal
    } = this.props

    const countCondition = numberOfConditions(condition)

    return (
      <div>
        <SelectedConditionBlock
          prefectures={prefectures}
          area1s={area1s}
          area2s={area2s}
          area3s={area3s}

          qualifications={qualifications}
          jobContracts={jobContracts}
          hospitalTypes={hospitalTypes}
          jobTypes={jobTypes}
          jobDetails={jobDetails}
          condition={condition}
          deleteCondition={deleteCondition}
          openModalWithPosition={this.openModalWithPosition}
          countCondition={countCondition}
        />

        <div style={{ marginTop: '1em',
                      display: 'flex',
                      flexDirection: 'row',
                      justifyContent: 'center',
                      alignItems: 'center'
        }}>
          <Button style={styleMobile.conditionButton}
                  onClick={() => openSearchModal('detail')}
          >
            <Icon name='star' color='blue' />
            条件を変更・追加
          </Button>
          {countCondition > 0 && (
             <a onClick={this.reset} style={{ cursor: 'pointer' }}><Icon name='close' />リセット</a>
          )}
        </div>
      </div>
    )
  }
}

class SelectedConditionBlock extends PureComponent {

  constructor(props) {
    super(props)
  }

  openModalArea = () => this.props.openModalWithPosition('area')
  openModalQualification = () => this.props.openModalWithPosition('qualification')
  openModalJobContract = () => this.props.openModalWithPosition('jobContract')
  openModalHospitalType = () => this.props.openModalWithPosition('hospitalType')
  openModalJobType = () => this.props.openModalWithPosition('jobType')
  openModalJobDetail = () => this.props.openModalWithPosition('jobDetail')
/*
  deleteConditionPrefecture = () => this.props.deleteCondition('prefectureIds')
  deleteConditionQualification = () => this.props.deleteCondition('qualificationIds')
  deleteConditionJobContract = () => this.props.deleteCondition('jobContractIds')
  deleteConditionHospitalType = () => this.props.deleteCondition('hospitalTypeIds')
  deleteConditionJobType = () => this.props.deleteCondition('jobTypeIds')
  deleteConditionJobDetail = () => this.props.deleteCondition('jobDetailIds')
*/

  render () {

    const {
      prefectures,
      area1s,
      area2s,
      area3s,

      qualifications,
      jobContracts,
      hospitalTypes,
      jobTypes,
      jobDetails,
      condition,
      countCondition
    } = this.props

    if(countCondition == 0) {
      return null
    }
    else if(countCondition == 1) {
      return (
        <Grid padded>
          <Grid.Row style={{ paddingTop: '1rem', paddingBottom: '0' }}>
            <Grid.Column width={8} stretched style={{ paddingRight: '0.5rem' }}>
              <Button
                color='lightgrey'
                style={styleMobile.gradientButton}
                onClick={this.openModalArea}
              ><Icon name='map marker alternate' />都道府県で探す</Button>
            </Grid.Column>
            <Grid.Column width={8} stretched style={{ paddingLeft: '0.5rem' }}>
              <Button
                color='lightgrey'
                style={styleMobile.gradientButton}
                onClick={this.openModalJobType}
              ><Icon name='suitcase' />仕事内容で探す</Button>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row style={{ paddingTop: '1rem', paddingBottom: '0' }}>
            <Grid.Column width={8} stretched style={{ paddingRight: '0.5rem' }}>
              <Button
                color='lightgrey'
                style={styleMobile.gradientButton}
                onClick={this.openModalJobContract}
              ><Icon name='file alternate' />雇用形態で探す</Button>
            </Grid.Column>
            <Grid.Column width={8} stretched style={{ paddingLeft: '0.5rem' }}>
              <Button
                color='lightgrey'
                style={styleMobile.gradientButton}
                onClick={this.openModalHospitalType}
              ><Icon name='hospital' />病院施設で探す</Button>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row style={{ paddingTop: '1rem', paddingBottom: '0' }}>
            <Grid.Column width={8} stretched style={{ paddingRight: '0.5rem' }}>
              <Button
                color='lightgrey'
                style={styleMobile.gradientButton}
                onClick={this.openModalJobDetail}
              ><Icon name='hand point up' />こだわりで探す</Button>
            </Grid.Column>
            <Grid.Column width={8} stretched style={{ paddingLeft: '0.5rem' }}>
              <Button
                style={styleMobile.gradientButton}
                onClick={this.openModalQualification}
              ><Icon name='certificate' />保有資格で探す</Button>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      )
    }
    else {
      let prefectureText  = null
      //areaWithAncestors: [],
      let qualificationText  = null
      let jobContractText  = null
      let hospitalTypeText  = null
      let jobTypeText  = null
      let jobDetailText  = null

      const selectedSKText = (field, fieldCapital) => {
        const skText = this.props[field]
                           .filter(sk => (condition[fieldCapital]||[]).indexOf(sk.id) != -1)
                           .map(sk => sk.name).join(',')
        return skText
      }


      const prefecture = selectedPrefecture(prefectures, condition)
      const areaNames = selectedAreaNames(area1s, area2s, area3s, condition)
      if(prefecture && areaNames.length == 0) {
        prefectureText = prefecture.name
      }
      else if(areaNames.length > 0) {
        prefectureText = areaNames.join('、')
      }


      if(qualifications) {
        qualificationText = selectedSKText('qualifications', 'qualificationIds')
      }
      if(jobContracts) {
        jobContractText = selectedSKText('jobContracts', 'jobContractIds')
      }
      if(hospitalTypes) {
        hospitalTypeText = selectedSKText('hospitalTypes', 'hospitalTypeIds')
      }
      if(jobTypes) {
        jobTypeText = selectedSKText('jobTypes', 'jobTypeIds')
      }
      if(jobDetails) {
        jobDetailText = selectedSKText('jobDetails', 'jobDetailIds')
      }

      return (
        <Grid style={{
          border: '1px solid #ccc',
          margin: '0.4em',
          borderRadius: '0.5em',
        }}>
          {prefectureText && (
             <SelectedCondition title='勤務地'
                                icon={<Icon name='marker' size='large' style={{color: '#c8c8c8'}}/>}
                                onClick={this.openModalArea}
                                text={prefectureText} />
          )}
          {qualificationText && (
             <SelectedCondition title='保有資格'
                                icon={<i className='nurseIcon'/>}
                                onClick={this.openModalQualification}
                                text={qualificationText} />
          )}
          {jobContractText && (
             <SelectedCondition title='雇用形態'
                                icon={<i className='clockIcon' />}
                                onClick={this.openModalJobContract}
                                text={jobContractText} />
          )}
          {hospitalTypeText && (
             <SelectedCondition title='病院施設'
                                icon={<Icon name='hospital' style={{color: '#c8c8c8', fontSize: '1.2rem'}}/>}
                                onClick={this.openModalHospitalType}
                                text={hospitalTypeText} />
          )}
          {jobTypeText && (
             <SelectedCondition title='仕事内容'
                                icon={<i className='jobDescriptionIcon' />}
                                onClick={this.openModalJobType}
                                text={jobTypeText} />
          )}
          {jobDetailText && (
             <SelectedCondition title='その他こだわり'
                                icon={<Icon name='back' />}
                                onClick={this.openModalJobDetail}
                                text={jobDetailText}
                                noIcon
             />
          )}
        </Grid>
      )
    }
  }
}


class SelectedCondition extends PureComponent {

  render() {
    const { icon, noIcon, title, text, onClick } = this.props

    if(noIcon) {
      return (
        <Grid.Row style={styleMobile.conditionRow}>
          <Grid.Column width={5} style={styleMobile.conditionColumnCenterBold}>
            {title}
          </Grid.Column>
          <Grid.Column width={7} style={styleMobile.conditionColumn}>
            {text}
          </Grid.Column>
          <Grid.Column width={4} style={styleMobile.conditionColumnCenter}>
            {/*<a onClick={onClick} style={{ cursor: 'pointer' }}>取り消し<Icon name='arrow right' /></a>*/}
            <a onClick={onClick} style={{ cursor: 'pointer' }}>変更する<Icon name='chevron right' /></a>
          </Grid.Column>
        </Grid.Row>
      )
    }
    else {
      return (
        <Grid.Row style={styleMobile.conditionRow}>
          <Grid.Column width={2} style={styleMobile.conditionColumnCenter}>
            {icon}
          </Grid.Column>
          <Grid.Column width={3} style={styleMobile.conditionColumnBold}>
            {title}
          </Grid.Column>
          <Grid.Column width={7} style={styleMobile.conditionColumn}>
            {text}
          </Grid.Column>
          <Grid.Column width={4} style={styleMobile.conditionColumnCenter}>
            {/*<a onClick={onClick} style={{ cursor: 'pointer' }}>取り消し<Icon name='arrow right' /></a>*/}
            <a onClick={onClick} style={{ cursor: 'pointer' }}>変更する<Icon name='chevron right' /></a>
          </Grid.Column>
        </Grid.Row>
      )
    }
  }
}



const stylePc =  {
  iconGroup: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center'
  },
  notPadding: {
    padding: 0
  },
  notMargin: {
    margin: 0
  }
}


const styleMobile =  {
  gradientButton: {
    background: 'linear-gradient(180deg, #fff 14.52%, #e6e7e8)',
    height: '4em',
    border: '1px solid #ccc',
  },
  conditionButton: {
    background: 'linear-gradient(180deg, #fff 14.52%, #e6e7e8)',
    border: '1px solid #ccc',
    width: '12em',
    paddingRight: 0,
    paddingLeft: 0,
  },
  conditionRow: {
    padding: '0.5em 0',
    borderBottom: '1px solid #ccc',
  },

  conditionColumn: {
    paddingLeft: '0.1em',
    paddingRight: '0.1em',
  },

  conditionColumnCenter: {
    paddingLeft: '0.1em',
    paddingRight: '0.1em',
    textAlign: 'center',
  },

  conditionColumnBold: {
    paddingLeft: '0.1em',
    paddingRight: '0.1em',
    fontWeight: 'bold',
  },

  conditionColumnCenterBold: {
    paddingLeft: '0.1em',
    paddingRight: '0.1em',
    textAlign: 'center',
    fontWeight: 'bold',
  },

}
