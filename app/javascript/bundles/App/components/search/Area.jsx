import React, { PureComponent } from 'react'
import PropType from 'prop-types'
import {
  Button,
  Image,
  Icon,
  Checkbox,
  List,
  Header,
  Radio,
  Loader,
  Dimmer,
  Grid,
} from 'semantic-ui-react'
import BoxTitle from '../_BoxTitle'


const extractPrefecturesInRegion = (prefectures, regionId) => prefectures.filter(p => p.region_id == regionId)

const extractSelectedPrefecturesWithChildren = (prefectures, area1s, prefectureId) => {
  const prefecture = prefectures.find(p => p.id == prefectureId)
  prefecture.children = area1s
  return [prefecture]
}

const extractSelectedArea1sWithChildren = (area1s, area2s, areaIds) => {
  const selectedArea1s = area1s.filter(a1 => areaIds.find(id => id == a1.id))
  selectedArea1s.forEach(area1 => {
    const area2sInArea1 = area2s.filter(a => a.parent_id == area1.id)
    area1.children = area2sInArea1
  })
  return selectedArea1s
}

const extractSelectedArea2sWithChildren = (area1s, area2s, area3s, areaIds) => {
  const selectedArea2s = area2s.filter(a2 => areaIds.find(id => id == a2.id))
  selectedArea2s.forEach(area2 => {
    const area3sInArea2 = area3s.filter(a => a.parent_id == area2.id)
    area2.children = area3sInArea2
  })
  return selectedArea2s
}

const area1sWithChildren = (area1s, area2s) => {
  area1s.forEach(a1 => {
    const area2sInArea1 = area2s.filter(a2 => a2.parent_id == a1.id)
    a1.children = area2sInArea1
  })
  return area1s
}

const area2sWithChildrenInSelectedArea1 = (area2s, area3s, areaIds) => {
  const area2sInSelectedArea1 = area2s.filter(a => areaIds.find(id => id == a.parent_id))
  area2sInSelectedArea1.forEach(a2 => {
    const area3sInArea2 = area3s.filter(a3 => a3.parent_id == a2.id)
    a2.children = area3sInArea2
  })
  return area2sInSelectedArea1
}


export class AreaPc extends PureComponent {

//  onChangeArea = (id) => this.props.onChangeArea(id, false)

  render() {
    const {
      regions,
      prefectures,
      area1s,
      area2s,
      area3s,

      regionId,
      prefectureId,
      areaIds,

      onChangeRegion,
      onChangePrefecture,
      onChangeArea,
      area1InTokyo,

    } = this.props


    let prefectureInRegion = []
    let prefecturesWithChildren = []
    let area1sWithChildren = []
    let area2sWithChildren = []
    let selectedPrefecture = null
    let selectedArea1 = null

    if(regionId) {
      prefectureInRegion = extractPrefecturesInRegion(prefectures, regionId)
    }
    if(prefectureId) {
      prefecturesWithChildren = extractSelectedPrefecturesWithChildren(prefectures, area1s, prefectureId)
      area1sWithChildren = extractSelectedArea1sWithChildren(area1s, area2s, areaIds)
      area2sWithChildren = extractSelectedArea2sWithChildren(area1s, area2s, area3s, areaIds)

      selectedPrefecture = prefecturesWithChildren.find(p => p.id == prefectureId)
      selectedArea1 = area1sWithChildren.find(a => areaIds.find(id => id == a.id))
    }

    return (
      <div style={{ padding: '0 2rem 2rem 2rem' }}>

        {regions && regions.length > 0 && (
           <SingleSelectPcView
             boxTitle='地域選択'
             title={null}
             list={regions}
             selectedIds={[regionId]}
             onSelect={onChangeRegion}
           />
        )}

        {prefectureInRegion.length > 0 && (
           <SingleSelectPcView
             boxTitle='都道府県を選択'
             title={null}
             list={prefectureInRegion}
             selectedIds={[prefectureId]}
             onSelect={onChangePrefecture}
           />
        )}

        {prefectureId && prefectureId == 13 /* Tokyo */ && (
           <div>
             <SingleSelectPcView
               boxTitle='大エリアを選択'
               titleBase={null}
               list={area1s}
               selectedIds={areaIds}
               onSelect={onChangeArea}
             />

             {area1sWithChildren.length > 0 && (
                <div>
                  <MultiSelectPcView
                    boxTitle='中エリアを選択'
                    titleBase={`${selectedPrefecture.name}`}
                    list={area1sWithChildren}
                    selectedIds={areaIds}
                    onSelect={onChangeArea}
                  />

                  {area2sWithChildren.length > 0 && (
                     <MultiSelectPcView
                       boxTitle='小エリアを選択'
                       titleBase={`${selectedPrefecture.name} > ${selectedArea1.name}`}
                       list={area2sWithChildren}
                       selectedIds={areaIds}
                       onSelect={onChangeArea}
                     />
                  )}
                </div>
             )}
           </div>
        )}
        {prefectureId && prefectureId != 13 /* Tokyo */ && (
           <div>
             <MultiSelectPcView
               boxTitle='大エリアを選択'
               titleBase={null}
               list={prefecturesWithChildren}
               selectedIds={areaIds}
               onSelect={onChangeArea}
             />

             {area1sWithChildren.length > 0 && (
                <MultiSelectPcView
                  boxTitle='中エリアを選択'
                  titleBase={`${selectedPrefecture.name}`}
                  list={area1sWithChildren}
                  selectedIds={areaIds}
                  onSelect={onChangeArea}
                />
             )}
           </div>
        )}
      </div>
    )
  }
}

class SingleSelectPcView extends PureComponent {

  onSelect = (id) => () => this.props.onSelect(id)

  render() {
    const {
      boxTitle,
      title,
      list,
      selectedIds,
    } = this.props
    return (
      <div>
        <BoxTitle title={boxTitle} />
        {title && title.length > 0 && (
           <Header as='h3' color='pink' size='medium'>{title}</Header>
        )}
        <List horizontal>
          {list.map(item =>
            <List.Item
              key={item.id}
              style={{lineHeight: '25px', marginLeft: 0, marginRight: '1rem'}}
              >
              <Radio
                label={item.name}
                checked={selectedIds.find(id => id == item.id)}
                value={item.id}
                onChange={this.onSelect(item.id)}
              />
            </List.Item>
          )}
        </List>
      </div>
    )
  }
}

class MultiSelectPcView extends PureComponent {

  onSelect = (id) => () => this.props.onSelect(id)

  render() {
    const {
      boxTitle,
      titleBase,
      list,
      selectedIds,
    } = this.props
    const hasItem = list.some(item => item.children && item.children.length > 0)

    if(!hasItem) {
      return null
    }

    return (
      <div>
        <BoxTitle title={boxTitle} />

        {list.map((item, j) => {
           if(!selectedIds || !item.children || item.children.length == 0) {
             return null
           }

           const title = !titleBase ? item.name : `${titleBase} > ${item.name}`
           return (
             <div key={j}>
               {title && title.length > 0 && (
                  <Header as='h3' color='pink' size='medium'>{title}</Header>
               )}
               <List horizontal>
                 {item.children.map((childItem, i) =>
                   <List.Item key={i} style={{lineHeight: '25px', marginLeft: 0, marginRight: '1rem'}}>
                     <Checkbox label={childItem.name}
                               checked={selectedIds.findIndex(id => id == childItem.id) >= 0}
                               value={childItem.id}
                               onChange={this.onSelect(childItem.id)}
                     />
                   </List.Item>
                 )}
               </List>
             </div>
           )
        })}
      </div>
    )
  }
}







export class AreaMobile extends PureComponent {

  render() {
    const {
      regions,
      prefectures,
      area1s,
      area2s,
      area3s,

      regionId,
      prefectureId,
      areaIds,

      onChangeRegion,
      onChangePrefecture,
      onChangeArea,
      area1InTokyo,

    } = this.props


    let title
    if(prefectureId) {
      const prefecture = prefectures.filter(p => p.id == prefectureId)[0]
      title = `エリア選択「${prefecture.name}」`
    }
    else if(regionId) {
      const region = regions.filter(r => r.id == regionId)[0]
      title = `エリア選択「${region.name}」`
    }
    else {
      title = `エリア選択`
    }


    return (
      <Grid padded>
        {/*Modal Content*/}
        <Grid.Row style={{padding: 0}}>
          <Grid.Column width={16} style={{padding: 0}}>
            <ViewSelector
              regions={regions}
              prefectures={prefectures}
              area1s={area1s}
              area2s={area2s}
              area3s={area3s}
              regionId={regionId}
              prefectureId={prefectureId}
              areaIds={areaIds}
              onChangeRegion={onChangeRegion}
              onChangePrefecture={onChangePrefecture}
              onChangeArea={onChangeArea}
              area1InTokyo={area1InTokyo}
            />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    )
  }
}


class ViewSelector extends PureComponent {

  onChangePrefecture = (regionId) => () => this.props.onChangePrefecture(regionId)

  render() {
    const {
      regions,
      prefectures,
      area1s,
      area2s,
      area3s,

      regionId,
      prefectureId,
      areaIds,

      onChangeRegion,
      onChangePrefecture,
      onChangeArea,
      area1InTokyo,
    } = this.props


    let list
    if(prefectureId) {
      // Area list
      if(prefectureId == 13 /* Tokyo */) {
        const hasArea1 = extractSelectedArea1sWithChildren(area1s, area2s, areaIds)
        if(hasArea1.length > 0) {
          list = area2sWithChildrenInSelectedArea1(area2s, area3s, areaIds)
          console.log('list', list)
          return <MultiSelectView
                   goBackTitle='エリア選択に戻る'
                   list={list}
                   selectedIds={areaIds}
                   onSelect={onChangeArea}
                   onGoBack={() => area1InTokyo()}
          />
        }
        else {
          list = area1s
          return <SingleSelectView
                   goBackTitle='エリア選択に戻る'
                   list={list}
                   onSelect={onChangeArea}
                   onGoBack={() => onChangePrefecture()}
          />
        }
      }
      else {
        list = area1sWithChildren(area1s, area2s)
        return <MultiSelectView
                 goBackTitle='都道府県選択に戻る'
                 list={list}
                 selectedIds={areaIds}
                 onSelect={onChangeArea}
                 onGoBack={() => onChangePrefecture()}
        />
      }
    }
    else if(regionId) {
      // Prefecture list
      list = extractPrefecturesInRegion(prefectures, regionId)
      return <SingleSelectView
               goBackTitle='エリア選択に戻る'
               list={list}
               onSelect={onChangePrefecture}
               onGoBack={() => onChangeRegion()}
      />
    }
    else {
      // Region list
      return <SingleSelectView
               list={regions}
               onSelect={onChangeRegion}
      />
    }
  }
}

class SingleSelectView extends PureComponent {

  onSelect = (id) => () => this.props.onSelect(id)

  render() {
    const {
      goBackTitle,
      list,
      onGoBack,
    } = this.props
    return (
      <List selection divided verticalAlign='middle'>
        {onGoBack && (
           <BackButton title={goBackTitle}
                       reset={onGoBack} />
        )}
        {list && list.map((item, i) =>
          <List.Item
            key={i}
            onClick={this.onSelect(item.id)}
            style={{
              padding: '0 1rem 0 1rem',
              fontWeight: 'bold'
            }}>
            <List.Content floated='right' style={{lineHeight: '65px'}}>
              <Icon floated='right' name='chevron right' color='grey'/>
            </List.Content>
            <List.Content style={{lineHeight: '65px'}}>
              {item.name}
            </List.Content>
          </List.Item>
        )}
      </List>
    )
  }
}

class MultiSelectView extends PureComponent {

  onSelect = (id) => () => this.props.onSelect(id)

  render() {
    const {
      goBackTitle,
      list,
      selectedIds,
      onGoBack,
    } = this.props


    // Create new array flatting the child array
    const flatList = []
    list.forEach(l => {
      l.indent = 0
      flatList.push(l)
      if(l.children && l.children.length > 0) {
        l.children.forEach(c => {
          // Set indent level
          c.indent = 1
          flatList.push(c)
          if(c.children && c.children.length > 0) {
            c.children.forEach(c2 => {
              c2.indent = 2
              flatList.push(c2)
            })
          }
        })
      }
    })

    return (
      <List selection divided verticalAlign='middle'>
        {onGoBack && (
           <BackButton title={goBackTitle}
                       reset={onGoBack} />
        )}
        {flatList && flatList.map((item, i) =>
          <List.Item key={i}
                     onClick={this.onSelect(item.id)}
                     style={{
                       padding: `0 1rem 0 ${1 + item.indent}rem`,
                       fontWeight: 'bold'
                     }}
            >
            <List.Content style={{lineHeight: '65px'}}>
              <Checkbox label={item.name}
                        checked={selectedIds.findIndex(id => id == item.id) >= 0}
                        value={item.id}
              />
            </List.Content>
          </List.Item>
        )}
      </List>
    )
  }
}

class BackButton extends PureComponent {

  render() {
    const { reset } = this.props
    return (
      <List.Item
        onClick={reset}
        style={{
          backgroundColor: '#f2f2f2',
/*          lineHeight: '45px', */
          paddingLeft: '0 0 0 1rem',
          fontSize: '1.3rem',
          borderRadius: 0
        }}
      >
        <List.Icon name='chevron left' color='grey' verticalAlign='middle'/>
        <List.Content style={{ lineHeight: '2em', fontWeight: 'bold'}} className='ui text pink'>
          {this.props.title}
        </List.Content>
      </List.Item>
    )
  }
}
