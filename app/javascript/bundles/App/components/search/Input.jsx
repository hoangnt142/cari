import React, { PureComponent } from 'react'
import PropType from 'prop-types'
import {
  Icon,
  Input,
} from 'semantic-ui-react'

export default class SearchInput extends PureComponent {

  onChangeText = (e,data) => {
    this.props.onChangeText(data.value)
  }

  onKeyPress = (e) => {
    if (e.key == 'Enter') {
      this.props.searchByText()
    }
  }

  render() {
    console.log('rendering')
    const {
      text,
      onChangeText,
      searchByText
    } = this.props

    return (
      <Input
        icon
        onChange={this.onChangeText}
        onKeyPress={this.onKeyPress}
        value={text}
        style={styles.input}
        placeholder='看護師求人を探す'
      >
        <input style={styles.innerInput} />
        <a onClick={searchByText}
           style={styles.button}
        ><Icon name='search' color='pink' style={styles.icon} circular inverted /></a>
      </Input>
    )
  }
}

const styles = {
  input: {
    flex: 0.88,
    color: '#999',
    height: '3.3em'
  },
  innerInput: {
    borderRadius: '2em',
    borderWidth: '1px',
    borderColor: 'rgb(243,101,129)'
  },
  button: { position: 'absolute', right: '0.5em', top: '0.4em' },
  icon: {
    fontSize: '1.2em',
    lineHeight: '1.2em',
    opacity: 1,
  }
}
