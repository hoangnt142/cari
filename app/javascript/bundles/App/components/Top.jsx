import React, { PureComponent } from 'react'
import Link from './_Link'
import BoxTitle from './_BoxTitle'
import SearchPopup from '../containers/SearchContainer'
import RightSide from '../containers/RightSideContainer'
import _ from 'lodash'
import SearchKey from './_SearchKey'
import PageHelmet from './PageHelmet'
import HospitalListMobile from './mobile/_HospitalListMobile'
import TopSlider from './TopSlider'
import {
  Button, Image, Icon,
  Search, Segment,
  Header, Grid, Container, Label,
  List, Responsive
} from 'semantic-ui-react'
import { pathToUrl } from '../lib/url'
import SearchInput from '../containers/search/InputContainer'

const Section = (props) => <Segment basic {...Object.assign({ style: { backgroundColor: '#fff', borderRadius: 5 } }, props)} />
// React Helmet
// The tags will be embedded dynamically in <head>.
//
// <Helmet>
//   <title>Top page</title>
//   <meta name="description" content="" />
//   <meta name="keywords" content="" />
//   <meta name="robots" content="index,follow" />
//   <link rel="canonical" href={window.location.href} />
// </Helmet>


// Stylesheet using CSS Module
// https://glenmaddern.com/articles/css-modules
//
// We use `babel-plugin-react-css-modules` implementation and written the setting in .babelrc.
// When we use it, write with 'styleName' attribute.
// The class name are absolutely independent as it rewrite the class name uniquely per file basis.
// e.g. <h1 styleName='red'>
// for named style
//      <h1 styleName='common.red'>

//import '../../../app/assets/stylesheets/common.scss'
//import top from '../../../app/assets/stylesheets/top.css'

// Image files are put in app/javascripts/app/assets/images
// We can use it without considering the path
// e.g. <img src={file1} />

import womanImage from 'app/assets/images/woman'
import japanMap from 'app/assets/images/japan'

export default class Top extends PureComponent {

  // It may not be needed as we use redux
  static propTypes = {

  }

  constructor(props) {
    super(props)
    // Getting methods from redux.
    // It consists of action, constant of action name, reducer and container.
    // All of them are put directly in `app/javascripts/bundles/App/`.
    // - actions/
    // - constants/constants.js
    // - reducers/
    // - containers/
    const { fetchPage, reset, initialize } = this.props

    // Resets the state as initial condition even when coming back from the other page.
    reset()
    initialize()

    // Getting data from the server and set the data to state to Redux.
    // we can get the data with this.props.api according to the TopContainer.
    //
    // `this.props.api.isFetching` returns true while getting data from the server.
    // As Fetch works asynchrnously, we have to handle it carefully when multiple fetchings are working.
    // The true returned by `isFetching` means fetching at least one of requests is fetching now.
    // When it's need to specify a URL of isFetching, use `this.props.api.fetchingURLs`, which contains fetching URLs.
    //
    // Data coming from the server can be gotten directly with `this.props.api.data1`.
    //
    // Refer to `actions/apiActions.js` and `reducers/apiReducer.js`.

    fetchPage()
  }

  componentDidMount() {
  }

  afterFetchSub = () => {
    this.props.setVisible(true)
  }

  onOpenRegion = (regionId) => {
    this.props.openSearchModal('area', regionId, true)
  }

  onOpenDetail = () => {
    this.props.openSearchModal('detail', null, true)
  }

  render() {
    const {
      api,
      page,
      top,
      history,
    } = this.props
    const regions = top.regions
    const widgets = top.widgets

    const prefectureDetailRender = (regions, last = false) => {
      return (
        regions.map((region, i) =>
          <div key={i} className="prefectureDetail">
            <Label color='grey' size='large' style={{ textAlign: 'center', width: '' }}>
              {region.label}
            </Label>
            <div className="areas">
              <ul className={last ? 'lastList' : 'list'}>
                {region.prefectures.map((prefecture, i) =>
                  <li key={prefecture.id}><Link to={`/${prefecture.name_roman}`}>{prefecture.name}</Link></li>
                )}
              </ul>
            </div>
          </div>
        )
      )
    }

    return (
      <div>
        <PageHelmet page={page} />

        {/*Banner PC*/}

        <Responsive minWidth={992} className='topBanner' style={{ height: '275px', marginBottom: '1rem' }}>
          <Container>
            <Grid padded>
              <Grid.Row>
                <Grid.Column width={2} style={{ padding: '0px' }}>
                  <Segment circular inverted color='blue' style={{ width: 120, minWidth: 120, height: 120, padding: 0 }} className='circularLabel'>
                    <Header as='h2' inverted style={{ fontSize: '29px' }}>
                      看護師
                      <Header.Subheader style={{ fontSize: '19px', fontWeight: 'bold', color: '#fff' }}>
                        求人を探す
                      </Header.Subheader>
                    </Header>
                  </Segment>
                </Grid.Column>
                <Grid.Column width={10}>
                  <Grid padded>
                    <Grid.Row style={{ marginTop: '30px', padding: '10px 0' }}>
                      <Header style={{
                        fontSize: '38px',
                        whiteSpace: 'nowrap',
                        overflow: 'hidden',
                        textOverflow: 'ellipsis'
                      }}>キャリアに合わせて転職しよう。</Header>
                    </Grid.Row>
                    <Grid.Row>
                      <SearchInput
                      />
                    </Grid.Row>
                    <Grid.Row style={{ padding: '0px' }}>
                      <Grid.Column width={8} style={{ paddingLeft: 0 }}>
                        <Button color='blue' fluid style={{ height: '3em', fontSize: '20px', borderRadius: '2em' }}
                                onClick={this.onOpenDetail}
                        >
                          <Icon name='star' />
                          希望条件で探す
                        </Button>

                      </Grid.Column>
                      {/*<Grid.Column width={8} style={{paddingRight: 0}}>
                          <Button color='blue' fluid style={{height: '70px', fontSize: '20px'}} onClick={this.handleModal.bind(this, 'SearchKey')}>
                          <Icon name='subway' />
                          勤務地で探す
                          </Button>
                          </Grid.Column>*/}
                    </Grid.Row>
                  </Grid>
                </Grid.Column>
                <Grid.Column width={4} style={{ position: 'relative' }}>
                  <Image src={womanImage} style={{ position: 'absolute', bottom: '-1rem' }} />
                  <Grid style={{
                    backgroundColor: '#ffffff94',
                    position: 'absolute',
                    bottom: '1rem',
                    width: '100%'
                  }}>
                    <Grid.Column width={7}>
                      <div style={{ borderRight: '1px solid #ccc', marginRight: '-1rem' }}>
                        <p style={{ fontSize: '15px', fontWeight: 'bold', margin: '0' }}>掲載求人数</p>
                        <p className='ui text pink' style={{ fontSize: '11px' }}>[{api.import_log_created_at}]</p>
                      </div>
                    </Grid.Column>
                    <Grid.Column width={9} style={{ fontSize: '25px', lineHeight: '38px', fontWeight: 'bold' }} textAlign='right'>
                      {api.job_count}
                      <span style={{ fontSize: '15px' }}>件</span>
                    </Grid.Column>
                  </Grid>
                </Grid.Column>

              </Grid.Row>
            </Grid>
          </Container>
        </Responsive>

        {/*Banner mobile*/}

        <Responsive maxWidth={991} minWidth={769}>
          <div className='topBanner' style={{ height: '275px', position: 'relative' }}>
            <Grid padded>
              <Responsive minWidth={645} as={() =>
                <Header style={{
                  fontWeight: 'bold',
                  position: 'absolute',
                  top: '1rem',
                  fontSize: '40px',
                  width: '70%'
                }}>キャリアに合わせて<br /><span className='ui text pink'>看護師</span>転職しよう。</Header>
              } />
              <Responsive maxWidth={644} as={() =>
                <Header style={{
                  fontWeight: 'bold',
                  position: 'absolute',
                  top: '1rem',
                  fontSize: '30px',
                  width: '60%'
                }}>キャリアに合わせて<span className='ui text pink'>看護師</span>転職しよう。</Header>
              } />
              <Grid.Column width={3} style={{ padding: '0px', height: '275px' }}>
                <Segment circular inverted color='blue' style={{
                  width: 120,
                  minWidth: 120,
                  height: 120,
                  padding: 0,
                  position: 'absolute',
                  bottom: '1rem',
                  left: '1rem',
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center'
                }} className='circularLabel'>
                  <Header as='h2' inverted style={{ fontSize: '29px' }}>
                    看護師
                    <Header.Subheader style={{ fontSize: '19px', fontWeight: 'bold', color: '#fff' }}>
                      求人を探す
                    </Header.Subheader>
                  </Header>
                </Segment>
              </Grid.Column>
              <Grid.Column width={13} style={{ height: '275px' }}>
                <Image src={womanImage} style={{ position: 'absolute', bottom: 0, right: '1rem' }} />
                <Grid style={{
                  backgroundColor: '#ffffff94',
                  position: 'absolute',
                  bottom: '2rem',
                  width: '100%',
                  maxWidth: '250px',
                  right: '2rem'
                }}>
                  <Grid.Column width={7}>
                    <div style={{ borderRight: '1px solid #ccc', marginRight: '-1rem' }}>
                      <p style={{ fontSize: '15px', fontWeight: 'bold', margin: '0' }}>掲載求人数</p>
                      <p className='ui text pink' style={{ fontSize: '11px' }}>[{api.import_log_created_at}]</p>
                    </div>
                  </Grid.Column>
                  <Grid.Column width={9} style={{ fontSize: '25px', lineHeight: '38px', fontWeight: 'bold' }}>
                    {api.job_count}
                    <span style={{ fontSize: '15px' }}>件</span>
                  </Grid.Column>
                </Grid>
              </Grid.Column>

            </Grid>
          </div>
        </Responsive>

        {/*<Responsive as={TopSlider} maxWidth={768} />*/}

        <Responsive maxWidth={991} style={{marginTop: '-6px'}}>
          <Grid padded style={{ backgroundColor: '#fff' }}>
            <Grid.Row>
              <Grid.Column width={16}>
                <Grid style={{ marginBottom: '1em' }}>
                  <Grid.Row style={{ fiex:1,
                                     margin: 0,
                                     flexDirection: 'row',
                                     justifyContent: 'center',
                                     /*backgroundColor: 'rgb(243,101,129)'*/ }}>
                    <SearchInput
                    />
                  </Grid.Row>
                  <Grid.Row style={{ fiex:1, flexDirection: 'row', justifyContent: 'center', paddingTop: '0' }}>
                    <span className='ui text pink' style={{ fontSize: '1em', marginRight: '0.5em' }}>【{api.import_log_created_at}】</span>
                    <span style={{ fontSize: '1em', fontWeight: 'bold' }}>掲載求人数 {api.job_count}件</span>
                  </Grid.Row>
                  <Grid.Row style={{ padding: '0px' }}>
                    <Grid.Column width={16} style={{ padding: 0,
                                                     flex: 1,
                                                     flexDirection: 'row',
                                                     display: 'flex',
                                                     justifyContent: 'center' }}>


                      <Button style={{ borderRadius: '2em', display: 'flex', flex: 0.87, justifyContent: 'center' }}
                              color='blue'
                              icon='star'
                              size='huge'
                              content='希望条件で探す'
                              onClick={this.onOpenDetail}
                      />
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Responsive>

        {/*End banner mobile*/}

        <Container>
          <Grid style={{marginTop: 0, marginLeft: '-5px', marginRight: '-5px' }}>
            <Grid.Column computer={12} mobile={16} style={{ paddingLeft: '5px', paddingRight: '5px' }}>
              {regions.length > 0 &&
               <React.Fragment>
                 <Responsive maxWidth={729} as={Segment} >
                   <BoxTitle title="都道府県から求人を探す" />
                   <Grid>
                     <Grid.Row className='smartPhoneMap'>
                       <Grid.Column width={12} style={{ position: 'relative' }}>
                         <Grid>

                           <Grid.Row style={{ fontWeight: 'bold', fontSize: '1.7rem', height: '224px', alignItems: 'center', lineHeight: '2.3rem' }}>
                             <Grid.Column width={16}>
                               ご希望のエリアを<span className='ui text pink'>タップ</span>してください
                             </Grid.Column>
                           </Grid.Row>

                           <Grid.Row>
                             <Grid.Column width={4} style={{ paddingRight: 0 }}>
                               <Label onClick={() => this.onOpenRegion(8)} color='green' style={{ width: '100%', margin: '0 0 10px 0', height: '65px' }}><div>九州</div><Icon name='caret down' /></Label>
                               <Label onClick={() => this.onOpenRegion(9)} color='green' style={{ width: '75%', margin: 0 }} ><div>沖縄</div><Icon name='caret down' /></Label>
                             </Grid.Column>

                             <Grid.Column width={4} style={{ paddingRight: 5, paddingLeft: '7px' }}>
                               <Label onClick={() => this.onOpenRegion(6)} color='green' style={{ margin: 0, width: '100%', }}><div>中国</div><Icon name='caret down' /></Label>
                               <Label onClick={() => this.onOpenRegion(7)} color='green' style={{ margin: 0, float: 'right', width: '75%', marginTop: '10px' }}><div>四国</div><Icon name='caret down' /></Label>
                             </Grid.Column>

                             <Grid.Column width={5} style={{ paddingLeft: 0, paddingRight: 5, position: 'relative' }}>
                               <Label onClick={() => this.onOpenRegion(5)} color='green' style={{ width: '100%', height: '65px' }}><div style={{ marginTop: '-0.5rem' }}>近畿</div><Icon name='caret down' /></Label>
                               <div style={{ width: '30%', height: '30px', display: 'block', backgroundColor: '#fff', position: 'absolute', top: '35px', right: '5px' }}></div>
                             </Grid.Column>
                             <Grid.Column width={3} style={{ paddingRight: 0, paddingLeft: 0, position: 'relative' }}>
                               <Label onClick={() => this.onOpenRegion(4)} color='green' style={{ width: '40px', height: '49px', display: 'block', top: '-55px', left: '-37px', position: 'absolute' }}></Label>
                               <Label onClick={() => this.onOpenRegion(4)} color='green' style={{ width: '100%', position: 'absolute', top: '-55px', height: '120px', margin: 0 }}><div style={{ marginTop: '-3rem' }}>中部</div><Icon name='caret down' /></Label>
                               <div style={{ width: '50%', height: '30px', display: 'block', backgroundColor: '#fff', position: 'absolute', top: '35px', left: '0px' }}></div>
                             </Grid.Column>
                           </Grid.Row>
                         </Grid>
                       </Grid.Column>

                       <Grid.Column width={4} style={{ paddingLeft: '7px' }}>
                         <Label onClick={() => this.onOpenRegion(1)} color='green' style={{ width: '100%', margin: '0 0 1rem 0', height: '85px' }}><div style={{ marginTop: '1rem' }}>北海道</div><Icon name='caret down' /></Label>
                         <Label onClick={() => this.onOpenRegion(2)} color='green' style={{ width: '100%', margin: '0 0 10px 0', height: '90px' }}><div style={{ marginTop: '1rem' }}>東北</div><Icon name='caret down' /></Label>
                         <Label onClick={() => this.onOpenRegion(3)} color='green' style={{ width: '100%', margin: 0, height: '90px' }}><div style={{ marginTop: '1rem' }}>関東</div><Icon name='caret down' /></Label>

                       </Grid.Column>
                     </Grid.Row>

                     {api.top_links && api.top_links.map((top_link, i) =>
                       <Grid.Column computer={16} key={i}>
                         <Segment basic style={{ backgroundColor: '#f6f6f6' }}>
                           <Header as='h3' size='large' color='pink' style={{ borderBottom: '1px solid #ccc', paddingBottom: '10px', fontSize: '19px' }}>{top_link.title}</Header>
                           {top_link.links.map((link, ii) =>
                             <Label basic key={ii}
                                    style={{ marginBottom: '0.5rem', fontSize: '15px', fontWeight: 'bold' }}>
                               <Link
                                 to={link.url}
                                 style={{
                                   flex: 1,
                                   display: 'flex',
                                   flexDirection: 'row'
                                 }}>
                                 {link.name}
                               </Link>
                             </Label>
                           )}
                         </Segment>
                       </Grid.Column>
                     )}
                   </Grid>
                 </Responsive>


                 <Responsive minWidth={730} as={Segment} >
                   <BoxTitle as='h2' title="エリアから看護師求人を探す 【全国対応】" style={{fontSize: '21px'}}/>
                   <Grid padded>
                     <Grid.Column computer={8}>
                       <div className='japanMap'>
                         <Image src={japanMap} />
                         <Label basic style={{ top: '88px', left: '225px' }} className='ui text pink'>北海道<br />・東北</Label>
                         <Label basic style={{ top: '158px', left: '80px' }} className='ui text pink'>甲信越<br />・北陸</Label>
                         <Label basic style={{ top: '203px', left: '187px' }} className='ui text pink'>関東</Label>
                         <Label basic style={{ top: '223px', left: '70px' }} className='ui text pink'>関西</Label>
                         <Label basic style={{ top: '248px', left: '152px' }} className='ui text pink'>東海</Label>
                         <Label basic style={{ top: '266px', left: '48px' }} className='ui text pink'>中国<br />・四国</Label>
                         <Label basic style={{ top: '323px' }} className='ui text pink'>九州<br />・沖縄</Label>
                       </div>
                       {prefectureDetailRender([regions[4]])}
                     </Grid.Column>
                     <Grid.Column className="prefecture-group" computer={8}>
                       {prefectureDetailRender(
                          [regions[0],
                           regions[1],
                           regions[2],
                           regions[3]]
                       )}
                     </Grid.Column>

                     {prefectureDetailRender([regions[5]], true)}

                     {api.top_links && api.top_links.map((top_link, i) =>
                       <Grid.Column computer={16} key={i}>
                         <Segment basic style={{ backgroundColor: '#f6f6f6' }}>
                           <Header as='h3' size='large' color='pink' style={{ borderBottom: '1px solid #ccc', paddingBottom: '10px', fontSize: '19px' }}>{top_link.title}</Header>
                           {top_link.links.map((link, ii) =>
                             <Label basic key={ii}
                                    style={{ marginBottom: '0.5rem', fontSize: '15px', fontWeight: 'bold' }}
                               >
                               <Link
                                 to={link.url}
                                 style={{
                                   flex: 1,
                                   display: 'flex',
                                   flexDirection: 'row'
                                 }}>
                                 {link.name}
                               </Link>
                             </Label>
                           )}
                         </Segment>
                       </Grid.Column>
                     )}
                   </Grid>
                 </Responsive>
               </React.Fragment>
              }

              <Responsive minWidth={992} as={Section}>
                <Grid>
                  <Grid.Column width={16}>
                    <BoxTitle as='h2' title="新着の看護師求人" style={{fontSize: '21px'}}/>
                    <List divided relaxed>
                      {api.jobs && api.jobs.map((job, i) =>
                        <List.Item key={i}>
                          <List>
                            <List.Item>
                              <List.Icon name='hospital' size='large' color='grey' />
                              <List.Content><Link style={{ fontSize: '17px', fontWeight: 'bold' }} to={job.hospital.url}>{job.hospital.name}</Link></List.Content>
                            </List.Item>
                            <List.Item>
                              <List.Icon name='marker' size='large' color='grey' />
                              <List.Content>{job.hospital.work_location}</List.Content>
                            </List.Item>
                            <List.Item>
                              <List.Icon name='subway' size='large' color='grey' />
                              <List.Content>{job.hospital.access}</List.Content>
                            </List.Item>
                            {job.qualifications.length > 0 &&
                             <List.Item style={{ marginTop: '10px', marginBottom: '5px' }}>
                               <List.Content>
                                 <SearchKey keys={job.qualifications} />
                               </List.Content>
                             </List.Item>
                            }
                            <List.Item>
                              <List.Content>
                                {job.salary_type}
                                <span style={{ fontSize: '25px', fontWeight: 'bold' }}>{job.salary_formated}</span>
                                {/*円～（手当含む）*/}
                              </List.Content>
                            </List.Item>
                            <List.Item>
                              <List.Content>
                                {job.contracts.map(contract => contract.name).join('／')}
                              </List.Content>
                            </List.Item>
                          </List>
                        </List.Item>
                      )}
                    </List>
                  </Grid.Column>
                </Grid>
              </Responsive>
              {api.hospitals && <Responsive maxWidth={991} as={HospitalListMobile} hospitals={api.hospitals}/> }
              {page.content_bottom &&
               <Section className="chosen-reason">
                 <div className='carriee-editor' dangerouslySetInnerHTML={{ __html: page.content_bottom }} />
               </Section>
              }

              {/*<Section className="chosen-reason">
                  <BoxTitle title="Carrieeが選ばれる理由" />
                  <List>
                  {[...Array(5)].map((x, i) =>
                  <List.Item key={i} className="reason-item">
                  <List.Content>
                  <div style={{display: 'flex', margin: '20px 0px', alignItems: 'center'}}>
                  <Label circular color='blue' style={{
                  minWidth: 60,
                  width: '60px',
                  height: 60,
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                  flexDirection: 'column',
                  fontSize: '18px'
                  }}>
                  point
                  <span style={{fontSize: '25px'}}>{i+1}</span>
                  </Label>
                  <Link to="/blog" className='ui text pink' style={{
                  fontWeight: 'bold',
                  fontSize: '22px',
                  marginLeft: '10px',
                  lineHeight: '30px'
                  }}>病院・施設に勤務していた看護師の口コミ情報を掲載！</Link>
                  </div>
                  <Grid>
                  <Grid.Column computer={6} mobile={8} className="reason-image">
                  <Image src={exampleImage} style={{width: '100%'}}/>
                  </Grid.Column>
                  <Grid.Column computer={10} mobile={8} style={{paddingLeft: 0, lineHeight: '25px'}}>
                  テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト
                  </Grid.Column>
                  </Grid>
                  </List.Content>
                  </List.Item>
                  )}
                  </List>
                  <Link to="/">
                  <Button style={{
                  height: '60px',
                  width: '70%',
                  display: 'block',
                  margin: '25px auto 10px'
                  }}><span className='ui text pink' style={{fontSize: '21px'}}>病院・施設の口コミ情報へ</span></Button>
                  </Link>
                  </Section>*/}

            </Grid.Column>
            {widgets &&
             <RightSide widgets={widgets} />
            }

          </Grid>
        </Container>
      </div>
    )
}
}
