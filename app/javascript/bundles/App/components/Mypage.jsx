import React, { PureComponent } from 'react'
import PropType from 'prop-types'
import { Link } from 'react-router-dom'

import {
  Button, Image, Icon,
  Search, Modal, Segment, Sticky,
  Header,
  Item, Loader, Dimmer,
  Responsive,
  Container,
  Grid
} from 'semantic-ui-react'

import PageTitle from './PageTitle'


export default class Mypage extends PureComponent {

  static propTypes = {
  }
  static defaultProps = {
  }

  constructor(props) {
    super(props)

    const { fetchPage } = this.props
    fetchPage()
  }

  componentDidMount() {
    const { fetch } = this.props
    // Sub data should be gotten after being mounted.
    //fetch('/api/cache/pages/1/sub')
  }

  componentWillUnmount() {
    const { reset } = this.props
    // Resets the state as initial condition even when coming back from the other page.
    reset()
  }

  render() {
    const { api, user, page } = this.props
    if(!user) {
      return null
    }

    return (
      <div>
        <PageTitle
          title={`"${user.nickname}"さんのマイページ`}
          page={page}
          breadCrumbs={[
            { title: '看護師求人サイトCarriee', url: '/' },
            { title: 'Mypage', url: '/mypage' },
          ]}
          showLastBreadCrumbs={false}
        />

        <Container>
          <Grid columns={2}>
            <Grid.Column width={12}>
              <Segment style={{ paddingTop: '50px', paddingBottom: '50px' }}>

                <Segment color='pink' inverted textAlign='center' style={{ lineHeight: '3em' }}>
                  {/* Shown while the email has not been confirmed yet. */}
                  登録いただいたメールアドレスに認証メールをお送りしております。<br />
                  <Icon name='info' bordered circular color='white' size='large' />メールの認証を行ってください。<br />
                  <Button style={{ width: '70%', marginTop: '1em' }}>認証メールを送る</Button>
                </Segment>

                <Header>マイデータ</Header>

                <Grid columns='equal' padded textAlign='center'>
                  <Grid.Row>
                    <Grid.Column>
                      <Segment color='grey' inverted>
                        <Icon name='heart' color='pink' size='huge' /><br />
                        保存した求人<br />
                      </Segment>
                    </Grid.Column>
                    <Grid.Column>
                      <Segment color='grey' inverted>
                        <Icon name='checkmark' color='blue' size='huge' /><br />
                        求人閲覧<br />
                      </Segment>
                    </Grid.Column>
                    <Grid.Column>
                      <Segment color='grey' inverted>
                        <Icon name='comments' color='green' size='huge' /><br />
                        病院の口コミ投稿<br />
                      </Segment>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>

              </Segment>
            </Grid.Column>

            <Grid.Column width={4}>
              <Grid columns='equal' textAlign='center'>
                <Grid.Row>
                  <Grid.Column>
                    <Link to='/mypage'>
                      <Icon name='user' color='pink' size='big' /><br />
                      マイページ<br />
                      Top
                    </Link>
                  </Grid.Column>
                  <Grid.Column>
                    <Link to='/mypage/my-reputes'>
                      <Icon name='comments' color='pink' size='big' /><br />
                      口コミ<br />
                      投稿履歴
                    </Link>
                  </Grid.Column>
                  <Grid.Column>
                    <Link to='/mypage/settings'>
                      <Icon name='setting' color='pink' size='big' /><br />
                      各種設定
                    </Link>
                  </Grid.Column>
                </Grid.Row>
              </Grid>

              <Segment color='green' inverted>
                <Grid columns={2} verticalAlign='middle'>
                  <Grid.Column width={6}>
                    <Icon name='write' size='huge' />
                  </Grid.Column>
                  <Grid.Column width={10}>
                    病院の<br />
                    <span text='ui text yellow big'>口コミ投稿</span>を<br />
                    しよう！
                  </Grid.Column>
                </Grid>

              </Segment>

              banner

            </Grid.Column>
          </Grid>
        </Container>

      </div>
    )
  }
}
