import React, { PureComponent } from 'react';
import PropType from 'prop-types';
import { Link } from 'react-router-dom';
import PageTitle from './PageTitle'
import Pagination from './SEOPagination'
import ArticleList from './ArticleList'
import ArticleListMobile from './ArticleListMobile'
import { Image, Responsive, Container } from 'semantic-ui-react'

export default class Blog extends PureComponent {

  static propTypes = {

  }

  constructor(props) {
    super(props)

    const { fetchPage } = this.props
  }

  render() {
    const {
      pageNumber,
      api,
      page
    } = this.props;
    const { totalPages, currentPage, blog_articles } = api
    return (
      <React.Fragment>
        <PageTitle
            title={page.title}
            page={page}
            seoTitle={page.seo_title || page.title}
            showTitle={false}
            breadCrumbs={[
              { title: '看護師求人サイトCarriee', url: '/' }
            ]}
            showLastBreadCrumbs={true}
          />
        <Container>
          <Responsive minWidth={992} as={ArticleList} articles={blog_articles} />
          <Responsive maxWidth={991} as={ArticleListMobile} articles={blog_articles} hideFirstItem/>
        </Container>
        {totalPages > 1 &&
          <div className='carrieePagination'>
            <Pagination totalPages={totalPages}
                        currentPage={parseInt(currentPage)}
                        basePath={page.url}
                />
          </div>
        }
      </React.Fragment>
    );
  }
}
