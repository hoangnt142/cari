import React, { PureComponent } from 'react'
import PropType from 'prop-types'
import { Helmet } from 'react-helmet'
import { pathToUrl } from '../lib/url'

export default class PageHelmet extends PureComponent {

  static propTypes = {
    page: PropType.object.isRequired,
    seoTitle: PropType.string,
    seoDescription: PropType.string,
    seoKeywords: PropType.string,
    canonicalUrl: PropType.string,
  }
  static defaultProps = {
    clear: false
  }

  constructor(props) {
    super(props)
  }

  render() {
    const { page,
            seoTitle,
            seoDescription,
            seoKeywords,
            pageNumber,
            clear, canonicalUrl } = this.props
    const canonicalUrlWithServerAndPort = pathToUrl(canonicalUrl)

    const page2OrMore = pageNumber !== null && pageNumber > 1
    const title = (page.seo_title || seoTitle || '') + (page2OrMore ? ` - ${pageNumber}ページ目`: '')
    const description = !page2OrMore ? (page.seo_description || seoDescription) : ''
    const keywords = !page2OrMore ? (page.seo_keywords || seoKeywords) : ''

    if(clear) {
      return (
        <Helmet>
          {this.props.children}
        </Helmet>
      )
    }
    else {
      return (
        <Helmet>
          <title>{title}</title>
          <meta name="description" content={description} />
          <meta name="keywords" content={keywords} />
          <meta name="robots" content={`${page.seo_robot_noindex ? 'noindex' : 'index'},${page.seo_robot_nofollow ? 'nofollow' : 'follow'}`} />
          {canonicalUrlWithServerAndPort && (
             <link rel="canonical" href={canonicalUrlWithServerAndPort} />
          )}
          {this.props.children}
        </Helmet>
      )
    }
  }
}
