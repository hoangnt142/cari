import React, { PureComponent } from 'react'
import PropType from 'prop-types'
import { Link } from 'react-router-dom'

import {
  Button, Image, Icon,
  Search, Modal, Segment, Sticky,
  Header,
  Item, Loader, Dimmer,
  Responsive,
  Container
} from 'semantic-ui-react'



const style = {
  icon: {
    margin: 0
  }
}

export default class SEOPagination extends PureComponent {

  static propTypes = {
    totalPages: PropType.int.required,
    currentPage: PropType.int.required,
    maxBlocks: PropType.int.required,
  }
  static defaultProps = {
    maxBlocks: 7,
    paginationRange: []
  }

  constructor(props) {
    super(props)
  }

  buildPageArray = () => {
    const {
      totalPages,
      currentPage,
      maxBlocks,
    } = this.props
    const pages = []
    if(totalPages >= maxBlocks) {
      if(currentPage < (maxBlocks - 2)) {
        // e.g. 1 2 3 4 5 .. 40
        for(let i = 1; i <= (maxBlocks - 2); i++) {
          pages.push(i)
        }
        pages.push(null) // ellipsis item
        pages.push(totalPages)
      }
      else if(currentPage > (totalPages - (maxBlocks - 2) + 1)) {
        // e.g. 1 .. 36 37 38 39 40
        pages.push(1)
        pages.push(null) // ellipsis item
        for(let i = (totalPages - (maxBlocks - 2) + 1); i <= totalPages; i++) {
          pages.push(i)
        }
      }
      else {
        // e.g. 1 .. 4 5 6 .. 40
        pages.push(1)
        pages.push(null) // ellipsis item
        for(let i = (currentPage - 1); i <= (currentPage + 1); i++) {
          pages.push(i)
        }
        pages.push(null) // ellipsis item
        pages.push(totalPages)
      }
    }

    return pages
  }

  render() {
    const {
      totalPages,
      currentPage,
      basePath,
      totalCount,
      paginationRange,
    } = this.props

    const pages = this.buildPageArray()
    const firstPageLink = basePath
    const previousPageLink = Math.max(currentPage - 1, 1) > 1 ? `${basePath}/page/${Math.max(currentPage - 1, 1)}` : basePath
    const hasPrevious = currentPage > 1
    const nextPageLink = Math.min(currentPage + 1, totalPages) > 1 ? `${basePath}/page/${Math.min(currentPage + 1, totalPages)}` : basePath
    const hasNext = currentPage < totalPages
    const lastPageLink = totalPages > 1 ? `${basePath}/page/${totalPages}` : basePath

    return (
      <div>
        <Responsive maxWidth={991}
                    aria-label="Pagination Navigation"
                    role="navigation"
                    className="ui pagination menu"
                    style={{ display: 'flex', flex: 1, flexDirection: 'row' }}
        >
          {hasPrevious ? (
             <Link to={previousPageLink}
                   aria-current="false"
                   tabIndex="0"
                   value={currentPage > 1 ? currentPage - 1 : 1}
                   rel='prev'
                   type="prevItem"
                   className="item"
                   style={{ display: 'flex',
                            flex: 1,
                            borderRadius: '0.3rem'
                   }}>
               <div><i style={style.icon} aria-hidden="true" className="chevron left icon"></i>前</div>
             </Link>
          ) : (
             <span className="item"
                   style={{ display: 'flex',
                            flex: 1,
                            borderRadius: '0.3rem',
                            color: 'lightgrey'
                   }}>
               <div><i style={style.icon} aria-hidden="true" className="chevron left icon"></i>前</div>
             </span>
          )}
          {hasNext ? (
             <Link to={nextPageLink}
                   aria-current="false"
                   tabIndex="0"
                   value={currentPage < totalPages ? currentPage + 1 : totalPages}
                   rel='next'
                   type="nextItem"
                   className="item"
                   style={{ display: 'flex',
                            flex: 1,
                            borderRadius: '0.3rem'
                   }}>
               <div>次<i style={style.icon} aria-hidden="true" className="chevron right icon"></i></div>
             </Link>
          ) : (
             <span className="item"
                   style={{ display: 'flex',
                            flex: 1,
                            borderRadius: '0.3rem',
                            color: 'lightgrey'
                   }}>
               <div>次<i style={style.icon} aria-hidden="true" className="chevron right icon"></i></div>
             </span>
          )}
        </Responsive>

        <Responsive maxWidth={991}
                    className="ui pagination menu"
                    style={{ display: 'flex',
                             flex: 1,
                             flexDirection: 'column',
                             justifyContent: 'center',
                             alignItems: 'center',
                             marginTop: '1em',
                             lineHeight: '1.4em'
                    }}
        >
          <span style={{ display: 'flex' }}>
            該当件数
            <span className='ui text pink'
                  style={{fontSize: '1.5em',
                          marginLeft: '0.5em',
                          marginRight: '0.5em',
                          fontWeight: 'bold'
                  }}>
              {totalCount}
            </span>
            件 ／
            <span className='ui text pink'
                  style={{fontSize: '1.5rem',
                          marginLeft: '0.5em',
                          marginRight: '0.5em',
                          fontWeight: 'bold'
                  }}>{currentPage}</span>
            ページ目
          </span>
          <span style={{ display: 'flex' }}>
            （{paginationRange[0]}件~{paginationRange[1]}件を表示）
          </span>
        </Responsive>


        <Responsive minWidth={992}
                    aria-label="Pagination Navigation"
                    className="ui pagination menu"
                    role="navigation"
                    style={{ margin: 'auto' }}
        >
          <Link to={firstPageLink}
                aria-current="false"
                tabIndex="0"
                value="1"
                type="firstItem"
                className="item">
            <div className="ui text blue">
              <i style={style.icon} aria-hidden="true" className="chevron left icon"></i>最初
            </div>
          </Link>
          <Link to={previousPageLink}
                aria-current="false"
                tabIndex="0"
                value={currentPage > 1 ? currentPage - 1 : 1}
                rel='prev'
                type="prevItem"
                className="item">
            <div><i style={style.icon} aria-hidden="true" className="chevron left icon"></i>前</div>
          </Link>
          {pages.map((p, i) => {
             if(p) {
               return <Link key={i}
                            to={p > 1 ? `${basePath}/page/${p}` : basePath}
                            aria-current="false"
                            tabIndex="0"
                            value={p}
                            type="pageItem"
                            className={"item" + (p == currentPage ? ' active':'')}>{p}</Link>
             }
             else {
               return <a key={i}
                         aria-current="false"
                         tabIndex="-1"
                         type="ellipsisItem"
                         className="disabled icon item">
                 <i aria-hidden="true" className="ellipsis horizontal icon"></i>
               </a>
             }
          })}
          <Link to={nextPageLink}
                aria-current="false"
                tabIndex="0"
                value={currentPage < totalPages ? currentPage + 1 : totalPages}
                rel='next'
                type="nextItem"
                className="item"
          >
            <div>次<i style={style.icon} aria-hidden="true" className="chevron right icon"></i></div>
          </Link>
          <Link to={lastPageLink}
                aria-current="false"
                tabIndex="0"
                value={totalPages}
                type="lastItem"
                className="item">
            <div className="ui text blue">最後<i style={style.icon} aria-hidden="true" className="chevron right icon"></i></div>
          </Link>

        </Responsive>
      </div>
    )
  }
}
