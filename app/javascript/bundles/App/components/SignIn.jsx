import React, { PureComponent } from 'react'
import PropType from 'prop-types'
import { Link } from 'react-router-dom'


import {
  Button, Image, Icon,
  Segment,
  Form, Message,
  Container,
  Grid,
  Header,
  List,
  Responsive
} from 'semantic-ui-react'

import PageTitle from './PageTitle'

export default class SignIn extends PureComponent {

  static propTypes = {
  }

  constructor(props) {
    super(props)
    const { fetchPage } = this.props
    fetchPage()
  }

  componentWillUnmount() {
    const { logout } = this.props
    const { loggedInFailed } = this.props.user
    const { reset } = this.props

    // Resets the state as initial condition even when coming back from the other page.
    reset()

    if(loggedInFailed) {
      logout()
    }
  }

  onClickSignIn = () => {
    const { email, password } = this.props.signInPage
    const { signInClick, login } = this.props
    signInClick()
    login(email, password)
  }

  render() {
    const { page, onChange } = this.props
    const { logging, loggedInFailed } = this.props.user
    const { email, password, signInClicked } = this.props.signInPage

    const canClickLogin = !logging &&
                          email.length > 0 &&
                          password.length > 0

    return (
      <div>
        <PageTitle
          title='Carriee【ログイン】'
          page={page}
          breadCrumbs={[
            { title: '看護師求人サイトCarriee', url: '/' },
            { title: 'ログイン', url: '/sign-in' },
          ]}
          showLastBreadCrumbs={false}
          showTitle={false}
        />


        <Container>
          <Segment style={{ paddingTop: '50px', paddingBottom: '50px' }}>
            <Grid columns={3} relaxed stackable>
              <Grid.Column width={6}>
                <Header as='h3'
                        size='huge'
                        content='SNSでログイン'
                        className='center aligned pink' />

                <List>
                  <List.Item style={{ marginBottom: '25px' }}>
                    <Button color='facebook' fluid size='large' style={{ padding: '20px' }}>
                      <Icon name='facebook' />Facebookでログイン
                    </Button>
                  </List.Item>
                  <List.Item style={{ marginBottom: '25px' }}>
                    <Button color='google plus' fluid size='large' style={{ padding: '20px' }}>
                      <Icon name='google plus' />Googleアカウントでログイン
                    </Button>
                  </List.Item>
                  <List.Item style={{ marginBottom: '25px' }}>
                    <Button color='twitter' fluid size='large' style={{ padding: '20px' }}>
                      <Icon name='twitter' />Twitterでログイン
                    </Button>
                  </List.Item>
                </List>

              </Grid.Column>
              <Grid.Column width={1}>
                <Responsive maxWidth={767} as={() =>
                  <div style={{ width: '100%', borderBottom: '1px solid rgb(245,244,242)' }}>
                                                  &nbsp;
                  </div>
                } />
                <Responsive minWidth={768} as={() =>
                  <div style={{ height: '100%', borderRight: '1px solid rgb(245,244,242)' }}>
                                                  &nbsp;
                  </div>
                } />
              </Grid.Column>

              <Grid.Column width={9}>
                <Header as='h3'
                        size='huge'
                        content='ログイン'
                        className='center aligned blue' />

                {(signInClicked && loggedInFailed) && (
                   <Message error
                            header='Authentication failed'
                            content='Please check email and password.' />
                )}

                <Form>
                  <Form.Input label='メールアドレス'
                              placeholder='email'
                              value={email}
                              onChange={(e, data) => onChange('email', data.value)} />
                  <Form.Input label='パスワード'
                              type='password'
                              placeholder='password'
                              value={password}
                              onChange={(e, data) => onChange('password', data.value)}
                  />

                  <p style={{ lineHeight: '3em', marginTop: '20px', textAlign: 'center' }}>
                    <Button onClick={this.onClickSignIn}
                            color='green'
                            loading={logging}
                            style={{ padding: '1em 4em', color: 'white', width: '70%' }}
                            size='massive'
                            disabled={!canClickLogin}>ログイン</Button>
                  </p>
                </Form>

              </Grid.Column>
            </Grid>
          </Segment>

          <div style={{ textAlign: 'center', marginTop: '3rem' }}>
            <Link to='sign-up'>
              <Button color='pink' size='huge'
                      style={{ padding: '1.5em 2em' }}>
                <Icon name='chevron right' color='white' />
                新規無料会員登録はこちら
              </Button>
            </Link>
          </div>
        </Container>
      </div>
    )
  }
}
