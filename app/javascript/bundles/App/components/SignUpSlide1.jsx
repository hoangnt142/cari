import React, { PureComponent } from 'react'
import PropType from 'prop-types'
import { Link } from 'react-router-dom'
import {
  Button, Image, Icon,
  Segment,
  Header,
  Grid, Form, Message,
  List,
  Responsive
} from 'semantic-ui-react'


import SignUpSlideStep from './SignUpSlideStep'


export default class SignUpSlide1 extends PureComponent {

  emailValid = (email) => {
    const regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex.test(email);
  }

  next = () => {
    const {
      email,
      password,
      emailExisted,
      next
    } = this.props
    if(!email || email.length == 0) {
      alert('メールアドレスを入力してください。')
    } else if(!password || password.length == 0) {
      alert('パスワードを入力してください。')
    } else if (!this.emailValid(email)) {
      alert('メールアドレスを正しく入力してください。')
    } else if (emailExisted) {
      alert('そのメールアドレスは既に使用されています。')
    }
    /* TODO: validation for email uniqueness
       TODO: validation for length of password and available characters(alphabet and numbers)
       else if() {
       alert('そのメールアドレスは既に使用されています。')
       }

     */
    else {
      next()
    }
  }

  render() {
    const {
      onChange,
      email,
      password
    } = this.props

    console.log('rendering')
    return (
      <div>
        <Segment style={{ paddingTop: '50px', paddingBottom: '50px' }}>

          <SignUpSlideStep step='1' />

          <Grid relaxed stackable>
            <Grid.Row>
              <Grid.Column width={5}>
                <Header as='h3'
                        size='huge'
                        content='SNSで登録'
                        className='center aligned pink' />
                <p>
                  「<Link to='/terms-of-service'>Carriee 会員登録規約</Link>」「<Link to='/privacy-policy'>プライバシーポリシー</Link>」に同意の上、進んでください。
                </p>

                <List>
                  <List.Item style={{ marginBottom: '25px' }}>
                    <a href='/api/uncache/users/auth/facebook'>
                      <Button color='facebook' fluid size='large' style={{ padding: '20px' }}>
                        <Icon name='facebook' />Facebookで登録
                      </Button>
                    </a>
                  </List.Item>
                  <List.Item style={{ marginBottom: '25px' }}>
                    <Button color='google plus' fluid size='large' style={{ padding: '20px' }}>
                      <Icon name='google plus' />Googleアカウントで登録
                    </Button>
                  </List.Item>
                  <List.Item style={{ marginBottom: '25px' }}>
                    <Button color='twitter' fluid size='large' style={{ padding: '20px' }}>
                      <Icon name='twitter' />Twitterで登録
                    </Button>
                  </List.Item>
                </List>

              </Grid.Column>

              <Grid.Column>
                <Responsive maxWidth={767} as={() =>
                  <div style={{ width: '100%', borderBottom: '1px solid rgb(245,244,242)' }}>
                                                  &nbsp;
                  </div>
                } />
                <Responsive minWidth={768} as={() =>
                  <div style={{ height: '100%', borderRight: '1px solid rgb(245,244,242)' }}>
                                                  &nbsp;
                  </div>
                } />
              </Grid.Column>

              <Grid.Column width={10}>
                <Header as='h3'
                        size='huge'
                        content='Carrieeで会員登録'
                        className='center aligned blue' />
                <Form>
                  <Form.Input label='メールアドレス'
                              placeholder='sample@carriee.jp'
                              value={email}
                              maxLength={200}
                              onChange={(e, data) => onChange('email', data.value)} />

                  <p className='ui text pink'>※ メールマガジン受信設定はメールアドレス入力後に可能です。</p>
                  <Form.Input label='パスワード'
                              type='password'
                              placeholder='6〜32文字、半角英数字のみ'
                              value={password}
                              maxLength={32}
                              onChange={(e, data) => onChange('password', data.value)} />


                  <p style={{ lineHeight: '3em', marginTop: '20px', textAlign: 'center' }}>
                    「<Link to='/terms-of-service'>Carriee 会員登録規約</Link>」「<Link to='/privacy-policy'>プライバシーポリシー</Link>」に<br />
                    <Button style={{ padding: '25px', color: 'white', width: '70%' }}
                            onClick={this.next}
                            color='green'
                            size='massive'>同意して 次に進む</Button>
                  </p>

                </Form>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Segment>

        <div>
          <Grid stackable>
            <Grid.Row>
              <Grid.Column width={8}>
                <Segment style={{ paddingTop: '30px', paddingBottom: '30px' }}
                         textAlign='center'>
                  <Icon name='chevron right' color='pink' />
                  <Link to='/for-beginner'>
                    <span className='ui text large'>Carrieeの無料会員登録で出来ること</span>
                  </Link>
                </Segment>
              </Grid.Column>
              <Grid.Column width={8}>
                <Segment style={{ paddingTop: '30px', paddingBottom: '30px' }}
                         textAlign='center'>
                  <Icon name='chevron right' color='pink' />
                  <Link to='/sign-in'>
                    <span className='ui text large'>すでに登録済みの方</span>
                  </Link>
                </Segment>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </div>

        <Segment>
          <Image alt='banner' />
        </Segment>
      </div>
    )
  }
}
