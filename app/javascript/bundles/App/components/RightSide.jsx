import React, { PureComponent } from 'react'
import PropType from 'prop-types'
//import { Link } from 'react-router-dom'
import Link from './_Link'
import BoxTitle from './_BoxTitle'

import {
  Grid, List, Segment, Button, Image
} from 'semantic-ui-react'

export default class RightSide extends PureComponent {

  render() {
    const { widgets } = this.props

    return (
      <Grid.Column computer={4} mobile={16} style={{ paddingLeft: '5px', paddingRight: '5px' }}>
        {widgets.map((widget, i) =>
          <Segment basic style={{backgroundColor: '#fff', borderRadius: 5}} key={i}>
            <BoxTitle as='h4' title={widget.title} style={{fontSize: '21px'}} />
            <List divided style={{margin: 0}} verticalAlign='middle' size='massive'>
              {widget.items.map((item, i) =>
                <List.Item key={i}>
                  <Link to={`/${item.name_roman}`}
                        style={{
                          flex: 1,
                          display: 'flex',
                          flexDirection: 'row'
                        }}>
                    <Image avatar src={`/images/search_keys/${item.id}.png`} style={{width: 30, height: 30}}/>
                    <List.Content style={{maxWidth: '175px', color: '#000', fontSize: '1rem', whiteSpace: 'nowrap', textDecoration: 'underline' }}>{item.name}</List.Content>
                  </Link>
                </List.Item>
              )}
            </List>
          </Segment>
        )}
      </Grid.Column>
    )
  }
}
