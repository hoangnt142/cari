import React, { PureComponent } from 'react'
import PropType from 'prop-types'
import { Link } from 'react-router-dom'
import SearchInput from '../containers/search/InputContainer'
import BoxTitle from './_BoxTitle'
import MobileBoxTitle from './mobile/_BoxTitle'
import { chunkArray } from '../lib/semantic'

import {
  Button, Grid, Icon,
  Search, Segment, Header, Label,
  Responsive, Image,
  Container, List
} from 'semantic-ui-react'

import PageTitle from './PageTitle'

const style = {
  title: {
    display: 'flex',
    padding: '40px 0px 35px 0px',
    alignItems: 'center'
  },
  circular: {
    width: 70,
    minWidth: 70,
    height: 70,
    padding: 0,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    margin: 0
  }
}

export default class NotFoundPage extends PureComponent {

  static propTypes = {
  }
  static defaultProps = {
  }

  constructor(props) {
    super(props)

    const designWidth = 640
    let innerWidth = window.innerWidth
    if (innerWidth < 400) {
      innerWidth = 400
    } else if (innerWidth > 640) {
      innerWidth = 640
    }
    this.state = {
      circularSize: `${(innerWidth*140)/designWidth}px`,
      circularFontSize1: `${(innerWidth*50)/designWidth}px`,
      circularFontSize2: `${(innerWidth*30)/designWidth}px`,
      titleSize: `${(innerWidth*32)/designWidth}px`,
      titleLineHeight: `${(innerWidth*46)/designWidth}px`
    }

    const { dataInitialize } = props
    dataInitialize()
    // this.responsveUpdate = this.responsiveUpdate.bind(this)
  }

  componentDidMount() {
    const { fetch } = this.props
    // Sub data should be gotten after being mounted.
    //fetch('/api/cache/pages/1/sub')
  }

  componentWillUnmount() {
    const { reset } = this.props
    // Resets the state as initial condition even when coming back from the other page.
    reset()
  }

  responsveUpdate = () => {
    const designWidth = 640
    let innerWidth = window.innerWidth
    if (innerWidth < 400) {
      innerWidth = 400
    } else if (innerWidth > 640) {
      innerWidth = 640
    }
    this.setState({
      circularSize: `${(innerWidth*140)/designWidth}px`,
      circularFontSize1: `${(innerWidth*50)/designWidth}px`,
      circularFontSize2: `${(innerWidth*30)/designWidth}px`,
      titleSize: `${(innerWidth*32)/designWidth}px`,
      titleLineHeight: `${(innerWidth*46)/designWidth}px`
    })
  }

  render() {
    console.log('Not found page is rendering')
    const { api, page, regions, widgets } = this.props
    const {circularSize, circularFontSize1, circularFontSize2, titleSize, titleLineHeight} = this.state
    return (
      <React.Fragment>
        <Responsive maxWidth={991} onUpdate={this.responsveUpdate.bind(this)}>
          <PageTitle
            title='NotFoundPage'
            showTitle={false}
            page={page}
            breadCrumbs={[
              { title: '看護師求人サイトCarriee', url: '/' },
              { title: 'NotFoundPage', url: '/not_found_page' },
            ]}
            showLastBreadCrumbs={false}
          >
            <div style={Object.assign({}, style.title, { flexDirection: 'column', padding: '30px 0px 10px 0px' })}>
              <Segment
                circular
                inverted
                color='pink'
                style={Object.assign({}, style.circular, { width: circularSize, height: circularSize, minWidth: circularSize, marginBottom: '1rem' })}>
                <Header inverted>
                  <p style={{ fontSize: circularFontSize1, margin: 0, lineHeight: circularFontSize1, fontWeight: '520' }}>404</p>
                  <p style={{ fontSize: circularFontSize2, margin: 0, lineHeight: circularFontSize2 }}>エラー</p>
                </Header>
              </Segment>
              <Header style={{color: '#666666', margin: '0', fontSize: titleSize, textAlign: 'center', lineHeight: titleLineHeight}}>申し訳ありません。<br />お探しのページが<br />見つかりませんでした。</Header>
            </div>
          </PageTitle>

          <Container style={{marginTop: '1.5rem'}}>
            <Segment style={{padding: '20px', boxShadow: 'none'}}>
              <div style={{fontSize: '16px'}}>
                <p style={{marginBottom: '10px'}}>お探しのページは「すでに削除されている」、「アクセスしたアドレスが異なっている」などの理由で見つかりませんでした。</p>
                <p style={{marginBottom: '14px'}}>お手数ですが、以下から改めてページをお探しください。</p>
              </div>
              <div style={{maxWidth: '570px',
                           margin: 'auto',
                           display: 'flex',
                           flexDirection: 'row',
                           justifyContent: 'center'
              }}>
                <SearchInput />
              </div>
              <BoxTitle
                as='h2'
                title='エリアから看護師求人を探す 【全国対応】'
                style={{width: 'unset', margin: '3rem 0px 0px'}}
              />
              <Grid style={{margin: '0 -20px'}}>
                <Grid.Column mobile={16} style={{padding: 0}}>
                  {regions.map((region, i) =>
                    <div key={i} style={{fontSize: '16px'}}>
                      <Label size='large' style={{ width: '100%', borderRadius: 0, fontSize: '16px', backgroundColor: '#eefaff' }}>
                        {region.label}
                      </Label>
                      <ul className='errorPageAreas'>
                        {region.prefectures.map((prefecture, i) =>
                          <li key={prefecture.id}><Link to={`/${prefecture.name_roman}`}>{prefecture.name}</Link></li>
                        )}
                      </ul>
                    </div>
                  )}
                </Grid.Column>
              </Grid>

              {widgets && widgets.map((widget, i) =>
                <Grid style={{margin: '0 -20px'}} key={i}>
                  <Grid.Column mobile={16} style={{padding: 0}}>
                    <MobileBoxTitle
                      as='h2'
                      title={widget.title}
                      fontSize='18px'
                      style={{width: 'unset', margin: 0}}
                    />
                    <ul className='errorPageAreas'>
                      {widget.items.map((item, ii) =>
                        <li key={item.id}><Link to={`/${item.name_roman}`}>{item.name}</Link></li>
                      )}
                    </ul>
                  </Grid.Column>
                </Grid>
              )}
            </Segment>
          </Container>
        </Responsive>

        <Responsive minWidth={992}>
          <PageTitle
            title='NotFoundPage'
            showTitle={false}
            page={page}
            breadCrumbs={[
              { title: '看護師求人サイトCarriee', url: '/' },
              { title: 'NotFoundPage', url: '/not_found_page' },
            ]}
            showLastBreadCrumbs={false}
          >
            <div style={style.title}>
              <Segment
                circular
                inverted
                color='pink'
                style={style.circular}>
                <Header inverted>
                  <p style={{ fontSize: '25px', margin: 0, lineHeight: '25px', fontWeight: '520' }}>404</p>
                  <p style={{ fontSize: '17px', margin: 0, lineHeight: '20px' }}>エラー</p>
                </Header>
              </Segment>
              <Header style={{color: '#666666', margin: '0 0 0 1.5rem', fontSize: '32px'}}>申し訳ありません。お探しのページが見つかりませんでした。</Header>
            </div>
          </PageTitle>

          <Container style={{marginTop: '2rem'}}>
            <Segment style={{padding: '45px 20px', boxShadow: 'none'}}>
              <div style={{textAlign: 'center', fontSize: '16px'}}>
                <p style={{marginBottom: '10px'}}>お探しのページは「すでに削除されている」、「アクセスしたアドレスが異なっている」などの理由で見つかりませんでした。</p>
                <p style={{marginBottom: '14px'}}>お手数ですが、以下から改めてページをお探しください。</p>
              </div>
              <div style={{maxWidth: '570px', margin: 'auto', display: 'flex' }}>
                <SearchInput />
              </div>
              <BoxTitle
                as='h2'
                title='エリアから看護師求人を探す 【全国対応】'
                style={{marginTop: '3.7rem', fontSize: '24px'}}
              />
              <Grid padded>
                {regions.length > 0 && chunkArray(regions, 3).map((chunkRegions, i) =>
                  <Grid.Column width={8} key={i} style={i == 0 ? {padding: '1rem 20px 0 0'} : {padding: '1rem 0 0 20px'}}>
                    {chunkRegions && chunkRegions.map((region, i) =>
                      <div key={i} style={{fontSize: '16px'}}>
                        <Label color='grey' size='large' style={{ width: '100%', borderRadius: 0, fontSize: '16px' }}>
                          {region.label}
                        </Label>
                        <ul className='errorPageAreas'>
                          {region.prefectures.map((prefecture, i) =>
                            <li key={prefecture.id}><Link to={`/${prefecture.name_roman}`}>{prefecture.name}</Link></li>
                          )}
                        </ul>
                      </div>
                    )}
                  </Grid.Column>
                )}
              </Grid>

              {widgets && widgets.map((widget, i) =>
                <React.Fragment key={i}>
                  <BoxTitle
                    as='h2'
                    title={widget.title}
                    style={{marginTop: '3rem', fontSize: '24px'}}
                  />
                  <List horizontal>
                    {widget.items.map((item, ii) =>
                      <List.Item key={ii} as={Link} to={`/${item.name_roman}`} style={{marginRight: '32px', marginLeft: 0}}>
                        <Image avatar src={`/images/search_keys/${item.id}.png`} style={{width: 30, height: 30}}/>
                        <List.Content style={{maxWidth: '215px', color: '#000', fontSize: '16px', whiteSpace: 'nowrap'}} >{item.name}</List.Content>
                      </List.Item>
                    )}
                  </List>
                </React.Fragment>
              )}
            </Segment>
          </Container>
        </Responsive>
      </React.Fragment>
    )
  }
}
