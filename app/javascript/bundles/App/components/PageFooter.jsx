import React, { PureComponent } from 'react'
//import { Link } from 'react-router-dom'
import Link from './_Link'

import {
  Button,
  Container,
  Grid,
  Icon,
  Label,
  List,
  Menu,
  Segment,
  Responsive,
  Image,
  Header,
  Dimmer,
  Loader
} from 'semantic-ui-react'

import BoxTitle from './_BoxTitle'

import '../../../app/assets/stylesheets/layout.scss'


const style = {
  icon: {
    color: '#989898',
    margin: '0 0 0.4rem 0'
  },
  menuGrid: {
    background: '#f6f3f3',
    fontSize: '1rem',
    margin: '0',
    height: '5rem'
  },
  menuGridPC: {
    background: '#f6f3f3',
    fontSize: '1rem',
    margin: '0',
    height: '3.8rem',
  },
  copyRight: {
    borderRadius: 0,
    margin: 0,
    padding: '10px'
  },
  logout: {
    cursor: 'pointer',
  }
}
import pekoLogo from 'app/assets/images/peko-logo'

const LoginLogoutMenu = (props) => {
  const { user, logout } = props
  if(user) {
    return (
      <span onClick={() => logout()} className='ui text pink' style={style.logout}>
        <Icon name='sign out' style={style.icon}></Icon>
        <Responsive maxWidth={991} as='br' />
        ログアウト
      </span>
    )
  }
  else {
    return (
      <Link to='/sign-in'>
        <Icon name='sign in' style={style.icon}></Icon>
        <Responsive maxWidth={991} as='br' />
        <span className='ui text pink'>ログイン</span>
      </Link>
    )}
}

const OtherMenu = (props) => {
  const { title, url, icon } = props
  return (
    <Link to={url}>
      <Icon name={icon} style={style.icon} size='large'></Icon>
      <Responsive maxWidth={991} as='br' />
      <span className='ui text pink'>{title}</span>
    </Link>
  )
}

const styles = {
  pc: {
    searchBox: {
      width: '950px',
    }
  },
  mobile: {
    searchBox: {
    }
  }
}

class SearchBox extends PureComponent {

  openModalArea = () => this.openModal('area')
  openModalSearchKey = () => this.openModal('detail')
  openModalJobContract = () => this.openModal('detail', 'jobContractIds')
  openModalQualification = () => this.openModal('detail', 'qualificationIds')
  openModalHospitalType = () => this.openModal('detail', 'hospitalTypeIds')
  openModalJobType = () => this.openModal('detail', 'jobTypeIds')
  openModalJobDetail = () => this.openModal('detail', 'jobDetailIds')

  openModal = (searchType, position=null) => {
    this.props.openSearchModal(searchType, position, true)
  }


  constructor(props) {
    super(props)
    this.state = Object.assign({}, SearchBox.initState)
  }

  render() {
    const {
      blocks,
      regions,
      prefectures
    } = this.props
    return (
      <Segment style={{
        marginTop: '2rem',
        marginLeft: '0',
        marginRight: '0',
        paddingLeft: '0',
        paddingRight: '0'
      }}>
        <div>

          <div style={{ margin: '0 0.8em' }}>
            <BoxTitle title='看護師求人を探す' as='h3' />
          </div>
          <Grid padded>
            <Grid.Row style={{ paddingTop: '1rem', paddingBottom: '0' }}>
              <Grid.Column width={8} stretched style={{ paddingRight: '0.5rem' }}>
                <Button
                  color='lightgrey'
                  style={styleMobile.gradientButton}
                  onClick={this.openModalArea}
                ><Icon name='map marker alternate' />都道府県で探す</Button>
              </Grid.Column>
              <Grid.Column width={8} stretched style={{ paddingLeft: '0.5rem' }}>
                <Button
                  color='lightgrey'
                  style={styleMobile.gradientButton}
                  onClick={this.openModalJobType}
                ><Icon name='suitcase' />仕事内容で探す</Button>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row style={{ paddingTop: '1rem', paddingBottom: '0' }}>
              <Grid.Column width={8} stretched style={{ paddingRight: '0.5rem' }}>
                <Button
                  color='lightgrey'
                  style={styleMobile.gradientButton}
                  onClick={this.openModalJobContract}
                ><Icon name='file alternate' />雇用形態で探す</Button>
              </Grid.Column>
              <Grid.Column width={8} stretched style={{ paddingLeft: '0.5rem' }}>
                <Button
                  color='lightgrey'
                  style={styleMobile.gradientButton}
                  onClick={this.openModalHospitalType}
                ><Icon name='hospital' />病院施設で探す</Button>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row style={{ paddingTop: '1rem', paddingBottom: '0' }}>
              <Grid.Column width={8} stretched style={{ paddingRight: '0.5rem' }}>
                <Button
                  color='lightgrey'
                  style={styleMobile.gradientButton}
                  onClick={this.openModalJobDetail}
                ><Icon name='hand point up' />こだわりで探す</Button>
              </Grid.Column>
              <Grid.Column width={8} stretched style={{ paddingLeft: '0.5rem' }}>
                <Button
                  color='lightgrey'
                  style={styleMobile.gradientButton}
                  onClick={this.openModalQualification}
                ><Icon name='certificate' />保有資格で探す</Button>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row width={16} style={{ paddingTop: '1rem' }}>
              <Grid.Column stretched>
                <Button color='blue'
                        style={{ height: '4em' }}
                        onClick={this.openModalSearchKey}
                ><Icon name='star' />希望条件で探す</Button>
              </Grid.Column>
            </Grid.Row>
          </Grid>

          <div style={{ marginTop: '1.5rem' }}>
            {blocks.map((b,i) => (
              <SearchKeyBlock key={i} title={b.title} searchKeys={b.searchKeys} />
            ))}

            <PrefecturesBlock regions={regions} prefectures={prefectures} />
          </div>
        </div>
      </Segment>
    )
  }
}


export default class PageFooter extends PureComponent {

  menus = [{ title: 'TOPページ', url: '/', icon: 'home'},
           { title: '求人検索', url: '/search', icon: 'search'}
  ]
  links = [
    /*           { title: '会員規約', url: '/' },*/
    { title: '利用規約', url: '/membership-regulations' },
    { title: '個人情報取り扱い', url: '/privacy' },
    /*           { title: '初めての方へ', url: '' },*/
    { title: 'お問い合わせ', url: '/contact' },
    /*           { title: 'サイトマップ', url: '/sitemap' },*/
    { title: '会社概要', url: '/about-us' }
  ]

  constructor(props) {
    super(props)
    this.props.init()
  }

  render() {
    const {
      user,
      logout,
      showNav,

      qualifications,
      job_contracts,
      hospital_types,
      job_types,
      job_details,
      prefectures,
      regions,

      openSearchModal,
    } = this.props

    const blocks = [
      { title: '雇用形態で探す', searchKeys: job_contracts },
      { title: '保有資格で探す', searchKeys: qualifications },
      { title: '病院施設で探す', searchKeys: hospital_types },
      { title: '仕事内容で探す', searchKeys: job_types },
      { title: 'こだわりの条件で探す', searchKeys: job_details },
    ]


    return (
      <div>
        {showNav && (
           <div>
             <Responsive maxWidth={991}
                         as='div'
                         style={{}}>
               <SearchBox blocks={blocks}
                          regions={regions}
                          prefectures={prefectures}
                          openSearchModal={openSearchModal}
               />
             </Responsive>
             <Responsive minWidth={992}
                         as='div'
                         style={{
                           maxWidth: '1027px',
                           margin: 'auto',
                           /*
                              paddingLeft: '2rem',
                              paddingRight:'2rem',
                            */
                           backgroundColro: 'white',
                         }}>
               <SearchBox blocks={blocks}
                          regions={regions}
                          prefectures={prefectures}
                          openSearchModal={openSearchModal}
               />
             </Responsive>
           </div>
        )}
        <footer style={{
          backgroundColor: 'white',
          padding: '0.4rem 0 0 0',
          marginTop: '10px'
        }}>
          <div style={{ textAlign: 'center' }}>

            {/* smartphone view */}
            <Responsive maxWidth={991} style={{ paddingLeft: '0.4rem', paddingRight: '0.4rem' }}>
              <Grid columns='equal'
                    divided
                    verticalAlign='middle'
                    textAlign='center'
                    style={style.menuGrid}>
                {this.menus.map((link, i) =>
                  <Grid.Column key={i} style={{ }}>
                    <OtherMenu {...link} />
                  </Grid.Column>
                )}
                {/*<Grid.Column>
                    <LoginLogoutMenu user={user} logout={logout} />
                    </Grid.Column>*/}
              </Grid>
            </Responsive>

            {/* pc view */}
            <Responsive minWidth={992} as='div' style={{ width: '700px', margin: '1rem auto 0 auto' }}>
              <Grid padded
                    centered
                    verticalAlign='middle'
                    columns={3}
                    style={style.menuGridPC}>
                {this.menus.map((link, i) =>
                  <Grid.Column key={i} width={3} textAlign='center'>
                    <OtherMenu {...link} />
                  </Grid.Column>
                )}
                {/*<Grid.Column width={3} textAlign='center'>
                    <LoginLogoutMenu user={user} logout={logout} />
                    </Grid.Column>*/}
              </Grid>
            </Responsive>

            <List horizontal celled style={{
              marginTop: '1.5rem',
              marginBottom: '0.5rem'
            }}>
              {this.links.map((link, i) =>
                <List.Item key={i}><Link to={link.url} style={{ textDecoration: 'underline' }}>{link.title}</Link></List.Item>
              )}
            </List>
          </div>

          <span style={{display: 'flex', justifyContent: 'center', marginBottom: '1rem', marginTop: '0.5rem'}}>
            <Image src={pekoLogo} wrapped size='tiny' alt='peko' />
          </span>
          <Segment inverted color='pink' align='center' style={style.copyRight}>
            peko Inc. &copy; Copyright All Rights Reserved.
          </Segment>

        </footer>
      </div>
    )
  }
}



class SearchKeyBlock extends PureComponent {
  render() {
    const {
      title,
      searchKeys,
    } = this.props

    const fetching = !searchKeys

    return (
      <Dimmer.Dimmable as='nav' dimmed={fetching}>
        <div style={{ flex: 1, flexDirection: 'row', display: 'flex' }}>
          <div style={{ flex: 4, height: '2px', backgroundColor: 'rgb(243,101,129)' }} />
          <div style={{ flex: 7, height: '2px', backgroundColor: 'rgb(201,201,201)' }} />
        </div>
        <div style={{
          height: '3em',
          lineHeight: '3em',
          color: 'pink',
          backgroundColor: 'rgb(242,242,242)',
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center',
          paddingLeft: '1em'
        }}>
          <Header as='h4' color='pink'>{title}</Header>
        </div>

        <Grid style={{
          padding: '0',
          margin: '0'
        }}>
          <Grid.Row style={{
            padding: '0.5em 1em'
          }}>
            <List horizontal celled style={{
              marginTop: '1rem',
              marginBottom: '1rem'
            }}>
              {searchKeys && searchKeys.map((sk, i) =>
                <List.Item key={i}
                           style={{ lineHeight: '1.4em' }}
                  ><Link to={`/${sk.name_roman}`} style={{ textDecoration: 'underline' }}>{sk.name}</Link></List.Item>
              )}
            </List>
          </Grid.Row>
        </Grid>
        <Dimmer active={fetching} inverted>
          <Loader inverted />
        </Dimmer>
      </Dimmer.Dimmable>
    )
  }
}

class PrefecturesBlock extends PureComponent {
  render() {
    const {
      prefectures,
      regions,
    } = this.props

    const fetching = !regions

    return (
      <Dimmer.Dimmable as='nav' dimmed={fetching}>
        <div style={{ flex: 1, flexDirection: 'row', display: 'flex' }}>
          <div style={{ flex: 4, height: '2px', backgroundColor: 'rgb(243,101,129)' }} />
          <div style={{ flex: 7, height: '2px', backgroundColor: 'rgb(201,201,201)' }} />
        </div>
        <div style={{
          height: '3em',
          lineHeight: '3em',
          color: 'pink',
          backgroundColor: 'rgb(242,242,242)',
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center',
          paddingLeft: '1em'
        }}>
          <Header as='h4' color='pink'>都道府県で探す</Header>
        </div>

        {regions && regions.map((r, i) =>
          <Grid key={i} style={{
            padding: '0',
            margin: '0'
          }}>
            <Grid.Row style={{
              backgroundColor: 'rgb(239,250,255)',
              lineHeight: '1.5em',
              padding: '0.5em 0.5em'
            }}>
              {r.name}
            </Grid.Row>
            <Grid.Row style={{
              padding: '0.5em 1em'
            }}>
              <List horizontal celled>
                {prefectures.filter(p => p.region_id == r.id).map((p,j) => (
                  <List.Item key={j}
                             style={{ lineHeight: '1.4em' }}
                    >
                    <Link to={`/${p.name_roman}`} style={{ textDecoration: 'underline' }}>{p.name}</Link>
                  </List.Item>
                ))}
              </List>
            </Grid.Row>
          </Grid>
        )}
        <Dimmer active={fetching} inverted>
          <Loader inverted />
        </Dimmer>
      </Dimmer.Dimmable>
    )
  }
}

const styleMobile =  {
  gradientButton: {
    background: 'linear-gradient(180deg, #fff 14.52%, #e6e7e8)',
    height: '4em',
    border: '1px solid #ccc',
  },
}
