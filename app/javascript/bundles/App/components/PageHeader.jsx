import ReactDOM from 'react-dom'
import React, { PureComponent } from 'react'
import { Link } from 'react-router-dom'
import { savedJobIds } from 'lib/function'

import {
  Button,
  Container,
  Grid,
  Icon,
  Label,
  Image,
  Menu,
  Segment,
  Responsive,
  Sticky,
  Dimmer,
  Progress,
  Divider
  //  Visibility
} from 'semantic-ui-react'
import logo from 'app/assets/images/logo'
import logoOld from 'app/assets/images/logo_old'
import '../../../app/assets/stylesheets/layout.scss'
import nurse from 'app/assets/images/nurse'


const style = {
  button: {
    width: '10em'
  }
}

const HeaderTab = (props) => {
  let icon;
  if(props.icon == 'nurse') {
    icon = (
      <Image
        style={{
          height: '1.6em',
          width: '1.6em',
          display: 'inline'
        }}
        src={nurse}
        alt='Nurse'
      />
    )
  }
  else {
    icon = (
      <Icon
        style={{
          fontSize: '1.6em',
          color: '#c8c8c8'
        }}
        name={props.icon} />
    )
  }

  return (
    <Link to={props.to} style={{display: 'block'}} className='headerSearchIcon'> {/*Temporary style*/}
      <div>
        {icon}
        <br />
        <span className='ui text grey'
              style={{ fontWeight: 'bold' }}>{props.children}</span>
      </div>
    </Link>
  )
}

const HeaderLink = (props) => (
  <span style={{ paddingLeft: '1rem' }}>
    <Icon name='chevron right' />
    <Link to={props.to} style={{
      color: '#fff'
    }}>
      {props.children}
    </Link>
  </span>
)

const GradientButon = (props) => (
  <Link to={props.to} className='gradientButton'>
    {props.children}
  </Link>
)




export default class PageHeader extends PureComponent {
  //state = { stickerActive: false }
  state = { devDimmer: false }
  setMenuRef = menu => this.setState({ menu: menu })

  /*
     handleUpdate = () => {
     console.log('handleUpdate')
     this.setState({ stickerActive: true })
     }
   */

  render() {

    const {
      stickerContext,
      logout,
      user
    } = this.props
    const { menu } = this.state

    return (
      <header>

        {/* needed only by pc */}
        <Responsive minWidth={992}>
          <Segment inverted color='pink'>
            <Container>
              <Grid verticalAlign='middle' >
                <Grid.Row style={{ padding: '0.4em' }}>
                  <Grid.Column width={16}>
                    <Link to='/'
                          style={{
                            fontWeight: 'bold',
                            color: '#fff'
                          }}>転職なら看護師求人サイトCarriee（キャリー）</Link>
                  </Grid.Column>
                  {/*<Grid.Column computer={11} tablet={16} textAlign='right'>
                      {user ? (
                      <span>
                      <Link to='/mypage'>
                      <Button className="sign-up"
                      color='green'
                      size='small'
                      style={style.button}>マイページ</Button>
                      </Link>
                      <Button className="logout"
                      onClick={() => logout()}
                      size='small'
                      style={style.button}>
                      ログアウト
                      </Button>
                      </span>
                      ) : (
                      <span>
                      <Link to='/sign-up'>
                      <Button className="sign-up"
                      color='green'
                      size='small'
                      style={style.button}>無料会員登録</Button>
                      </Link>
                      <Link to='/sign-in'>
                      <Button style={style.button}
                      className='login'
                      size='small'>
                      ログイン
                      </Button>
                      </Link>
                      </span>
                      )}

                      <HeaderLink to='/for-beginner'>
                      初めての方へ
                      </HeaderLink>
                      <HeaderLink to='/'>
                      採用ご担当者の方へ
                      </HeaderLink>

                      <Button icon='plus' onClick={() => this.setState({ devDimmer: true})} />
                      <Dimmer active={this.state.devDimmer}
                      page
                      inverted
                      onClickOutside={() => this.setState({ devDimmer: false})}>
                      <Segment size='large' inverted style={{ width: '30%' }}>
                      <h1>Required Pages to release</h1>
                      <ul>
                      <li><Link to="/">Top</Link></li>

                      <li><Link to="/search">Search</Link></li>

                      <li><Link to="/information">Information</Link></li>
                      <li><Link to="/repute-posting">ReputePosting</Link></li>
                      <li><Link to="/repute-posted">ReputePosted</Link></li>

                      <li><Link to="/contact">Contact</Link></li>
                      <li><Link to="/contact-sent">ContactSent</Link></li>

                      <li><Link to="/sitemap">Sitemap</Link></li>
                      <li><Link to="/my-history">MyHistory</Link></li>
                      <li><Link to="/my-jobs">MyJobs</Link></li>

                      <li><Link to="/sign-up">SignUp</Link></li>
                      <li><Link to="/sign-in">SignIn</Link></li>
                      <li><Link to="/password-generate">PasswordGenerate</Link></li>
                      <li><Link to="/password-change">PasswordChange</Link></li>

                      <li><Link to="/mypage">Mypage</Link></li>
                      <li><Link to="/mypage/jobs">MypageJobs</Link></li>
                      <li><Link to="/mypage/my-reputes">MypageMyReputes</Link></li>
                      <li><Link to="/mypage/settings">MypageSettings</Link></li>
                      <li><Link to="/mypage/password-change">MypagePasswordChange</Link></li>

                      <li><Link to="/page1/page404">404</Link></li>


                      </ul>

                      <h1>Optional</h1>
                      <ul>

                      <li><Link to="/blog">Blog</Link></li>
                      <li><Link to="/blog/page/3">Blog</Link></li>

                      <li><Link to="/reputes">Reputes</Link></li>


                      <li><Link to="/blog/カテゴリー1">Category</Link></li>
                      <li><Link to="/blog/カテゴリー1/page/3">Category</Link></li>
                      <li><Link to="/page1">page</Link></li>
                      <li><Link to="/agents">Agents</Link></li>
                      <li><Link to="/hello-work-list">HelloWorkList</Link></li>





                      </ul>
                      </Segment>

                      </Dimmer>
                      </Grid.Column>*/}
                </Grid.Row>
              </Grid>
            </Container>
          </Segment>
        </Responsive>

        <div style={{ backgroundColor: 'white' }}>

          {/* smartphone view */}
          <Responsive maxWidth={991}>
            <Grid verticalAlign='middle'
                  style={{
                    margin: 0,
                    backgroundColor: 'white',
                    height: '65px'
                  }}>
              <Grid.Row style={{
                height: '56px',
                borderTop: '2px solid rgb(243,101,129)',
                /*marginTop: '5px',*/
                padding: 0
              }}>

                <Grid.Column width={7}
                             style={{
                               backgroundColor: 'rgb(243,101,129)',
                               borderBottom: '2px solid rgb(243,101,129)',
                               height: '56px',
                               display: 'flex',
                             }}>
                  <Link to='/'
                        style={{
                          display: 'flex',
                          flex: 1,
                          flexDirection: 'column',
                          justifyContent: 'center'
                        }}>

                    <p style={{
                      color: 'white',
                      fontSize: '2rem',
                      margin: '0'
                    }}><Image src={logo} alt='看護師の転職・求人サイトCarriee（キャリー）' /></p>
                  </Link>
                </Grid.Column>

                <Grid.Column width={1}
                             style={{
                               height: '56px',
                               padding: 0
                             }}>
                  <svg height='100%' width='20'>
                    <polygon points='0,0 0,56 15,0' fill='rgb(243,101,129)' />
                  </svg>
                </Grid.Column>

                <Grid.Column width={4}
                             style={{
                               height: '56px',
                               display: 'flex',
                             }}>
                  <Link to='/my-history'
                        style={{
                          justifyContent: 'center',
                          flexDirection: 'column',
                          flex: 1,
                          display: 'flex',
                        }}>
                    <p id='historyCount' style={{ margin: '0',
                                                  fontSize: '1.3rem',
                                                  textAlign: 'center',
                                                  fontWeight: 'bold',
                                                  color: 'rgb(243,101,129)'
                    }}>{savedJobIds('history', true).length}</p>
                    <p style={{ fontSize: '0.75rem',
                                lineHeight: '1rem',
                                color: 'black',
                                textAlign: 'center'
                    }}>最近見た求人</p>
                  </Link>
                </Grid.Column>

                <Grid.Column width={4}
                             style={{
                               height: '54px',
                               display: 'flex',
                             }}>
                  <Link to='/my-jobs'
                        style={{
                          justifyContent: 'center',
                          flexDirection: 'column',
                          flex: 1,
                          display: 'flex',
                        }}>
                    <p id='favoriteCount' style={{ margin: '0',
                                                   fontSize: '1.3rem',
                                                   textAlign: 'center',
                                                   fontWeight: 'bold',
                                                   color: 'rgb(243,101,129)'
                    }}>{savedJobIds('favorite').length}</p>
                    <p style={{ fontSize: '0.75rem',
                                lineHeight: '1rem',
                                color: 'black',
                                textAlign: 'center'
                    }}>保存した求人</p>
                  </Link>
                </Grid.Column>
              </Grid.Row>
            </Grid>
            {/*<Sticky context={stickerContext()}
                onStick={() => {
                // Without setting zIndex, the Sticked element will be laid backward.
                const dom2 = ReactDOM.findDOMNode(menu)
                dom2.parentNode.style.zIndex = 100
                }}>
                <div ref={this.setMenuRef}>
                <Grid verticalAlign='middle'
                textAlign='center'
                columns='equal'
                celled
                style={{
                fontSize: '0.9rem',
                margin: '0 0',
                backgroundColor: 'white',
                borderBottom: '1px solid #ccc'
                }}>
                <Grid.Row>
                <Grid.Column>
                <HeaderTab icon='home' to='/'>
                ホーム
                </HeaderTab>
                </Grid.Column>
                {user ? (
                <Grid.Column>
                <HeaderTab icon='user' to='/mypage'>
                マイページ
                </HeaderTab>
                </Grid.Column>
                ) : (
                <Grid.Column>
                <HeaderTab icon='sign in' to='/sign-in'>
                ログイン
                </HeaderTab>
                </Grid.Column>
                )}
                <Grid.Column>
                <HeaderTab icon='search' to='/search'>
                求人検索
                </HeaderTab>
                </Grid.Column>
                <Grid.Column>
                <HeaderTab icon='sidebar' to='/mypage'>
                メニュー
                </HeaderTab>
                </Grid.Column>
                </Grid.Row>
                </Grid>
                </div>
                </Sticky>*/}
          </Responsive>

          {/* pc view */}
          <Responsive minWidth={992}>
            <Container>
              <Grid verticalAlign='middle' textAlign='center' padded>
                <Grid.Row style={{height: 95}}>
                  <Grid.Column width={3} textAlign='left' >
                    <Link to='/'>
                      <Image src={logoOld} wrapped size='large' alt='看護師の転職・求人サイトCarriee（キャリー）' />
                    </Link>
                  </Grid.Column>
                  <Grid.Column width={8}>
                    {/*<Grid verticalAlign='middle' columns='equal' divided>
                        <Grid.Row>
                        <Grid.Column textAlign='left'>
                        <HeaderTab icon="search"
                        to='/search'>
                        看護師求人を<br />
                        探す
                        </HeaderTab>
                        </Grid.Column>
                        <Grid.Column>
                        <HeaderTab icon="hospital"
                        to='/reputes'>
                        病院の<br />
                        評判
                        </HeaderTab>
                        </Grid.Column>
                        <Grid.Column>
                        <HeaderTab icon='nurse'
                        to='/media'>
                        看護師転職<br />
                        ノウハウ
                        </HeaderTab>
                        </Grid.Column>
                        <Grid.Column>
                        <HeaderTab icon="user"
                        to='/agents'>
                        エージェント<br />
                        に依頼
                        </HeaderTab>
                        </Grid.Column>
                        </Grid.Row>
                        </Grid>*/}
                  </Grid.Column>

                  <Grid.Column width={5}>
                    <Grid columns='equal'>
                      <Grid.Row>
                        <Grid.Column style={{ padding: '0.5em' }}>
                          <GradientButon to='/my-jobs'>
                            <Icon name="heart" color='pink'/>
                            <span>保存した求人</span><br />
                            <Label circular color='red'>{savedJobIds('favorite').length}</Label>
                          </GradientButon>
                        </Grid.Column>
                        <Grid.Column style={{ padding: '0.5em' }}>
                          <GradientButon to='/my-history'>
                            <Icon name="check" color='blue' />
                            <span>最近見た求人</span><br />
                            <Label circular color='red'>{savedJobIds('history', true).length}</Label>
                          </GradientButon>
                        </Grid.Column>
                      </Grid.Row>
                    </Grid>
                    </Grid.Column>

                </Grid.Row>
              </Grid>
            </Container>
          </Responsive>
        </div>
        <Responsive minWidth={992} as={Progress} percent={45} color='blue' size='tiny' style={{borderRadius: 0, margin: 0}}/>
      </header>
    );
  }
}
