import React, { PureComponent } from 'react'
import history from "../lib/history"

/* Link in react-router-dom don't change the scroll position.
 * This Link class will do it.
 */
export default class Link extends PureComponent {
  constructor(props) {
    super(props)
  }

  onClick = () => {
    history.push(this.props.to)
    scrollTo(0,0)
  }

  render() {
    const { to,children } = this.props
    return <a href={to} onClick={(e) => {
      e.preventDefault()
      this.onClick()
    }}
    {...this.props}>{children}</a>
  }
}
