import React, { PureComponent } from 'react'
import PropType from 'prop-types'

import Slider from 'react-slick'
import {
  Container,
} from 'semantic-ui-react'

import ReactHelmet from 'react-helmet'
import PageTitle from './PageTitle'

import SignUpSlide1 from '../containers/SignUpSlide1Container'
import SignUpSlide2 from '../containers/SignUpSlide2Container'
import SignUpSlide3 from '../containers/SignUpSlide3Container'
import SignUpSlide4 from '../containers/SignUpSlide4Container'
import SignUpSlide5 from '../containers/SignUpSlide5Container'


export default class SignUp extends PureComponent {

  static propTypes = {
  }

  constructor(props) {
    super(props)

    const { fetchPage } = this.props
    fetchPage()
  }

  componentDidMount() {
    const { fetchPrefectures } = this.props
    fetchPrefectures()
  }

  componentWillUnmount() {
    const { reset } = this.props
    // Resets the state as initial condition even when coming back from the other page.
    reset()
  }

  sliderNext = () => {
    this.slider.slickNext()
  }
  sliderPrevious = () => {
    this.slider.slickPrev()
  }

  render() {
    const { page, onChangeSlide } = this.props

    var sliderSettings = {
      dots: false,
      infinite: false,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      draggable: false,
      swipe: false,
      arrows: false,
      lazyLoad: true,
    }

    console.log('rendering')
    return (
      <div>

        <PageTitle
          title='Carriee【無料会員登録】'
          page={page}
          breadCrumbs={[
            { title: '看護師求人サイトCarriee', url: '/' },
            { title: '会員登録', url: '/sign-up' },
          ]}
          showLastBreadCrumbs={false}
        />

        <Container>
          <Slider
            ref={s => this.slider = s}
            afterChange={index => onChangeSlide(index)}
            {...sliderSettings}>
            <div><SignUpSlide1
                   next={this.sliderNext} /></div>
            <div><SignUpSlide2
                   next={this.sliderNext}
                   previous={this.sliderPrevious} /></div>
            <div><SignUpSlide3
                   next={this.sliderNext}
                   previous={this.sliderPrevious} /></div>
            <div><SignUpSlide4
                   next={this.sliderNext}
                   previous={this.sliderPrevious} /></div>
            <div><SignUpSlide5
                   previous={this.sliderPrevious} /></div>
          </Slider>
        </Container>
      </div>
    )
  }
}
