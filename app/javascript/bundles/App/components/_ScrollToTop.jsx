import React, { PureComponent } from 'react'
import ScrollToTop from 'react-scroll-up'
import {
  Icon,
  Responsive
} from 'semantic-ui-react'


export default class GoToTop extends PureComponent {
  render() {
    return (
      <ScrollToTop showUnder={160}>
        <Icon color='pink' size='big' inverted circular name='angle up' />
      </ScrollToTop>
    )
  }
}
