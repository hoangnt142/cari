import React, { PureComponent } from 'react'
import {
  Responsive,
} from 'semantic-ui-react'


export default class SimpleResponsive extends PureComponent {

  render() {
    const { children } = this.props
    return (
      <div>
        <Responsive maxWidth={991}
                    as='div'
                    style={{}}>
          {children}
        </Responsive>
        <Responsive minWidth={992}
                    as='div'
                    style={{
                      maxWidth: '1027px',
                      margin: 'auto',
                      backgroundColro: 'white',
                    }}>
          {children}
        </Responsive>
      </div>
    )
  }
}
