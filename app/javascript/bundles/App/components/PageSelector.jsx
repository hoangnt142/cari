import React, { Component } from 'react'
import PropType from 'prop-types'

import Page from '../containers/PageContainer'
import ArticlePage from '../containers/ArticlePageContainer'
import Category from '../containers/CategoryContainer'
import NotFoundPage from '../containers/NotFoundPageContainer'
import HospitalPage from '../containers/HospitalPageContainer'
import Search from '../containers/SearchContainer'
import Blog from '../containers/BlogContainer'

import { Segment, Dimmer, Loader } from 'semantic-ui-react'

const Empty = () => (
  <Segment style={{ height: '30rem' }}>
    <Dimmer active inverted>
      <Loader />
    </Dimmer>
  </Segment>
)


export default class PageSelector extends Component {

  static propTypes = {
    pathname: PropType.string.isRequired,
  }

  constructor(props) {
    super(props)
    const { fetchPage } = this.props
    fetchPage()
  }

  componentDidUpdate(prevProps, prevState, snapshot) {

    // When it goes to new URL in this instance, fetchPage in the constructor is not called anymore.
    // Call it here to get new page record and it should be called only when URL is changed.
    const { fetchPage } = this.props
    if(prevProps.location.pathname != this.props.location.pathname) {
      console.log('URL has been changed in PageSelector.')
      console.log('Scrolling to Top')
      window.scrollTo(0, 0)
      fetchPage()
    }
  }

  render() {
    const { page, code, location } = this.props
    console.log('PageSelector is rendering', page)
    console.log(this.props)
    if (!page) {
      return <Empty />
    } else if (code == '404' || page.id == 16) {
      return <NotFoundPage />
    } else if (page.id == 10) {
      return <Blog {...this.props} />
    } else {
      switch(page.pageable_type) {
        case 'Article':
          if (page.url != location.pathname) { //loading
            return <Empty />
          }
          return (<ArticlePage {...this.props}/>)
        case 'Category':
          if (page.url != location.pathname) { //loading
            return <Empty />
          }
          return (<Category {...this.props}/>)
        case 'Page':
          return (<Page {...this.props}/>)
        case 'Hospital':
          return (<HospitalPage {...this.props}/>)
        case 'Search':
          return (<Search {...this.props}/>)
        case null:
          return (<Page {...this.props}/>)
        default:
          return (<Empty />)
      }
    }
  }
}
