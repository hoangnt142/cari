import React, { PureComponent } from 'react'
import PropType from 'prop-types'
import { Link } from 'react-router-dom'
import {
  Button, Image, Icon,
  Segment,
  Header,
  Grid, Form, Message,
  Step, Divider, List,
  Responsive
} from 'semantic-ui-react'



const StepNumber = (props) => (
  <div className='ui text large bold pink' style={{ width: '2em',
                                                    backgroundColor: 'white',
                                                    borderRadius: '0.3rem',
                                                    padding: '0.3rem 0.5rem',
                                                    margin: 'auto',
                                                    textAlign: 'center'}}>{props.children}</div>
)

export default class SignUpSlideStep extends PureComponent {
  render() {
    const { step } = this.props

    return (
      <div style={{ marginBottom: '35px' }}>
        <Header as='h2' style={{ textAlign: 'center' }}>簡単な3つのステップでCarrieeに<span className='ui pink text'>無料登録</span>できます。</Header>
        <p className='ui text pink' style={{ textAlign: 'center' }}>※ 登録することで個人が求人募集側に特定されることは一切ございません。</p>

        <Responsive maxWidth={767}>
          <Step.Group unstackable widths={4}>
            {[
               { key: '1', number: '1', title: '会員情報<br />の入力', active: step == '1' },
               { key: '2', number: '2', title: 'プロフィール<br />設定', active: step == '1' },
               { key: '3', number: '3', title: '求人エリアの設定', active: step == '1' },
               { key: '4', title: 'ご登録完了', active: step == '1' },
            ].map(data => (
              <Step key={data.key} active={data.active} style={{ padding: '1em', fontSize: '0.7rem' }}>
                <Step.Content>
                  {data.number && (
                     <StepNumber>{data.number}</StepNumber>
                  )}
                  <Step.Title><span dangerouslySetInnerHTML={{__html: data.title}} /></Step.Title>
                </Step.Content>
              </Step>
            ))}
          </Step.Group>
        </Responsive>
        <Responsive minWidth={768}>
          <Step.Group unstackable widths={4} items={[
            { key: '1', icon: <StepNumber>1</StepNumber>, title: '会員情報の入力', active: step == '1' },
            { key: '2', icon: <StepNumber>2</StepNumber>, title: 'プロフィール設定', active: step == '2' },
            { key: '3', icon: <StepNumber>3</StepNumber>, title: '求人エリアの設定', active: step == '3' },
            { key: '4', title: 'ご登録完了', active: step == '4' },
          ]} />
        </Responsive>
      </div>
    )
  }
}
