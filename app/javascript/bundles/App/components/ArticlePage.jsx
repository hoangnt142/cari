import React, { PureComponent } from 'react'
import PropType from 'prop-types'
import { Link } from 'react-router-dom'
import BoxTitle from './_BoxTitle'
import moment from 'moment'

import {
  Grid, Image, Segment, Container, Header, Responsive, Icon
} from 'semantic-ui-react'

import PageTitle from './PageTitle'


export default class ArticlePage extends PureComponent {

  static propTypes = {
  }
  static defaultProps = {
  }

  constructor(props) {
    super(props)
  }

  componentDidMount() {
    const { fetch } = this.props
    // Sub data should be gotten after being mounted.
    //fetch('/api/cache/pages/1/sub')
  }

  componentWillUnmount() {
    const { reset } = this.props
    // Resets the state as initial condition even when coming back from the other page.
    reset()
  }

  render() {
    const { api, page } = this.props
    const { article, popular_articles } = api
    const { category, cta, image, search_key_articles, nextArticle, prevArticle } = article

    return (
      <div>
        <PageTitle
          title={page.title}
          page={page}
          breadCrumbs={[
            { title: '看護師求人サイトCarriee', url: '/' }
          ].concat(article.breadcrumb)}
          showLastBreadCrumbs={false}
          showTitle={false}
        >
          <Responsive as={Grid} minWidth={992} padded>
            <Grid.Column width={7} textAlign='right' style={{position: 'relative'}}>
              <svg style={{position: 'absolute', left: '30px', zIndex: 1}} xmlns="http://www.w3.org/2000/svg" width="50" height="63" viewBox="0 0 50 63">
                <polygon className="svg-pink-bg" points="0 53 0 0 50 0 50 63 25 53 0 63" />
              </svg>
              {image && <Image src={image.image_url.standard} alt={image.alt}/> }
            </Grid.Column>
            <Grid.Column width={9}>
              <Header as='h1' style={{fontSize: '34px', color: '#666666'}}>{page.title}</Header>
              <div style={{
                display: 'block',
                textAlign: 'right',
                fontSize: '16px',
                marginBottom: '15px'
              }}>
                {moment(page.published_at).format('YYYY.MM.DD')}
              </div>
              <div style={{
                border: '1px solid #e7e7e7',
                borderLeft: 'none',
                borderRight: 'none',
                textAlign: 'right',
                padding: '15px 0px',
                fontSize: '18px'
              }}>
                <span style={{fontWeight: 'bold', marginRight: '5px'}}>CATEGORY</span>
                <Link to={category.page.url}>{category.page.title}</Link>
              </div>
            </Grid.Column>
          </Responsive>

          <Responsive maxWidth={991} as={Grid} padded>
            <Grid.Row style={{display: 'flex'}}>
              <svg style={{flex: 10, minWidth: '50px'}} xmlns="http://www.w3.org/2000/svg" width="50" height="63" viewBox="0 0 50 63">
                <polygon className="svg-pink-bg" points="0 53 0 0 50 0 50 63 25 53 0 63" />
              </svg>
              <Header style={{flex: 90, marginLeft: '1rem', marginTop: 0}} as='h1'>{page.title}</Header>
            </Grid.Row>
            <Grid.Row style={{padding: 0, justifyContent: 'flex-end'}}>
              {moment(page.published_at).format('YYYY.MM.DD')}
            </Grid.Row>
            <Grid.Row>
              <Grid.Column width={16} style={{padding: 0}}>
                { image && <Image style={{display: 'block', width: '100%', maxWidth: '600px', margin: 'auto'}} src={image.image_url.standard} alt={image.alt}/> }
              </Grid.Column>
            </Grid.Row>
          </Responsive>
        </PageTitle>

        <Container style={{position: 'relative'}}>
          {popular_articles.length > 0 &&
            <Responsive as={Segment} minWidth={1100} basic style={{
                position: 'absolute',
                width: '200px',
                left: '-140px',
                padding: '10px'
              }}
              className='popularArticles'
              >
              <BoxTitle as='h2' title='Menu'/>
              <div className='popTitle'>
                人気記事
              </div>
              <div className='popContent'>
                {popular_articles.map((article, i) => {
                  const image = article.image
                  return (
                    <div key={article.id} className='popItem'>
                      <Link to={article.url} style={{display: 'flex'}}>
                        <div style={{flex: 40, display: 'flex', flexDirection: 'column', marginRight: '10px'}}>
                          <div className='numbered'>{i+1}</div>
                          <div>
                            {!!image ?
                              <Image src={image.image_url.thumb} alt={image.alt}/>
                              :
                              <div className='noImage' style={{
                                background: '#ececec',
                                display: 'block',
                                width: '100%',
                                height: '100%',}}></div>
                            }
                          </div>
                        </div>
                        <div style={{flex: 60, color: '#404040'}}>
                          <span>{article.title}</span>
                        </div>
                      </Link>
                      <div style={{display: 'block', textAlign: 'right', color: '#9d9d9d'}}>
                        {article.total_view_count} views
                      </div>
                    </div>
                  )
                })}
              </div>
            </Responsive>
          }
          <Segment className='contentWrap right' style={{minHeight: '785px'}}>
            <div className='carriee-editor' style={{padding: 0}} dangerouslySetInnerHTML={{ __html: article.content }} />
          </Segment>

          {cta &&
            <Segment className='contentWrap right'>
              <BoxTitle as='h2' title={cta.title} />
              <div className='carriee-editor' style={{padding: 0}} dangerouslySetInnerHTML={{ __html: cta.content }} />
            </Segment>
          }

          {search_key_articles.length > 0 &&
            <React.Fragment>
              <Responsive minWidth={992} as={Segment} basic className='categoryPage relatedArticle'>
                <BoxTitle as='h2' title='関連記事'/>
                <div className='inner'>
                  {search_key_articles.map(article => {
                    const image = article.image
                    return (
                      <div key={article.id} className='article'>
                        <Link to={article.url} className='eyeCatchImg'>
                          <div>{image && <Image src={image.image_url.standard} alt={image.alt} />}</div>
                        </Link>
                        <Link to={article.url}>{article.title}</Link>
                      </div>
                    )
                  })}
                </div>
              </Responsive>

              <Responsive maxWidth={991} as={Segment} basic className='categoryPage'>
                <BoxTitle as='h2' title='関連記事'/>
                {search_key_articles.map(article => {
                  const image = article.image
                  return (
                    <div key={article.id} className='article'>
                      <Link className='left' to={article.url}>
                        <div className='eyeCatchImg'>{image && <Image src={image.image_url.standard} alt={image.alt}/> }</div>
                      </Link>
                      <div className='right'>
                        <Link className='title' to={article.url}>{article.title}</Link>
                      </div>
                    </div>
                  )
                })}
              </Responsive>
            </React.Fragment>
          }
          {popular_articles.length > 0 &&
            <Responsive as={Segment} maxWidth={991} basic className='popularArticles'>
              <BoxTitle as='h2' title='人気記事'/>
              <div className='popContent'>
                {popular_articles.map((article, i) => {
                  const image = article.image
                  return (
                    <div key={article.id} className='popItem'>
                      <Link to={article.url} style={{display: 'flex'}}>
                        <div style={{flex: 40, display: 'flex', flexDirection: 'column', marginRight: '10px'}}>
                          <div className='numbered'>{i+1}</div>
                          <div>
                            {!!image ?
                              <Image src={image.image_url.thumb} alt={image.alt}/>
                              :
                              <div className='noImage' style={{
                                background: '#ececec',
                                display: 'block',
                                width: '100%',
                                height: '100%',}}></div>
                            }
                          </div>
                        </div>
                        <div style={{flex: 60}}>
                          <div style={{color: '#404040', fontSize: '16px', fontWeight: 'bold'}}>{article.title}</div>
                          <div style={{color: '#9d9d9d'}}>
                              {article.total_view_count} views
                          </div>
                        </div>
                      </Link>
                    </div>
                  )
                })}
              </div>
            </Responsive>
          }
          {(prevArticle || nextArticle) &&
            <div className='nextPrev'>
              {prevArticle &&
                <Link style={{width: '100%'}} to={prevArticle.url}>
                  <div className='prevArticle' style={!nextArticle ? {width: 'calc(100% - 503.5px - 20px)'} : {}}>
                    <Icon name='angle left' color='blue' />
                    <div className='title'>
                      <div style={{fontWeight: 'bold', fontSize: '20px'}}>前の記事</div>
                      <div>{prevArticle.title}</div>
                    </div>
                    <div className='image'>
                      {prevArticle.image && <Image src={prevArticle.image.image_url.thumb} alt={prevArticle.image.alt}/> }
                    </div>
                  </div>
                </Link>
              }
              {nextArticle &&
                <Link style={{width: '100%'}} to={nextArticle.url}>
                  <div className='nextArticle' style={!prevArticle ? {width: 'calc(100% - 503.5px - 20px)', float: 'right'} : {}}>
                    <div className='image'>
                      {nextArticle.image && <Image src={nextArticle.image.image_url.thumb} alt={nextArticle.image.alt}/> }
                    </div>
                    <div className='title'>
                      <div style={{fontWeight: 'bold', fontSize: '20px'}}>前の記事</div>
                      <div>{nextArticle.title}</div>
                    </div>
                    <Icon name='angle right' color='blue' />
                  </div>
                </Link>
              }
            </div>
          }
          
        </Container>
      </div>
    )
  }
}
