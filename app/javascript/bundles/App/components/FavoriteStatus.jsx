import React, { PureComponent } from 'react'
import PropType from 'prop-types'
import { savedJobIds, toggleSavedJob } from 'lib/function'
import { Icon, Label, Button } from 'semantic-ui-react'

export default class FavoriteStatus extends PureComponent {

  constructor(props) {
    super(props)
    const jobId = props.jobId
    this.state = {
      favorited: savedJobIds('favorite').indexOf(jobId) > -1
    }

    this.toggleFavorite = this.toggleFavorite.bind(this)
  }

  toggleFavorite = () => {
    const jobId = this.props.jobId
    toggleSavedJob('favorite', jobId)
    const jobIds = savedJobIds('favorite')
    const favorited = jobIds.indexOf(jobId) > -1
    this.setState({
      favorited: favorited
    })
    //Update job count in header
    document.getElementById('favoriteCount').innerHTML = savedJobIds('favorite').length

    let iconNode = document.getElementById(`favoriteIcon${jobId}`)
    let buttonNode = document.getElementById(`favoriteButton${jobId}`)
    if (favorited) {
      iconNode.style.color = '#f36581'
      if (buttonNode) {
        buttonNode.style.backgroundColor = '#ccc'
        buttonNode.textContent = '保存済み'
      }
    } else {
      iconNode.style.color = '#fff'
      if (buttonNode) {
        buttonNode.style.backgroundColor = '#f36581'
        buttonNode.textContent = '保存する'
      }
    }
  }

  render() {
    const { favorited } = this.state
    const { type, jobId, style } = this.props

    let mergedStyle
    let content
    if (type != 'button') {
      mergedStyle = {
        top: '0',
        left: '0'
      }
      mergedStyle = Object.assign(mergedStyle, style)

      content = (
        <Label as='a'
               corner='left'
               onClick={() => this.toggleFavorite()}
               style={mergedStyle}>
          <Icon
            name='heart'
            style={{
              fontSize: '1.1em',
              cursor: 'pointer',
              color: favorited ? '#f36581' : '#fff'
            }}
            id={`favoriteIcon${jobId}`}
          />
        </Label>
      )
    } else {
      mergedStyle = {
        marginTop: '0',
        marginBottom: '1.5em',
        backgroundColor: favorited ? '#ccc' : '#f36581',
        color: favorited ? '#777' : 'white'
      }
      mergedStyle = Object.assign(mergedStyle, style)

      content = (
        <a className='ui button'
          style={mergedStyle}
          id={`favoriteButton${jobId}`}
          onClick={() => this.toggleFavorite()}><Icon name='heart' inverted />{favorited ? '保存済み' :  '保存する'}</a>
      )
    }
    return content
  }
}
