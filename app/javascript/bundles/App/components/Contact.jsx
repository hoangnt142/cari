import React, { PureComponent } from 'react'
import PropType from 'prop-types'
import { Link } from 'react-router-dom'

import { Segment, Responsive, Container, Form, Button, Radio } from 'semantic-ui-react'

import PageTitle from './PageTitle'
import BoxTitle from './_BoxTitle'

export default class Contact extends React.Component {

  static propTypes = {
  }
  static defaultProps = {
  }

  static contextTypes = {
    router: PropType.object.isRequired,
  }

  constructor(props, context) {
    super(props)

    this.state = {
      contact: {need_reply: true},
      errors: {},
      contactTypes: [],
      fetching: false
    }
    this.onChange = this.onChange.bind(this)
  }

  componentDidMount() {
    const url = '/api/cache/contacts/new'
    const params = {
      method: 'GET',
      mode: 'same-origin',
      credentials: 'same-origin',
      headers: new Headers({
        "Content-Type": "application/json"
      })
    }
    fetch(url, params)
    .then(response => {
      response.json()
      .then(json => {
        const contact_types = json.contact_types
        this.setState({
          contactTypes: contact_types
        })
      })
    })
    .catch(error => {
      console.error(error)
    })
  }

  onChange = (name, value) => {
    let contact = this.state.contact
    eval(`contact.${name} = _value`)
    this.setState({
      contact: contact
    })
  }

  onSubmit = (e) => {
    this.toggleFetching()
    const url = '/api/cache/contacts'
    const params = {
      method: 'POST',
      mode: 'same-origin',
      credentials: 'same-origin',
      headers: new Headers({
        "Content-Type": "application/json"
      }),
      body: JSON.stringify({contact: this.state.contact})
    }
    fetch(url, params)
    .then(response => {
      response.json()
      .then(json => {
        const error = json.error
        if (error) {
          this.setState({
            errors: json.errors
          })
          let elmnt = document.getElementsByClassName("error")[0];
          elmnt.scrollIntoView();
        } else {
         this.context.router.history.push('/contact-sent')
        }
        this.toggleFetching()
      })
    })
    .catch(error => {
      console.error(error)
    })
  }

  toggleFetching() {
    this.setState({
      fetching: !this.state.fetching
    })
  }

  render() {
    const { api, page } = this.props
    const fetchingPage = api.fetchingURLs.indexOf('/api/cache/pages/1/sub') >= 0
    const { contact, contactTypes, errors, fetching } = this.state
    return (
      <div>
        <PageTitle
          title='お問合せ'
          page={page}
          breadCrumbs={[
            { title: '看護師求人サイトCarriee', url: '/' },
            { title: 'Contact', url: '/contact' },
          ]}
          showLastBreadCrumbs={false}
        >
          <div className='carriee-editor' style={{padding: 0, marginBottom: '1rem'}}>
            看護師の求人サイトCarriee（キャリー）のお問合せフォームにお越しいただきましてありがとうございます。以下お問合せフォームに必要事項をご入力の上お問合せください。
            <div className="p-content non-quote">
              <div className='ui text pink eighteenPx' style={{fontWeight: 'bold'}}>お問合せ内容に関して</div>
              <div>※お問合せいただいた内容は3営業日以内にご返信させていただきます。</div>
              <div>※お問合せ内容に関しては、ご返信できかねる場合がございます。あらかじめご了承ください。</div>
              {/*<div>※求人掲載等に関しては「<Link to=''>採用ご担当者の方へ</Link>」を確認してください。</div>*/}
            </div>
          </div>
        </PageTitle>

        <Container>
          <Segment className='contentWrap center'>
            <BoxTitle as='h2' title='お問合せフォーム' />
            <Form onSubmit={this.onSubmit}>

              <Form.Field style={style.formField}>
                <FormLabel title='お問合せの種類' required />
                <Form.Select
                  error={(errors.contact_type || []).length > 0 && !contact.contact_type}
                  name='contact_type'
                  options={contactTypes}
                  placeholder='選択してください'
                  onChange={(e, data) => this.onChange(data.name, data.value)}/>
              </Form.Field>

              <Form.Field style={style.formField}>
                <FormLabel title='会社名・病院名・施設名' subTitle='※個人の場合は「個人」と記載' required />
                <Form.Input
                  error={(errors.company || []).length > 0 && !contact.company}
                  name='company'
                  placeholder='例）キャリー病院'
                  style={{maxWidth: '500px'}}
                  onChange={(e) => this.onChange(e.target.name, e.target.value)}/>
              </Form.Field>

              <Form.Field style={style.formField}>
                <FormLabel title='お名前/ご担当者名' required />
                <Form.Input
                  error={(errors.name || []).length > 0 && !contact.name}
                  name='name'
                  placeholder='例）山田　太郎'
                  style={{maxWidth: '500px'}}
                  onChange={(e) => this.onChange(e.target.name, e.target.value)}/>
              </Form.Field>

              <Form.Field style={style.formField}>
                <FormLabel title='メールアドレス' required />
                <Form.Input
                  error={(errors.email || []).length > 0}
                  name='email'
                  style={{maxWidth: '500px'}}
                  onChange={(e) => this.onChange(e.target.name, e.target.value)}/>
              </Form.Field>

              <Form.Field style={style.formField}>
                <FormLabel title='ご返信の有無' />
                <Radio
                  style={{fontSize: '19px', fontWeight: 'bold', marginRight: '3rem'}}
                  className='carrieeCheckbox'
                  label='返信を希望しない'
                  checked={!contact.need_reply}
                  onChange={(e, data) => this.onChange('need_reply', false)}
                />
                <Radio
                  style={{fontSize: '19px', fontWeight: 'bold'}}
                  className='carrieeCheckbox'
                  label='返信を希望する'
                  checked={contact.need_reply}
                  onChange={(e, data) => this.onChange('need_reply', true)}
                />
              </Form.Field>

              <Form.Field style={style.formField}>
                <FormLabel title='お問合せ内容詳細' required />
                <Form.TextArea
                  error={(errors.content || []).length > 0 && !contact.content}
                  rows={10}
                  placeholder='お問合せの詳細内容を入れてください'
                  name='content'
                  style={{width: '100%'}}
                  onChange={(e) => this.onChange(e.target.name, e.target.value)}/>
              </Form.Field>

              <Button disabled={fetching} type='submit' color='green' size='massive'>送信内容を確認する</Button>
            </Form>
          </Segment>
        </Container>

      </div>
    )
  }
}

class FormLabel extends PureComponent {
  render() {
    const { title, required, subTitle } = this.props
    return (
      <label style={style.label}>
        {title}
        <span
          style={{
            backgroundColor: required ? '#f36580' : '#3eabde',
            fontSize: '16px',
            marginLeft: '10px',
            padding: '2px',
            color: '#fff'
          }}>
          {required ? '必須' : '任意'}
        </span>
        {subTitle &&
          <React.Fragment>
            <Responsive maxWidth={991} as='br' />
            <span style={{fontSize: '16px', fontWeight: 'normal', marginLeft: '5px'}}>
              {subTitle}
            </span>
          </React.Fragment>
        }
      </label>
    )
  }
}

const style = {
  label: {
    fontSize: '19px',
    marginBottom: '1rem'
  },
  formField: {
    marginBottom: '2.5rem'
  }
}
