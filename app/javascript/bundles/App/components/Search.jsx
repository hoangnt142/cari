import React, { PureComponent } from 'react'
import PropType from 'prop-types'
import { Link } from 'react-router-dom'
import Pagination from './SEOPagination'
import HospitalList from './_HospitalList'
import HospitalListMobile from './mobile/_HospitalListMobile'
import {
  Grid,
  List,
  Modal,
  Segment,
  Loader,
  Dimmer,
  Responsive,
  Container,
  Dropdown
} from 'semantic-ui-react'

import PageTitle from '../containers/PageTitleContainer'
import { ConditionMobile, ConditionPc } from '../containers/search/ConditionContainer'
import { numberOfConditions } from '../lib/search'


export default class Search extends PureComponent {

  constructor(props) {
    super(props)

    this.state = {
      resetAll: true,
      showTitle: window.innerWidth > 991
    }

    const {
      pageable,
      onChangeAllCondition,
    } = this.props

    /*
       if(!onlyPopup && pageable && props.hospitalTypes.length == 0 && props.match.path != '/search') {
     */
    if(pageable) {
      onChangeAllCondition({
        prefectureId: pageable.prefecture_id ? pageable.prefecture_id : null,
        areaIds: pageable.area_id ? [pageable.area_id] : [],
        hospitalTypeIds: pageable.hospital_type_id ? [pageable.hospital_type_id] : [],
        jobContractIds: pageable.job_contract_id ? [pageable.job_contract_id] : [],
        jobDetailIds: pageable.job_detail_id ? [pageable.job_detail_id] : [],
        jobTypeIds: pageable.job_type_id ? [pageable.job_type_id]: [],
        prefectureIds: pageable.prefecture_id ? [pageable.prefecture_id] : [],
        qualificationIds: pageable.qualification_id ? [pageable.qualification_id ] : [],
        page: this.props.match.params.page
      })
    }
  }

  handleShowTitle = () => {
    let windowWidth = window.innerWidth
    this.setState({
      showTitle: windowWidth > 991
    })
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    // Call SearchActions.onChangePage to change the page in the state.
    // It also triggers the search to update hospitals in its page.
    if(prevProps.match && (prevProps.match.params.page != this.props.match.params.page)) {
      console.log('page has been changed in Search.')
      this.props.onChangePage(this.props.match.params.page)
    }
  }

  componentWillUnmount() {
    const { reset } = this.props
    // Resets the state as initial condition even when coming back from the other page.
    const resetAll = this.state.resetAll // do not reset search condition while paging
    reset(resetAll)
  }


  render() {
    const {
      freeTextTitle,

      page,
      pageable,
      hospitals,
      condition,

      totalPages,
      currentPage,
      totalCount,
      paginationRange,

      fetching,
    } = this.props

    const canonicalUrl = page.url != '/search' ? page.url : null

    let hospitalCount
    if(page.url == '/search') {
      hospitalCount = paginationRange ? <div><span className='ui text pink' style={{fontSize: '2rem'}}>{totalCount}</span> 件中{paginationRange[0]}件～{paginationRange[1]}件表示</div> : null
    }
    else {
      hospitalCount = paginationRange ? <div>{page.title} <span className='ui text pink' style={{fontSize: '2rem'}}>{totalCount}</span> 件中{paginationRange[0]}件～{paginationRange[1]}件表示</div> : null
    }


    const pageTitle = (freeTextTitle &&
                       freeTextTitle.length > 0) ?
                      `「${freeTextTitle}」の求人` : page.title
    return (
      <div>
        <div>
          <PageTitle
            title={pageTitle}
            showTitle={this.state.showTitle}
            seoTitle={page ? page.seo_title : ''}
            seoDescription={page ? page.seo_description : ''}
            page={page}
            pageNumber={currentPage}
            breadCrumbs={[
              { title: '看護師求人サイトCarriee', url: '/' },
              { title: '看護師求人を検索', url: '/search' },
            ]}
            showLastBreadCrumbs={false}
            canonicalUrl={canonicalUrl}
          >

            <Responsive as={Grid} maxWidth={991} onUpdate={this.handleShowTitle}>

              <Grid.Row style={{
                fontSize: '1.7rem',
                lineHeight: '1.7rem',
                paddingLeft: '1rem',
                fontWeight: 'bold',
                alignItems: 'center',
                marginTop: '0',
                marginButtom: '0'
              }}>
                {pageTitle}
              </Grid.Row>

              <Grid.Row style={{
                padding: 0,
                marginTop: 0,
                marginBottom: '1rem'
              }}>
                {paginationRange &&
                 <Grid.Column width={16} textAlign='right'>
                   該当件数
                   <span className='ui text pink'
                         style={{fontSize: '1.5rem',
                                 marginLeft: '0.5em',
                                 marginRight: '0.5em',
                                 fontWeight: 'bold'
                         }}>
                     {totalCount}
                   </span>
                   件 ／
                   <span className='ui text pink'
                         style={{fontSize: '1.5rem',
                                 marginLeft: '0.5em',
                                 marginRight: '0.5em',
                                 fontWeight: 'bold'
                         }}>
                     {currentPage}
                   </span>
                   ページ目
                 </Grid.Column>
                }
              </Grid.Row>

              <Grid.Row style={{padding: 0,
                                justifyContent: 'center',
                                display: 'flex',
                                flex: 1,
                                paddingBottom: '1em',
                                marginButtom: '1em'
              }}>

                <ConditionMobile />

                {/*
                    <Grid.Column width={8} style={{paddingLeft: '0.5rem'}}>
                    <Dropdown placeholder='標準（並び替え）' style={{height: '40px'}} fluid selection options={[{key: 1, value: 'Desc', text: 'DESC'}, {key: 2, value: 'ASC', text: 'ASC'}]} />
                    </Grid.Column>
                  */}
              </Grid.Row>
            </Responsive>

            <Responsive minWidth={992}
                        as={ConditionPc}
            />
          </PageTitle>

          <Responsive minWidth={992}>
            <Paging
              showDetail={false}
              totalPages={totalPages}
              currentPage={currentPage}
              totalCount={totalCount}
              paginationRange={paginationRange}
              page={page}
              hospitalCount={hospitalCount}
            />
          </Responsive>

          <Dimmer.Dimmable as={'div'}
                           dimmed={fetching}
                           style={{
                             marginTop: '1rem',
                             marginBottom: '1rem',
                             marginLeft: '0.5rem',
                             marginRight: '0.5rem',
                             minHeight: '18rem'
                           }}>
            <Dimmer simple inverted active={fetching}><Loader /></Dimmer>

            {!fetching && (
               <div>
                 <Responsive as={HospitalListMobile} maxWidth={991} hospitals={hospitals} />

                 <Responsive minWidth={992} style={{
                   maxWidth: '1027px',
                   margin: 'auto'
                 }}>
                   <Segment className='totalCount' style={{paddingBottom: '1.5rem', fontSize: '1.4rem'}}>{hospitalCount}</Segment>
                   <HospitalList hospitals={hospitals} />
                 </Responsive>
               </div>
            )}
          </Dimmer.Dimmable>

          <Paging
            showDetail={true}
            totalPages={totalPages}
            currentPage={currentPage}
            totalCount={totalCount}
            paginationRange={paginationRange}
            page={page}
            hospitalCount={hospitalCount}
          />
        </div>
      </div>
    )
  }
}

const Paging = (props) => {
  const {
    totalPages,
    currentPage,
    page,
    totalCount,
    paginationRange,
    hospitalCount
  } = props
  if (totalPages && totalPages > 1 && parseInt(currentPage) <= totalPages) {
    return (
      <div className='carrieePagination'>
        <Pagination totalPages={totalPages}
                    currentPage={parseInt(currentPage)}
                    basePath={page.url}
                    totalCount={totalCount}
                    paginationRange={paginationRange}
        />
        {props.showDetail &&
         <Responsive minWidth={992} style={{fontWeight: 'bold', marginTop: '1rem'}}>
           {hospitalCount}
         </Responsive>
        }
      </div>
    )
  }
  return null
}
