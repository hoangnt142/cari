import React, { PureComponent } from 'react'
import PropType from 'prop-types'
import { Link } from 'react-router-dom'

import {
  Header, Icon, Grid, Responsive
} from 'semantic-ui-react'

export default class BoxTitle extends PureComponent {

  constructor(props) {
    super(props)
  }

  render() {
    const { as, icon, top, title, leftColor, rightNode, style, iconStyle} = this.props
    return (
      <Grid padded verticalAlign='middle' style={{width: '100%'}}>
        <Grid.Row style={{paddingBottom: 0}}>
          <Grid.Column width={rightNode ? 12 : 16} style={{paddingLeft: 0}}>
            <Header as={as || 'div'} style={Object.assign({display: 'flex'}, style || {} )}>
              {icon && icon != 'hospital' &&
                <Icon name={icon} size='small' />
              }
              {icon == 'hospital' &&
                <i className='hospitalIcon' style={iconStyle || {width: 40, minWidth: 40, height: 31}}/>
              }
              {title}
            </Header>
          </Grid.Column>
          <Responsive as={Grid.Column} minWidth={992} computer={4} textAlign='right' style={{paddingRight: 0}} >
            {rightNode}
          </Responsive>
        </Grid.Row>
        <Grid.Row>
          <div style={{display: 'flex', width: '100%', height: '2px', flexDirection: 'row', fontWeight: 'bold'}}>
            <div style={{flex: 25, backgroundColor: leftColor || 'rgb(243,101,129)'}}></div>
            <div style={{flex: 75, backgroundColor: '#dcddde'}}></div>
          </div>
        </Grid.Row>
      </Grid>
    )
  }
}
