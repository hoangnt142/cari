import React, { PureComponent } from 'react'
import PropType from 'prop-types'
import GroupTitle from './_GroupTitle'

import {
  Image, Icon,
  Segment,
  Header,
  Form, Message, Grid,
  Responsive,
  Dropdown
} from 'semantic-ui-react'
import { Button as OriginalButton } from 'semantic-ui-react'
import SignUpSlideStep from './SignUpSlideStep'

const Button = (props) => <OriginalButton {...Object.assign({ style: { height: '5em'}}, props)} />


export default class SignUpSlide4 extends PureComponent {

  next = () => {
    const {
      next,
      favoritePrefectureId1,
      favoriteCityId1,
      favoritePrefectureId2,
      favoriteCityId2
    } = this.props

    if(favoritePrefectureId1 == null ||
       (favoritePrefectureId1 != null && favoritePrefectureId1 > 0 && !favoriteCityId1)) {
      alert('１次希望を選択してください。')
    }
    else if(favoritePrefectureId2 == null ||
            (favoritePrefectureId2 != null && favoritePrefectureId2 > 0 && !favoriteCityId2)) {
      alert('２次希望を選択してください。')
    }
    else {
      next()
    }
  }

  render() {

    const {
      prefectures,
      city1,
      city2,
      city1IsLoading,
      city2IsLoading,

      onChangeFavoritePrefecture,
      onChangeCity,

      previous,

      favoritePrefectureId1,
      favoriteCityId1,
      favoritePrefectureId2,
      favoriteCityId2
    } = this.props

    // Gotten asynchrnously
    const prefectureOptions = prefectures ? prefectures.map(p => ({ text: p.name, value: p.id })) : null
    if(prefectureOptions) {
      prefectureOptions.splice(0, 0, { text: '決まっていない', value: 0 })
    }
    const city1Options = city1 ? city1.cities.map(p => ({ text: p.name, value: p.id })) : null
    const city2Options = city2 ? city2.cities.map(p => ({ text: p.name, value: p.id })) : null

    console.log('rendering')
    return (
      <div>
        <Segment style={{ paddingTop: '50px', paddingBottom: '50px' }}>

          <SignUpSlideStep step='3' />

          <Form>
            <GroupTitle as='h3' title='希望の勤務地を教えてください' optional />

            <Grid>
              <Grid.Row>
                <Grid.Column width={10}>
                  <div style={{ fontWeight: 'bold' }} className='ui text pink large'>１次希望（任意）</div>
                </Grid.Column>
              </Grid.Row>

              <Grid.Row>
                <Grid.Column width={5}>
                  <Dropdown fluid
                            selection
                            loading={prefectureOptions == null}
                            options={prefectureOptions}
                            value={favoritePrefectureId1}
                            onChange={(e, data) => {
                                const option = data.options.find(o => o.value == data.value)
                                onChangeFavoritePrefecture(1, data.value, option.text)
                            }}
                            placeholder='都道府県を選択' />
                </Grid.Column>
                <Grid.Column width={5}>
                  <Dropdown fluid
                            selection
                            disabled={!favoritePrefectureId1 || favoritePrefectureId1 == 0}
                            loading={city1IsLoading}
                            options={city1Options}
                            value={favoriteCityId1}
                            onChange={(e, data) => {
                                const option = data.options.find(o => o.value == data.value)
                                onChangeCity({ favoriteCityId1: data.value, favoriteCityName1: option.text })

                            }}
                            placeholder='市区町村を選択' />
                </Grid.Column>
              </Grid.Row>

              <Grid.Row>
                <Grid.Column width={10}>
                  <div style={{ fontWeight: 'bold' }} className='ui text pink large'>２次希望（任意）</div>
                </Grid.Column>
              </Grid.Row>

              <Grid.Row>
                <Grid.Column width={5}>
                  <Dropdown fluid
                            selection
                            loading={prefectureOptions == null}
                            options={prefectureOptions}
                            value={favoritePrefectureId2}
                            onChange={(e, data) => {
                                const option = data.options.find(o => o.value == data.value)
                                onChangeFavoritePrefecture(2, data.value, option.text)
                            }}
                            placeholder='都道府県を選択' />
                </Grid.Column>
                <Grid.Column width={5}>
                  <Dropdown fluid
                            selection
                            disabled={!favoritePrefectureId2 || favoritePrefectureId2 == 0}
                            loading={city2IsLoading}
                            options={city2Options}
                            value={favoriteCityId2}
                            onChange={(e, data) => {
                                const option = data.options.find(o => o.value == data.value)
                                onChangeCity({ favoriteCityId2: data.value, favoriteCityName2: option.text })

                            }}
                            placeholder='市区町村を選択' />
                </Grid.Column>
              </Grid.Row>
            </Grid>

            <p>※ 決まっていない場合は「決まっていない」を選択</p>



            <Grid columns={2} style={{ marginTop: '2rem' }}>
              <Grid.Row>
                <Grid.Column textAlign='right'>
                  <Button
                    color='light-grey'
                    size='large'
                    style={{ height: '4em', width: '70%' }}
                    onClick={previous}><Icon name='chevron left' color='pink' />前に戻る</Button>
                </Grid.Column>
                <Grid.Column textAlign='left'>
                  <Button
                    color='green'
                    size='large'
                    style={{ height: '4em', width: '70%' }}
                    onClick={this.next}>次に進む<Icon name='chevron right' color='white' /></Button>
                </Grid.Column>
              </Grid.Row>
            </Grid>

          </Form>
        </Segment>
      </div>
    )
  }
}
