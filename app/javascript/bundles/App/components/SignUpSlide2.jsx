import React, { PureComponent } from 'react'
import PropType from 'prop-types'
import {
  Image, Icon,
  Segment,
  Header,
  Form, Message, Grid,
  Responsive
} from 'semantic-ui-react'
import { Button as OriginalButton } from 'semantic-ui-react'
import SignUpSlideStep from './SignUpSlideStep'
import GroupTitle from './_GroupTitle'

const Button = (props) => <OriginalButton {...Object.assign({ style: { height: '5em'}}, props)} />


export default class SignUpSlide2 extends PureComponent {

  next = () => {
    const {
      next,
      gender,
    } = this.props

    if(!gender) {
      alert('性別を選択してください。')
    }
    else {
      next()
    }
  }

  render() {

    const {
      onChange,
      previous,
      gender,
      qualification1,
      qualification2,
      qualification3,
      qualification4,
      qualification5,
    } = this.props

    console.log('rendering')
    return (
      <div>
        <Segment style={{ paddingTop: '50px', paddingBottom: '50px' }}>

          <SignUpSlideStep step='2' />

          <Form>
            <GroupTitle  as='h3' title='あなたの性別を教えてください' required />

            <Grid columns={2} style={{ marginBottom: '4rem' }}>
              <Grid.Row>
                <Grid.Column>
                  <Button color='light-grey'
                          fluid
                          onClick={() => onChange('gender', 2)}
                          active={gender == 2}>
                    <Icon name='female' color={gender == 2 ? '' : 'pink'} size='big'/>
                    女性
                  </Button>
                </Grid.Column>
                <Grid.Column>
                  <Button color='light-grey'
                          fluid
                          onClick={() => onChange('gender', 1)}
                          active={gender == 1}>
                    <Icon name='male' color={gender == 1 ? '' : 'blue'} size='big'/>
                    男性
                  </Button>
                </Grid.Column>
              </Grid.Row>
            </Grid>

            <GroupTitle as='h3' title='保有資格を教えてください' required />

            <p className='ui text pink'>※ 複数回答可能です</p>

            <Responsive maxWidth={767} style={{ marginBottom: '4rem' }}>
              <Grid columns={2}>
                <Grid.Row>
                  <Grid.Column>
                    <Button color='light-grey'
                            fluid
                            onClick={() => onChange('qualification1', !qualification1)}
                            active={qualification1 == true}>正看護師</Button>
                  </Grid.Column>
                  <Grid.Column>
                    <Button color='light-grey'
                            fluid
                            onClick={() => onChange('qualification2', !qualification2)}
                            active={qualification2 == true}>准看護師</Button>
                  </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column>
                    <Button color='light-grey'
                            fluid
                            onClick={() => onChange('qualification3', !qualification3)}
                            active={qualification3 == true}>助産師</Button>
                  </Grid.Column>
                  <Grid.Column>
                    <Button color='light-grey'
                            fluid
                            onClick={() => onChange('qualification4', !qualification4)}
                            active={qualification4 == true}>保健師</Button>
                  </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column>
                    <Button color='light-grey'
                            fluid
                            onClick={() => onChange('qualification5', !qualification5)}
                            active={qualification5 == true}>看護学生</Button>
                  </Grid.Column>
                  <Grid.Column>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </Responsive>

            <Responsive minWidth={768} style={{ marginBottom: '4rem' }}>
              <Grid columns={3}>
                <Grid.Row>
                  <Grid.Column>
                    <Button fluid
                            onClick={() => onChange('qualification1', !qualification1)}
                            active={qualification1 == true}>正看護師</Button>
                  </Grid.Column>
                  <Grid.Column>
                    <Button color='light-grey' fluid
                            onClick={() => onChange('qualification2', !qualification2)}
                            active={qualification2 == true}>准看護師</Button>
                  </Grid.Column>
                  <Grid.Column>
                    <Button color='light-grey' fluid
                            onClick={() => onChange('qualification3', !qualification3)}
                            active={qualification3 == true}>助産師</Button>
                  </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column>
                    <Button color='light-grey' fluid
                            onClick={() => onChange('qualification4', !qualification4)}
                            active={qualification4 == true}>保健師</Button>
                  </Grid.Column>
                  <Grid.Column>
                    <Button color='light-grey' fluid
                            onClick={() => onChange('qualification5', !qualification5)}
                            active={qualification5 == true}>看護学生</Button>
                  </Grid.Column>
                  <Grid.Column>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </Responsive>


            <Grid columns={2} style={{ marginTop: '2rem' }}>
              <Grid.Row>
                <Grid.Column textAlign='right'>
                  <Button
                    color='light-grey'
                    size='large'
                    style={{ height: '4em', width: '70%' }}
                    onClick={previous}><Icon name='chevron left' color='pink' />前に戻る</Button>
                </Grid.Column>
                <Grid.Column textAlign='left'>
                  <Button
                    color='green'
                    size='large'
                    style={{ height: '4em', width: '70%' }}
                    onClick={this.next}>次に進む<Icon name='chevron right' color='white' /></Button>
                </Grid.Column>
              </Grid.Row>
            </Grid>

          </Form>
        </Segment>
      </div>
    )
  }
}
