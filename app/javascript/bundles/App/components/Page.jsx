import React, { PureComponent } from 'react'
import PropType from 'prop-types'
import { Link } from 'react-router-dom'
import BoxTitle from './_BoxTitle'

import {
  Button, Image, Icon,
  Search, Modal, Segment, Sticky,
  Header,
  Item, Loader, Dimmer,
  Responsive,
  Container
} from 'semantic-ui-react'

import PageTitle from './PageTitle'


export default class Page extends PureComponent {

  static propTypes = {
  }
  static defaultProps = {
  }

  constructor(props) {
    super(props)
  }

  componentDidMount() {
    const { fetch } = this.props
    // Sub data should be gotten after being mounted.
    //fetch('/api/cache/pages/1/sub')
  }

  componentWillUnmount() {
    const { reset } = this.props
    // Resets the state as initial condition even when coming back from the other page.
    reset()
  }

  render() {
    console.log('Page is rendering')
    const { api, page } = this.props
    const cta = page.cta
    return (
      <div>
        <PageTitle
          title={page.title}
          page={page}
          seoTitle={page.seo_title}
          breadCrumbs={[
            { title: '看護師求人サイトCarriee', url: '/' },
            { title: page.title, url: page.url }
          ]}
          showLastBreadCrumbs={false}
        >
          <div className='carriee-editor' style={{padding: 0, marginBottom: '1rem'}} dangerouslySetInnerHTML={{ __html: page.content_top }} />
        </PageTitle>
        <Container>

          <Segment className='contentWrap center'>
            <div className='carriee-editor' style={{padding: 0}} dangerouslySetInnerHTML={{ __html: page.content }} />
          </Segment>

          {cta &&
            <Segment className='contentWrap center'>
              <BoxTitle as='h2' title={cta.title} />
              <div className='carriee-editor' style={{padding: 0}} dangerouslySetInnerHTML={{ __html: cta.content }} />
            </Segment>
          }
        </Container>

      </div>
    )
  }
}
