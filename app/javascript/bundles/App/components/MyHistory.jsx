import React, { PureComponent } from 'react'
import PropType from 'prop-types'
import { Link } from 'react-router-dom'
import HospitalList from './_HospitalList'
import HospitalListMobile from './mobile/_HospitalListMobile'

import {
  Button, Image, Icon,
  Search, Modal, Segment, Sticky,
  Header,
  Item, Loader, Dimmer,
  Responsive,
  Container
} from 'semantic-ui-react'

import PageTitle from './PageTitle'


export default class MyHistory extends PureComponent {

  static propTypes = {
  }
  static defaultProps = {
  }

  constructor(props) {
    super(props)

    const { fetchPage, getHospitals } = this.props
    fetchPage()
    getHospitals()
  }

  componentDidMount() {
    const { fetch } = this.props
    // Sub data should be gotten after being mounted.
    //fetch('/api/cache/pages/1/sub')
  }

  componentWillUnmount() {
    const { reset } = this.props
    // Resets the state as initial condition even when coming back from the other page.
    reset()
  }

  render() {
    const { api, page, hospitals } = this.props
    const fetchingPage = api.fetchingURLs.indexOf('/api/cache/pages/1/sub') >= 0

    return (
      <div>
        <PageTitle
          title='最近見た求人'
          page={page}
          breadCrumbs={[
            { title: '看護師求人サイトCarriee', url: '/' },
            { title: 'MyHistory', url: '/my-history' },
          ]}
          showLastBreadCrumbs={false}
        />

        <div style={{ margin: '1rem 0.5rem', maxWidth: '1024px', margin: 'auto' }}>
          <Responsive as={HospitalListMobile} maxWidth={991} hospitals={hospitals} history={history} />
          <Responsive as={HospitalList} minWidth={992} hospitals={hospitals} history={history} />
        </div>
      </div>
    )
  }
}
