import React, { PureComponent } from 'react'
import PropType from 'prop-types'
import { Link } from 'react-router-dom'
import moment from 'moment'
import {
  Segment,
  Image
} from 'semantic-ui-react'

export default class ArticleList extends PureComponent {

  static propTypes = {
    articles: PropTypes.array.isRequired
  }

  static defaultProps = {
  }

  render() {
    const { articles} = this.props
    return (
      <Segment basic className='categoryPage'>
        <div className='inner'>
          <React.Fragment>
            {articles.map(article => {
              const image = article.image
              const category = article.category
              return (
                <div key={article.id} className='article'>
                  <Link to={article.url}>
                    <div className='eyeCatchImg'>{image && <Image src={image.image_url.standard} alt={image.alt}/> }</div>
                  </Link>
                  <div style={{margin: '5px 0 0 0'}}>
                    {moment(article.publish_at).format('YYYY.MM.DD')}
                    <Link className='ui text blue' to={category.url} style={{marginLeft: '10px', fontWeight: 'normal'}}>{category.title}</Link>
                  </div>
                  <Link to={article.url} className='title'>{article.title}</Link>
                </div>
              )
            })}
            {(articles.length % 3 != 0) && [...Array(3 - articles.length % 3)].map((x, i) =>
              <div key={i} className='article'></div>
            )}
          </React.Fragment>
        </div>
      </Segment>
    )
  }
}
