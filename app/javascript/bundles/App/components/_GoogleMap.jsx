import React, { PureComponent } from 'react'
import PropType from 'prop-types'

export default class GoogleMap extends PureComponent {

  constructor(props) {
    super(props)
    let userAgent = navigator.userAgent || navigator.vendor || window.opera
    let platform
    if (/android/i.test(userAgent)) {
      platform = "Android"
    }

    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
      platform = "iOS"
    }
    this.state = {
      platform: platform
    }
  }

  static defaultProps = {
    lat: 35.6894875,
    long: 139.6917064,
  }

  componentWillMount() {
    this.initializeMapSDK()
  }

  initializeMapSDK() {
    const googleApiKey = process.env.GOOGLE_API

    let scripts = document.getElementsByTagName("script")

    scripts = Array.from(scripts).filter(script => script.src.indexOf(googleApiKey) > -1)

    if (scripts.length == 0) {
      let s = document.createElement('script')
      s.type = 'text/javascript'
      s.id = 'googleMapAPI'
      s.src = `https://maps.googleapis.com/maps/api/js?key=${googleApiKey}`
      if (document.body.appendChild(s) != null) {
        setTimeout(() => {this.renderMap()}, 500);
      }
    } else {
      setTimeout(() => {this.renderMap()}, 0);
    }
  }

  openInApp = () => {
    const { lat, long } = this.props
    const platform = this.state.platform
    if (platform == 'Android') {
      window.open(`geo:${lat},${long}`)
    } else if (platform == 'iOS') {
      window.open(`maps://maps.google.com/maps?daddr=${lat},${long}&amp;ll=`);
    }
  }

  renderMap() {
    const { hospitalId, lat, long } = this.props

    let map = new google.maps.Map(document.getElementById('map'+hospitalId), {
      zoom: 18,
      center: {
        lat: lat,
        lng: long
      }
    });
    let markers = [];
    return markers[0] = new google.maps.Marker({
      map: map,
      position: new google.maps.LatLng(lat, long),
      draggable: true,
      animation: google.maps.Animation.DROP
    });
  }

  render() {
    const { hospitalId } = this.props
    const platform = this.state.platform
    return (
      <div style={{
        position: 'relative'
      }}>
        <div id={`map${hospitalId}`} style={{display: 'block', width: '100%', height: 200}}></div>
        {platform && <div
          onClick={() => this.openInApp()}
          style={{
            position: 'absolute',
            left: 0,
            bottom: 0,
            background: '#40abe1',
            padding: '5px',
            color: '#fff',
            fontWeight: 'bold',
          }}>
          アプリで開く
        </div>}
      </div>
    )
  }
}
