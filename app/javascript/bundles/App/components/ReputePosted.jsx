import React, { PureComponent } from 'react'
import PropType from 'prop-types'
import { Link } from 'react-router-dom'

import {
  Button, Image, Icon,
  Search, Modal, Segment, Sticky,
  Header,
  Item, Loader, Dimmer,
  Responsive,
  Container
} from 'semantic-ui-react'

import PageTitle from './PageTitle'


export default class ReputePosted extends PureComponent {

  static propTypes = {
  }
  static defaultProps = {
  }

  constructor(props) {
    super(props)

    const { fetchPage } = this.props
    fetchPage()
  }

  componentDidMount() {
    const { fetch } = this.props
    // Sub data should be gotten after being mounted.
    //fetch('/api/cache/pages/1/sub')
  }

  componentWillUnmount() {
    const { reset } = this.props
    // Resets the state as initial condition even when coming back from the other page.
    reset()
  }

  render() {
    const { api, page } = this.props
    const fetchingPage = api.fetchingURLs.indexOf('/api/cache/pages/1/sub') >= 0

    return (
      <div>
        <PageTitle
          title='ReputePosted'
          page={page}
          breadCrumbs={[
            { title: '看護師求人サイトCarriee', url: '/' },
            { title: 'ReputePosted', url: '/repute_posted' },
          ]}
          showLastBreadCrumbs={false}
        />

        <Container>
          <div>
            <Segment style={{ paddingTop: '50px', paddingBottom: '50px' }}>
              ReputePosted
            </Segment>
          </div>
        </Container>

      </div>
    )
  }
}
