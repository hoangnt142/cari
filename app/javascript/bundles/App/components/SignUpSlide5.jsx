import React, { PureComponent } from 'react'
import PropType from 'prop-types'
import { Link } from 'react-router-dom'

import {
  Image, Icon,
  Segment,
  Header,
  Form, Message, Grid,
  Responsive,
  Table
} from 'semantic-ui-react'
import { Button as OriginalButton } from 'semantic-ui-react'
import SignUpSlideStep from './SignUpSlideStep'

const Button = (props) => <OriginalButton {...Object.assign({ style: { height: '5em'}}, props)} />


export default class SignUpSlide5 extends PureComponent {

  render() {
    const {
      previous,
      signUp,

      signing,
      signedUp,

      nickname,
      email,
      gender,
      prefectureName,
      cityName,
      favoritePrefectureName1,
      favoriteCityName1,
      favoritePrefectureName2,
      favoriteCityName2,
      qualification1,
      qualification2,
      qualification3,
      qualification4,
      qualification5,
      birth,
    } = this.props

    console.log('props', this.props)
    let genderName = ''
    if(gender == 1) {
      genderName = '男性'
    }
    else if(gender == 2) {
      genderName = '女性'
    }

    let address = ''
    if(prefectureName) {
      address = prefectureName

      if(cityName) {
        address += cityName
      }
    }

    let favoriteAddress1 = ''
    if(favoritePrefectureName1) {
      favoriteAddress1 = favoritePrefectureName1

      if(favoriteCityName1) {
        favoriteAddress1 += favoriteCityName1
      }
    }

    let favoriteAddress2 = ''
    if(favoritePrefectureName2) {
      favoriteAddress2 = favoritePrefectureName2

      if(favoriteCityName2) {
        favoriteAddress2 += favoriteCityName2
      }
    }


    const birthText = birth ? `${birth.getFullYear()}年${birth.getMonth() + 1}月${birth.getDate()}日` : ''

    const qualificationDefinition = [
      '正看護師',
      '准看護師',
      '助産師',
      '保健師',
      '看護学生'
    ]

    let qualifications = ''
    for(let i = 0; i < qualificationDefinition.length; i++) {
      if(eval(`qualification${i+1}`)) {
        if(qualifications != '') {
          qualifications += ', '
        }
        qualifications += qualificationDefinition[i]
      }
    }



    const tableData = [
      { label: 'メールアドレス', value: email },
      { label: '性別', value: genderName },
      { label: '保有資格', value: qualifications },
      { label: 'ニックネーム', value: nickname },
      { label: '生年月日', value: birthText },
      { label: '住所', value: address },
      { label: '希望の勤務地１次希望', value: favoriteAddress1 },
      { label: '希望の勤務地２次希望', value: favoriteAddress2 },
    ]


    const tableRows = []
    tableData.forEach((h, i) => {
      tableRows.push(
        <Table.Row key={i}>
          <Table.Cell width={5}>{h.label}</Table.Cell>
          <Table.Cell width={10}>{h.value}</Table.Cell>
        </Table.Row>
      )})


    console.log('rendering')
    if(signedUp === null) {
      return (
        <div>
          <Segment style={{ paddingTop: '50px', paddingBottom: '50px' }}>

            <SignUpSlideStep step='4' />

            <Header as='h3'>ご登録はまだ完了していません。</Header>

            <p>以下、選択内容を確認の上、「登録する」ボタンを押してください。</p>

            {signedUp === false && <Message>登録に失敗しました。</Message>}

            <Table>
              <Table.Body>
                {tableRows.map(r => r)}
              </Table.Body>
            </Table>

            <Grid columns={2} style={{ marginTop: '2rem' }}>
              <Grid.Row>
                <Grid.Column textAlign='right'>
                  <Button
                    color='light-grey'
                    size='large'
                    style={{ height: '4em', width: '70%' }}
                    disabled={signing}
                    onClick={previous}><Icon name='chevron left' color='pink' />前に戻る</Button>
                </Grid.Column>
                <Grid.Column textAlign='left'>
                  <Button
                    color='green'
                    size='large'
                    style={{ height: '4em', width: '70%' }}
                    disabled={signing}
                    loading={signing}
                    onClick={() => signUp(this.props)}>この内容で登録する</Button>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Segment>
        </div>
      )
    }
    else {
      return (
        <div>
          <Segment style={{ paddingTop: '50px', paddingBottom: '50px' }}>

            <SignUpSlideStep step='4' />

            <Header as='h3'>ご登録完了</Header>

            <Message>以下の内容で、会員登録登録が完了しました。</Message>

            <Table>
              <Table.Body>
                {tableRows.map(r => r)}
              </Table.Body>
            </Table>

            <Grid style={{ marginTop: '2rem' }}>
              <Grid.Row>
                <Grid.Column textAlign='center'>
                  <Link to='sign-in'>
                    <Button
                      color='blue'
                      size='large'
                      style={{ height: '4em' }}>ログイン</Button>
                  </Link>
                </Grid.Column>
              </Grid.Row>
            </Grid>

          </Segment>
        </div>
      )
    }
  }
}
