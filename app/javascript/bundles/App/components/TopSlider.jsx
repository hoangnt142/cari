import React, { PureComponent } from 'react'
import Slider from "react-slick"

function NextArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{ ...style, display: "block", right: 15 }}
      onClick={onClick}
    />
  );
}

function PrevArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{ ...style, display: "block", left: 5, zIndex: 1 }}
      onClick={onClick}
    />
  );
}

export default class TopSlider extends PureComponent {

  render() {

    const settings = {
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      nextArrow: <NextArrow />,
      prevArrow: <PrevArrow />,
      dots: true,
      dotsClass: "slider paging",
      customPaging: function(i) {
        return (
          <div style={{width: '100%', height: '1px', display: 'block'}}></div>
        );
      },
    }
    const images = [
      'https://www.ionutrition.com/wp-content/uploads/2016/02/nurse-meal-delivery.jpg',
      'https://t3.ftcdn.net/jpg/01/19/61/52/240_F_119615202_dCkpG9RjlXE0PKh9wioJT8XKGoDWPBPs.jpg',
      'https://t4.ftcdn.net/jpg/01/56/05/41/240_F_156054154_fpDQ6gmpHigk7A8jL3bF3Yn8F2HmmYTH.jpg',
      'https://t4.ftcdn.net/jpg/01/58/55/01/240_F_158550109_PINiEhjKfPePW9kvBAf1zl0Sw65Amx11.jpg',
      'https://t3.ftcdn.net/jpg/01/19/61/52/240_F_119615202_dCkpG9RjlXE0PKh9wioJT8XKGoDWPBPs.jpg'
    ]
    return (
      <Slider {...settings}>
        {images.map((image, i) =>
          <div key={i}>
            <img src={image} style={{width: '100%'}} />
          </div>
        )}
      </Slider>
    )
  }
}