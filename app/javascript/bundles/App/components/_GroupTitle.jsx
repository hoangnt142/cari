import React, { PureComponent } from 'react'
import PropType from 'prop-types'
import { Link } from 'react-router-dom'

import {
  Header, Label
} from 'semantic-ui-react'

export default class GroupTitle extends PureComponent {

  constructor(props) {
    super(props)
  }

  render() {
    return (
      <div>
        <Header as={this.props.as || 'h2'}>
          {this.props.title}
          {this.props.required &&
            <Label color='pink'>必須</Label>
          }
          {this.props.optional &&
            <Label color='blue'>任意</Label>
          }
        </Header>
        <div style={{display: 'flex', height: '2px', flexDirection: 'row', marginBottom: '1rem'}}>
          <div style={{flex: 20, backgroundColor: 'rgb(243,101,129)'}}></div>
          <div style={{flex: 80, backgroundColor: '#dcddde'}}></div>
        </div>
      </div>
    )
  }
}
