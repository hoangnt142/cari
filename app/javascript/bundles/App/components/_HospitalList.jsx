import React, { PureComponent } from 'react'
import PropType from 'prop-types'
import { Link } from 'react-router-dom'
import BoxTitle from './_BoxTitle'
import SearchKey from './_SearchKey'
import FavoriteStatus from './FavoriteStatus'
import JobInfo from './mobile/_JobInfo'

import {
  Button, Image, Icon,
  Checkbox,
  Grid,
  Input,
  List,
  Modal, Segment, Loader, Dimmer,
  Container, Rating, Label, Divider, Dropdown
} from 'semantic-ui-react'

class Hospital extends PureComponent {
  render() {
    const { hospital } = this.props
    return (
      <Segment basic style={{ backgroundColor: '#fff', borderRadius: '4px', paddingBottom: '3rem', marginBottom: '2rem'}} key={hospital.id}>
        <BoxTitle
          as='div'
          icon='hospital'
          iconStyle={{backgroundSize: '28px 23px', minWidth: '28px'}}
          title={hospital.name}
          top='0px'
          rightNode={
            <div>
              <div className='ui text pink' style={{fontWeight: 'bold'}}>NEW</div>
              <div className='ui text grey'>{hospital.max_job_created}</div>
            </div>
          }
        />
        <Grid divided='vertically' style={{marginTop: 0}} verticalAlign='middle'>
          <Grid.Row style={{padding: 0}}>
            {/*<Grid.Column width={16} textAlign='right'>
                <FavoriteStatus jobId={hospital.id} />
                </Grid.Column>*/}
          </Grid.Row>
          {hospital.jobs_details.length > 0 &&
           <Grid.Column width={16} style={{marginTop: 0}}>
             <SearchKey keys={hospital.jobs_details} />
           </Grid.Column>
          }

          <Grid.Row style={{padding: 0}}>
            <Grid.Column computer={2} mobile={6} style={{fontWeight: 'bold'}}>
              <Icon name='marker' color='grey' size='large' />
              勤務地
            </Grid.Column>

            <Grid.Column computer={6} mobile={10} style={{borderLeft: '1px solid #ccc'}}>
              {hospital.work_location}
            </Grid.Column>

            <Grid.Column computer={2} mobile={6} style={{fontWeight: 'bold'}}>
              <Icon name='subway' color='grey' size='large' />
              アクセス
            </Grid.Column>

            <Grid.Column computer={6} mobile={10} style={{borderLeft: '1px solid #ccc'}}>
              {hospital.access || '-'}
            </Grid.Column>

          </Grid.Row>
          <Grid.Row style={{padding: 0}}>
            <Grid.Column computer={2} mobile={6} style={{fontWeight: 'bold'}}>
              <Icon name='hospital' color='grey' size='large' />
              施設形態
            </Grid.Column>

            <Grid.Column computer={6} mobile={10} style={{borderLeft: '1px solid #ccc'}}>
              {hospital.hospital_types.map(type => type.name).join(' | ') || '-'}
            </Grid.Column>

            <Grid.Column computer={2} mobile={6} style={{fontWeight: 'bold'}}>
              <Icon name='bed' color='grey' size='large' />
              病床数
            </Grid.Column>

            <Grid.Column computer={6} mobile={10} style={{borderLeft: '1px solid #ccc'}}>
              {hospital.number_of_beds_text || '-'}
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            {hospital.jobs.length == 0 &&
             <Grid.Column width={16}>
               <Segment basic style={{backgroundColor: '#f6f3f3', display: 'flex', borderRadius: '6px', padding: 0}}>
                 <Label color='blue' style={{
                   flex: 10,
                   borderTopRightRadius: 0,
                   borderBottomRightRadius: 0,
                   display: 'flex',
                   flexDirection: 'column',
                   alignItems: 'center',
                   justifyContent: 'center',
                   fontSize: '1.1rem',
                   padding: 0,
                   maxWidth: '45px'
                 }}>
                   <div style={{fontSize: '25px'}}>!</div>
                 </Label>
                 <div style={{flex: 90, padding: '1rem 0.5rem', fontWeight: 'bold'}}>
                   <div>現在募集している求人はありません。</div>
                   <div>転職エージェントに依頼することで、逆指名での転職活動は可能です。</div>
                 </div>
               </Segment>
             </Grid.Column>
            }
            {hospital.jobs.map((job, i) =>
              <JobInfo key={i}
                       i={i}
                       hospital={hospital}
                       job={job} />

            )}
          </Grid.Row>
          <Grid.Row textAlign='center' className='rowDividerless'>
            <Grid.Column computer={8} mobile={12} style={{margin: '1rem auto'}}>
              <Link to={hospital.url}>
                <Button color='green' size='huge' style={{width: '100%', height: '80px'}}>
                  詳細を見る
                </Button>
              </Link>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Segment>
    )
  }
}

export default class HospitalList extends PureComponent {

  render() {
    const {
      hospitals
    } = this.props

    if(hospitals.length == 0) {
      return (
        <Segment basic style={{ backgroundColor: '#fff', borderRadius: '4px'}}>検索結果はありません。</Segment>
      )
    }
    else {
      let content = hospitals.map(hospital =>
        <Hospital key={hospital.id} hospital={hospital} />
      )
      return content
    }
  }
}
