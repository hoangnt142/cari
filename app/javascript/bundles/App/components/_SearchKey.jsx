import React, { PureComponent } from 'react'
import { Label } from 'semantic-ui-react'

export default class SearchKey extends PureComponent {
  render() {
    const { keys } = this.props
    return (
      <div style={{display: 'inline-block'}}>
        {keys.map((key, i) =>
          <Label basic color='pink' style={{margin: '0.25rem 0.5rem 0.25rem 0'}} key={i}>{key.name}</Label>
        )}
      </div>
    )
  }
}