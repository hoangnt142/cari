import React, { PureComponent } from 'react'
import PropType from 'prop-types'
import { Link } from 'react-router-dom'

import {
  Button, Image, Icon,
  Search, Modal, Segment, Sticky,
  Header,
  Item, Loader, Dimmer,
  Responsive,
  Container
} from 'semantic-ui-react'

import PageTitle from './PageTitle'


export default class ContactSent extends PureComponent {

  static propTypes = {
  }
  static defaultProps = {
  }

  constructor(props) {
    super(props)

    const { fetchPage } = this.props
    fetchPage()
  }

  componentDidMount() {
    const { fetch } = this.props
    // Sub data should be gotten after being mounted.
    //fetch('/api/cache/pages/1/sub')
  }

  componentWillUnmount() {
    const { reset } = this.props
    // Resets the state as initial condition even when coming back from the other page.
    reset()
  }

  render() {
    const { api, page } = this.props
    const fetchingPage = api.fetchingURLs.indexOf('/api/cache/pages/1/sub') >= 0

    return (
      <div>
        <PageTitle
          title='お問い合わせが完了しました'
          page={page}
          breadCrumbs={[
            { title: '看護師求人サイトCarriee', url: '/' },
            { title: 'お問い合わせが完了しました', url: '/contact_sent' },
          ]}
          showLastBreadCrumbs={false}
        >

          <div style={{ paddingTop: '3em', paddingBottom: '3em' }}>
            内容を確認させていただき、ご返信が必要な場合には、最短でも3営業日ほどお時間をいただく場合がございます。<br />
            <br />
            あらかじめご了承ください。<br />

            <div style={{ marginTop: '4em', display: 'flex', flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
              <Link to='/'>
                <Button color='blue' style={{ width: '13em' }}>トップページへ戻る</Button>
              </Link>
            </div>
          </div>

        </PageTitle>
      </div>
    )
  }
}
