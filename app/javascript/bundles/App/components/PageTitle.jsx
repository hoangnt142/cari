import React, { PureComponent } from 'react'
import PropType from 'prop-types'
import { Link } from 'react-router-dom'
import {
  Breadcrumb,
  Segment,
  Header,
  Grid,
  Icon,
  Input,
  Container,
  Responsive
} from 'semantic-ui-react'
import PageHelmet from './PageHelmet'
import { breadCrumbList } from '../../../lib/structured-data'
import SearchInput from '../containers/search/InputContainer'

export default class PageTitle extends PureComponent {

  static propTypes = {
    title: PropType.string.isRequired,
    seo_title: PropType.string,
    seo_description: PropType.string,
    seo_keywords: PropType.string,
    page: PropType.object.isRequired,
    breadCrumbs: PropType.array,
    showLastBreadCrumbs: PropType.bool,
    showTitle: PropType.bool,
    canonicalUrl: PropType.string,
  }
  static defaultProps = {
    showLastBreadCrumbs: true,
    showTitle: true,
  }

  render() {
    console.log('rendering')

    return (
      <div style={{ paddingBottom: '0.2rem'}}>
        <Responsive minWidth={992}
                    as='div'>
          <Content {...this.props}>
            {this.props.children}
          </Content>
        </Responsive>

        <Responsive maxWidth={991}
                    as='div'>
          <Content {...this.props}>
            {this.props.children}
          </Content>
        </Responsive>
      </div>
    )
  }
}

class Content extends PureComponent {
  render() {
    const {
      title,
      seoTitle,
      seoDescription,
      seoKeywords,
      page,
      pageNumber,
      breadCrumbs,
      showLastBreadCrumbs,
      showTitle,
      children,
      canonicalUrl,
    } = this.props

    let list = breadCrumbs
    if(!showLastBreadCrumbs) {
      list = breadCrumbs.slice(0, -1)
    }

    const breadCrumbsWithDivider = []
    list.forEach((bc, i) => {
      breadCrumbsWithDivider.push(
        <Breadcrumb.Section key={i}>
          <Link to={bc.url}>{bc.title}</Link>
          <Breadcrumb.Divider key={`divider-${i}`} icon="right angle" style={{fontSize: '1rem'}}/>
        </Breadcrumb.Section>
      )
    })

    const additional = {}
    if(seoTitle) {
      additional.seoTitle = seoTitle
    }
    if(seoDescription) {
      additional.seoDescription = seoDescription
    }
    if(seoKeywords) {
      additional.seoKeywords = seoKeywords
    }
    if(pageNumber) {
      additional.pageNumber = pageNumber
    }


    return (
      <div>
        <PageHelmet page={page}
                    canonicalUrl={canonicalUrl}
                    {...additional}
        >
          {breadCrumbList(breadCrumbs)}
        </PageHelmet>

        <Segment id="page-title" style={{padding: '0', borderRadius: 0, boxShadow: 'none'}}>
          <div>
            <div style={{ backgroundColor: '#fff1f2',
                          paddingTop: '0.6em',
                          paddingBottom: '0.8em',
                          marginBottom: '1em'
            }}>

              <Responsive minWidth={992}
                          as='div'
                          style={{
                            maxWidth: '1027px',
                            margin: 'auto'
                          }}
              >
                <Breadcrumb style={{marginBottom: 0}}>
                  {breadCrumbsWithDivider.map(bc => bc)}
                </Breadcrumb>

                <Grid>
                  <Grid.Row style={{ flex: 0.7, justifyContent: 'center', margin: 'auto' }}>
                    <SearchInput
                    />
                  </Grid.Row>
                </Grid>
              </Responsive>
              <Responsive maxWidth={991}
                          as='div'
                          style={{
                          }}
              >
                <Breadcrumb style={{marginBottom: 0}}>
                  {breadCrumbsWithDivider.map(bc => bc)}
                </Breadcrumb>

                <div style={{ paddingTop: '0.4em' }}>
                  <Grid.Row style={{
                    display: 'flex',
                    fiex:1,
                    flexDirection: 'row',
                    justifyContent: 'center',
                  }}>
                    <SearchInput
                    />
                  </Grid.Row>
                </div>
              </Responsive>
            </div>

            <Responsive minWidth={992}
                        as='div'
                        style={{
                          backgroundColor: 'white',
                          maxWidth: '1027px',
                          margin: 'auto',
                          paddingBottom: (children || showTitle) ? '1em' : '0rem'
                        }}>
              {showTitle && (<Header style={{paddingTop: '1.3rem', paddingBottom: '1rem' }} as='h1' className='big grey'>{title}</Header>)}
              {children}
            </Responsive>
            <Responsive maxWidth={991}
                        as='div'
                        style={{
                          marginLeft: '1em',
                          marginRight: '1em',
                          backgroundColor: 'white',
                          paddingBottom: (children || showTitle) ? '1em' : '0rem'
                        }}>
              {showTitle && (<Header style={{paddingTop: '1.3rem'}} as='h1' className='big grey'>{title}</Header>)}
              {children}
            </Responsive>
          </div>
        </Segment>
      </div>
    )
  }
}
