import React, { PureComponent } from 'react'
import PropType from 'prop-types'
import { Link } from 'react-router-dom'
import { savedJobIds } from 'lib/function'
import HospitalList from './_HospitalList'
import HospitalListMobile from './mobile/_HospitalListMobile'

import {
  Button, Image, Icon,
  Search, Modal, Segment, Sticky,
  Header,
  Item, Loader, Dimmer,
  Responsive,
  Container
} from 'semantic-ui-react'

import PageTitle from './PageTitle'


export default class MyJobs extends PureComponent {

  static propTypes = {
  }
  static defaultProps = {
  }

  constructor(props) {
    super(props)
    const { fetchPage, getHospitals } = this.props
    // fetchPage()
    getHospitals()
  }

  componentWillUnmount() {
    const { reset } = this.props
    // Resets the state as initial condition even when coming back from the other page.
    reset()
  }

  render() {
    const { page, history, hospitals } = this.props
    return (
      <div>
        <PageTitle
          title='保存した求人'
          page={page}
          breadCrumbs={[
            { title: '看護師求人サイトCarriee', url: '/' },
            { title: '保存した求人', url: '/my_jobs' },
          ]}
          showLastBreadCrumbs={false}
        />

        <div style={{ margin: '1rem 0.5rem', maxWidth: '1024px', margin: 'auto' }}>
          <Responsive as={HospitalListMobile} maxWidth={991} hospitals={hospitals} history={history} />
          <Responsive as={HospitalList} minWidth={992} hospitals={hospitals} history={history} />
        </div>

      </div>
    )
  }
}
