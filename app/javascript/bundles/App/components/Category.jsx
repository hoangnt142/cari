import React, { PureComponent } from 'react'
import PropType from 'prop-types'
import { Link } from 'react-router-dom'
import moment from 'moment'
import Pagination from './SEOPagination'
import ArticleList from './ArticleList'
import ArticleListMobile from './ArticleListMobile'

import { Image, Responsive, Container } from 'semantic-ui-react'

import PageTitle from './PageTitle'

export default class Category extends PureComponent {

  static propTypes = {
  }
  static defaultProps = {
  }

  constructor(props) {
    super(props)
  }

  componentDidMount() {
    const { fetch } = this.props
    // Sub data should be gotten after being mounted.
    //fetch('/api/cache/pages/1/sub')
  }

  componentWillUnmount() {
    const { reset } = this.props
    // Resets the state as initial condition even when coming back from the other page.
    reset()
  }

  render() {
    const { api, page } = this.props

    const { category, totalPages, currentPage } = api
    const articles = category.articles
    return (
      <React.Fragment>
        <PageTitle
          title={page.title}
          page={page}
          seoTitle={page.seo_title}
          breadCrumbs={[
            { title: '看護師求人サイトCarriee', url: '/' }
          ].concat(category.breadcrumb)}
          showLastBreadCrumbs={false}
        >
          <Responsive minWidth={992} as='div' className='carriee-editor' style={{padding: 0, marginBottom: '1rem'}} dangerouslySetInnerHTML={{ __html: page.content }} />
          {articles[0]&&
            <Responsive maxWidth={991} className='firstArticle'>
              <div className='eyeCatch'>
                <Link to={articles[0].url}>
                  {articles[0].image && <Image src={articles[0].image.image_url.large} alt={articles[0].image.alt}/>}
                </Link>
                <div className='firstArticleInfo'>
                  <div>
                    <span className='pubDate'>{moment(articles[0].publish_at).format('YYYY.MM.DD')}</span>
                    <Link className='ui text blue' to={articles[0].category.url}>{articles[0].category.title}</Link>
                  </div>
                  <Link to={articles[0].url} className='firstArticleTitle'>{articles[0].title}</Link>
                </div>
              </div>
              <div className='shortContent'>{articles[0].short_content}</div>
            </Responsive>
          }
        </PageTitle>

        <Container>
          <Responsive minWidth={992} as={ArticleList} articles={articles} />
          <Responsive maxWidth={991} as={ArticleListMobile} articles={articles} hideFirstItem/>
        </Container>
        {totalPages > 1 &&
          <div className='carrieePagination'>
            <Pagination totalPages={totalPages}
                        currentPage={parseInt(currentPage)}
                        basePath={page.url}
                />
          </div>
        }
      </React.Fragment>
    )
  }
}
