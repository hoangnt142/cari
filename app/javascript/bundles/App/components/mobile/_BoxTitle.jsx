import React, { PureComponent } from 'react'
import PropType from 'prop-types'
import { Link } from 'react-router-dom'

import {
  Header
} from 'semantic-ui-react'

export default class MobileBoxTitle extends PureComponent {

  render() {
    const { as, style, fontSize } = this.props

    return (
      <div style={Object.assign({width: '100%'}, style)}>
        <div style={{display: 'flex', height: '2px', flexDirection: 'row'}}>
          <div style={{flex: 30, backgroundColor: this.props.leftColor || 'rgb(243,101,129)'}}></div>
          <div style={{flex: 70, backgroundColor: '#c9c9c9'}}></div>
        </div>
        <Header color='pink' as={as || 'h2'} style={{
          backgroundColor: '#f2f2f2',
          height: '65px',
          display: 'flex',
          alignItems: 'center',
          marginTop: 0,
          paddingLeft: '1rem',
          paddingRight: '1rem',
          fontSize: fontSize || '19px'
        }}>{this.props.title}</Header>
        
      </div>
    )
  }
}
