import React, { PureComponent } from 'react'
import PropType from 'prop-types'
import { Link } from 'react-router-dom'
import BoxTitle from '../_BoxTitle'
import SearchKey from '../_SearchKey'
import GoogleMap from '../_GoogleMap'
import JobInfo from './_JobInfo'
import HospitalBasicInfo from './_HospitalBasicInfo'

import {
  Button,
  Icon,
  Grid,
  Segment,
  Container,
  Rail,
  Rating,
  Label,
  Transition,
  List,
  Header,
  Sticky,
} from 'semantic-ui-react'

export default class HospitalListMobile extends PureComponent {

  static propTypes = {
    deletable: PropType.boolean.isRequired
  }

  static defaultProps = {
    deletable: false
  }

  render() {
    const {
      hospitals,
      deletable
    } = this.props

    if(hospitals.length == 0) {
      return (
        <Segment basic style={{ backgroundColor: '#fff', borderRadius: '4px'}}>検索結果はありません。</Segment>
      )
    }
    else {
      let content = hospitals.map(hospital =>
        <Hospital key={hospital.id} hospital={hospital} deletable={deletable} />
      )
      return content
    }
  }
}

class Hospital extends PureComponent {
  state = {
    visibleDetail: false,
    visibleMap: false
  }
  toggleDetailVisibility = () => this.setState({ visibleDetail: !this.state.visibleDetail })
  toggleMapVisibility = () => this.setState({ visibleMap: !this.state.visibleMap })

  render() {
    const { hospital, deletable } = this.props
    const { visibleDetail, visibleMap } = this.state
    const jobs = hospital.jobs
    return (
      <div>
        <Segment basic
                 style={{ backgroundColor: '#fff',
                          borderRadius: '4px',
                          paddingTop: '1rem',
                          paddingBottom: '3rem',
                          marginBottom: '1.8rem' }}
                 key={hospital.id}>
          <div style={{ textAlign: 'right',
                        marginBottom: '0.5em' }}>
            <span className='ui text pink'
                  style={{ paddingRight: '0.75em', fontFamily: 'arial' }}>NEW</span>
            <span className='ui text'
                  style={{ color: '#b0b0b0' }}>{hospital.max_job_created}</span>
          </div>

          <Grid style={{marginTop: 0}}
                verticalAlign='middle'>

            <Grid.Row style={{ padding: 0, marginBottom: '0.7em' }}>
              <Grid.Column width={2} style={{ paddingLeft: 0 }}>
                <i className='hospitalIcon' style={{
                  fontSize: '1.5em',
                  width: '1.6em'
                }} />
              </Grid.Column>
              <Grid.Column width={14}
                           style={{ lineHeight: '1.3em',
                                    fontSize: '1.5em',
                                    paddingLeft: '0',
                                    paddingRight: '0.3em'}}>
                <Link to={hospital.url} style={{ textDecoration: 'underline' }}>{hospital.name}</Link>
              </Grid.Column>
            </Grid.Row>

            {[
               {
                 icon: 'marker',
                 text: hospital.work_location
               }].map(({ icon, text }, i) => {
                 const style = { padding: 0 }
                 if(i == 0) {
                   style.marginBottom = '1rem'
                 }

                 return (
                   <Grid.Row style={style} key={i}>
                     <Grid.Column width={1}
                                  style={{ padding: '0.3em' }}>
                       <Icon name={icon} color='grey' size='large' />
                     </Grid.Column>
                     <Grid.Column width={15}
                                  style={{ lineHeight: '1.3em' }}>
                       {text}
                     </Grid.Column>
                   </Grid.Row>
                 )
               })}

            <Grid.Row style={{ margin: '0 2.8em',
                               paddingTop: '0'
            }}>
              <Grid.Column width={7}
                           as={'a'}
                           onClick={this.toggleDetailVisibility}
                           textAlign='center'
                           style={{ backgroundColor: '#40abe1',
                                    borderRadius: '1em',
                                    color: 'white',
                                    paddingTop: '0.2em',
                                    paddingBottom: '0.2em'
                           }}>
                <Icon name='table' color='white' />
                詳細
                <Icon name='caret down' color='white' style={{ marginLeft: '1em' }} />
              </Grid.Column>
              <Grid.Column width={2} />
              <Grid.Column width={7}
                           as={'a'}
                           onClick={this.toggleMapVisibility}
                           textAlign='center'
                           style={{ backgroundColor: '#40abe1',
                                    borderRadius: '1em',
                                    color: 'white',
                                    paddingTop: '0.2em',
                                    paddingBottom: '0.2em'
                           }}>
                <Icon name='map' color='white' />
                地図
                <Icon name='caret down' color='white' style={{ marginLeft: '1em' }} />
              </Grid.Column>
            </Grid.Row>

            <Transition.Group as={List} animation='fade down' duration={500} style={{margin: 0, width: '100%'}}>
              {visibleDetail &&
               <List.Item>
                 <HospitalBasicInfo hospital={hospital} />
               </List.Item>
              }
              {visibleMap &&
               <List.Item>
                 <GoogleMap hospitalId={hospital.id} lat={hospital.latitude} long={hospital.longitude} />
               </List.Item>
              }
            </Transition.Group>

            {hospital.jobs_details.length > 0 &&
             <Grid.Column width={16} style={{marginTop: 0, padding: '0.5em 0.4em 1em 0.4em' }}>
               <SearchKey keys={hospital.jobs_details} />
             </Grid.Column>
            }

            <Grid.Row style={{padding: 0}}>
              {jobs.length == 0 ? (
                 <Grid.Column width={16}>
                   <Segment basic style={{backgroundColor: '#f6f3f3',
                                          display: 'flex',
                                          borderRadius: '6px',
                                          alignItems: 'center'}}>
                     <Label
                       color='blue'
                       style={{
                         width: 30,
                         height: 30,
                         minWidth: 30,
                         textAlign: 'center',
                         marginRight: '1rem',
                         display: 'flex',
                         alignItems: 'center',
                         justifyContent: 'center'
                       }}
                     >
                       <div style={{fontSize: '25px'}}>!</div>
                     </Label>
                     <div style={{fontWeight: 'bold'}}>
                       <div>現在募集している求人はありません。</div>
                       <div>転職エージェントに依頼することで、逆指名での転職活動は可能です。</div>
                     </div>
                   </Segment>
                 </Grid.Column>
              ) : jobs.map((job, i) => (
                <JobInfo key={i}
                         i={i}
                         hospital={hospital}
                         job={job} />
              ))}
            </Grid.Row>
               </Grid>
        </Segment>
      </div>
    )
  }
}
