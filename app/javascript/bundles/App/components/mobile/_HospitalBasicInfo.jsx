import React, { PureComponent } from 'react'
import PropType from 'prop-types'
import { Link } from 'react-router-dom'

import {
  Icon,
  List,
  Header,
  Responsive,
} from 'semantic-ui-react'

class HospitalBasicInfo extends PureComponent {

  header = (title) => (
    <p style={{ fontSize: '1em', marginBottom: 0 }}>
      <Icon name='circle' style={{ color: '#40abe1' }} /><span style={{ fontWeight: 'bold' }}>{title}</span>
    </p>
  )

  content = (content) => (
    <p style={{ marginTop: '0.4em', marginLeft: '1.4em', marginBottom: '0.4em' }}>
      {content}
    </p>
  )

  render() {
    const { hospital } = this.props
    const itemData = [
      { title: '設立日', content: hospital.established },
      { title: '施設形態', content: hospital.hospital_types.map(type => type.name).join(' ／ ') },
      { title: '病床数', content: hospital.number_of_beds_text },
      { title: '診療科目', content: hospital.medical_subjects },
      { title: '診療時間', content: hospital.business_time },
      { title: '休診日', content: hospital.holidays },
      { title: '施設の特徴', content: hospital.features },
      { title: '交通手段', content: hospital.access },
    ]
    const items = itemData.filter(id => id.content )
                          .map((id, i) => ({ key: i,
                                             header: this.header(id.title),
                                             content: this.content(id.content)}))

    return (
      <List style={{ paddingTop: '0.55em',
                     paddingLeft: '0.4em',
                     paddingRight: '0.4em' }}
            items={items} />
    )
  }
}

export default HospitalBasicInfo
