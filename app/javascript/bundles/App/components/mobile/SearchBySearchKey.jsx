import React, { PureComponent } from 'react'
import PropType from 'prop-types'
import MobileBoxTitle from './_BoxTitle'
import { chunkArray } from '../../lib/semantic'

import {
  Button,
  Checkbox,
  Icon,
  List,
  Loader, Dimmer,
  Grid, Segment, Label, Table
} from 'semantic-ui-react'
import { Element, animateScroll as scroll } from 'react-scroll'

export default class SearchBySearchKey extends PureComponent {

  checked = (field, value) => {
    return this.props[field].indexOf(value) != -1
  }

  focusOnBlock = () => {
    console.log('focusOnBlock', this.props.modalBlock)
    if (this.props.modalBlock) {
      //let modalNode = document.getElementsByClassName('dimmable')[0]
      let elem = document.getElementById(this.props.modalBlock)
      if (elem) {
        console.log('offset', elem.offsetTop)
        //this.modalNode.current.scrollTop = elem.offsetTop
/*
        scroll.scrollTo(this.props.modalBlock, {
          isDynamic: true,
          duration: 1000,
          delay: 1500,
          containerId: 'containerElement',
        })
*/
      }
    }
  }

  constructor(props) {
    super(props)
  }

  componentDidMount = () => {
    this.focusOnBlock()
  }

  componentDidUpdate = () => {
    this.focusOnBlock()
  }

  render() {
    const {
      qualifications,
      jobContracts,
      hospitalTypes,
      jobTypes,
      jobDetails,
      fetching,
      onChangeJobContract,
      onChangeQualification,
      onChangeHospitalType,
      onChangeJobType,
      onChangeJobDetail,
      search,
      count,
      changeScreen,
      closeModal,
      mobileMode,
      popupStyle,
      onSubmit,
      selectedItems,
    } = this.props

    const keyList = (keys, title, checkedMark, onChange) => {
      return (
        <Element name={checkedMark} style={{width: '100%', padding: 0}}>
          <Grid.Row id={checkedMark}>
            <MobileBoxTitle title={title} as='h3'/>
          </Grid.Row>
          <Table celled unstackable style={{marginTop: 0, borderRadius: 0, border: 'none'}}>
            <Table.Body>
              {keys &&
                chunkArray(keys, 2).map((sks, i) =>
                  <Table.Row key={i}>
                    {sks.map(sk =>
                      <Table.Cell key={sk.id} width={8} style={{height: '65px'}}>
                        <Checkbox
                          label={sk.name}
                          onChange={onChange}
                          value={sk.id}
                          checked={this.checked(checkedMark, sk.id)}
                        />
                      </Table.Cell>
                    )}
                    {sks.length == 1 &&
                      <Table.Cell></Table.Cell>
                    }
                  </Table.Row>
                )
              }
            </Table.Body>
          </Table>
        </Element>
      )
    }

    return (
      <div id='containerElement'>
        <Dimmer.Dimmable as='div' dimmed={fetching} style={popupStyle}>
          <Dimmer simple inverted><Loader /></Dimmer>
          <Grid padded>
            {/*Modal Header*/}
            <Grid.Row
              color='blue'
              verticalAlign='middle'
              style={{
                height: '65px',
                fontSize: '22px',
                fontWeight: 'bold',
                position: 'fixed',
                top: 0,
                zIndex: 1
              }}>
              <Grid.Column width={16} style={{
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'center'
              }}>
                希望条件で探す
                <Icon name='close'
                      size='large'
                      style={{cursor: 'pointer', marginLeft: 'auto'}} onClick={() => closeModal()}/>
              </Grid.Column>
            </Grid.Row>
            {/*Modal Header*/}
            <Grid.Row>
              <Grid.Column width={16}>
                <div style={style.changeScreen}>
                  <Label color='green' style={style.changeScreenItem}>
                    <i className='areaIcon' />
                    エリア
                  </Label>
                  {selectedItems.length == 0 ?
                   '未設定'
                   :
                   selectedItems.map((item, i) => {
                     if (i == 1) {
                       return ` ${item}`
                     } else if (i > 1) {
                       return `, ${item}`
                     } else {
                       return item
                     }
                   })
                  }
                  <Button style={style.changeScreenButton} onClick={() => changeScreen('Area')} color='grey'>設定</Button>
                </div>

              </Grid.Column>
            </Grid.Row>

            {keyList(jobContracts, '雇用形態', 'jobContractIds', onChangeJobContract)}
            {keyList(qualifications, '保有資格', 'qualificationIds', onChangeQualification)}
            {keyList(hospitalTypes, '病院・施設', 'hospitalTypeIds', onChangeHospitalType)}
            {keyList(jobTypes, '仕事内容', 'jobTypeIds', onChangeJobType)}
            {keyList(jobDetails, 'こだわり条件', 'jobDetailIds', onChangeJobDetail)}

          </Grid>

          {/*Modal Footer*/}
          <Grid padded verticalAlign='middle' style={{
            height: 95,
            backgroundColor: '#eeeeee',
            borderBottomLeftRadius: '4px',
            borderBottomRightRadius: '4px',
            position: 'fixed',
            bottom: 0,
            width: '100%'
          }}>
            <Grid.Column width={8}>
              <Button color='blue' style={{
                height: 55,
                color: '#fff',
                width: '100%',
                padding: '5px'
              }} onClick={() => onSubmit()}><Icon name='search' />この条件で検索する</Button>
            </Grid.Column>
            <Grid.Column width={8} style={{
              fontSize: '1.3rem',
              fontWeight: 'bold',
              color: '#000',
              fontSize: '1rem',
              paddingLeft: '0px',
              display: 'flex',
              alignItems: 'flex-end'
            }}>
              絞り込んだ場合の求人数
              <div style={{lineHeight: '40px'}}><span className='ui text pink' style={{fontSize: '30px'}}>{fetching ? <Loader active inline /> : count}</span>件</div>
            </Grid.Column>

          </Grid>
          {/*Modal Footer*/}
        </Dimmer.Dimmable>
      </div>
    )
  }
}

const style = {
  changeScreen: {
    padding: '0 0.5rem 0 0',
    border: '1px solid #ccc',
    display: 'flex',
    alignItems: 'center',
    height: '95px',
    fontSize: '18px',
    marginBottom: '0.5rem'
  },
  changeScreenItem: {
    border: 'none',
    borderRadius: '0px',
    height: '100%',
    width: '90px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: '18px',
    marginRight: '1rem'

  },
  changeScreenButton: {
    marginLeft: 'auto',
    fontSize: '18px'
  }
}
