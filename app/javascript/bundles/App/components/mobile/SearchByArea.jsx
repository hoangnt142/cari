import React, { PureComponent } from 'react'
import PropType from 'prop-types'
import { Link } from 'react-router-dom'
//import _ from 'lodash'

import {
  Button, Image, Icon,
  Checkbox,
  List,
  Header,
  Loader, Dimmer, Grid, Responsive
} from 'semantic-ui-react'

export default class SearchByArea extends PureComponent {

  render() {
    const {
      regions,
      prefectureList,
      area1s,
      area2s,
      area3s,
      onChangeRegion,
      onChangePrefecture,
      onChangeArea,
      changeScreen,
      search,
      regionId,
      prefectureIds,
      area1Ids,
      area2Ids,
      area3Ids,

      count,
      counting,
      fetching,
      closeModal,
      popupStyle,
      onSubmit
    } = this.props

    let content = null
    let popupTitle = 'エリア選択'

    console.log('prefectureListprefectureListprefectureList', prefectureList)
    let showArea3 = false
    let showArea2 = false

    area2s.forEach(data => {
      if(area1Ids.findIndex(id => data.area1.id == id) >= 0 && data.area2s.length > 0) {
        showArea2 = true
      }
    })

    area3s.forEach(data => {
      if(area2Ids.findIndex(id => data.area2.id == id) >= 0 && data.area3s.length > 0) {
        showArea3 = true
      }
    })

    if (showArea3) {
      popupTitle = `エリア選択「${area2s.filter(a => a.area1.id == area1Ids[0])[0].area1.name}」`
      content =
        <Area3
          area3s={area3s}
          area2Ids={area2Ids}
          area3Ids={area3Ids}
          onChangeArea={onChangeArea}
        />
    } else if (showArea2) {
      popupTitle = `エリア選択「${area2s.filter(a => a.area1.id == area1Ids[0])[0].area1.name}」`
      content =
        <Area2
          area2s={area2s}
          area1Ids={area1Ids}
          area2Ids={area2Ids}
          onChangeArea={onChangeArea}
        />
    } else if (area1s.length > 0) {
      popupTitle = `エリア選択「${area1s[0].prefecture.name}」`
      content =
        <Area1
          area1s={area1s}
          prefectureIds={prefectureIds}
          area1Ids={area1Ids}
          onChangeArea={onChangeArea}
          onChangePrefecture={onChangePrefecture}
        />
    } else if (prefectureList && prefectureList.length > 0) {
      popupTitle = regionId != null ? `エリア選択「${regions.filter(region => region.id == regionId)[0].name}」` : popupTitle
      content =
        <List selection divided verticalAlign='middle'>
          <BackButton title='エリア選択に戻る' reset={(e, data) => onChangeRegion({}, {checked: false, reset: true})} />
          {prefectureList.map(prefecture =>
            <List.Item
              onClick={(e, data) => onChangePrefecture({}, {checked: true, value: prefecture.id})}
              key={prefecture.id}
              style={{
              padding: '0 1rem 0 1rem',
              fontWeight: 'bold'
            }}>
              <List.Content floated='right' style={{lineHeight: '65px'}}>
                <Icon floated='right' name='chevron right' color='grey'/>
              </List.Content>
              <List.Content style={{lineHeight: '65px'}}>
                {prefecture.name}
              </List.Content>
            </List.Item>
          )}
        </List>
    } else {
      content =
        <List selection divided verticalAlign='middle'>
          {regions.map(region =>
            <List.Item
              onClick={(e, data) => onChangeRegion({}, {checked: true, value: region.id})}
              key={region.id}
              style={{
              padding: '0 1rem 0 1rem',
              fontWeight: 'bold'
            }}>
              <List.Content floated='right' style={{lineHeight: '65px'}}>
                <Icon floated='right' name='chevron right' color='grey'/>
              </List.Content>
              <List.Content style={{lineHeight: '65px'}}>
                {region.name}
              </List.Content>
            </List.Item>
          )}
        </List>
    }
    return (
      <Dimmer.Dimmable as='div' dimmed={fetching}  style={popupStyle}>
        <Dimmer simple inverted><Loader /></Dimmer>

        <Grid padded>

          {/*Modal Header*/}
          <Grid.Row color='green' verticalAlign='middle' style={{height: '65px', fontSize: '22px', fontWeight: 'bold', position: 'fixed', top: 0, zIndex: 1 }}>
            <Grid.Column width={16} style={{
              display: 'flex',
              flexDirection: 'row',
              alignItems: 'center'
            }}>
              {popupTitle}
              <Icon name='close' style={{fontSize: '40px'}} size='large' style={{cursor: 'pointer', marginLeft: 'auto'}} onClick={() => closeModal()}/>
            </Grid.Column>
          </Grid.Row>
          {/*Modal Header*/}

          <Grid.Row style={{padding: 0}}>
            <Grid.Column width={16} style={{padding: 0}}>
              {content}
            </Grid.Column>
          </Grid.Row>

          <Grid.Row verticalAlign='middle' style={{
            height: 95,
            backgroundColor: '#eeeeee',
            borderBottomLeftRadius: '4px',
            borderBottomRightRadius: '4px',
            position: 'fixed',
            bottom: 0,
            width: '100%'
          }}>
            <Grid.Column width={5}>
              <Button color='pink' style={{
                minHeight: '55px',
                color: '#fff',
                width: '100%',
                fontSize: '1rem',
                padding: '0.5rem'
              }} onClick={() => onSubmit()}>
                <Icon name='search' style={{margin: '0'}}/>
                <Responsive as='br' maxWidth={494}/>
                この条件で検索する
              </Button>
            </Grid.Column>
            <Grid.Column width={4} style={{paddingLeft: 0}}>
              <Button onClick={() => changeScreen('SearchKey')} color='grey' style={{
                minHeight: '55px',
                color: '#fff',
                width: '100%',
                fontSize: '1rem',
                padding: '0.5rem'
              }}>条件追加</Button>
            </Grid.Column>
            <Grid.Column width={7} style={{
              fontSize: '1.3rem',
              fontWeight: 'bold',
              color: '#000',
              fontSize: '1rem',
              paddingLeft: '0px',
              display: 'flex',
              alignItems: 'flex-end'
            }}>
              絞り込んだ場合の求人数
              <div style={{lineHeight: '40px'}}><span className='ui text pink' style={{fontSize: '30px'}}>{counting ? <Loader active inline /> : count}</span>件</div>
            </Grid.Column>
          </Grid.Row>

        </Grid>
      </Dimmer.Dimmable>
    )
  }
}

class Area1 extends PureComponent {
  render() {
    const {
      area1s,
      prefectureIds,
      area1Ids,
      onChangeArea,
      onChangePrefecture
    } = this.props

    if(area1s.length == 0 || prefectureIds.length == 0) {
      return null
    }
    else {
      return (
        <div>
          {area1s.map((data, j) => {
            const {
              prefecture,
              area1s,
            } = data

            if(!prefectureIds || prefectureIds.findIndex(id => prefecture.id == id) == -1) {
              return null
            } else {
              return (
                <List selection divided verticalAlign='middle' key={j}>
                  <BackButton title='都道府県選択に戻る'  reset={(e, data) => onChangePrefecture({}, {checked: false, reset: true})} />
                  {area1s.map((area, i) =>
                    <List.Item
                      onClick={(e, data) => onChangeArea({}, {checked: true, value: area.id})}
                      key={area.id}
                      style={{
                      padding: '0 1rem 0 1rem',
                      fontWeight: 'bold'
                    }}>
                      <List.Content floated='right' style={{lineHeight: '65px'}}>
                        <Icon floated='right' name='chevron right' color='grey'/>
                      </List.Content>
                      <List.Content style={{lineHeight: '65px'}}>
                        {area.name}
                      </List.Content>
                    </List.Item>
                  )}
                </List>
              )
            }
          })}
        </div>
      )
    }
  }
}

class Area2 extends PureComponent {
  render() {
    const {
      area2s,
      area1Ids,
      area2Ids,
      onChangeArea
    } = this.props

    if(area2s.length == 0) {
      return null
    }

    let hideAll = true
    area2s.forEach(data => {
      if(area1Ids.findIndex(id => data.area1.id == id) >= 0 && data.area2s.length > 0) {
        hideAll = false
      }
    })

    if(hideAll) {
      return null
    }

    return (
      <div>
        {area2s.map((data, j) => {
           const {
             prefecture,
             area1,
             area2s
           } = data

           if(!area1Ids || area1Ids.findIndex(id => area1.id == id) == -1) {
             return null
           } else {
            return (
              <List selection divided verticalAlign='middle' key={j}>
                <BackButton title='大エリア選択に戻る' reset={(e, data) => onChangeArea({}, {checked: false, value: null, resetArea: true})} />
                 {area2s.map((area, i) =>
                  <List.Item key={i}
                    onClick={(e, data) => onChangeArea({}, {checked: area2Ids.findIndex(id =>id == area.id) == -1, value: area.id})}
                    style={{
                      padding: '0 1rem 0 1rem',
                      fontWeight: 'bold'
                    }}
                  >
                    <List.Content style={{lineHeight: '65px'}}>
                      <Checkbox label={area.name}
                                checked={area2Ids.findIndex(id => id == area.id) >= 0}
                                value={area.id}
                                onChange={onChangeArea}
                      />
                    </List.Content>
                  </List.Item>
                )}
              </List>
            )
          }
        })}
      </div>
    )
  }
}

class Area3 extends PureComponent {
  render() {
    const {
      area3s,
      area2Ids,
      area3Ids,
      onChangeArea
    } = this.props

    if(area3s.length == 0) {
      return null
    }

    let hideAll = true
    area3s.forEach(data => {
      if(area2Ids.findIndex(id => data.area2.id == id) >= 0 && data.area3s.length > 0) {
        hideAll = false
      }
    })

    if(hideAll) {
      return null
    }

    return (
      <div>
        {area3s.map((data, j) => {
           const {
             prefecture,
             area1,
             area2,
             area3s
           } = data

           if(!area2Ids || area2Ids.findIndex(id => area2.id == id) == -1) {
             return null
           }
           else {
             return (
                <List selection divided verticalAlign='middle' key={j}>
                  <BackButton title='小エリアを選択' reset={(e, data) => onChangeArea({}, {checked: false, value: null, resetArea: true})} />
                  {area3s.map((area, i) =>
                    <List.Item key={i}
                      onClick={(e, data) => onChangeArea({}, {checked: area3Ids.findIndex(id => id == area.id) == -1, value: area.id})}
                      style={{
                        padding: '0 1rem 0 1rem',
                        fontWeight: 'bold'
                      }}
                    >
                      <List.Content style={{lineHeight: '65px'}}>
                        <Checkbox label={area.name}
                                  checked={area3Ids.findIndex(id => id == area.id) >= 0}
                                  value={area.id}
                                  onChange={onChangeArea}
                        />
                      </List.Content>
                    </List.Item>
                  )}
                </List>
             )
           }
        })}
      </div>
    )
  }
}
class BackButton extends PureComponent {

  render() {
    const { reset } = this.props
    return (
      <List.Item
        onClick={reset}
        style={{
          backgroundColor: '#f2f2f2',
          lineHeight: '70px',
          paddingLeft: '0 0 0 1rem',
          fontSize: '1.3rem',
          borderRadius: 0
        }}
      >
        <List.Icon name='chevron left' color='grey' verticalAlign='middle'/>
        <List.Content style={{lineHeight: '70px', fontWeight: 'bold'}} className='ui text pink'>
          {this.props.title}
        </List.Content>
      </List.Item>
    )
  }
}
