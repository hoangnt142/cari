import React, { PureComponent } from 'react'
import PropType from 'prop-types'
import { Link } from 'react-router-dom'
import FavoriteStatus from '../FavoriteStatus'
import { savedJobIds, toggleSavedJob } from 'lib/function'

import {
  Button,
  Icon,
  Grid,
  Segment,
  Container,
  Label,
  Transition,
  List,
  Header,
  Responsive,
} from 'semantic-ui-react'

class JobInfo extends PureComponent {

  static propTypes = {
    i: PropType.int.isRequired,
    hospital: PropType.object.isRequired,
    job: PropType.object.isRequired,
    minimize: PropType.bool.isRequired,
    showOpener: PropType.bool.isRequired,
  }

  state = {
    visibleJob: false
  }

  static defaultProps = {
    minimize: true,
    showOpener: true,
  }


  toggleJobVisibility = (jobId=null) => {
    this.setState({ visibleJob: !this.state.visibleJob })
    const jobIds = savedJobIds('history')
    //save job to history
    if (jobId && jobIds.indexOf(jobId) == -1) {
      toggleSavedJob('history', jobId)
      //Update job count in header
      document.getElementById('historyCount').innerHTML = savedJobIds('history').length
    }
  }

  render() {
    const { hospital, job, i, minimize, showOpener } = this.props
    const { visibleJob } = this.state

    let websiteGuideUrl = ''
    if(job.website_id == 1) {
      websiteGuideUrl = '/website-guide/kango-no-oshigoto'
    }
    else if(job.website_id == 2) {
      websiteGuideUrl = '/website-guide/mynavi-kangoshi'
    }
    else if(job.website_id == 3) {
      websiteGuideUrl = '/website-guide/nurse-jinzai-bank'
    }



    return (
      <Grid.Column width={16}
                   style={{ margin: '0.5rem 0 1.2rem 0',
                            paddingLeft: '5px',
                            paddingRight: '5px' }}>
        <Segment basic
                 style={{ backgroundColor: '#f6f3f3',
                          display: 'flex',
                          borderRadius: '6px',
                          alignItems: 'center',
                          padding: '0.8rem',
                          zIndex: 0 }}>

          <FavoriteStatus jobId={job.id} />

          {/*
              <Label
              color='blue'
              style={{
              width: 20,
              height: 30,
              minWidth: 20,
              textAlign: 'center',
              marginRight: '0.5rem',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center'
              }}>
              <p style={{fontSize: '21px'}}>{i+1}</p>
              </Label>
            */}

          <div style={{padding: '0.5rem 1em 0.5rem 2em'}}
               onClick={() => showOpener ? this.toggleJobVisibility(job.id) : null}
          >
            <p style={{ margin: 0, lineHeight: '1.3em' }}>
              <span className='ui text pink'
                    style={{ fontWeight: 'bold' }}>
                {job.qualifications.map(qualification => qualification.name).join('/')}
              </span>
              {job.salary_formated && (
                 <span className='ui text black'>
                   ／
                   {job.salary_type}
                   <em style={{ fontSize: '24px',
                                fontWeight: 'bold',
                                fontFamily: 'arial',
                                fontStyle: 'normal',
                                margin: '0 0.15em' }}>
                     {job.salary_formated}
                   </em>
                   円
                 </span>
              )}
            </p>
            <p style={{ lineHeight: '1.3em', marginTop: '0.45em' }}>
              {job.contracts.map(contract => contract.name).join('、')}
              {job.types.length > 0 && '／' + job.types.map(type => type.name).join('、')}
            </p>
          </div>

          {showOpener && (
             <Transition visible={!visibleJob} animation='fade' duration={500}>
               <div style={{ position: 'absolute', right: '-0.5em', }}
                    onClick={() => this.toggleJobVisibility(job.id)}
               >
                 <Icon name='chevron down'
                       circular
                       style={{ fontSize: '1.2em',
                                lineHeight: '1em',
                                color: '#bbb',
                                backgroundColor: 'white'
                         /*color: '#40abe1'*/ }} />
               </div>
             </Transition>
          )}

        </Segment>

        <Transition visible={!minimize || visibleJob} animation='slide down' duration={300}>
          <div>
            <JobInfoContent
              hospital={hospital}
              job={job}
              toggleJobVisibility={this.toggleJobVisibility}
              showOpener={showOpener}
            />
          </div>
        </Transition>

      </Grid.Column>
    )
  }
}

class JobInfoContent extends PureComponent {

  header = (title) => (
    <p style={{ fontSize: '1em', marginBottom: 0 }}>
      <Icon name='circle' style={{ color: '#40abe1' }} /><span style={{ fontWeight: 'bold' }}>{title}</span>
    </p>
  )

  content = (content) => {
    const { job } = this.props

    let html = ''
    if(content) {
      html = content.replace('\n', '<br />')
    }
    else if(job.website_id == 4) {
      html = '下記、募集詳細ページより確認して下さい。'
    }
    else {
      html = '下記、ご登録後に担当者に確認して下さい。'
    }

    return (
      <p style={{ marginTop: '0.4em', marginLeft: '1.4em', marginBottom: '0.4em' }}
         dangerouslySetInnerHTML={{__html: html}}
      />
    )
  }

  render() {
    const {
      hospital,
      job,
      toggleJobVisibility,
      showOpener
    } = this.props

    const itemData = [
      { title: '雇用形態', content: job.contracts.map(contract => contract.name).join('／') },
      { title: '求人内容・配属先', content: job.job_type_text },
      { title: '給与／年収', content: job.salary_detail },
      { title: '手当／福利厚生', content: job.allowance },
      { title: '勤務時間', content: job.work_time },
    ]

    const items = itemData.map((id, i) => ({
      key: i,
      header: this.header(id.title),
      content: this.content(id.content)}
    ))

    return (
      <Segment basic
               style={{ backgroundColor: '#f6f3f3',
                        borderRadius: '6px',
                        padding: '1rem 0.4rem 0.8rem 0.4rem',
                        marginTop: '-2rem'}}>

        <div style={{ backgroundColor: 'white',
                      borderRadius: '1em',
                      paddingTop: '1.2rem',
                      paddingBottom: '1.2rem',
                      marginBottom: '1rem'}}>

          <List style={{ paddingTop: '0.55em',
                         paddingLeft: '0.4em',
                         paddingRight: '0.4em' }}
                items={items} />

        </div>

        <RowColumnResponsive>
          <a
            className='ui lightgrey button'
            href={job.original_url}
            target='_blank'
            style={{ marginBottom: '1.5em',
                     padding: 0,
                     width: '16em',
            }}>
            <Button style={{ width: '100%' }}>
              <Icon name='file' />応募詳細ページへ</Button>
          </a>

          {/*job.website_id != 4 && job.website_redirect_link_url &&
           <a
             className='ui green button'
             href={job.website_redirect_link_url}
             target='_blank'
             style={{ marginBottom: '1.5em',
                      padding: 0,
                      width: '16em',
             }}>
             <Button color='green'>
               <Icon name='envelope' />転職エージェントへ依頼</Button>
           </a>
          */}

          <FavoriteStatus jobId={job.id}
                          type='button'
                          style={{
                            width: '16em',
                          }}
          />
        </RowColumnResponsive>

        {showOpener && (
           <div style={{ display: 'flex', justifyContent: 'center' }}
                onClick={() => toggleJobVisibility()}
             >
             <Icon name='chevron up'
                   circular
                   style={{ fontSize: '1.2em',
                            lineHeight: '1em',
                            color: '#bbb',
                            backgroundColor: 'white'
                   }} />
           </div>
        )}
      </Segment>
    )
  }
}

const styleButtons = {
  display: 'flex',
}
const styleButtonsColumn = Object.assign({
  margin: 'auto',
  justifyContent: 'center',
  flexDirection: 'column',
  width: '16em'
}, styleButtons)
const styleButtonsRow = Object.assign({
  marginTop: '2em',
  justifyContent: 'space-around',
  flexDirection: 'row'
}, styleButtons)
class RowColumnResponsive extends PureComponent {
  render() {
    return (
      <div>
        <Responsive maxWidth={991}
                    as='div'
                    style={styleButtonsColumn}>
          {this.props.children}
        </Responsive>
        <Responsive minWidth={992}
                    as='div'
                    style={styleButtonsRow}>
          {this.props.children}
        </Responsive>
      </div>
    )
  }
}


export default JobInfo
