import React, { PureComponent } from 'react'
import PropType from 'prop-types'
import { Link } from 'react-router-dom'
import BoxTitle from './_BoxTitle'
import GoogleMap from './_GoogleMap'
import SearchKey from './_SearchKey'

import {
  Button, Image, Icon,
  Search, Modal, Segment,
  Header,
  Item, Loader, Dimmer,
  Responsive,
  Container, Grid, Label, Divider, Table, Tab
} from 'semantic-ui-react'

import PageTitle from './PageTitle'
import JobInfo from './mobile/_JobInfo'
import HospitalListMobile from './mobile/_HospitalListMobile'
import SimpleResponsive from './_SimpleResponsive'

const JobDetail = ({jobsDetails}) => (
  jobsDetails.length > 0 ?
                       <Grid.Row style={{paddingTop: 0}}>
                         <Grid.Column width={16}>
                           <SearchKey keys={jobsDetails} />
                         </Grid.Column>
                       </Grid.Row>
                     : null
)

export default class HospitalPage extends PureComponent {

  static propTypes = {
  }
  static defaultProps = {
  }

  constructor(props) {
    super(props)
  }

  componentDidMount() {
    const { fetch } = this.props
    // Sub data should be gotten after being mounted.
    //fetch('/api/cache/pages/1/sub')

    this.props.init(this.props.page.pageable_id)
  }

  componentWillUnmount() {
    const { reset } = this.props
    // Resets the state as initial condition even when coming back from the other page.
    reset()
  }

  render() {
    const { api, page, history, relatedHospitals } = this.props
    const hospital = api.pageable
    const jobs = hospital.jobs

    return (
      <div>
        <PageTitle
          title=''
          showTitle={false}
          seoTitle={page ? page.seo_title : ''}
          seoDescription={page ? page.seo_description : ''}
          page={page}
          breadCrumbs={hospital.breadcrumb}
          showLastBreadCrumbs={true}
        >

          <BoxTitle
            as='h1' icon='hospital' title={hospital.name}
          />
          <Responsive as={Grid} maxWidth={991} verticalAlign='middle'>
            <JobDetail jobsDetails={hospital.jobs_details} />
          </Responsive>

          <Responsive as={Tab.Pane}
                      maxWidth={991}
                      style={{backgroundColor: '#f6f3f3', marginTop: '1rem', padding: '2rem 10px 1rem 10px'}}>
            <Grid verticalAlign='middle'>
              <Grid.Row style={{paddingTop: 0}}>
                <Grid.Column computer={2} mobile={5} style={{fontWeight: 'bold', paddingRight: 0, borderRight: '1px solid #ccc'}}>
                  <Icon name='subway' color='grey' style={{fontSize: '1rem'}} />
                  最寄り駅
                </Grid.Column>

                <Grid.Column computer={6} mobile={11}>
                  {hospital.access}
                </Grid.Column>
              </Grid.Row>

              <Grid.Row style={{paddingTop: 0}}>
                <Grid.Column computer={2} mobile={5} style={{fontWeight: 'bold', paddingRight: 0, borderRight: '1px solid #ccc'}}>
                  <Icon name='marker' color='grey' style={{fontSize: '1.2rem', marginLeft: '-3px'}} />
                  勤務地
                </Grid.Column>

                <Grid.Column computer={6} mobile={11}>
                  {hospital.work_location}
                </Grid.Column>
              </Grid.Row>

              <Grid.Row style={{paddingTop: 0}}>
                <Grid.Column mobile={5} style={{fontWeight: 'bold', paddingRight: 0, borderRight: '1px solid #ccc'}}>
                  <Icon name='hospital' color='grey' style={{fontSize: '1rem'}} />
                  施設形態
                </Grid.Column>

                <Grid.Column mobile={11}>
                  {hospital.hospital_types.map((type, i) =>
                    <span key={i}>
                      <Link to={`/${type.name_roman}`} style={{ textDecoration: 'underline' }}>{type.name}</Link>
                      {(i+1) != hospital.hospital_types.length &&
                       ' / '
                      }
                    </span>
                  )}
                </Grid.Column>
              </Grid.Row>

              <Grid.Row>
                <Grid.Column computer={8} mobile={16}>
                  <GoogleMap lat={hospital.latitude} long={hospital.longitude}/>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Responsive>

          <Responsive as={Grid}
                      minWidth={992}
                      verticalAlign='middle'>
            <Grid.Row style={{paddingTop: 0}}>
              <Grid.Column computer={2} mobile={5} style={{fontWeight: 'bold'}}>
                <Icon name='subway' color='grey' size='large' />
                最寄り駅
              </Grid.Column>

              <Grid.Column computer={6} mobile={11} style={{borderLeft: '1px solid #ccc'}}>
                {hospital.access}
              </Grid.Column>

              <Responsive minWidth={992}
                          as={Grid.Column}
                          computer={2}
                          mobile={5}
                          style={{fontWeight: 'bold'}}>
                <Icon name='hospital' color='grey' size='large' />
                施設形態
              </Responsive>

              <Responsive minWidth={992}
                          as={Grid.Column}
                          computer={6}
                          mobile={11}
                          style={{borderLeft: '1px solid #ccc'}}>
                {hospital.hospital_types.map((type, i) =>
                  <span key={i}>
                    <Link to={`/${type.name_roman}`}>{type.name}</Link>
                    {(i+1) != hospital.hospital_types.length &&
                     ' / '
                    }
                  </span>
                )}
              </Responsive>

            </Grid.Row>
            <Divider clearing style={{marginTop: 0, marginBottom: 0}}/>
            <Grid.Row>
              <Grid.Column computer={2} mobile={5} style={{fontWeight: 'bold'}}>
                <Icon name='marker' color='grey' size='large' />
                勤務地
              </Grid.Column>

              <Grid.Column computer={6} mobile={11} style={{borderLeft: '1px solid #ccc'}}>
                {hospital.work_location}
              </Grid.Column>

            </Grid.Row>

            <Grid.Row>
              <Grid.Column computer={8} mobile={16}>
                <GoogleMap lat={hospital.latitude} long={hospital.longitude}/>
              </Grid.Column>
            </Grid.Row>

            <JobDetail jobsDetails={hospital.jobs_details} />
          </Responsive>

        </PageTitle>

        <SimpleResponsive>
          <div>
            <Grid padded verticalAlign='middle'>
              <Grid.Column width={10} className='jobGuidline'>
                <h2>求人募集要項</h2>
              </Grid.Column>
            </Grid>
            {jobs.length > 0 ? (
               <Segment className='hospitalJobList'>
                 {hospital.title &&
                  <div>
                    <Header color='blue'>{hospital.title}</Header>
                    <Divider clearing />
                  </div>
                 }
                 <div style={{
                   fontSize: '1rem',
                   marginBottom: '1rem',
                   marginLeft: '0.5rem'
                 }}>
                   <span className='ui text pink'
                         style={{
                           fontSize: '1.3rem',
                           marginRight: '0.2em'
                         }}>{jobs.length}</span>
                   件の求人情報があります。
                 </div>

                 {jobs.map((job, i) =>
                   <JobInfo key={i}
                            i={i}
                            history={history}
                            hospital={hospital}
                            job={job}
                            minimize={false}
                            showOpener={false}
                   />
                 )}
               </Segment>
            ) : (
               <Segment>

                 <p style={{ fontWeight: 'bold', fontSize: '1.3rem' }}>現在募集している看護師求人はございません。</p>

                 <p style={{ lineHeight: '1.5em' }}>
                   <span style={{ fontWeight: 'bold', lineHeight: '2em' }}>【逆指名転職について】</span><br />
                   逆指名転職とは、看護師転職エージェントが行っているサービスの1つで、求人募集がない看護師求人の場合でも、担当するエージェントが交渉（入職の有無、給料、待遇）を行ってくれる無料のサービスです。<br />
                   しかし、逆指名転職を行っている転職エージェントは限られており、以下となります。
                 </p>


                 <Label basic color='pink' className='inquiryBox'>
                   <div className='leftBox'>
                     <div className='circular'>
                       <div className='intext'>
                         <div>この求人に</div>
                         <div>問い合わせる</div>
                       </div>
                     </div>
                   </div>
                   <div className='rightBox'>
                     <div>｢看護のお仕事｣は看護師の逆指名転職サービスを行っている転職エージェントです。</div>
                     <Responsive as={Link} to='/' minWidth={992}>
                       <Button color='green' style={{height: '60px', fontSize: '1.5rem', width: '300px', marginTop: '3.5rem'}}>詳細を確認する</Button>
                     </Responsive>
                   </div>
                   <Responsive as={Link} to='/' maxWidth={991} className='computer'>
                     <Button color='green' style={{height: '60px', fontSize: '1.5rem', width: '100%', marginTop: '0.5rem'}}>詳細を確認する</Button>
                   </Responsive>
                 </Label>
               </Segment>
            )}

            <Segment style={{
              marginTop: '30px',
              paddingBottom: '2rem' }}>
              <BoxTitle as='h2' title='基本情報' />
              <Table definition>
                <Table.Body className='hospitalBasicInfo'>
                  {hospital.name &&
                   <Table.Row>
                     <Table.Cell width={4}>名称</Table.Cell>
                     <Table.Cell style={{ paddingBottom: '1em' }}>{hospital.name}</Table.Cell>
                   </Table.Row>
                  }
                  {hospital.established &&
                   <Table.Row>
                     <Table.Cell width={4}>設立日</Table.Cell>
                     <Table.Cell>{hospital.established}</Table.Cell>
                   </Table.Row>
                  }
                  {hospital.hospital_types.length > 0 &&
                   <Table.Row>
                     <Table.Cell width={4}>施設形態</Table.Cell>
                     <Table.Cell>
                       {hospital.hospital_types.map(type => type.name).join(' ／ ')}
                     </Table.Cell>
                   </Table.Row>
                  }
                  {hospital.number_of_beds_text &&
                   <Table.Row>
                     <Table.Cell width={4}>病床数</Table.Cell>
                     <Table.Cell>
                       {hospital.number_of_beds_text}
                     </Table.Cell>
                   </Table.Row>
                  }
                  {hospital.medical_subjects &&
                   <Table.Row>
                     <Table.Cell width={4}>診療科目</Table.Cell>
                     <Table.Cell>{hospital.medical_subjects}</Table.Cell>
                   </Table.Row>
                  }
                  {hospital.business_time &&
                   <Table.Row>
                     <Table.Cell width={4}>診療時間</Table.Cell>
                     <Table.Cell>{hospital.business_time}</Table.Cell>
                   </Table.Row>
                  }
                  {hospital.holidays &&
                   <Table.Row>
                     <Table.Cell width={4}>休診日</Table.Cell>
                     <Table.Cell>{hospital.holidays}</Table.Cell>
                   </Table.Row>
                  }
                  {hospital.features &&
                   <Table.Row>
                     <Table.Cell width={4}>施設の特徴</Table.Cell>
                     <Table.Cell>{hospital.features}</Table.Cell>
                   </Table.Row>
                  }
                  {hospital.work_location &&
                   <Table.Row>
                     <Table.Cell width={4}>所在地</Table.Cell>
                     <Table.Cell>{hospital.work_location}</Table.Cell>
                   </Table.Row>
                  }
                  {hospital.access &&
                   <Table.Row>
                     <Table.Cell width={4}>交通手段</Table.Cell>
                     <Table.Cell>{hospital.access}</Table.Cell>
                   </Table.Row>
                  }
                </Table.Body>
              </Table>
              {page.content_bottom && <div className='carriee-editor' style={{marginTop: '2rem'}} dangerouslySetInnerHTML={{ __html: page.content_bottom }} /> }

            </Segment>
          </div>

          <Dimmer.Dimmable as='div'
                           dimmed={relatedHospitals == null}
                           style={{ marginTop: '2em',
                           }}>
            {relatedHospitals && (
               <div>
                 <div style={{ backgroundColor: 'white',
                               paddingTop: '1em',
                               paddingLeft: '0.8em',
                               paddingRight: '0.8em',
                 }}>
                   <BoxTitle as='h3' title='関連している求人' />
                 </div>

                 <HospitalListMobile hospitals={relatedHospitals}
                                     history={history}
                                     deletable={false}
                 />
               </div>
            )}
            <Dimmer active={relatedHospitals == null} inverted>
              <Loader inverted />
            </Dimmer>
          </Dimmer.Dimmable>
        </SimpleResponsive>
      </div>
    )
  }
}





class Job extends PureComponent {

  state = {open: window.innerWidth > 991}

  showHide = () => {
    this.setState({
      open: !this.state.open
    })
  }

  render() {

    const { open } = this.state
    const { job, i} = this.props

    let websiteGuideUrl = ''
    if(job.website_id == 1) {
      websiteGuideUrl = '/website-guide/kango-no-oshigoto'
    }
    else if(job.website_id == 2) {
      websiteGuideUrl = '/website-guide/mynavi-kangoshi'
    }
    else if(job.website_id == 3) {
      websiteGuideUrl = '/website-guide/nurse-jinzai-bank'
    }

    return (
      <div style={{marginBottom: '30px'}}>
        <Label color='blue' size='massive' style={{
          width: '100%',
          fontSize: '1.2rem',
          borderBottomRightRadius: 0,
          borderBottomLeftRadius: 0,
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between'
        }}>
          <div style={{flex: 70}}>
            募集<span className='ui text yellow' style={{marginRight: '1rem'}}>{i}</span>
            <SearchKey keys={job.qualifications} />
          </div>
          <Responsive maxWidth={991} style={{cursor: 'pointer', flex: 30, fontSize: '1rem', display: 'flex', justifyContent: 'flex-end'}} onClick={this.showHide}>
            {open &&
             <div style={{textAlign: 'center'}}>
               <Icon name='remove' />
               閉じる
               <Icon name='chevron up'/>
             </div>
            }
            {!open &&
             <div style={{textAlign: 'center'}}>
               詳細を開く
               <Icon name='chevron down'/>
             </div>
            }
          </Responsive>
        </Label>
        <div className='jobDetailCover'>
          <Responsive as={Segment}>
            {open &&
             <Grid divided='vertically'>
               <Grid.Row>
                 <Grid.Column className='qualificationAttr' computer={4} mobile={16} style={{marginTop: '1rem', fontWeight: 'bold'}}>
                   <div>雇用形態</div>
                 </Grid.Column>
                 <Grid.Column computer={12} mobile={16} style={{marginTop: '1rem'}}>
                   {job.contracts.map(contract => contract.name).join('／') || (job.website_id == 4 ? '下記、募集詳細ページより確認して下さい。' : '下記、ご登録後に担当者に確認して下さい。')}
                 </Grid.Column>
               </Grid.Row>
               <Grid.Row>
                 <Grid.Column className='qualificationAttr' computer={4} mobile={16} style={{fontWeight: 'bold'}}>
                   <div>求人内容・配属先</div>
                 </Grid.Column>
                 <Grid.Column computer={12} mobile={16}>
                   {job.job_type_text || (job.website_id == 4 ? '下記、募集詳細ページより確認して下さい。' : '下記、ご登録後に担当者に確認して下さい。')}
                 </Grid.Column>
               </Grid.Row>
               <Grid.Row>
                 <Grid.Column className='qualificationAttr' computer={4} mobile={16} style={{fontWeight: 'bold'}}>
                   <div>給与／年収</div>
                 </Grid.Column>
                 <Grid.Column computer={12} mobile={16}>
                   {job.salary_detail || (job.website_id == 4 ? '下記、募集詳細ページより確認して下さい。' : '下記、ご登録後に担当者に確認して下さい。')}
                 </Grid.Column>
               </Grid.Row>

               <Grid.Row>
                 <Grid.Column className='qualificationAttr' computer={4} mobile={16} style={{fontWeight: 'bold'}}>
                   <div>手当／福利厚生</div>
                 </Grid.Column>
                 <Grid.Column computer={12} mobile={16}>
                   {job.allowance || (job.website_id == 4 ? '下記、募集詳細ページより確認して下さい。' : '下記、ご登録後に担当者に確認して下さい。')}
                 </Grid.Column>
               </Grid.Row>

               <Grid.Row>
                 <Grid.Column className='qualificationAttr' computer={4} mobile={16} style={{fontWeight: 'bold'}}>
                   <div>勤務時間</div>
                 </Grid.Column>
                 <Grid.Column computer={12} mobile={16}>
                   {job.work_time || (job.website_id == 4 ? '下記、募集詳細ページより確認して下さい。' : '下記、ご登録後に担当者に確認して下さい。')}
                 </Grid.Column>
               </Grid.Row>

               <Grid.Row>
                 <Grid.Column width={16}>
                   {job.website_id == 4 && (
                      <p>この求人はハローワークにある求人情報です。</p>
                   )}

                   <Icon name='arrow right' color='grey' />
                   <a href={job.original_url} target='_blank'>募集詳細ページへ</a>
                 </Grid.Column>
               </Grid.Row>

               {job.website_id != 4 && (
                  <Grid.Row className='inquiry'>
                    <Grid.Column computer={16}>
                      <Label basic color='pink' className='inquiryBox'>
                        <div className='leftBox'>
                          <div className='circular'>
                            <div className='intext'>
                              <div>この求人に</div>
                              <div>問い合わせる</div>
                            </div>
                          </div>
                        </div>
                        <div className='rightBox'>
                          <div>この求人は看護師転職サイトの</div>
                          <span>
                            <Link to={websiteGuideUrl} className='ui text pink'>
                              <Icon name='external' color='grey'/>
                              {job.website_name}
                            </Link>
                            に求人があります。さらに詳しい情報は登録後に確認しましょう。
                          </span>
                          <Responsive as={Link} to={websiteGuideUrl} minWidth={992} style={{display: 'block'}}>
                            <Button color='green' style={{height: '60px', fontSize: '1.5rem', width: '300px', marginTop: '0.5rem'}}>詳細を確認する</Button>
                          </Responsive>
                        </div>
                        <Responsive as={Link} to={websiteGuideUrl} maxWidth={991} className='computer'>
                          <Button color='green' style={{height: '60px', fontSize: '1.5rem', width: '100%', marginTop: '0.5rem'}}>詳細を確認する</Button>
                        </Responsive>
                      </Label>
                      <div style={{float: 'right', textAlign: 'right', margin: '0.5rem 1rem'}}>
                        <div>※登録後、担当者に募集状況を確認してください。</div>
                        <div>※求人募集は終了している場合もございます。</div>
                      </div>
                    </Grid.Column>
                  </Grid.Row>
               )}
             </Grid>
            }
            {!open &&
             <Grid divided='vertically'>
               <Grid.Row>
                 <Grid.Column className='qualificationAttr' computer={4} mobile={16} style={{marginTop: '1rem', fontWeight: 'bold'}}>
                   <div>雇用形態</div>
                 </Grid.Column>
                 <Grid.Column computer={12} mobile={16} style={{marginTop: '1rem'}}>
                   {job.contracts.map(contract => contract.name).join('／')}
                 </Grid.Column>
               </Grid.Row>
               <Grid.Row>
                 <Grid.Column className='qualificationAttr' computer={4} mobile={16} style={{fontWeight: 'bold'}}>
                   <div>求人内容・配属先</div>
                 </Grid.Column>
                 <Grid.Column computer={12} mobile={16}>
                   {job.job_type_text || (job.website_id == 4 ? '下記、募集詳細ページより確認して下さい。' : '下記、ご登録後に担当者に確認して下さい。')}
                 </Grid.Column>
               </Grid.Row>
             </Grid>
            }
          </Responsive>
        </div>
      </div>
    )
  }
}


const style = {
  cellHeight: {
    minHeight: '60px',
    height: '60px'
  }
}
