import React, { PureComponent } from 'react'
import PropType from 'prop-types'
import { Link } from 'react-router-dom'
import moment from 'moment'
import {
  Segment,
  Image
} from 'semantic-ui-react'

export default class ArticleListMobile extends PureComponent {

  static propTypes = {
    articles: PropTypes.array.isRequired
  }

  static defaultProps = {
  }

  render() {
    let { articles, hideFirstItem } = this.props
    if (hideFirstItem) {
      articles.splice(0,1)
    }
    return (
      <Segment basic className='categoryPage'>
        {articles.map((article, i) => {
          if (i != 0) {
            const image = article.image
            const category = article.category
            return (
              <div key={article.id} className='article'>
                <Link className='left' to={article.url}>
                  {image ?
                    <Image src={image.image_url.thumb} alt={image.alt}/>
                    :
                    <div className='noImage' style={{
                      background: '#ececec',
                      display: 'block',
                      width: '100%',
                      height: '100%',}}></div>
                  }
                </Link>
                <div className='right'>
                  <div>
                    {moment(article.publish_at).format('YYYY.MM.DD')}
                    <Link className='ui text blue' to={category.url} style={{marginLeft: '10px', fontWeight: 'normal'}}>{category.title}</Link>
                  </div>
                  <Link className='title' to={article.url}>{article.title}</Link>
                </div>
              </div>
            )
          }
        })}
      </Segment>
    )
  }
}
