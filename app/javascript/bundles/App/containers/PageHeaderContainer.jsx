import { connect } from 'react-redux';
import PageHeader from '../components/PageHeader';
import * as actions from '../actions/userActions';


const mapStateToProps = (state) => ({
  user: state.user.user
});

export default connect(mapStateToProps, actions)(PageHeader);
