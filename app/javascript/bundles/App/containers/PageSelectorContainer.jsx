import { connect } from 'react-redux'
import PageSelector from '../components/PageSelector'
import { fetch, fetchPage } from '../actions/apiActions'


const mapStateToProps = (state) => ({
  api: state.api,
  page: state.api.page || null,
  code: state.api.code || null
})

export default connect(mapStateToProps, { fetch, fetchPage })(PageSelector)
