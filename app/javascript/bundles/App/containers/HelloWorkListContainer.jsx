import { connect } from 'react-redux'
import HelloWorkList from '../components/HelloWorkList'
import * as actions from '../actions/helloWorkListActions'
import { fetch, fetchPage } from '../actions/apiActions'
import * as userActions from '../actions/userActions'

const mapStateToProps = (state) => ({
  api: state.api,
  page: state.api.page || {},
  user: state.user,
  helloWorkListActions: state.helloWorkListActions,
})

export default connect(mapStateToProps, Object.assign(userActions, actions, { fetch, fetchPage }))(HelloWorkList)
