import { connect } from 'react-redux'
import ArticlePage from '../components/ArticlePage'
import * as actions from '../actions/articlePageActions'

const mapStateToProps = (state) => ({
  api: state.api
})

export default connect(mapStateToProps, actions)(ArticlePage)
