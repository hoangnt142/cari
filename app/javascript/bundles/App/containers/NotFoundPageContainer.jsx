import { connect } from 'react-redux'
import NotFoundPage from '../components/NotFoundPage'
import * as actions from '../actions/notFoundPageActions'
import { initialize as dataInitialize} from '../actions/topActionCreators'

const mapStateToProps = (state) => ({
  api: state.api,
  page: state.api.page || {},
  regions: state.top.regions,
  widgets: state.top.widgets
})

export default connect(mapStateToProps, Object.assign(actions, { dataInitialize }))(NotFoundPage)
