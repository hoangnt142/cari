import { connect } from 'react-redux'
import PasswordGenerate from '../components/PasswordGenerate'
import * as actions from '../actions/passwordGenerateActions'
import { fetch, fetchPage } from '../actions/apiActions'
import * as userActions from '../actions/userActions'

const mapStateToProps = (state) => ({
  api: state.api,
  page: state.api.page || {},
  user: state.user,
  passwordGenerateActions: state.passwordGenerateActions,
})

export default connect(mapStateToProps, Object.assign(userActions, actions, { fetch, fetchPage }))(PasswordGenerate)
