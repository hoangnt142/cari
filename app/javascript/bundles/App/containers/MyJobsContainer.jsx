import { connect } from 'react-redux'
import MyJobs from '../components/MyJobs'
import * as actions from '../actions/myJobsActions'
import { fetch, fetchPage } from '../actions/apiActions'

const mapStateToProps = (state) => ({
  api: state.api,
  page: state.api.page || {},
  hospitals: state.myJobs.hospitals
})

export default connect(mapStateToProps, Object.assign(actions, { fetch, fetchPage }))(MyJobs)
