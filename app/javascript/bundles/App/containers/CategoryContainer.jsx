import { connect } from 'react-redux';
import Category from '../components/Category';
import * as actions from '../actions/categoryActionCreators';

const mapStateToProps = (state) => ({
  api: state.api
});

export default connect(mapStateToProps, actions)(Category);
