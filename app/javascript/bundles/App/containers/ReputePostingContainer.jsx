import { connect } from 'react-redux'
import ReputePosting from '../components/ReputePosting'
import * as actions from '../actions/reputePostingActions'
import { fetch, fetchPage } from '../actions/apiActions'
import * as userActions from '../actions/userActions'

const mapStateToProps = (state) => ({
  api: state.api,
  page: state.api.page || {},
  user: state.user,
  reputePostingActions: state.reputePostingActions,
})

export default connect(mapStateToProps, Object.assign(userActions, actions, { fetch, fetchPage }))(ReputePosting)
