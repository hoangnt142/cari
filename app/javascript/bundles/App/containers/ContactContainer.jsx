import { connect } from 'react-redux'
import Contact from '../components/Contact'
import * as actions from '../actions/contactActions'
import { fetch, fetchPage } from '../actions/apiActions'
import * as userActions from '../actions/userActions'

const mapStateToProps = (state) => ({
  api: state.api,
  page: state.api.page || {},
  user: state.user,
  contactActions: state.contactActions,
})

export default connect(mapStateToProps, Object.assign(userActions, actions, { fetch, fetchPage }))(Contact)
