import { connect } from 'react-redux'
import MyHistory from '../components/MyHistory'
import * as actions from '../actions/myHistoryActions'
import { fetch, fetchPage } from '../actions/apiActions'

const mapStateToProps = (state) => ({
  api: state.api,
  page: state.api.page || {},
  hospitals: state.myHistory.hospitals
})

export default connect(mapStateToProps, Object.assign(actions, { fetch, fetchPage }))(MyHistory)
