import { connect } from 'react-redux'
import Reputes from '../components/Reputes'
import * as actions from '../actions/reputesActions'
import { fetch, fetchPage } from '../actions/apiActions'
import * as userActions from '../actions/userActions'

const mapStateToProps = (state) => ({
  api: state.api,
  page: state.api.page || {},
  user: state.user,
  reputesActions: state.reputesActions,
})

export default connect(mapStateToProps, Object.assign(userActions, actions, { fetch, fetchPage }))(Reputes)
