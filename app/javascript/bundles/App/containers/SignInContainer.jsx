import { connect } from 'react-redux'
import SignIn from '../components/SignIn'
import { fetch, fetchPage } from '../actions/apiActions'
import * as userActions from '../actions/userActions'
import * as actions from '../actions/signInActions'

const mapStateToProps = (state) => ({
  page: state.api.page || {},
  user: state.user,
  signInPage: state.signInPage
})

export default connect(mapStateToProps, Object.assign(userActions, actions, { fetch, fetchPage }))(SignIn)
