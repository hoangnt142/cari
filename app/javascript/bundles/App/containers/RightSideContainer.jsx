import { connect } from 'react-redux'
import RightSide from '../components/RightSide'
import * as actions from '../actions/rightSideActions'

const mapStateToProps = (state) => ({
})

export default connect(mapStateToProps, actions)(RightSide)
