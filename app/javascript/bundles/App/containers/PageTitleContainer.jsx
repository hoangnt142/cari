import { connect } from 'react-redux';
import PageTitle from '../components/PageTitle';

import * as actions from '../actions/searchActions';


const mapStateToProps = (state) => ({

});

export default connect(mapStateToProps, actions)(PageTitle);
