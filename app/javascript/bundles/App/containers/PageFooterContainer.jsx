import { connect } from 'react-redux'
import PageFooter from '../components/PageFooter'
//import * as actions from '../actions/userActions';
import * as actions from '../actions/pageFooterActions'
import { openSearchModal } from '../actions/searchModalActions'

const mapStateToProps = (state) => ({
  user: state.user.user,

  qualifications: state.pageFooter.qualifications,
  job_contracts: state.pageFooter.job_contracts,
  hospital_types: state.pageFooter.hospital_types,
  job_types: state.pageFooter.job_types,
  job_details: state.pageFooter.job_details,
  prefectures: state.pageFooter.prefectures,
  regions: state.pageFooter.regions,
});

export default connect(mapStateToProps, Object.assign(actions, { openSearchModal }))(PageFooter)
