import { connect } from 'react-redux'
import SignUpSlide2 from '../components/SignUpSlide2'
import { fetch } from '../actions/apiActions'
import * as actions from '../actions/signUpActions'

const mapStateToProps = (state) => {
  const {
    gender,
    qualification1,
    qualification2,
    qualification3,
    qualification4,
    qualification5
  } = state.signUpPage

  return {
//    api: state.api,
    gender,
    qualification1,
    qualification2,
    qualification3,
    qualification4,
    qualification5
    //  signUpPage: state.signUpPage,
  }
}

export default connect(mapStateToProps, Object.assign(actions, { fetch }))(SignUpSlide2)
