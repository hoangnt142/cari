import { connect } from 'react-redux'
import PasswordChange from '../components/PasswordChange'
import * as actions from '../actions/passwordChangeActions'
import { fetch, fetchPage } from '../actions/apiActions'
import * as userActions from '../actions/userActions'

const mapStateToProps = (state) => ({
  api: state.api,
  page: state.api.page || {},
  user: state.user,
  passwordChangeActions: state.passwordChangeActions,
})

export default connect(mapStateToProps, Object.assign(userActions, actions, { fetch, fetchPage }))(PasswordChange)
