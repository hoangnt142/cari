import { connect } from 'react-redux'
import Search from '../components/Search'
import * as actions from '../actions/searchModalActions'

const mapStateToProps = (state) => ({
  page: state.api.page || {},
  pageable: state.api.pageable,
  user: state.user,

  condition: state.searchModal.condition,
  freeTextTitle: state.searchModal.freeTextTitle,

  hospitals: state.searchModal.hospitals,
  fetching: state.searchModal.fetching,
  totalPages: state.searchModal.totalPages,
  currentPage: state.searchModal.currentPage,
  totalCount: state.searchModal.totalCount,
  lastDate: state.searchModal.lastDate,
  paginationRange: state.searchModal.paginationRange,
})

export default connect(mapStateToProps, actions)(Search)
