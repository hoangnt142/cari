import { connect } from 'react-redux'
import SignUpSlide3 from '../components/SignUpSlide3'
import { fetch } from '../actions/apiActions'
import * as actions from '../actions/signUpActions'

const mapStateToProps = (state) => {
  const {
    nickname,
    birthYear,
    birthMonth,
    birthDay,
    birth,
    prefectureId,
    cityId
  } = state.signUpPage

  const {
    prefectures,
    cities,
    fetchingURLs,
  } = state.api

  let cityIsLoading = false
  fetchingURLs.forEach(u => {
    if(u.match(/cities/)) {
      cityIsLoading = true
    }
  })


  return {
    prefectures,
    cities,
    cityIsLoading,

    nickname,
    birthYear,
    birthMonth,
    birthDay,
    birth,
    prefectureId,
    cityId
  }
}

export default connect(mapStateToProps, Object.assign(actions, { fetch }))(SignUpSlide3)
