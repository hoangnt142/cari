import { connect } from 'react-redux'
import Agents from '../components/Agents'
import * as actions from '../actions/agentsActions'
import { fetch, fetchPage } from '../actions/apiActions'
import * as userActions from '../actions/userActions'

const mapStateToProps = (state) => ({
  api: state.api,
  page: state.api.page || {},
  user: state.user,
  agentsActions: state.agentsActions,
})

export default connect(mapStateToProps, Object.assign(userActions, actions, { fetch, fetchPage }))(Agents)
