import { connect } from 'react-redux'
import { ConditionMobile, ConditionPc } from '../../components/search/Condition'
import * as actions from '../../actions/searchModalActions'

const mapStateToProps = (state) => ({

  regions: state.searchModal.regions,
  prefectures: state.searchModal.prefectures,
  area1s: state.searchModal.area1s,
  area2s: state.searchModal.area2s,
  area3s: state.searchModal.area3s,

  qualifications: state.searchModal.qualifications,
  jobContracts: state.searchModal.jobContracts,
  hospitalTypes: state.searchModal.hospitalTypes,
  jobTypes: state.searchModal.jobTypes,
  jobDetails: state.searchModal.jobDetails,
  condition: state.searchModal.condition
})

const ConditionMobileContainer = connect(mapStateToProps, actions)(ConditionMobile)
const ConditionPcContainer = connect(mapStateToProps, actions)(ConditionPc)

export {
  ConditionMobileContainer as ConditionMobile,
  ConditionPcContainer as ConditionPc
}
