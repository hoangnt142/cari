import { connect } from 'react-redux'
import { AreaMobile, AreaPc } from '../../components/search/Area'
import * as actions from '../../actions/searchModalActions'

const mapStateToProps = (state) => ({
  prefectures: state.searchModal.prefectures,
  regions: state.searchModal.regions,

  area1s: state.searchModal.area1s,
  area2s: state.searchModal.area2s,
  area3s: state.searchModal.area3s,

  regionId: state.searchModal.condition.regionId,
  prefectureId: state.searchModal.condition.prefectureId,
  areaIds: state.searchModal.condition.areaIds,
  /*
     area1Ids: state.searchByArea.condition.area1Ids,
     area2Ids: state.searchByArea.condition.area2Ids,
     area3Ids: state.searchByArea.condition.area3Ids,
   */
  //  areaWithAncestors: state.searchModal.areaWithAncestors,
})

const AreaMobileContainer = connect(mapStateToProps, actions)(AreaMobile)
const AreaPcContainer = connect(mapStateToProps, actions)(AreaPc)

export {
  AreaMobileContainer as AreaMobile,
  AreaPcContainer as AreaPc
}
