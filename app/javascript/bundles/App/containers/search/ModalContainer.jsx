import { connect } from 'react-redux'
import Modal from '../../components/search/Modal'
import * as actions from '../../actions/searchModalActions'

const mapStateToProps = (state) => ({
  isOpen: state.searchModal.isOpen,
  fetching: state.searchModal.fetching,
  totalCount: state.searchModal.totalCount,
  searchType: state.searchModal.searchType,
  childModal: state.searchModal.childModal,
})

export default connect(mapStateToProps, actions)(Modal)
