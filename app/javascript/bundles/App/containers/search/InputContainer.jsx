import { connect } from 'react-redux'
import Input from '../../components/search/Input'
import * as actions from '../../actions/searchModalActions'

const mapStateToProps = (state) => ({
  text: state.searchModal.condition.text,
})

export default connect(mapStateToProps, actions)(Input)
