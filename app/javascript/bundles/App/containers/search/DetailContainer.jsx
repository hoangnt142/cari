import { connect } from 'react-redux'
import { DetailMobile, DetailPc } from '../../components/search/Detail'
import * as actions from '../../actions/searchModalActions'

const mapStateToProps = (state) => ({
  prefectures: state.searchModal.prefectures,
  area1s: state.searchModal.area1s,
  area2s: state.searchModal.area2s,
  area3s: state.searchModal.area3s,


  qualifications: state.searchModal.qualifications,
  jobContracts: state.searchModal.jobContracts,
  hospitalTypes: state.searchModal.hospitalTypes,
  jobTypes: state.searchModal.jobTypes,
  jobDetails: state.searchModal.jobDetails,

  hospitals: state.searchModal.hospitals,
  totalPages: state.searchModal.totalPages,
  currentPage: state.searchModal.currentPage,
  totalCount: state.searchModal.totalCount,
  lastDate: state.searchModal.lastDate,
  paginationRange: state.searchModal.paginationRange,

  condition: state.searchModal.condition,

})


const DetailMobileContainer = connect(mapStateToProps, actions)(DetailMobile)
const DetailPcContainer = connect(mapStateToProps, actions)(DetailPc)
export {
  DetailMobileContainer as DetailMobile,
  DetailPcContainer as DetailPc
}
