import { connect } from 'react-redux'
import MypageMyReputes from '../components/MypageMyReputes'
import * as actions from '../actions/mypageMyReputesActions'
import { fetch, fetchPage } from '../actions/apiActions'
import * as userActions from '../actions/userActions'

const mapStateToProps = (state) => ({
  api: state.api,
  page: state.api.page || {},
  user: state.user,
  mypageMyReputesActions: state.mypageMyReputesActions,
})

export default connect(mapStateToProps, Object.assign(userActions, actions, { fetch, fetchPage }))(MypageMyReputes)
