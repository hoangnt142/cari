import { connect } from 'react-redux'
import MypageSettings from '../components/MypageSettings'
import * as actions from '../actions/mypageSettingsActions'
import { fetch, fetchPage } from '../actions/apiActions'
import * as userActions from '../actions/userActions'

const mapStateToProps = (state) => ({
  api: state.api,
  page: state.api.page || {},
  user: state.user,
  mypageSettingsActions: state.mypageSettingsActions,
})

export default connect(mapStateToProps, Object.assign(userActions, actions, { fetch, fetchPage }))(MypageSettings)
