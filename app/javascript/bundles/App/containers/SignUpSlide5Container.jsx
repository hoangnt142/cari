import { connect } from 'react-redux'
import SignUpSlide5 from '../components/SignUpSlide5'
import { fetch } from '../actions/apiActions'
import * as userActions from '../actions/userActions'
import * as actions from '../actions/signUpActions'

const mapStateToProps = (state) => {
  const {
    nickname,
    email,
    password,
    gender,
    prefectureName,
    cityName,
    favoritePrefectureName1,
    favoriteCityName1,
    favoritePrefectureName2,
    favoriteCityName2,
    qualification1,
    qualification2,
    qualification3,
    qualification4,
    qualification5,
    birth,
  } = state.signUpPage

  const {
    signing,
    signedUp,
  } = state.user

  return {
    nickname,
    email,
    password,
    gender,
    prefectureName,
    cityName,
    favoritePrefectureName1,
    favoriteCityName1,
    favoritePrefectureName2,
    favoriteCityName2,
    qualification1,
    qualification2,
    qualification3,
    qualification4,
    qualification5,
    birth,
    signing,
    signedUp,
  }
}

export default connect(mapStateToProps, Object.assign(userActions, actions, { fetch }))(SignUpSlide5)
