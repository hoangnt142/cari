import { connect } from 'react-redux'
import ReputePosted from '../components/ReputePosted'
import * as actions from '../actions/reputePostedActions'
import { fetch, fetchPage } from '../actions/apiActions'
import * as userActions from '../actions/userActions'

const mapStateToProps = (state) => ({
  api: state.api,
  page: state.api.page || {},
  user: state.user,
  reputePostedActions: state.reputePostedActions,
})

export default connect(mapStateToProps, Object.assign(userActions, actions, { fetch, fetchPage }))(ReputePosted)
