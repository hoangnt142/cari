import { connect } from 'react-redux';
import Top from '../components/Top';
import * as actions from '../actions/topActionCreators';
import { fetch, fetchPage } from '../actions/apiActions';
import { openSearchModal } from '../actions/searchModalActions'

const mapStateToProps = (state) => ({
  top: state.top,
  api: state.api,
  page: state.api.page || {},

  text: state.searchModal.condition.text,
});

export default connect(mapStateToProps, Object.assign(actions,
                                                      { fetch,
                                                        fetchPage,
                                                        openSearchModal }))(Top);
