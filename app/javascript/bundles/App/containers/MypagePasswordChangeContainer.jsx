import { connect } from 'react-redux'
import MypagePasswordChange from '../components/MypagePasswordChange'
import * as actions from '../actions/mypagePasswordChangeActions'
import { fetch, fetchPage } from '../actions/apiActions'
import * as userActions from '../actions/userActions'

const mapStateToProps = (state) => ({
  api: state.api,
  page: state.api.page || {},
  user: state.user,
  mypagePasswordChangeActions: state.mypagePasswordChangeActions,
})

export default connect(mapStateToProps, Object.assign(userActions, actions, { fetch, fetchPage }))(MypagePasswordChange)
