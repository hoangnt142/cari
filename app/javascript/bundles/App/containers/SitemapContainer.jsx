import { connect } from 'react-redux'
import Sitemap from '../components/Sitemap'
import * as actions from '../actions/sitemapActions'
import { fetch, fetchPage } from '../actions/apiActions'
import * as userActions from '../actions/userActions'

const mapStateToProps = (state) => ({
  api: state.api,
  page: state.api.page || {},
  user: state.user,
  sitemapActions: state.sitemapActions,
})

export default connect(mapStateToProps, Object.assign(userActions, actions, { fetch, fetchPage }))(Sitemap)
