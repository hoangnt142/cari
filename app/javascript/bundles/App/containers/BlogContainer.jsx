import { connect } from 'react-redux';
import Blog from '../components/Blog';
import * as actions from '../actions/blogActionCreators';
import { fetchPage } from '../actions/apiActions';


const mapStateToProps = (state) => ({
  top: state.top,
  api: state.api
});

export default connect(mapStateToProps, Object.assign(actions, { fetchPage: fetchPage }))(Blog);
