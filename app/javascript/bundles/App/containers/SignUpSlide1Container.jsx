import { connect } from 'react-redux'
import SignUpSlide1 from '../components/SignUpSlide1'
import { fetch } from '../actions/apiActions'
import * as actions from '../actions/signUpActions'

const mapStateToProps = (state) => {
  const {
    email,
    password
  } = state.signUpPage

  const {
    emailExisted
  } = state.api

  return {
//    api: state.api,
    email,
    password,
    emailExisted
  }
}

export default connect(mapStateToProps, Object.assign(actions, { fetch }))(SignUpSlide1)
