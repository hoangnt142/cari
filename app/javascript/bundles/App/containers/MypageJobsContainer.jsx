import { connect } from 'react-redux'
import MypageJobs from '../components/MypageJobs'
import * as actions from '../actions/mypageJobsActions'
import { fetch, fetchPage } from '../actions/apiActions'
import * as userActions from '../actions/userActions'

const mapStateToProps = (state) => ({
  api: state.api,
  page: state.api.page || {},
  user: state.user,
  mypageJobsActions: state.mypageJobsActions,
})

export default connect(mapStateToProps, Object.assign(userActions, actions, { fetch, fetchPage }))(MypageJobs)
