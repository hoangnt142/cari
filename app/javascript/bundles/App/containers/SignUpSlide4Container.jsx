import { connect } from 'react-redux'
import SignUpSlide4 from '../components/SignUpSlide4'
import { fetch } from '../actions/apiActions'
import * as actions from '../actions/signUpActions'

const mapStateToProps = (state) => {

  const {
    favoritePrefectureId1,
    favoriteCityId1,
    favoritePrefectureId2,
    favoriteCityId2
  } = state.signUpPage

  const {
    prefectures,
    city1,
    city2,
    fetchingNames,
  } = state.api

  let city1IsLoading = false
  let city2IsLoading = false
  fetchingNames.forEach(u => {
    if(u.match(/city1/)) {
      city1IsLoading = true
    }
    else if(u.match(/city2/)) {
      city2IsLoading = true
    }
  })


  return {
    prefectures,
    city1,
    city2,
    city1IsLoading,
    city2IsLoading,

    favoritePrefectureId1,
    favoriteCityId1,
    favoritePrefectureId2,
    favoriteCityId2
  }
}

export default connect(mapStateToProps, Object.assign(actions, { fetch }))(SignUpSlide4)
