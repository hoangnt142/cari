import { connect } from 'react-redux'
import Mypage from '../components/Mypage'
import * as actions from '../actions/mypageActions'
import { fetch, fetchPage } from '../actions/apiActions'
import * as userActions from '../actions/userActions'

const mapStateToProps = (state) => ({
  api: state.api,
  page: state.api.page || {},
  user: state.user.user,
  mypageActions: state.mypageActions,
})

export default connect(mapStateToProps, Object.assign(userActions, actions, { fetch, fetchPage }))(Mypage)
