import { connect } from 'react-redux'
import ContactSent from '../components/ContactSent'
import * as actions from '../actions/contactSentActions'
import { fetch, fetchPage } from '../actions/apiActions'
import * as userActions from '../actions/userActions'

const mapStateToProps = (state) => ({
  api: state.api,
  page: state.api.page || {},
  user: state.user,
  contactSentActions: state.contactSentActions,
})

export default connect(mapStateToProps, Object.assign(userActions, actions, { fetch, fetchPage }))(ContactSent)
