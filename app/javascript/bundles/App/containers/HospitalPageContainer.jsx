import { connect } from 'react-redux'
import HospitalPage from '../components/HospitalPage'
import * as actions from '../actions/hospitalPageActions'
import { fetch, fetchPage } from '../actions/apiActions'
import * as userActions from '../actions/userActions'

const mapStateToProps = (state) => ({
  api: state.api,
  page: state.api.page || {},
  user: state.user,
  hospitalPageActions: state.hospitalPageActions,
  relatedHospitals: state.hospitalPage.relatedHospitals
})

export default connect(mapStateToProps, Object.assign({}, actions, userActions, { fetch, fetchPage }))(HospitalPage)
