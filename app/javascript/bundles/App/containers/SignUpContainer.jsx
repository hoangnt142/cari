import { connect } from 'react-redux'
import SignUp from '../components/SignUp'
import { fetch, fetchPage } from '../actions/apiActions'
import * as userActions from '../actions/userActions'
import * as actions from '../actions/signUpActions'

const mapStateToProps = (state) => ({
  api: state.api,
  page: state.api.page || {},
  user: state.user,
  signUpPage: state.signUpPage,
})

export default connect(mapStateToProps, Object.assign(userActions, actions, { fetch, fetchPage }))(SignUp)
