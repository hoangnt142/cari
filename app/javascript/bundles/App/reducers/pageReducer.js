
import { PAGE_RESET } from '../constants/constants'

const initialState = {}

const pageReducer = (state = initialState, action) => {
  switch (action.type) {
    case PAGE_RESET:
      return initialState
    default:
      return state
  }
}

export default pageReducer
