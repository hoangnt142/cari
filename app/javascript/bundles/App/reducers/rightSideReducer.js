import { RIGHT_SIDE_INITIALIZE } from '../constants/constants';

const initialState = {
  widgets: []
};

const rightSideReducer = (state = initialState, action) => {
  switch (action.type) {
    case RIGHT_SIDE_INITIALIZE:
      return Object.assign({}, state, { widgets: action.widgets });
    default:
      return state;
  }
};

export default rightSideReducer;
