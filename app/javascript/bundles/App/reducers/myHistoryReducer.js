import {
  MY_HISTORY_GET_HOSPITALS
} from '../constants/constants'

const initialState = {
	hospitals: []
}

const myHistoryReducer = (state = initialState, action) => {
  switch (action.type) {
    case MY_HISTORY_GET_HOSPITALS:
      return Object.assign({}, initialState, {hospitals: action.hospitals})
/*
    case MY_HISTORY_RESET:
      return Object.assign({}, initialState)
*/
    default:
      return state
  }
}

export default myHistoryReducer
