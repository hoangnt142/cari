import { MY_JOBS_RESET, MY_JOB_GET_HOSPITALS } from '../constants/constants'

const initialState = {
  hospitals: []
}

const myJobsReducer = (state = initialState, action) => {
  switch (action.type) {
    case MY_JOB_GET_HOSPITALS:
      return Object.assign({}, initialState, {hospitals: action.hospitals})
    default:
      return state
  }
}

export default myJobsReducer
