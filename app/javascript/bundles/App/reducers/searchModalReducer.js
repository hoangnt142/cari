import {
  SEARCH_MODAL_OPEN,
  SEARCH_MODAL_CLOSE,
  SEARCH_MODAL_CHANGE_MODAL,
  SEARCH_MODAL_OPEN_CHILD_MODAL,
  SEARCH_MODAL_CLOSE_CHILD_MODAL,

  SEARCH_MODAL_INIT,
  SEARCH_MODAL_RESET,
  SEARCH_MODAL_ON_CHANGE_CONDITION,
  SEARCH_MODAL_ON_CHANGE_ALL_CONDITION,
  SEARCH_MODAL_DELETE_CONDITION,
  SEARCH_MODAL_BEFORE_SEARCHING,
  SEARCH_MODAL_SEARCHED,
  SEARCH_MODAL_SEARCHING_FAILED,
  SEARCH_MODAL_ON_CHANGE_PAGE,
  SEARCH_MODAL_ON_CHANGE_TEXT,

  SEARCH_MODAL_ON_CHANGE_REGION,
  SEARCH_MODAL_ON_CHANGE_PREFECTURE,
  SEARCH_MODAL_ON_CHANGE_AREA,
  SEARCH_MODAL_AREA1_IN_TOKYO,
  SEARCH_MODAL_REMOVE_CONDITION_EXCEPT_TEXT,
  SEARCH_MODAL_SAVE_TO_BACK_STATE,
  SEARCH_MODAL_ON_CLICK_SEARCH,
} from '../constants/constants';
import { updateCheck } from '../lib/semantic'


const initialState = {
  isOpen: false,
  fetching: false,
  searchType: null,
  childModal: null,

  regions: [],
  prefectures: [],
  qualifications: [],
  jobContracts: [],
  hospitalTypes: [],
  jobTypes: [],
  jobDetails: [],

  area1s: [],
  area2s: [],
  area3s: [],

  hospitals: [],
  totalPages: null,
  currentPage: 1,
  totalCount: null,
  lastDate: null,
  paginationRange: null,
  freeTextTitle: '',

  condition: {

    regionId: null,
    prefectureId: null,
    areaIds: [],

    qualificationIds: [],
    jobContractIds: [],
    hospitalTypeIds: [],
    jobTypeIds: [],
    jobDetailIds: [],
    text: '',

    page: 1
  },

  backState: null, // this field keeps all other fields of this state
}

const apiReducer = (state = initialState, action) => {
  let condition;
  switch (action.type) {
    case SEARCH_MODAL_OPEN:
      return Object.assign({}, state, { isOpen: true, searchType: action.searchType })
    case SEARCH_MODAL_CLOSE:
      if(state.backState) {
        return Object.assign(state.backState)
      }
      else {
        return Object.assign({}, state, { isOpen: false })
      }

    case SEARCH_MODAL_CHANGE_MODAL:
      return Object.assign({}, state, { searchType: action.searchType })

    case SEARCH_MODAL_OPEN_CHILD_MODAL:
      return Object.assign({}, state, { childModal: action.searchType })
    case SEARCH_MODAL_CLOSE_CHILD_MODAL:
      return Object.assign({}, state, { childModal: null })

    case SEARCH_MODAL_INIT:
      return Object.assign({}, state, {
        condition: action.condition,
        regions: action.regions,
        prefectures: action.prefectures,
        areas: action.areas,
        qualifications: action.qualifications,
        jobContracts: action.jobContracts,
        hospitalTypes: action.hospitalTypes,
        jobTypes: action.jobTypes,
        jobDetails: action.jobDetails,
        hospitals: action.hospitals
      })

    case SEARCH_MODAL_RESET:
      return Object.assign({}, state, { condition: initialState.condition })
    case SEARCH_MODAL_ON_CHANGE_CONDITION:
      condition = Object.assign({}, state.condition,
                                {
                                  [action.field]: updateCheck(action.data, state.condition[action.field]),
                                  page: 1,
                                  text: ''
                                })
      return Object.assign({}, state, { condition: condition })
    case SEARCH_MODAL_ON_CHANGE_ALL_CONDITION:
      condition = Object.assign({}, initialState.condition, action.condition)
      return Object.assign({}, state, { condition })
    case SEARCH_MODAL_DELETE_CONDITION:
      condition = Object.assign({}, state.condition,
                                {
                                  page: 1
                                })
      delete condition[action.field]
      return Object.assign({}, state, { condition: condition })
    case SEARCH_MODAL_BEFORE_SEARCHING:
      return Object.assign({}, state, { fetching: true })
    case SEARCH_MODAL_SEARCHED:
      return Object.assign({}, state, { fetching: false,
                                        freeTextTitle: action.freeTextTitle,
                                        hospitals: action.hospitals,
                                        totalPages: action.totalPages,
                                        currentPage: action.currentPage,
                                        totalCount: action.totalCount,
                                        lastDate: action.lastDate,
                                        paginationRange: action.paginationRange
      })
    case SEARCH_MODAL_SEARCHING_FAILED:
      return Object.assign({}, state, { fetching: false })
    case SEARCH_MODAL_ON_CHANGE_PAGE:
      condition = Object.assign({}, state.condition,
                                {
                                  page: action.page
                                })
      return Object.assign({}, state, { condition: condition })



    case SEARCH_MODAL_ON_CHANGE_REGION:
      condition = Object.assign({}, state.condition,
                                {
                                  regionId: action.regionId,
                                  prefectureId: null,
                                  areaIds: [],
                                  text: '',
                                })
      return Object.assign({}, state, { condition: condition })
    case SEARCH_MODAL_ON_CHANGE_PREFECTURE:
      condition = Object.assign({}, state.condition,
                                {
                                  prefectureId: action.prefectureId,
                                  areaIds: [],
                                  text: '',
                                })
      return Object.assign({}, state, {
        condition: condition,
        area1s: action.area1s,
        area2s: action.area2s,
        area3s: action.area3s,
      })
    case SEARCH_MODAL_ON_CHANGE_AREA:
      condition = Object.assign({}, state.condition,
                                {
                                  areaIds: action.areaIds,
                                  text: '',
                                })
      return Object.assign({}, state, {
        condition: condition,
      })
    case SEARCH_MODAL_AREA1_IN_TOKYO:
      condition = Object.assign({}, state.condition,
                                {
                                  areaIds: [],
                                })
      return Object.assign({}, state, {
        condition: condition,
      })
    case SEARCH_MODAL_ON_CHANGE_TEXT:
      condition = Object.assign({}, state.condition,
                                {
                                  text: action.text,
                                })
      return Object.assign({}, state, {
        condition: condition,
      })
    case SEARCH_MODAL_ON_CLICK_SEARCH:
      return Object.assign({}, state, { isOpen: false, backState: null })
    case SEARCH_MODAL_REMOVE_CONDITION_EXCEPT_TEXT:
      condition = { page: state.condition.page,
                    text: state.condition.text }
      return Object.assign({}, state, {
        condition: condition,
      })
    case SEARCH_MODAL_SAVE_TO_BACK_STATE:
      return Object.assign({}, initialState, {
        regions: state.regions,
        prefectures: state.prefectures,
        qualifications: state.qualifications,
        jobContracts: state.jobContracts,
        hospitalTypes: state.hospitalTypes,
        jobTypes: state.jobTypes,
        jobDetails: state.jobDetails,

        // if it already has backState, don't update the backState anymore
        // backState should keep only the first time state.
        backState: state.backState || state

      })
    default:
      return state
  }
}


export default apiReducer
