import {
  PAGE_FOOTER_SEARCH_KEYS_FETCHING,
  PAGE_FOOTER_SEARCH_KEYS_FETCHED,
  PAGE_FOOTER_SEARCH_KEYS_SET,
  PAGE_FOOTER_SEARCH_KEYS_FETCH_FAILED,

  PAGE_FOOTER_PREFECTURES_FETCHING,
  PAGE_FOOTER_PREFECTURES_FETCHED,
  PAGE_FOOTER_PREFECTURES_SET,
  PAGE_FOOTER_PREFECTURES_FETCH_FAILED
} from '../constants/constants'

const initialState = {
  searchKeyFetching: false,
  prefecturesFetching: false,
  qualifications: null,
  job_contracts: null,
  hospital_types: null,
  job_types: null,
  job_details: null,
}

export default (state = initialState, action) => {
  switch (action.type) {
    case PAGE_FOOTER_SEARCH_KEYS_FETCHING:
      return Object.assign({}, state, { searchKeyFetching: true })
    case PAGE_FOOTER_SEARCH_KEYS_FETCHED:
      return Object.assign({}, state, { searchKeyFetching: false })
    case PAGE_FOOTER_SEARCH_KEYS_SET:
      return Object.assign({}, state, {
        qualifications: action.qualifications,
        job_contracts: action.job_contracts,
        hospital_types: action.hospital_types,
        job_types: action.job_types,
        job_details: action.job_details
      })
    case PAGE_FOOTER_SEARCH_KEYS_FETCH_FAILED:
      return Object.assign({}, state, {
      })
    case PAGE_FOOTER_PREFECTURES_FETCHING:
      return Object.assign({}, state, { prefecturesFetching: true })
    case PAGE_FOOTER_PREFECTURES_FETCHED:
      return Object.assign({}, state, { prefecturesFetching: false })
    case PAGE_FOOTER_PREFECTURES_SET:
      return Object.assign({}, state, {
        prefectures: action.prefectures,
        regions: action.regions
      })
    case PAGE_FOOTER_PREFECTURES_FETCH_FAILED:
      return Object.assign({}, state, {
      })
    default:
      return state
  }
}
