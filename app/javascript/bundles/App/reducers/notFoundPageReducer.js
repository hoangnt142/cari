import { NOT_FOUND_PAGE_RESET } from '../constants/constants'

const initialState = {}

const notFoundPageReducer = (state = initialState, action) => {
  switch (action.type) {
    case NOT_FOUND_PAGE_RESET:
      return initialState
    default:
      return state
  }
}

export default notFoundPageReducer
