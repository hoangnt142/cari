
import { ARTICLE_PAGE_RESET } from '../constants/constants'

const initialState = {}

const articlePageReducer = (state = initialState, action) => {
  switch (action.type) {
    case ARTICLE_PAGE_RESET:
      return initialState
    default:
      return state
  }
}

export default articlePageReducer
