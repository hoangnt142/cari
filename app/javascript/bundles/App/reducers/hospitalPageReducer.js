import {
  HOSPITAL_PAGE_RELATED_JOBS_SEARCHING,
  HOSPITAL_PAGE_RELATED_JOBS_SET,
  HOSPITAL_PAGE_RELATED_JOBS_FAILED,
  HOSPITAL_PAGE_RESET
} from '../constants/constants'

const initialState = {
  searchingRelatedHospitals: false,
}

const hospitalPageReducer = (state = initialState, action) => {
  switch (action.type) {

    case HOSPITAL_PAGE_RELATED_JOBS_SEARCHING:
      return Object.assign({}, state, { searchingRelatedHospitals: true })
    case HOSPITAL_PAGE_RELATED_JOBS_SET:
      return Object.assign({}, state, { searchingRelatedHospitals: false,
                                        relatedHospitals: action.relatedHospitals
      })
    case HOSPITAL_PAGE_RELATED_JOBS_FAILED:
      Object.assign({}, state, { searchingRelatedHospitals: false })
    case HOSPITAL_PAGE_RESET:
      return Object.assign({}, initialState)

    default:
      return state
  }
}

export default hospitalPageReducer
