import { SIGN_IN_RESET, SIGN_IN_ON_CHANGE, SIGN_IN_CLICKED } from '../constants/constants'

const initialState = {
  signInClicked: false,
  email: '',
  password: ''
}

const signInReducer = (state = initialState, action) => {
  switch (action.type) {
    case SIGN_IN_RESET:
      return initialState
    case SIGN_IN_ON_CHANGE:
      return Object.assign({}, state, { [action.field]: action.value })
    case SIGN_IN_CLICKED:
      return Object.assign({}, state, { signInClicked: true })
    default:
      return state
  }
}

export default signInReducer
