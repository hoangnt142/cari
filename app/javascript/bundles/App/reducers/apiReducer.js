import {
  API_BEGIN,
  API_RECEIVED,
  API_FAILED,
  UPDATE_PAGE,
  API_RESET_AREAS,
  RESET_SEARCH_RESULT,
  API_RESET_ARTICLE,
  API_RESET_CATEGORY
} from '../constants/constants';

const initialState = {
  isFetching: false,
  fetchingURLs: [],
  fetchingNames: [],
  code: null,
  totalPages: 0,
  currentPage: 0
};

const apiReducer = (state = initialState, action) => {

  let urls, isFetching, i, names;
  switch (action.type) {
    case API_BEGIN:
      // create unique array of urls
      urls = state.fetchingURLs.slice()
      urls.push(action.url)
      urls = Array.from(new Set(urls)) // remove duplication

      names = state.fetchingNames
      if(action.requestName) {
        names = names.slice()
        names.push(action.requestName)
        names = Array.from(new Set(names))
      }

      return Object.assign({}, state, {
        isFetching: true,
        fetchingURLs: urls,
        fetchingNames: names,
      });
    case API_RECEIVED:
      i = state.fetchingURLs.indexOf(action.url)
      urls = state.fetchingURLs.slice();
      urls.splice(i, 1)
      isFetching = urls.length > 0

      let receivedParams = action.json;
      names = state.fetchingNames
      if(action.requestName) {
        i = names.indexOf(action.requestName)
        names = names.slice();
        names.splice(i, 1)
        receivedParams = {
          [action.requestName]: action.json
        }
      }

      return Object.assign(
        {
        },
        state,
        { // page and pageable have to be reset. remaining these data causes unnexpected problem.
          page: null,
          pageable: null,
        },
        {
          isFetching: isFetching,
          fetchingURLs: urls,
          fetchingNames: names,
          code: null // this param can be overwritten by the receivedParams
        },
                           receivedParams)
    case API_FAILED:
      i = state.fetchingURLs.indexOf(action.url)
      urls = state.fetchingURLs.slice();
      urls.splice(i, 1)
      isFetching = urls.length > 0

      names = state.fetchingNames
      if(action.requestName) {
        i = names.indexOf(action.requestName)
        names = names.slice();
        names.splice(i, 1)
      }

      return Object.assign({}, state, {
        isFetching: isFetching,
        fetchingURLs: urls,
        fetchingNames: names,
      })
    case UPDATE_PAGE:
      return Object.assign({}, { page: action.page })
    case API_RESET_AREAS:
      return Object.assign({}, state, { areas: [] })
    case RESET_SEARCH_RESULT:
      return Object.assign({}, state, { total_count: 0, total_pages: 0, pagination_range: undefined })
    case API_RESET_ARTICLE:
      return Object.assign({}, state, { page: null, article: null })
    case API_RESET_CATEGORY:
      return Object.assign({}, state, { category: null })
    default:
      return state;
  }
};

export default apiReducer;
