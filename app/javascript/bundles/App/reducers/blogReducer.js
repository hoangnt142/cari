
//import { ACTION_SAMPLE1, RESET } from '../constants/constants';

const initialState = {};

const blogReducer = (state = initialState, action) => {
  switch (action.type) {
/*
    case ACTION_SAMPLE1:
      return Object.assign({}, state, {
        sampleMessage: action.text
      });
    case RESET:
      return Object.assign({}, initialState);
*/
    default:
      return state;
  }
};

export default blogReducer;
