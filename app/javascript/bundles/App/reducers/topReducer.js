import { TOP_SET_VISIBLE, TOP_INITIALIZE } from '../constants/constants';

const initialState = {
  regions: [],
  widgets: [],
  visible: false // test code
};

const topReducer = (state = initialState, action) => {
  switch (action.type) {
/*
    case ACTION_SAMPLE1:
      return Object.assign({}, state, {
        sampleMessage: action.text
      });
    case RESET:
      return Object.assign({}, initialState);
*/
    case TOP_SET_VISIBLE:
      // test code
      return Object.assign({}, state, { visible: action.visible });
    case TOP_INITIALIZE:
      return Object.assign({}, state, { regions: action.regionsWithPrefectures, widgets: action.widgets });
    default:
      return state;
  }
};

export default topReducer;
