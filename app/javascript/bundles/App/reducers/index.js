import { combineReducers } from 'redux';

import agentsReducer from './agentsReducer';
import blogReducer from './blogReducer';
import categoryReducer from './categoryReducer';
import contactReducer from './contactReducer';
import contactSentReducer from './contactSentReducer';
import helloWorkListReducer from './helloWorkListReducer';
import informationReducer from './informationReducer';
import myHistoryReducer from './myHistoryReducer';
import myJobsReducer from './myJobsReducer';
import mypageJobsReducer from './mypageJobsReducer';
import mypageMyReputesReducer from './mypageMyReputesReducer';
import mypagePasswordChangeReducer from './mypagePasswordChangeReducer';
import mypageReducer from './mypageReducer';
import mypageSettingsReducer from './mypageSettingsReducer';
import pageReducer from './pageReducer';
import passwordChangeReducer from './passwordChangeReducer';
import passwordGenerateReducer from './passwordGenerateReducer';
import reputePostedReducer from './reputePostedReducer';
import reputePostingReducer from './reputePostingReducer';
import reputesReducer from './reputesReducer';
import signInReducer from './signInReducer';
import signUpReducer from './signUpReducer';
import sitemapReducer from './sitemapReducer';
import topReducer from './topReducer';
import apiReducer from './apiReducer';
import userReducer from './userReducer';
import rightSideReducer from './rightSideReducer';
import pageFooterReducer from './pageFooterReducer';
import hospitalPageReducer from './hospitalPageReducer';
import searchModalReducer from './searchModalReducer'

const reducers = combineReducers({
  'agents': agentsReducer,
  'blog': blogReducer,
  'category': categoryReducer,
  'contact': contactReducer,
  'contactSent': contactSentReducer,
  'helloWorkList': helloWorkListReducer,
  'information': informationReducer,
  'myHistory': myHistoryReducer,
  'myJobs': myJobsReducer,
  'mypageJobs': mypageJobsReducer,
  'mypageMyReputes': mypageMyReputesReducer,
  'mypagePasswordChange': mypagePasswordChangeReducer,
  'mypage': mypageReducer,
  'mypageSettings': mypageSettingsReducer,
  'page': pageReducer,
  'passwordChange': passwordChangeReducer,
  'passwordGenerate': passwordGenerateReducer,
  'reputePosted': reputePostedReducer,
  'reputePosting': reputePostingReducer,
  'reputes': reputesReducer,
  'signInPage': signInReducer,
  'signUpPage': signUpReducer,
  'sitemap': sitemapReducer,
  'top': topReducer,
  'api': apiReducer,
  'user': userReducer,
  'rightSide': rightSideReducer,
  'pageFooter': pageFooterReducer,
  'hospitalPage': hospitalPageReducer,
  'searchModal': searchModalReducer
});

export default reducers;
