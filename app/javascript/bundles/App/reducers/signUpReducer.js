import {
  SIGN_UP,
  SIGN_UP_RESET,
  SIGN_UP_ON_CHANGE,
  SIGN_UP_ON_CHANGE_SLIDE,
  SIGN_UP_ON_CHANGE_PREFECTURE,
  SIGN_UP_ON_CHANGE_BIRTH,
  SIGN_UP_ON_CHANGE_FAVORITE_PREFECTURE,
  SIGN_UP_ON_CHANGE_CITY
} from '../constants/constants'

const initialState = {

  nickname: '',
  email: '',
  password: '',

  gender: null,
  qualification1: false,
  qualification2: false,
  qualification3: false,
  qualification4: false,
  qualification5: false,

  prefectureId: null,
  prefectureName: '',
  cityId: null,
  cityName: '',

  birth: null,
  birthYear: null,
  birthMonth: null,
  birthDay: null,

  favoritePrefectureId1: null,
  favoritePrefectureName1: '',
  favoriteCityId1: null,
  favoriteCityName1: '',
  favoritePrefectureId2: null,
  favoritePrefectureName2: '',
  favoriteCityId2: null,
  favoriteCityName2: '',

  currentSlide: 1
}

const signUpReducer = (state = initialState, action) => {
  console.log('action', action)
  switch (action.type) {
    case SIGN_UP_RESET:
      return initialState
    case SIGN_UP_ON_CHANGE:
      return Object.assign({}, state, { [action.field]: action.value })
    case SIGN_UP_ON_CHANGE_SLIDE:
      return Object.assign({}, state, { 'currentSlide': action.currentSlide })
    case SIGN_UP_ON_CHANGE_PREFECTURE:
      return Object.assign({}, state, {
        prefectureId: action.prefectureId,
        prefectureName: action.prefectureName,
        cityId: null,
        cityName: ''
      })
    case SIGN_UP_ON_CHANGE_CITY:
      delete action.type;
      return Object.assign({}, state, action)
    case SIGN_UP_ON_CHANGE_FAVORITE_PREFECTURE:
      return Object.assign({}, state, {
        [`favoritePrefectureId${action.number}`]: action.prefectureId,
        [`favoritePrefectureName${action.number}`]: action.prefectureName,
        [`favoriteCityId${action.number}`]: null,
        [`favoriteCityName${action.number}`]: ''
      })
    case SIGN_UP_ON_CHANGE_BIRTH:

      const change = {}
      if(action.field == 'year') {
        //birthDate.setFullYear(action.value)
        change.birthYear = action.value
      }
      else if (action.field == 'month') {
        //birthDate.setMonth(action.value - 1)
        change.birthMonth = action.value
      }
      else if(action.field == 'day') {
        //birthDate.setDate(action.value)
        change.birthDay = action.value
      }
      else {
        console.error(`Unknown field of birth: ${action.field}`)
      }


      const mergedState = Object.assign({}, state, change)
      if(mergedState.birthYear &&
         mergedState.birthMonth &&
         mergedState.birthDay) {
        const birthDate = new Date(mergedState.birthYear,
                                   mergedState.birthMonth - 1,
                                   mergedState.birthDay)

        // After converting the text to Date, reset each value so that birth and year,month,day will be same.
        mergedState.birthYear = birthDate.getFullYear()
        mergedState.birthMonth = birthDate.getMonth() + 1
        mergedState.birthDay = birthDate.getDate()
        mergedState.birth = birthDate
      }

      return Object.assign(mergedState)
    case SIGN_UP:
    default:
      return state
  }
}

export default signUpReducer
