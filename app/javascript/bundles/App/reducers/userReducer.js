import {
  LOGGING,
  LOGGED_IN,
  LOGGED_IN_FAILED,
  LOGGED_OUT,
  SIGNING,
  SIGN_UP_SUCCEEDED,
  SIGN_UP_FAILED
} from '../constants/constants'

const initialState = {
  user: null,
  logging: false,
  loggedInFailed: false,
  signing: false,
  signedUp: null,
}

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGGING:
      return Object.assign({}, state, {
        logging: true,
        loggedInFailed: false,
      })
    case LOGGED_IN:
      return Object.assign({}, state, {
        user: action.user,
        logging: false,
        loggedInFailed: false,
      })
    case LOGGED_IN_FAILED:
      return Object.assign({}, state, {
        logging: false,
        loggedInFailed: true,
      })
    case LOGGED_OUT:
      return initialState

    case SIGNING:
      return Object.assign({}, state, {
        signing: true
      })
    case SIGN_UP_SUCCEEDED:
      return Object.assign({}, state, {
        signing: false,
        signedUp: true
      })
    case SIGN_UP_FAILED:
      return Object.assign({}, state, {
        signing: false,
        signedUp: false
      })
    default:
      return state
  }
}

export default userReducer
