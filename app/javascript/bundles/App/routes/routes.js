import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Bundle from '../components/Bundle'
import PageHeader from '../containers/PageHeaderContainer'
import PageFooter from '../containers/PageFooterContainer'
import PageSelector from '../containers/PageSelectorContainer'
import ScrollToTop from '../components/_ScrollToTop'
import SearchModal from '../containers/search/ModalContainer'

const asyncComponent = (imp, props={}) =>
  <Bundle
    load={() => imp()}>
    {Comp => <Comp {...props} />}
  </Bundle>
;


/* page list
   Top
   Information
   Agents
   Reputes
   HelloWorkList
   ReputePosting
   ReputePosted
   Blog
   Category
   Contact
   ContactSent
   Sitemap
   MyHistory
   MyJobs
   SignUp
   SignIn
   PasswordGenerate
   PasswordChange
   Mypage
   MypageJobs
   MypageMyReputes
   MypageSettings
   MypagePasswordChange
   PageSelector
 */


const AsyncTop = (props) => {
  return asyncComponent(() => import(/* webpackChunkName: "Top" */'../containers/TopContainer'),
                        props
  )
}

const AsyncInformation = () => asyncComponent(() =>
  import(/* webpackChunkName: "Information" */'../containers/InformationContainer')
);
const AsyncAgents = () => asyncComponent(() =>
  import(/* webpackChunkName: "Agents" */'../containers/AgentsContainer')
);
const AsyncReputes = () => asyncComponent(() =>
  import(/* webpackChunkName: "Reputes" */'../containers/ReputesContainer')
);
const AsyncHelloWorkList = () => asyncComponent(() =>
  import(/* webpackChunkName: "HelloWorkList" */'../containers/HelloWorkListContainer')
);
const AsyncReputePosting = () => asyncComponent(() =>
  import(/* webpackChunkName: "ReputePosting" */'../containers/ReputePostingContainer')
);
const AsyncReputePosted = () => asyncComponent(() =>
  import(/* webpackChunkName: "ReputePosted" */'../containers/ReputePostedContainer')
);
const AsyncBlog = ({ match }) => {
  return asyncComponent(() => import(/* webpackChunkName: "Blog" */'../containers/BlogContainer'),
                        match.params
  );
}
const AsyncCategory = ({ match }) => {
  return asyncComponent(() => import(/* webpackChunkName: "Category" */'../containers/CategoryContainer'),
                        match.params
  );
}
const AsyncContact = () => asyncComponent(() =>
  import(/* webpackChunkName: "Contact" */'../containers/ContactContainer')
);
const AsyncContactSent = () => asyncComponent(() =>
  import(/* webpackChunkName: "ContactSent" */'../containers/ContactSentContainer')
);

const AsyncSitemap = () => asyncComponent(() =>
  import(/* webpackChunkName: "Sitemap" */'../containers/SitemapContainer')
);
const AsyncMyHistory = () => asyncComponent(() =>
  import(/* webpackChunkName: "MyHistory" */'../containers/MyHistoryContainer')
);
const AsyncMyJobs = () => asyncComponent(() =>
  import(/* webpackChunkName: "MyJobs" */'../containers/MyJobsContainer')
);
const AsyncSignUp = () => asyncComponent(() =>
  import(/* webpackChunkName: "SignUp" */'../containers/SignUpContainer')
);
const AsyncSignIn = () => asyncComponent(() =>
  import(/* webpackChunkName: "SignIn" */'../containers/SignInContainer')
);
const AsyncPasswordGenerate = () => asyncComponent(() =>
  import(/* webpackChunkName: "PasswordGenerate" */'../containers/PasswordGenerateContainer')
);
const AsyncPasswordChange = () => asyncComponent(() =>
  import(/* webpackChunkName: "PasswordChange" */'../containers/PasswordChangeContainer')
);
const AsyncMypage = () => asyncComponent(() =>
  import(/* webpackChunkName: "Mypage" */'../containers/MypageContainer')
);
const AsyncMypageJobs = () => asyncComponent(() =>
  import(/* webpackChunkName: "MypageJobs" */'../containers/MypageJobsContainer')
);
const AsyncMypageMyReputes = () => asyncComponent(() =>
  import(/* webpackChunkName: "MypageMyReputes" */'../containers/MypageMyReputesContainer')
);
const AsyncMypageSettings = () => asyncComponent(() =>
  import(/* webpackChunkName: "MypageSettings" */'../containers/MypageSettingsContainer')
);
const AsyncMypagePasswordChange = () => asyncComponent(() =>
  import(/* webpackChunkName: "MypagePasswordChange" */'../containers/MypagePasswordChangeContainer')
);

const PageSelectorWithParams = (props) => <PageSelector {...props} />

/*
   /:url // hospital
   /:url/repute-list // hospital
   /:url/repute-list/page/2,3,4,… // hospital
   /:url // article
   /:url // page
 */


export default () => {
  let context = null
  const getContext = () => context

  return (
    <div>
      <Route path="/" component={() => <PageHeader stickerContext={getContext} />} />
      <div ref={_context => context = _context }>
        <SearchModal />
        <Switch>
          <Route exact path="/" component={AsyncTop} />
          <Route path="/my-jobs" component={AsyncMyJobs} />
          <Route path="/contact" component={AsyncContact} />
          <Route path="/contact-sent" component={AsyncContactSent} />
          <Route path="/my-history" component={AsyncMyHistory} />
          
          {/*<Route path="/information" component={AsyncInformation} />
              <Route exact path="/blog" component={AsyncBlog} />
              <Route path="/blog/page/:pageNumber" component={AsyncBlog} />
              <Route path="/agents" component={AsyncAgents} />
              <Route path="/reputes" component={AsyncReputes} />
              <Route path="/hello-work-list" component={AsyncHelloWorkList} />
              <Route path="/repute-posting" component={AsyncReputePosting} />
              <Route path="/repute-posted" component={AsyncReputePosted} />

              <Route exact path="/blog" component={AsyncBlog} />
              <Route path="/blog/page/:pageNumber" component={AsyncBlog} />

              <Route exact path="/blog/:categorySlug" component={AsyncCategory} />
              <Route path="/blog/:categorySlug/page/:pageNumber" component={AsyncCategory} />



              <Route path="/sitemap" component={AsyncSitemap} />


              <Route path="/sign-up" component={AsyncSignUp} />
              <Route path="/sign-in" component={AsyncSignIn} />
              <Route path="/password-generate" component={AsyncPasswordGenerate} />
              <Route path="/password-change" component={AsyncPasswordChange} />

              <Route exact path="/mypage" component={AsyncMypage} />
              <Route path="/mypage/jobs" component={AsyncMypageJobs} />
              <Route path="/mypage/my-reputes" component={AsyncMypageMyReputes} />
              <Route path="/mypage/settings" component={AsyncMypageSettings} />
              <Route path="/mypage/password-change" component={AsyncMypagePasswordChange} />*/}

          <Route path="/search/page/:page" component={PageSelectorWithParams} />
          <Route path="/search" component={PageSelectorWithParams} />

          <Route path="/:url+/page/:page" component={PageSelectorWithParams} />
          <Route path="/:url+" component={PageSelectorWithParams} />

        </Switch>
      </div>
      <Route path="/" exact component={() => <PageFooter showNav={false} />} />
      <Route path="/:any+" component={() => <PageFooter showNav={true} />} />
      <Route path="/" component={ScrollToTop} />
    </div>
  )
}
