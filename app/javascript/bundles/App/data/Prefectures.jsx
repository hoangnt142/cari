export default {
  "china": {
    "name": "中国・四国",
    "url": "china",
    "areas": [
      ["広島", "/sample-url"],
      ["岡山", "/sample-url"],
      ["山口", "/sample-url"],
      ["島根", "/sample-url"],
      ["鳥取", "/sample-url"],
      ["愛媛", "/sample-url"],
      ["香川", "/sample-url"],
      ["徳島", "/sample-url"],
      ["高知", "/sample-url"]
    ]
  },
  "kyushu": {
    "name": "九州・沖縄",
    "url": "kyushu",
    "areas": [
      ["福岡", "/sample-url"],
      ["熊本", "/sample-url"],
      ["鹿児島", "/sample-url"],
      ["長崎", "/sample-url"],
      ["大分", "/sample-url"],
      ["宮崎", "/sample-url"],
      ["佐賀", "/sample-url"],
      ["沖縄", "/sample-url"]
    ]
  },
  "hokkaido": {
    "name": "北海道・東北",
    "url": "hokkaido",
    "areas": [
      ["北海道", "/sample-url"],
      ["宮城", "/sample-url"],
      ["福島", "/sample-url"],
      ["青森", "/sample-url"],
      ["岩手", "/sample-url"],
      ["山形", "/sample-url"],
      ["秋田", "/sample-url"]
    ]
  },
  "kanto": {
    "name": "関東",
    "url": "kanto",
    "areas": [
      ["東京", "/sample-url"],
      ["神奈川", "/sample-url"],
      ["埼玉", "/sample-url"],
      ["千葉", "/sample-url"],
      ["茨城", "/sample-url"],
      ["栃木", "/sample-url"],
      ["群馬", "/sample-url"]
    ]
  },
  "koushinetsu": {
    "name": "甲信越・北陸",
    "url": "koushinetsu",
    "areas": [
      ["新潟", "/sample-url"],
      ["長野", "/sample-url"],
      ["山梨", "/sample-url"],
      ["石川", "/sample-url"],
      ["富山", "/sample-url"],
      ["福井", "/sample-url"]
    ]
  },
  "tokai": {
    "name": "東海",
    "url": "tokai",
    "areas": [
      ["愛知", "/sample-url"],
      ["静岡", "/sample-url"],
      ["岐阜", "/sample-url"],
      ["三重", "/sample-url"]
    ]
  },
  "kansai": {
    "name": "関西",
    "url": "kansai",
    "areas": [
      ["大阪", "/sample-url"],
      ["兵庫", "/sample-url"],
      ["京都", "/sample-url"],
      ["滋賀", "/sample-url"],
      ["奈良", "/sample-url"],
      ["和歌山", "/sample-url"]
    ]
  },
}
