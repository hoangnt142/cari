import React, { PureComponent } from 'react'
import { Provider } from 'react-redux'
import { Router } from 'react-router-dom'

import configureStore from '../store/store'
//import Routing from '../routes/routes'
import routes from '../routes/routes'
import history from '../lib/history'
import { login } from '../actions/userActions'

import '../../../app/assets/stylesheets/common.scss'
import '../../../app/assets/stylesheets/layout.scss'
import '../../../../../semantic/dist/semantic.min.css'


// See documentation for https://github.com/reactjs/react-redux.
// This is how you get props from the Rails view into the redux store.
// This code here binds your smart component to the redux store.


export default class App extends PureComponent {

  constructor(props) {
    super(props)
    this.store = configureStore(this.props)
    // Check the logging status and set the information in state.
    this.store.dispatch(login())
  }

  render() {
    return (
      <Provider store={this.store}>
        <Router history={history}>
          {routes()}
        </Router>
      </Provider>
    )
  }
}
