import { createStore, applyMiddleware } from 'redux'
import reducers from '../reducers'
import { createLogger } from 'redux-logger'
import thunkMiddleware from 'redux-thunk'
import { composeWithDevTools } from 'remote-redux-devtools'

const middleware = [thunkMiddleware]
// Include logger only in development
if(process.env.NODE_ENV != 'production') {
  const logger = createLogger({
    level: 'log'
  })
  middleware.push(logger)
}

const configureStore = (railsProps) => {
  return createStore(reducers, railsProps, composeWithDevTools(
    applyMiddleware(...middleware)
  ))
}

export default configureStore
