export const objectToParameters = (condition) => {
  Object.keys(condition).forEach((key) => (condition[key] == null || condition[key].length == 0 ) && delete condition[key]) // remove blank value
  const parameters = Object.entries(condition).map(([key, val]) => `${key}=${encodeURIComponent(val)}`).join('&')
  return parameters
}

export const pathToUrl = (path) => {
  const serverAndPort = window.location.href.match(/https?\:\/\/[^\/]+\//)[0].replace(/\/$/, '')
  const url = path ? `${serverAndPort}${path}` : null
  return url
}
