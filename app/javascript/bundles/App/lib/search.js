export const selectedPrefecture = (prefectures, condition) => {
  const {
    prefectureId,
  } = condition

  let prefecture = null
  if(prefectureId) {
    prefecture = prefectures.find(p => p.id == prefectureId)
  }
  return prefecture
}

export const selectedAreaNames = (area1s, area2s, area3s, condition) => {
  const {
    prefectureId,
    areaIds,
  } = condition

  let areaNames = []
  if(areaIds && areaIds.length > 0) {
    if(prefectureId == 13) {

      area1s.filter(a1 => areaIds.find(id => id == a1.id)).forEach(a1 => {
        if(area2s.some(a2 => a2.parent_id == a1.id && areaIds.find(id => id == a2.id))) {
          // Having chldren selected

          area2s.filter(a2 => a2.parent_id == a1.id && areaIds.find(id => id == a2.id)).forEach(a2 => {
            if(area3s.some(a3 => a3.parent_id == a2.id && areaIds.find(id => id == a3.id))) {
              // Having chldren selected -> show the area3 without showing area1,2
              area3s.filter(a3 => a3.parent_id == a2.id && areaIds.find(id => id == a3.id)).forEach(a3 => {
                areaNames.push(a3.name)
              })
            }
            else {
              // Having chldren selected -> show the area2 without showing area1
              areaNames.push(a2.name)
            }
          })
        }
        else {
          // Not having children selected -> show the area1
          areaNames.push(a1.name)
        }
      })
    }
    else {
      area1s.filter(a1 => areaIds.find(id => id == a1.id)).forEach(a1 => {
        if(area2s.some(a2 => a2.parent_id == a1.id && areaIds.find(id => id == a2.id))) {
          // Having chldren selected -> show the selected area2 without showing area1
          area2s.filter(a2 => a2.parent_id == a1.id && areaIds.find(id => id == a2.id)).forEach(a2 => {
            areaNames.push(a1.name + a2.name)
          })
        }
        else {
          // Not having children selected -> show the area1
          areaNames.push(a1.name)
        }
      })
    }
  }
  return areaNames
}


export const numberOfConditions = (condition) => {
  const {
    qualificationIds,
    jobContractIds,
    hospitalTypeIds,
    jobTypeIds,
    jobDetailIds,
  } = condition

  const count = [
    qualificationIds,
    jobContractIds,
    hospitalTypeIds,
    jobTypeIds,
    jobDetailIds,
  ].reduce((accumulator, item) => accumulator + (item && item.length > 0 ? 1 : 0), 0)

  return count + ((condition.prefectureId || condition.areaIds.length > 0) ? 1 : 0)
}
