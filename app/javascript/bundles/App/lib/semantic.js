import _ from 'lodash'

export const updateCheck = (data, array) => {
  let newArray;
  if (data.reset) {
    return []
  }
  if(data.checked) {
    newArray = array ? array.slice() : []
    if(newArray.findIndex(a => a == data.value) == -1) {
      newArray.push(data.value)
    }
  }
  else {
    newArray = _.without(array, data.value)
  }
  return newArray
}

export const chunkArray = (array, chunk_size) => {
  let index = 0;
  let arrayLength = array.length;
  let output = [];

  for (index = 0; index < arrayLength; index += chunk_size) {
      let chunk = array.slice(index, index+chunk_size);
      output.push(chunk);
  }

  return output;
}

export const pluck = (array, key) => {
  return array.map(o => o[key]);
}