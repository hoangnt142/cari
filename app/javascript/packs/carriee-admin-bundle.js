import ReactOnRails from 'react-on-rails';

import AdminApp from '../bundles/AdminApp/startup/AdminApp';

ReactOnRails.register({
  AdminApp,
});
