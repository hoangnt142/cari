# About

http://mroonga.org/docs/index.html


# How to setup

## Homebrew

Follow this link.
https://github.com/mroonga/homebrew-mroonga


Enable Mroonga

```
mysql -uroot test < /usr/local/Cellar/mroonga/7.10/share/mroonga/install.sql
```
