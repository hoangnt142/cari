# Carriee Web

## Environment

This application is SPA(Single Page Application).\
Server side uses Ruby on Rails and only provide the APIs returning JSON.\
Client side(Web browser) uses React.
`rbenv`, `yarn`, `MariaDB` with [`Mroonga`](./MROONGA.md) are needed to set up in advance to start the development section.


### Server

- Rbenv
- Ruby
- Rails

### Client

- React
- React router
- Redux
- yarn
- CSS Module

### DB

- MariaDB(Mroonga Engine)

### Design framework

- Semantic UI(https://react.semantic-ui.com/introduction)


## Development

### Setup

#### Ruby

```shell
bundle install
```

#### Javascript

```shell
yarn install
```


#### create DB

```
bin/rails db:create
```


#### migration

```
bin/rails db:schema:load
bin/rails db:migrate
```

#### data

```
bin/rails db:seed
```

#### run the application

```shell
foreman start
```




## Sample code

### Server-side

View file is not needed.


#### models

...

#### controllers

##### app/controllers/api/pages_controller.rb

Just reutrn the json then client side can get them.

```ruby
class Api::PagesController < ApplicationController

  def show
    sample = {
      a: 'test1',
      b: 'test2'
    }
    render json: sample
  end
end
```

#### routings

##### config/routes.rb

Routing should be added.\

```ruby
  # API
  namespace :api do
    resources :pages
  end
```

Executing `bin/rails routes` shows All of URLs by this setting.




### Client-side

Basically, one page consists of component, container, action and reducer.


#### Component

##### app/javascript/bundles/App/components/Top.jsx

Component is just a view file with no action, no state.\
When we set same props, it should always return same result.\


```javascript
import React, { PureComponent } from 'react'
import PropType from 'prop-types'
import { Link } from 'react-router-dom'

// React Helmet
// The tags will be embedded dynamically in <head>.
//
// <Helmet>
//   <title>Top page</title>
//   <meta name="description" content="" />
//   <meta name="keywords" content="" />
//   <meta name="robots" content="index,follow" />
//   <link rel="canonical" href={window.location.href} />
// </Helmet>

import { Helmet } from 'react-helmet'

import { breadCrumbList } from '../../../lib/structured-data'


// Stylesheet using CSS Module
// https://glenmaddern.com/articles/css-modules
//
// We use `babel-plugin-react-css-modules` implementation and written the setting in .babelrc.
// When we use it, write with 'styleName' attribute.
// The class name are absolutely independent as it rewrite the class name uniquely per file basis.
// e.g. <h1 styleName='red'>

import './style.css'


// Image files are put in app/javascripts/app/assets/images
// We can use it without considering the path
// e.g. <img src={file1} />

import file1 from 'app/assets/images/file1'
import file2 from 'app/assets/images/file2'


export default class Top extends PureComponent {

  // It may not be needed as we use redux
  static propTypes = {

  }

  constructor(props) {
    super(props)

    // Getting methods from redux.
    // It consists of action, constant of action name, reducer and container.
    // All of them are put directly in `app/javascripts/bundles/App/`.
    // - actions/
    // - constants/constants.js
    // - reducers/
    // - containers/
    const { fetchPage, reset } = this.props

    // Resets the state as initial condition even when coming back from the other page.
    reset()

    // Getting data from the server and set the data to state to Redux.
    // we can get the data with this.props.api according to the TopContainer.
    //
    // `this.props.api.isFetching` returns true while getting data from the server.
    // As Fetch works asynchrnously, we have to handle it carefully when multiple fetchings are working.
    // The true returned by `isFetching` means fetching at least one of requests is fetching now.
    // When it's need to specify a URL of isFetching, use `this.props.api.fetchingURLs`, which contains fetching URLs.
    //
    // Data coming from the server can be gotten directly with `this.props.api.data1`.
    //
    // Refer to `actions/apiActions.js` and `reducers/apiReducer.js`.
    fetchPage('/api/pages/1')
  }

  render() {

    //console.log(style)
    const { api } = this.props

    return (
      <div>
        <Helmet>
          <title>Top page</title>
          <meta name="description" content="" />
          <meta name="keywords" content="" />
          <meta name="robots" content="index,follow" />
          <link rel="canonical" href={window.location.href} />
          {breadCrumbList([{ title: 'Top', url: window.location.href}])}
        </Helmet>

        <h1 styleName='red'>Top {api.isFetching ? "..." : ""}</h1>

        <img src={file1} />
        <img src={file2} />

        <div styleName='small'>
          {api.a}
          {api.b}
        </div>

      </div>
    )
  }
}
```



#### Container

##### app/javascript/bundles/App/containers/TopContainer.jsx

To use Redux, each page needs a container as a connected component with Redux.


```javascript
import { connect } from 'react-redux'
import Top from '../components/Top'
import * as actions from '../actions/topActionCreators'
import { fetchPage } from '../actions/apiActions'


const mapStateToProps = (state) => ({
  top: state.top,
  api: state.api
})

export default connect(mapStateToProps, Object.assign(actions, { fetchPage: fetchPage }))(Top)
```



#### Actions

##### app/javascript/bundles/App/actions/TopActions.js

Each page would need an action scoping the page along with the reducer.


```javascript
import { TOP_RESET } from '../constants/constants';

export const reset = () => {
  return {
    type: TOP_RESET,
  };
};
```


##### app/javascript/bundles/App/actions/apiActions.js


```javascript
import { REQUEST_PAGE, RECEIVE_PAGE, REQUEST_PAGE_FAILED } from '../constants/constants';


export const requestPage = (url) => ({
  type: REQUEST_PAGE,
  url
})
export const receivePage = (url, json) => ({
  type: RECEIVE_PAGE,
  url,
  json
})
export const requestPageFailed = (url) => ({
  type: REQUEST_PAGE_FAILED,
  url
})

export const fetchPage = (url) => {
  return dispatch => {
    dispatch(requestPage(url))
    return fetch(url)
      .then(response => {
        if(!response.ok) {
          throw `Fetch was failed: ${response.code}`
        }
        response.json()
                .then(json => {
                  console.log('json', json)
                  dispatch(receivePage(url, json))
                })
                .catch(error => {
                  console.error(error)
                  dispatch(requestPageFailed(url))
                })
      })
      .catch(error => {
        console.error(error)
        dispatch(requestPageFailed(url))
      })
  }
}
```




#### Reducers

##### app/javascript/bundles/App/reducers/topReducer.js

Each page would need a reducer scoping the page.

```javascript
//import { ACTION_SAMPLE1, RESET } from '../constants/constants';

const initialState = {};

const topReducer = (state = initialState, action) => {
  switch (action.type) {
/*
    case ACTION_SAMPLE1:
      return Object.assign({}, state, {
        sampleMessage: action.text
      });
    case RESET:
      return Object.assign({}, initialState);
*/
    default:
      return state;
  }
};

export default topReducer;
```

##### app/javascript/bundles/App/reducers/apiReducer.js

This file is prepared for common use. No need to prepare it for developing each page.

```javascript
import { REQUEST_PAGE, RECEIVE_PAGE, REQUEST_PAGE_FAILED } from '../constants/constants';

const initialState = {
  isFetching: false,
  fetchingURLs: []
};

const apiReducer = (state = initialState, action) => {

  let urls, isFetching, i;
  switch (action.type) {
    case REQUEST_PAGE:
      // create unique array of urls
      urls = state.fetchingURLs.slice()
      urls.push(action.url)
      urls = Array.from(new Set(urls))
      return Object.assign({}, state, {
        isFetching: true,
        fetchingURLs: urls
      });
    case RECEIVE_PAGE:
      i = state.fetchingURLs.indexOf(action.url)
      urls = state.fetchingURLs.slice();
      urls.splice(i, 1)
      isFetching = urls.length > 0
      return Object.assign({}, state, {
        isFetching: isFetching,
        fetchingURLs: urls,
      },
      action.json)
    case REQUEST_PAGE_FAILED:
      i = state.fetchingURLs.indexOf(action.url)
      urls = state.fetchingURLs.slice();
      urls.splice(i, 1)
      isFetching = urls.length > 0
      return Object.assign({}, state, {
        isFetching: isFetching,
        fetchingURLs: urls
      })

    default:
      return state;
  }
};

export default apiReducer;
```


##### app/javascript/bundles/App/reducers/index.js

All of reducers have to be combined here.


```javascript
import { combineReducers } from 'redux';

import topReducer from './topReducer';
import apiReducer from './apiReducer';

const reducers = combineReducers({
  'top': topReducer,
  'api': apiReducer
});

export default reducers;
```



##### app/javascript/bundles/App/constants/constants.js

constant file defines the actions used by Redux(actions and reducers).
This file should be only one as the dupicated action cause unnexpected error.

```javascript
// Sample, constants should be defined only in this file.
export const ACTION_SAMPLE1 = 'ACTION_SAMPLE1'
export const TOP_RESET = 'TOP_RESET'

// Api actions
export const REQUEST_PAGE = 'REQUEST_PAGE'
export const RECEIVE_PAGE = 'RECEIVE_PAGE'
export const REQUEST_PAGE_FAILED = 'REQUEST_PAGE_FAILED'
```




#### Router

##### app/javascript/bundles/App/routes/routes.js

It uses react router.\
To add a page with URL, write like this sample with the URL.\
In this example, page is loaded asynchrnously.


```javascript
import React from 'react';
import { Switch, Route, Link } from 'react-router-dom';
import Bundle from '../components/Bundle';

const asyncComponent = (imp, props={}) =>
  <Bundle
    load={() => imp()}>
    {Comp => <Comp {...props} />}
  </Bundle>
;


const AsyncTop = () => asyncComponent(() =>
  import(/* webpackChunkName: "Top" */'../containers/TopContainer')
);

const AsyncPageSelector = ({ location }) => {
  return asyncComponent(() => import(/* webpackChunkName: "Page" */'../containers/PageSelectorContainer'),
                        location
  );
}

export default (
  <div>
    <header>
      {/* list of all pages. Only for development purpose */}
      <h1>sample page list</h1>
      <ul>
        <li><Link to="/">Top</Link></li>
        <li><Link to="/page1">page</Link></li>
        <li><Link to="/page1/page404">404</Link></li>

      </ul>
    </header>

    <Switch>
      <Route exact path="/" component={AsyncTop} />
      <Route path="/:url" component={AsyncPageSelector} />
    </Switch>

  </div>
);
```
