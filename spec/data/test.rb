class Test
  include FactoryBot::Syntax::Methods

  def set1
    create_list :user, 50
    create_list :hospital, 10
    create_list :hospital, 10, with_reviews: true
    create_list :hospital, 10, :deleted
    create_list :hospital, 10, :blank, published: false

    Hospital.count_reviews_and_calculate_point

    create_list :hellowork, 10
    create_list :information, 10

  end
end
