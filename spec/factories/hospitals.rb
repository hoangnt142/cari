FactoryBot.define do
  factory :hospital do

    sequence(:id) {|id| id }
    version 1
    sequence(:name) { |n| "Hospital #{n}" }
    sequence(:title) { |n| "Hospital title #{n}" }

    number_of_beds 200
    sequence(:postal_code) { |n| "#{n}".ljust(7, '0') }
    sequence(:prefecture_id) { |n| n%47 + 1 }

    sequence(:city_id) { |n| City.where(prefecture_id: n%47 + 1).take.id }
    address "美浜区中瀬1-3 幕張テクノガーデンCB棟3F MBP"
    access "アクセス"
    standard "標準"
    karte "カルテ"
    business_time "平日 10:00 - 18:00"
    holidays "土日祝日"
    hospital_url "http://www.childfoot.jp"
    features "Feature"
    phone "043-312-6571"
    fax "043-312-6571"
    representative "代表"

    sequence(:established_date) { |n| n.day.ago }

    longitude 35.662
    latitude 140.0418

    review_count 0
    average_point 0
    locked 0
    deleted 0

    created_at { Time.now }
    updated_at { Time.now }





    transient do
      with_reviews false
      published true
      full_search_keys true
    end


    trait :deleted do
      deleted 1
      after(:build) do |hospital|
        hospital.name = "DELETED: #{hospital.name}"
      end
    end

    trait :blank do

      published false

      number_of_beds nil
      postal_code nil
      address nil
      access nil
      standard nil
      karte nil
      business_time nil
      holidays nil
      hospital_url nil
      features nil
      phone nil
      fax nil
      representative nil
      established_date nil
      after(:build) do |hospital|
        hospital.name = "BLANK: #{hospital.name}"
      end
    end


    after(:create) do |hospital, evaluator|
      ## hopital_reviews
      if evaluator.with_reviews
        User.where(id: [1..10].to_a).each do |user|
          create :hospital_review, hospital_id: hospital.id, user_id: user.id, gender: user.gender
        end
        User.where(id: [11..15].to_a).each do |user|
          create :hospital_review, :not_approved,  hospital_id: hospital.id, user_id: user.id, gender: user.gender
        end
      end

      ## pages
      # create :page, pageable_id: hospital.id, pageable_type: 'Hospital', url: hospital.name.underscore, publish_status: (case when hospital.deleted then 2 when evaluator.published then 1 else 0 end)
      hospital.page.publish_status = (case when hospital.deleted then 2 when evaluator.published then 1 else 0 end)
      hospital.page.save!

      ## hospital_hospital_types
      if evaluator.full_search_keys
        hospital.hospital_types << HospitalType.all
      end
    end

  end
end
