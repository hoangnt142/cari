FactoryBot.define do
  factory :user do
    sequence(:id) {|id| id }
    sequence(:email) { |n| "test#{n}@childfoot-lab.jp" }
    password "password"

    gender { rand(1..2) }
    nickname { "Nick name#{id}"}

    banned false
#    encrypted_password { User.new(:password => "password").encrypted_password }
    created_at { Time.now }
    updated_at { Time.now }


    trait :banned do
      banned true
    end

  end
end
