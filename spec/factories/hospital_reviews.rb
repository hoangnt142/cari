FactoryBot.define do
  factory :hospital_review do

    sequence(:id) {|id| id }

#    nickname
#    email
    working_type "Working type"
    point_recommends { rand(3..5) }
    point_career { rand(3..5) }
    point_environment { rand(3..5) }
    text_recommends "Recommend"
    text_career "Career"
    text_environment "Environment"
    useful_count { rand(1..30) }
    title "Title"
    secret false
    approved true

    created_at { Time.now }
    updated_at { Time.now }


    trait :secret do
      secret true

      after(:build) do |hr|
        hr.title = "Secret: #{hr.title}"
      end
    end

    trait :not_approved do
      approved false

      after(:build) do |hr|
        hr.title = "Not apprved: #{hr.title}"
      end
    end

    after(:build) do |hr|
      hr.average_point = (hr.point_recommends + hr.point_career + hr.point_environment) / 3.0
      hr.nickname = hr.user.nickname
      hr.email = hr.user.email
    end

  end
end
