FactoryBot.define do
  factory :hellowork do

    sequence(:id) {|id| id }
    sequence(:name) { |n| "Hello work #{n}" }
    sequence(:prefecture_id) { |n| n%47 + 1 }

    sequence(:city_id) { |n| City.where(prefecture_id: n%47 + 1).take.id }
    sequence(:address) { |n| "Address #{n}" }
    sequence(:postal_code) { |n| "#{n}".ljust(7, '0') }
    phone "043-312-6571"
    fax "043-312-6571"
    business_time "平日 10:00 - 18:00"
    detail "detail"
    url "http://childfoot.jp"
    longitude 35.662
    latitude 140.0418

    created_at { Time.now }
    updated_at { Time.now }

  end
end
