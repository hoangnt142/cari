FactoryBot.define do
  factory :information do

    sequence(:id) {|id| id }

    sequence(:show_on_top_page) { |n| n%2 }
    sequence(:information_date) { |n| n.days.ago }
    sequence(:title) { |n| "Title #{n}" }
    sequence(:content) { |n| "Content#{n}" }

    created_at { Time.now }
    updated_at { Time.now }

    after(:build) do |information|
      unless information.show_on_top_page
        information.title = "Hide: #{information.title}"
      end
    end

  end
end
