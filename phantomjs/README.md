When executing the command below,
If it shows any error, Google won't render the view correctly.

```
phantomjs capture.js
```

When success, simply the message below will be shown.

```
Status: success
```
