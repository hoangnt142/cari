env :PATH, ENV['PATH']
set :rbenv, '"$HOME/.rbenv/shims":"$HOME/.rbenv/bin"'
set :output, 'log/whenever.log'
set :bundle_command, "/home/childfoot/.rbenv/shims/bundler exec"

#job_type :rake,   'cd :path && PATH=:rbenv:"$PATH" :environment_variable=:environment bundle exec rake :task --silent :output'
#job_type :runner, 'cd :path && PATH=:rbenv:"$PATH" bin/rails runner -e :environment ":task" :output'


# config/schedule.rb
every 1.day, :at => '5:00 am' do
  rake "-s sitemap:refresh"
end
