require 'rubygems'
require 'sitemap_generator'

SitemapGenerator::Sitemap.default_host = 'https://www.carriee.jp'
#SitemapGenerator::Sitemap.compress = false
SitemapGenerator::Sitemap.include_root = false
SitemapGenerator::Sitemap.create do

  Page.where(publish_status: 1).find_each do |page|
    priority = page.id == 1 ? 1.0 : 0.9
    add page.url, changefreq: 'daily', :priority => priority
  end

end
