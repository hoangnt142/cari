const { environment } = require('@rails/webpacker')
const webpack = require('webpack')
const dotenv = require('dotenv')
// Remove unnecessary loaders
//environment.loaders.delete('coffee')
//environment.loaders.delete('elm')
//environment.loaders.delete('typescript')
//environment.loaders.delete('vue')
//environment.loaders.delete('file')
//environment.loaders.delete('style')

const dotenvFiles = [
  '.env'
]

dotenvFiles.forEach((dotenvFile) => {
  dotenv.config({ path: dotenvFile, silent: true })
})
//environment.plugins.set('Environment', new webpack.EnvironmentPlugin(JSON.parse(JSON.stringify(process.env))))

const fileLoader = environment.loaders.get('file')
fileLoader.test = /\.(jpg|jpeg|png|gif|svg|eot|otf|ttf|woff|woff2)$/i
fileLoader.use = [
  // Embedding data directly in the page with 'url-loader' when size of an image is samller
  // It can reduce the number of requests.
  // And use 'file-loader' as normally when the size is larger than the limit.
  { loader: 'url-loader',
    options: {
      //        limit: 300000,
      limit: 80000,
      fallback: 'file-loader'
    }
  }
]

module.exports = environment
