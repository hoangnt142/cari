const webpack = require('webpack')
const environment = require('./environment')
const ExtractTextPlugin = require("extract-text-webpack-plugin")


environment.loaders.delete('css')
environment.loaders.append('css', {
  test: /\.css$/,
  exclude: /semantic\.min\.css/,
  use: ExtractTextPlugin.extract({
    fallback: 'style-loader',
    use: {
      loader: 'css-loader',
      options: {
        modules: true,
        localIdentName: "[path]___[name]__[local]___[hash:base64:5]"
      }
    }
  })
})
environment.loaders.append('css2', {
  test: /semantic\.min\.css/,
  use: ExtractTextPlugin.extract({
    fallback: 'style-loader',
    use: [
      {
        loader: 'css-loader',
        options: {
          minimize: true
        }
      }
    ]
  })
})

environment.loaders.delete('sass')
environment.loaders.append('scss', {
  test: /\.scss$/,
  exclude: /semantic\.min\.css/,
  use: ExtractTextPlugin.extract({
    fallback: "style-loader",
    use: [
      {
        loader: "css-loader",
        options: {
          minimize: true
        }
      },
      {
        loader: "sass-loader"
      }
    ]
  })
})
environment.plugins.delete('ExtractText')
environment.plugins.append('ExtractText',
                        new ExtractTextPlugin({
                          filename: "style-bundle-[contenthash].css",
                          allChunks: true
                        }))

environment.plugins.append('CommonsChunkPlugin1',
                        new webpack.optimize.CommonsChunkPlugin({
                          name: "commons",
                        }))
environment.plugins.append('CommonsChunkPlugin2',
                        new webpack.optimize.CommonsChunkPlugin({
                          children: true,
                          async: true,
                          minChunks: 2, // the least number of chunks reusing a module before module can be exported
                        }))
environment.plugins.delete('UglifyJs')
environment.plugins.append('UglifyJs',
                        new webpack.optimize.UglifyJsPlugin({
                          sourceMap: true,
                          comments: false,
                          compress: {
                            warnings: false,
                            drop_console: true
                          },
                          output: { comments: false }
                        }))

const config = environment.toWebpackConfig()
console.log(config)
module.exports = config
//module.exports = environment.toWebpackConfig()
