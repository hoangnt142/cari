const environment = require('./environment')

environment.loaders.delete('css')
environment.loaders.append('semantic', {
  test: /semantic\.min\.css/,
  use: ['style-loader', 'css-loader']
})

environment.loaders.delete('sass')
environment.loaders.append('scss', {

  test: /\.scss$/,
  use: [
    {
      loader: "style-loader", // creates style nodes from JS strings
      options: {
        sourceMap: true
      }
    },
    {
      loader: "css-loader" // translates CSS into CommonJS
    }, {
      loader: "sass-loader" // compiles Sass to CSS
    }
  ]
})

/*
   environment.plugins.set('Chunk',
   new webpack.optimize.CommonsChunkPlugin({
   name: 'common' // Specify the common bundle's name.
   }));
 */

console.log(environment.loaders);
//console.log(environment.plugins);
//console.log(environment);

const config = environment.toWebpackConfig()
console.log(config)
module.exports = config
