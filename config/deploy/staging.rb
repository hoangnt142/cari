set :stage, :production
set :rails_env, :production

server 'git.childfoot-lab.jp', user: 'childfoot', roles: %w{web app db}, primary: true, port: 10004

set :default_env, {
  'NODE_ENV': 'production',
}


namespace :slack do
  desc 'Notify update of the server'
  task :notify do
    on roles(:app) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, 'slack:notify'
        end
      end
    end
  end
end

#after 'deploy:finished', 'slack:notify'
after 'deploy:finished', 'sitemap:refresh:no_ping'
