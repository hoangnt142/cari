Rails.application.routes.draw do

  # API
  namespace :api do

    namespace :admin do
      # devise_for :users,
      #             controllers: {sessions: 'api/admin/sessions'},
      #             path_names: {
      #               sign_in: 'sign-in'
      #             }


      devise_scope :user do
        post 'sign-in' => 'sessions#create', :as => :login
        get 'sign-out' => 'sessions#destroy', :as => :logout
      end
      resources :pages, only: [:index, :show, :new, :create, :destroy, :update]
      resources :hospitals, only: [:index, :show, :new, :create, :destroy, :update]
      resources :categories, only: [:index, :show, :new, :create, :destroy, :update]
      resources :articles, only: [:index, :show, :new, :create, :destroy, :update]
      resources :images, only: [:index, :show, :new, :create, :destroy, :update]
      resources :ctas, only: [:index, :show, :new, :create, :destroy, :update]
      resources :system_configs, only: [:show, :create]
    end
    
    ## Cacheable APIs
    scope :cache do

      get 'pages', to: 'pages#index'

      resources :prefectures # TODO: restrict only to index, show
      resources :cities # TODO: restrict only to index, show
      resources :areas do
        member do
          get 'with_ancestors'
        end
        collection do
          get 'area123_by_prefecture_ids'
        end
      end # index, show
      resources :search_keys # index

      resources :widgets, only: [:index]
      resources :contacts, only: [:new, :create]

      resources :hospitals do
        collection do
          get 'count'
          post 'favorite-jobs' => 'hospitals#favorite_jobs'
          post 'related-jobs' => 'hospitals#related_jobs'
        end
      end

      scope :pages do
        get '*path', to: 'pages#index'
      end

    end
  end

  scope :api do
    ## Uncacheable APIs
    scope :uncache do
      # TODO: Commented to hide the URLs before releasing the features of signing in
      devise_for :users,
                 controllers: {
                   sessions: 'api/users/sessions',
                   passwords: 'api/users/passwords',
                   registrations: 'api/users/registrations',
                   omniauth_callbacks: 'api/users/omniauth_callbacks'
                 },
                 path_names: {
                   sign_in: 'sign-in',
                   sign_up: 'sign-up',
                   sign_out: 'sign-out',
                   edit: 'edit-password'
                 }
      # devise_scope :user do
      #   get 'check-email-exist' => 'api/users/registrations#check_email_exist'
      # end
    end
  end

  get 'rss', to: 'rss#index'
  get 'redirect-links/:website_id' => 'redirect_links#index', as: :redirect_links

  root 'main#index'

  get '/admin', to: 'admin#index' #admin root
  get '/admin/*path', to: 'admin#index'
  # All of remained URLs are handled by this controller.
  get '*pages', to: 'main#index'

end
