# config valid for current version and patch releases of Capistrano
# lock "~> 3.10.0"

set :application, 'carriee-web'
set :repo_url, 'git@git.childfoot-lab.jp:peko/carriee-web.git'

set :rails_env, 'production'
set :migration_role, :app
set :migration_servers, -> { primary(fetch(:migration_role)) }
set :conditionally_migrate, true
set :assets_roles, [:web, :app]

set :rbenv_type, :user
set :rbenv_ruby, File.read('.ruby-version').strip
set :rbenv_prefix, "RBENV_ROOT=#{fetch(:rbenv_path)} RBENV_VERSION=#{fetch(:rbenv_ruby)} #{fetch(:rbenv_path)}/bin/rbenv exec"
set :rbenv_map_bins, %w{rake gem bundle ruby rails}
append :rbenv_map_bins, 'puma', 'pumactl'
set :rbenv_roles, :all # default value

set :puma_init_active_record, true


# Default branch is :master
ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, "/var/www/#{fetch(:application)}"

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: "log/capistrano.log", color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
append :linked_files,
       "config/application.yml",
       "config/database.yml",
       "config/local_env.yml",
       "config/puma.rb",
       ".env"

# Default value for linked_dirs is []
append :linked_dirs,
       ".bundle",
       "client/node_modules",
       "log",
       "public/system",
       "public/upload_images",
       "tmp/cache",
       "tmp/pids",
       "tmp/sockets",
       "vendor/bundle"

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

namespace :react_on_rails do
  task :assets do
    on roles(:app) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute "cd #{release_path} && yarn install && sh -c 'rm -rf public/packs/* || true'"
          execute :rake, 'react_on_rails:locale'
        end
      end
    end
  end
end
# after 'deploy:published', 'delayed_job:restart' do
#   invoke 'delayed_job:restart'
# end

before 'deploy:assets:precompile', 'react_on_rails:assets'
