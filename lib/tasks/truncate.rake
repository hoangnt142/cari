namespace :db do
  desc "Truncate all tables"
  task :truncate => :environment do
    conn = ActiveRecord::Base.connection
    conn.execute "SET FOREIGN_KEY_CHECKS=0"
    tables = conn.execute("show tables").map { |r| r[0] }
    tables.delete "schema_migrations"
    tables.each { |t| conn.execute("TRUNCATE #{t}") }
    conn.execute "SET FOREIGN_KEY_CHECKS=1"
  end
end
