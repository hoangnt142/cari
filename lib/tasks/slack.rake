namespace :slack do
  desc "Send notification to Slack"
  task :notify => :environment do

    notifier = Slack::Notifier.new('https://hooks.slack.com/services/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')
    notifier.ping('更新が完了しました。')

  end
end
