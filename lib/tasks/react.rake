namespace :react do
  namespace :generate do

    desc "Generate all"
    task :all => :environment do
      params = ARGV
      force = false
      unless params.index('f').nil?
        force = true
        params.delete_at(params.index('f'))
      end

      unless params.length == 2
        puts "rails #{params[0]} ComponentName"
        abort
      end
      g = ReactGenerator.new(params[1], force)
      g.write :all
      exit
    end

    desc "Generate component"
    task :component => :environment do
      unless ARGV.length == 2
        puts "rails #{ARGV[0]} ComponentName"
        abort
      end
      g = ReactGenerator.new(ARGV[1])
      g.write :component
      exit
    end

    desc "Generate container"
    task :container => :environment do
      unless ARGV.length == 2
        puts "rails #{ARGV[0]} ComponentName"
        abort
      end
      g = ReactGenerator.new(ARGV[1])
      g.write :container
      exit
    end

    desc "Generate reducer"
    task :reducer => :environment do
      unless ARGV.length == 2
        puts "rails #{ARGV[0]} ComponentName"
        abort
      end
      g = ReactGenerator.new(ARGV[1])
      g.write :reducer
      exit
    end

    desc "Generate action"
    task :action => :environment do
      unless ARGV.length == 2
        puts "rails #{ARGV[0]} ComponentName"
        abort
      end
      g = ReactGenerator.new(ARGV[1])
      g.write :action
      exit
    end
  end
end






require 'active_support'
class ReactGenerator

  @@path = "#{Rails.root}/app/javascript/bundles/App"

  def initialize(component_name, overwrite=true)
    @component_name = component_name
    @comp_downcase = "#{component_name[0].downcase}#{component_name[1..-1]}"
    @overwrite = overwrite
  end

  def action(comp)

    constant = comp.underscore.upcase

    <<FILE
import { #{constant}_RESET } from '../constants/constants'

export const reset = () => {
  return {
    type: #{constant}_RESET,
  }
}
FILE
  end


  def reducer(comp)

    comp_downcase = "#{comp[0].downcase}#{comp[1..-1]}"
    constant = comp.underscore.upcase

    <<FILE

//import { #{constant}_RESET } from '../constants/constants'

const initialState = {}

const #{comp_downcase}Reducer = (state = initialState, action) => {
  switch (action.type) {
/*
    case #{constant}_RESET:
      return Object.assign({}, initialState)
*/
    default:
      return state
  }
}

export default #{comp_downcase}Reducer
FILE
  end

  def component(comp)
    comp_underscore = comp.underscore

    <<FILE
import React, { PureComponent } from 'react'
import PropType from 'prop-types'
import { Link } from 'react-router-dom'

import {
  Button, Image, Icon,
  Search, Modal, Segment, Sticky,
  Header,
  Item, Loader, Dimmer,
  Responsive,
  Container
} from 'semantic-ui-react'

import PageTitle from './PageTitle'


export default class #{comp} extends PureComponent {

  static propTypes = {
  }
  static defaultProps = {
  }

  constructor(props) {
    super(props)

    const { fetchPage } = this.props
    fetchPage()
  }

  componentDidMount() {
    const { fetch } = this.props
    // Sub data should be gotten after being mounted.
    //fetch('/api/cache/pages/1/sub')
  }

  componentWillUnmount() {
    const { reset } = this.props
    // Resets the state as initial condition even when coming back from the other page.
    reset()
  }

  render() {
    const { api, page } = this.props
    const fetchingPage = api.fetchingURLs.indexOf('/api/cache/pages/1/sub') >= 0

    return (
      <div>
        <PageTitle
          title='#{comp}'
          page={page}
          breadCrumbs={[
            { title: '看護師求人サイトCarriee', url: '/' },
            { title: '#{comp}', url: '/#{comp_underscore}' },
          ]}
          showLastBreadCrumbs={false}
        />

        <Container>
          <div>
            <Segment style={{ paddingTop: '50px', paddingBottom: '50px' }}>
              #{comp}
            </Segment>
          </div>
        </Container>

      </div>
    )
  }
}
FILE
  end



  def container(comp)
    action = "#{comp[0].downcase}#{comp[1..-1]}Actions"

    <<FILE
import { connect } from 'react-redux'
import #{comp} from '../components/#{comp}'
import * as actions from '../actions/#{action}'
import { fetch, fetchPage } from '../actions/apiActions'
import * as userActions from '../actions/userActions'

const mapStateToProps = (state) => ({
  api: state.api,
  page: state.api.page || {},
  user: state.user,
  #{action}: state.#{action},
})

export default connect(mapStateToProps, Object.assign(userActions, actions, { fetch, fetchPage }))(#{comp})
FILE
  end

  # write :all
  # write :component
  # write :container
  # write :reducer
  # write :action
  def write(target)
    return unless target.is_a?(Symbol)
    send("write_#{target}")
  end

  private
  def write_all
    paths = []
    paths << write_container
    paths << write_component
    paths << write_reducer
    paths << write_action
    paths
  end

  def write_file(path, content)
    if File.exist?(path)
      unless @overwrite
        puts "File: `#{path}` already exist."
        return false
      else
        puts "File: `#{path}` already exist. overwrite."
      end
    end
    File.open(path, 'w') do |file|
      file.write(content)
      puts "Wrote #{path}"
    end
    path
  end

  def write_container
    container_file_name = "#{@component_name}Container.jsx"
    path = "#{@@path}/containers/#{container_file_name}"
    return write_file(path, container(@component_name))
  end

  def write_component
    component_file_name = "#{@component_name}.jsx"
    path = "#{@@path}/components/#{component_file_name}"
    return write_file(path, component(@component_name))
  end

  def write_reducer
    reducer_file_name = "#{@comp_downcase}Reducer.js"
    path = "#{@@path}/reducers/#{reducer_file_name}"
    return write_file(path, reducer(@component_name))
  end

  def write_action
    action_file_name = "#{@comp_downcase}Actions.js"
    path = "#{@@path}/actions/#{action_file_name}"
    return write_file(path, action(@component_name))
  end
end
