require 'yaml'
require 'rake'
require 'parallel'
require 'rspec/core/rake_task'

RSpec::Core::RakeTask.new(:single) do |t|
  ENV['CONFIG_NAME'] = "browserstack_single"
  t.verbose = false
end

task :default => :single


task :parallel do |t, args|

  config_name = "browserstack_parallel"
  config = YAML.load(File.read(File.join(File.dirname(__FILE__), "../../config/#{config_name}.yml")))
  @num_parallel = config['browser_caps'].size

  Parallel.map([*1..@num_parallel], :in_processes => @num_parallel) do |task_id|
    ENV["TASK_ID"] = (task_id - 1).to_s
    ENV['name'] = "parallel_test"
    ENV['CONFIG_NAME'] = config_name

    Rake::Task["single"].invoke
    Rake::Task["single"].reenable
  end
end




namespace :spec do
  desc "Insert data from a file in spec/data/"
  task :data => :environment do

    if ARGV.size != 3
      abort "#{ARGV[0]} (file) (method)"
    end

    underscore_text = ARGV[1].sub(/\..*/, '')
    class_name = underscore_text.camelize
    file =  "#{underscore_text}.rb"

    require "#{Rails.root}/spec/data/#{file}"
    test = eval(class_name).new


    Rake::Task["db:truncate"].execute
    Rake::Task["db:seed"].execute

    test.send(ARGV[2])
    exit
  end
end
