# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)



# 1. create cities.sql
require_relative "data/cities.rb"
require 'shell'

# 2. load data/*.sql
sql_files = (
  Dir.entries(File.dirname(__FILE__)+"/data").select do |item|
    item =~ /.*\.sql/
  end).sort


ActiveRecord::Base.connection.execute "SET FOREIGN_KEY_CHECKS=0"
sql_files.each do |file_name|
  file = File.open(File.dirname(__FILE__)+"/data/#{file_name}")
  ActiveRecord::Base.connection.execute file.read
  puts "data/#{file_name} has been loaded"
end
ActiveRecord::Base.connection.execute "SET FOREIGN_KEY_CHECKS=1"


# 3. websites

Website.create(id: 1, name: '看護のお仕事', url: 'https://kango-oshigoto.jp/', version: 1, redirect_url: '')
Website.create(id: 2, name: 'マイナビ看護師', url: 'https://kango.mynavi.jp/', version: 1, redirect_url: '')
Website.create(id: 3, name: 'ナース人材バンク', url: 'https://www.nursejinzaibank.com/', version: 1, redirect_url: '')
Website.create(id: 4, name: 'ハローワーク', url: 'https://www.hellowork.go.jp/servicef/130020.do', version: 1, redirect_url: '')



# 4. Search keys

Qualification.create(name: '正看護師', name_roman: '', match_text: '', description: '')
Qualification.create(name: '准看護師', name_roman: '', match_text: '', description: '')
Qualification.create(name: '助産師', name_roman: '', match_text: '', description: '')
Qualification.create(name: '保健師', name_roman: '', match_text: '', description: '')
Qualification.create(name: 'ケアマネジャー（介護支援専門員）', name_roman: '', match_text: '', description: '')
JobContract.create(name: '常勤(夜勤あり)', name_roman: '', match_text: '', description: '')
JobContract.create(name: '日勤常勤（夜勤なし）', name_roman: '', match_text: '', description: '')
JobContract.create(name: '非常勤・パート・アルバイト', name_roman: '', match_text: '', description: '')
JobContract.create(name: '夜勤専従', name_roman: '', match_text: '', description: '')
HospitalType.create(name: '病院', name_roman: '', match_text: '', description: '')
HospitalType.create(name: '急性期病院', name_roman: '', match_text: '', description: '')
HospitalType.create(name: 'ケアミックス型病院', name_roman: '', match_text: '', description: '')
HospitalType.create(name: '療養型病院', name_roman: '', match_text: '', description: '')
HospitalType.create(name: '精神科病院・精神科クリニック', name_roman: '', match_text: '', description: '')
HospitalType.create(name: '健診センター', name_roman: '', match_text: '', description: '')
HospitalType.create(name: 'クリニック', name_roman: '', match_text: '', description: '')
HospitalType.create(name: '美容外科・美容皮膚科', name_roman: '', match_text: '', description: '')
HospitalType.create(name: 'リハビリテーション病院', name_roman: '', match_text: '', description: '')
HospitalType.create(name: '訪問看護', name_roman: '', match_text: '', description: '')
HospitalType.create(name: '企業', name_roman: '', match_text: '', description: '')
HospitalType.create(name: '学校', name_roman: '', match_text: '', description: '')
HospitalType.create(name: '保育園', name_roman: '', match_text: '', description: '')
JobType.create(name: '病棟', name_roman: '', match_text: '', description: '')
JobType.create(name: '外来', name_roman: '', match_text: '', description: '')
JobType.create(name: '手術室（オペ室）', name_roman: '', match_text: '', description: '')
JobType.create(name: '救急外来', name_roman: '', match_text: '', description: '')
JobType.create(name: '透析室', name_roman: '', match_text: '', description: '')
JobType.create(name: '内視鏡室', name_roman: '', match_text: '', description: '')
JobType.create(name: '夜勤専従の看護師求人', name_roman: '', match_text: '', description: '')
JobType.create(name: 'ICU', name_roman: '', match_text: '', description: '')
JobDetail.create(name: '2交代制', name_roman: '', match_text: '', description: '')
JobDetail.create(name: '3交代制', name_roman: '', match_text: '', description: '')
JobDetail.create(name: '4週8休以上', name_roman: '', match_text: '', description: '')
JobDetail.create(name: '土日祝休み', name_roman: '', match_text: '', description: '')
JobDetail.create(name: '高収入', name_roman: '', match_text: '', description: '')
JobDetail.create(name: '住宅手当・寮あり', name_roman: '', match_text: '', description: '')
JobDetail.create(name: 'ブランク可・未経験歓迎', name_roman: '', match_text: '', description: '')
JobDetail.create(name: '託児所あり', name_roman: '', match_text: '', description: '')
JobDetail.create(name: '車通勤可', name_roman: '', match_text: '', description: '')
JobDetail.create(name: '駅近', name_roman: '', match_text: '', description: '')
JobDetail.create(name: '教育充実', name_roman: '', match_text: '', description: '')
JobDetail.create(name: '電子カルテ', name_roman: '', match_text: '', description: '')
JobDetail.create(name: '資格取得支援あり', name_roman: 'shikakusyutokushien-ari', match_text: '', description: '')
JobDetail.create(name: '産休・育休実績あり', name_roman: 'sankyuu-ikukyuu-jisekki-ari', match_text: '', description: '')
JobDetail.create(name: '残業少なめ', name_roman: 'zangyou-sukuname', match_text: '', description: '')
JobDetail.create(name: '離職率低い', name_roman: 'rishokuritsu-hikui', match_text: '', description: '')
JobDetail.create(name: '退職金あり', name_roman: 'taishokukin-ari', match_text: '', description: '')
JobDetail.create(name: '新卒歓迎', name_roman: 'shinsotsu-kangei', match_text: '', description: '')

sh = Shell.new
SearchKey.all.each do |sk|
  name = sk.name.gsub(/[（）\(\)・ー]/, '').downcase
  roman = sh.transact { system("echo", name) | system("kakasi", "-Ja", "-Ha", "-Ka", "-Ea", "-i", "utf-8", "-o", "utf-8") }
  roman = roman.to_s.sub(/\n/,'')
  sk.name_roman = roman
  sk.save
end




# 5. system config
SystemConfig.create(show_information: true)



# 6. pages
Page.create(id: 1, name: 'トップ', predefined: 1,
            url: '/', url_slug: '/',
            seo_title: nil, seo_description: nil, seo_keywords: nil,
            seo_robot_noindex: false, seo_robot_nofollow: false)
Page.create(id: 2, name: 'お知らせ', predefined: 1,
            url: '/information', url_slug: '/information',
            seo_title: '', seo_description: '', seo_keywords: '',
            seo_robot_noindex: false, seo_robot_nofollow: false)
Page.create(id: 3, name: 'エージェント比較データ', predefined: 1,
            url: '/agents', url_slug: '/agents',
            seo_title: nil, seo_description: nil, seo_keywords: nil,
            seo_robot_noindex: false, seo_robot_nofollow: false)
Page.create(id: 4, name: '口コミトップ', predefined: 1,
            url: '/reputes', url_slug: '/reputes',
            seo_title: nil, seo_description: nil, seo_keywords: nil,
            seo_robot_noindex: false, seo_robot_nofollow: false)
Page.create(id: 5, name: 'ハローワーク一覧', predefined: 1,
            url: '/hello-work-list', url_slug: '/hello-work-list',
            seo_title: nil, seo_description: nil, seo_keywords: nil,
            seo_robot_noindex: false, seo_robot_nofollow: false)
Page.create(id: 8, name: '病院の口コミ投稿(5ページ構成、スライド式)', predefined: 1,
            url: '/repute-posting', url_slug: '/repute-posting',
            seo_title: '', seo_description: '', seo_keywords: '',
            seo_robot_noindex: false, seo_robot_nofollow: false)
Page.create(id: 9, name: '病院の口コミ投稿完了', predefined: 1,
            url: '/repute-posted', url_slug: '/repute-posted',
            seo_title: '', seo_description: nil, seo_keywords: nil,
            seo_robot_noindex: true, seo_robot_nofollow: true)
Page.create(id: 10, name: 'ブログ', predefined: 1,
            url: '/blog', url_slug: '/blog',
            seo_title: '', seo_description: '', seo_keywords: '',
            seo_robot_noindex: true, seo_robot_nofollow: true)
Page.create(id: 14, name: 'お問い合わせ', predefined: 1,
            url: '/contact', url_slug: '/contact',
            seo_title: '', seo_description: '', seo_keywords: '',
            seo_robot_noindex: false, seo_robot_nofollow: false)
Page.create(id: 15, name: 'お問い合わせ完了', predefined: 1,
            url: '/contact-sent', url_slug: '/contact-sent',
            seo_title: '', seo_description: nil, seo_keywords: nil,
            seo_robot_noindex: true, seo_robot_nofollow: true)
# Page.create(id: 16, name: 'Page not found 404', predefined: 1,
#             url: nil, url_slug: nil,
#             seo_title: '', seo_description: '', seo_keywords: '',
#             seo_robot_noindex: false, seo_robot_nofollow: false)
Page.create(id: 17, name: 'サイトマップ', predefined: 1,
            url: '/sitemap', url_slug: '/sitemap',
            seo_title: '', seo_description: '', seo_keywords: '',
            seo_robot_noindex: false, seo_robot_nofollow: false)
Page.create(id: 18, name: '採用ご担当者の方へ問い合わせフォーム', predefined: 1,
            url: '/contact-for-recruiting-manager', url_slug: '/contact-for-recruiting-manager',
            seo_title: '', seo_description: '', seo_keywords: '',
            seo_robot_noindex: false, seo_robot_nofollow: false)
Page.create(id: 101, name: '最近チェックした求人', predefined: 1,
            url: '/my-history', url_slug: '/my-history',
            seo_title: '', seo_description: nil, seo_keywords: nil,
            seo_robot_noindex: true, seo_robot_nofollow: true)
Page.create(id: 102, name: '保存した求人', predefined: 1,
            url: '/my-jobs', url_slug: '/my-jobs',
            seo_title: '', seo_description: nil, seo_keywords: nil,
            seo_robot_noindex: true, seo_robot_nofollow: true)
Page.create(id: 201, name: '会員登録(5ページ構成、スライド式)', predefined: 1,
            url: '/sign-up', url_slug: '/sign-up',
            seo_title: '', seo_description: '', seo_keywords: '',
            seo_robot_noindex: false, seo_robot_nofollow: false)
Page.create(id: 202, name: 'ログイン', predefined: 1,
            url: '/sign-in', url_slug: '/sign-in',
            seo_title: '', seo_description: '', seo_keywords: '',
            seo_robot_noindex: false, seo_robot_nofollow: false)
Page.create(id: 203, name: 'パスワード再発行', predefined: 1,
            url: '/password-generate', url_slug: '/password-generate',
            seo_title: '', seo_description: '', seo_keywords: '',
            seo_robot_noindex: true, seo_robot_nofollow: true)
Page.create(id: 204, name: 'パスワード設定', predefined: 1,
            url: '/password-change', url_slug: '/password-change',
            seo_title: '', seo_description: nil, seo_keywords: nil,
            seo_robot_noindex: true, seo_robot_nofollow: true)
Page.create(id: 301, name: 'マイページ(トップ)', predefined: 1,
            url: '/mypage', url_slug: '/mypage',
            seo_title: '', seo_description: nil, seo_keywords: nil,
            seo_robot_noindex: true, seo_robot_nofollow: true)
Page.create(id: 302, name: 'マイページ(保存した求人)', predefined: 1,
            url: '/mypage/jobs', url_slug: '/mypage/jobs',
            seo_title: '', seo_description: nil, seo_keywords: nil,
            seo_robot_noindex: true, seo_robot_nofollow: true)
Page.create(id: 303, name: 'マイページ(口コミ投稿履歴)', predefined: 1,
            url: '/mypage/my-reputes', url_slug: '/mypage/my-reputes',
            seo_title: '', seo_description: nil, seo_keywords: nil,
            seo_robot_noindex: true, seo_robot_nofollow: true)
Page.create(id: 304, name: 'マイページ(各種設定)', predefined: 1,
            url: '/mypage/settings', url_slug: '/mypage/settings',
            seo_title: '', seo_description: nil, seo_keywords: nil,
            seo_robot_noindex: true, seo_robot_nofollow: true)
Page.create(id: 305, name: 'マイページ(パスワード変更)', predefined: 1,
            url: '/mypage/password-change', url_slug: '/mypage/password-change',
            seo_title: '', seo_description: nil, seo_keywords: nil,
            seo_robot_noindex: true, seo_robot_nofollow: true)
Page.create(id: 401, name: '個人情報取り扱い', predefined: 1,
            url: '/privacy-policy', url_slug: '/privacy-policy',
            seo_title: '', seo_description: '', seo_keywords: '',
            seo_robot_noindex: false, seo_robot_nofollow: false)
Page.create(id: 402, name: '会員規約', predefined: 1,
            url: '/terms-of-service', url_slug: '/terms-of-service',
            seo_title: '', seo_description: '', seo_keywords: '',
            seo_robot_noindex: false, seo_robot_nofollow: false)
Page.create(id: 403, name: '利用規約', predefined: 1,
            url: '/terms-of-use', url_slug: '/terms-of-use',
            seo_title: '', seo_description: '', seo_keywords: '',
            seo_robot_noindex: false, seo_robot_nofollow: false)
Page.create(id: 404, name: '初めての方へ', predefined: 1,
            url: '/for-beginner', url_slug: '/for-beginner',
            seo_title: '', seo_description: '', seo_keywords: '',
            seo_robot_noindex: false, seo_robot_nofollow: false)
Page.create(id: 405, name: '会社概要', predefined: 1,
            url: '/company', url_slug: '/company',
            seo_title: '', seo_description: '', seo_keywords: '',
            seo_robot_noindex: false, seo_robot_nofollow: false)
Page.create(id: 406, name: '採用ご担当者の方へ', predefined: 1,
            url: '/採用ご担当者の方へ', url_slug: '/採用ご担当者の方へ',
            seo_title: '', seo_description: '', seo_keywords: '',
            seo_robot_noindex: false, seo_robot_nofollow: false)

Page.create(id: 501, name: '検索', predefined: 1,
            pageable_id: 1, pageable_type: 'Search',
            url: '/search', url_slug: '/search',
            seo_title: nil, seo_description: nil, seo_keywords: nil,
            seo_robot_noindex: false, seo_robot_nofollow: false)



# 7. search

## Data patterns
# Prefectures
#   a. /(prefecture)

# Prefectures
# Areas1,2,3
#   b. /(prefecture)/(area1)
#   c. /(prefecture)/(area1)/(area2)
#   d. /(prefecture)/(area1)/(area2)/(area3)

# Prefectures, なし
# Areas, なし
# SearchKey(Qualifications, JobContract, HospitalTypes, JobTypes, JobDetails)
#   e. /(search_key)
#   f. /(prefecture)/(search_key)
#   g. /(prefecture)/(area1)/(search_key)
#   h. /(prefecture)/(area1)/(area2)/(search_key)
#   i. /(prefecture)/(area1)/(area2)/(area3)/(search_key)

class SearchData
  def initialize(*objects)
    objects.each do |o|
      case o.class.name
      when "Prefecture"
        @prefecture = o
      when "Area"
        @area = o
      when "Qualification"
        @qualification = o
      when "JobContract"
        @job_contract = o
      when "HospitalType"
        @hospital_type = o
      when "JobType"
        @job_type = o
      when "JobDetail"
        @job_detail = o
      end
    end
    build
  end

  def title
    @title
  end
  def url
    @url
  end

  def create
    attrs = {}
    attrs[:prefecture_id] = @prefecture.id if @prefecture
    attrs[:area_id] = @area.id if @area
    attrs[:qualification_id] = @qualification.id if @qualification
    attrs[:job_contract_id] = @job_contract.id if @job_contract
    attrs[:hospital_type_id] = @hospital_type.id if @hospital_type
    attrs[:job_type_id] = @job_type.id if @job_type
    attrs[:job_detail_id] = @job_detail.id if @job_detail

    search = Search.create(attrs)
    search.create_page(name: @title,
                       title: @title,
                       predefined: true,
                       url: @url,
                       created_at: Time.now,
                       updated_at: Time.now)
  end

  private
  def build
    @title = ""
    @url = ""

    if @prefecture
      @title += @prefecture.name
      @url += "/#{@prefecture.name_roman}"
    end
    if @area
      areas = [@area]
      a = @area.parent
      while a
        areas << a
        a = a.parent
      end
      areas.reverse.each do |area|
        @title += area.name
        @url += "/#{area.name_roman}"
      end
    end
    if @job_contract
      @title += "で#{@job_contract.name}"
      @url += "/#{@job_contract.name_roman}"
    end
    if @hospital_type
      @title += "で#{@hospital_type.name}"
      @url += "/#{@hospital_type.name_roman}"
    end
    if @job_type
      @title += "で#{@job_type.name}"
      @url += "/#{@job_type.name_roman}"
    end
    if @job_detail
      @title += "で#{@job_detail.name}"
      @url += "/#{@job_detail.name_roman}"
    end
    if @qualification
      @title += "の#{@qualification.name}求人・転職"
      @url += "/#{@qualification.name_roman}"
    else
      @title += "の看護師求人・転職"
    end

  end
end



Prefecture.all.each do |prefecture|
  puts "Creating search for prefecture: #{prefecture.id}"
  #   a. /(prefecture)
  SearchData.new(prefecture).create

  #   f. /(prefecture)/(search_key)
  %w(Qualification JobContract HospitalType JobType JobDetail).each do |search_key|
    eval(search_key).all.each do |record|
      next if record.id == 1 # 正看護師 should be skipped
      SearchData.new(prefecture, record).create
    end
  end

  prefecture.areas.each do |area1|

    #   b. /(prefecture)/(area1)
    SearchData.new(prefecture, area1).create

    #   g. /(prefecture)/(area1)/(search_key)
    %w(Qualification JobContract HospitalType JobType JobDetail).each do |search_key|
      eval(search_key).all.each do |record|
        next if record.id == 1 # 正看護師 should be skipped
        SearchData.new(prefecture, area1, record).create
      end
    end

    area1.children.each do |area2|
      #   c. /(prefecture)/(area1)/(area2)
      SearchData.new(prefecture, area2).create

      #   h. /(prefecture)/(area1)/(area2)/(search_key)
      %w(Qualification JobContract HospitalType JobType JobDetail).each do |search_key|
        eval(search_key).all.each do |record|
          next if record.id == 1 # 正看護師 should be skipped
          SearchData.new(prefecture, area2, record).create
        end
      end

      area2.children.each do |area3|
        #   d. /(prefecture)/(area1)/(area2)/(area3)
        SearchData.new(prefecture, area3).create

        #   i. /(prefecture)/(area1)/(area2)/(area3)/(search_key)
        %w(Qualification JobContract HospitalType JobType JobDetail).each do |search_key|
          eval(search_key).all.each do |record|
            next if record.id == 1 # 正看護師 should be skipped
            SearchData.new(prefecture, area3, record).create
          end
        end
      end
    end
  end
end

#   e. /(search_key)
puts "Creating search for no prefecture"
%w(Qualification JobContract HospitalType JobType JobDetail).each do |search_key|
  eval(search_key).all.each do |record|
    next if record.id == 1 # 正看護師 should be skipped
    SearchData.new(record).create
  end
end
