class CreatePrefectures < ActiveRecord::Migration[5.1]
  def change
    create_table :prefectures, id: :integer, unsigned: true, limit: 1  do |t|
      t.integer :region_id, unsigned: true, limit: 1, null: false
      t.string :name, null: false
      t.string :name_roman, null: false
      t.integer :total_jobs, unsigned: true, default: 0, null: false
      t.timestamps
    end

    add_foreign_key :prefectures, :regions, on_delete: :cascade, name: 'fk_prefectures_1'

  end
end
