class CreateHelloworks < ActiveRecord::Migration[5.1]
  def change
    create_table :helloworks, id: :integer, unsigned: true, limit: 2 do |t|

      t.string :name, null: false
      t.integer :prefecture_id, null: false, unsigned: true, limit: 1
      t.integer :city_id, unsigned: true
      t.string :address
      t.string :postal_code
      t.string :phone
      t.string :fax
      t.string :business_time
      t.text :detail
      t.string :url
      t.float :latitude
      t.float :longitude
      t.timestamps
    end

    add_foreign_key :helloworks, :prefectures, on_delete: :cascade, name: 'fk_helloworks_1'
    add_foreign_key :helloworks, :cities, on_delete: :cascade, name: 'fk_helloworks_2'

  end
end
