class AddImageIdToPages < ActiveRecord::Migration[5.1]
  def change
    add_column :pages, :image_id, :bigint, after: :cache_fulltext
    # add_foreign_key :pages, :images, name: "index_pages_on_image_id", on_delete: :cascade
  end
end