class CreateLines < ActiveRecord::Migration[5.1]
  def change
    create_table :lines do |t|
      t.string :name, null: false
      t.string :line_cd, null: false
      t.integer :sort_no, null: false
      t.timestamps
    end
  end
end
