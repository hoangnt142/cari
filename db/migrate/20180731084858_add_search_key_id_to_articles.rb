class AddSearchKeyIdToArticles < ActiveRecord::Migration[5.1]
  def change
    add_reference :articles, :search_key, after: :category_id
  end
end
