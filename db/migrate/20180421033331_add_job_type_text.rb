class AddJobTypeText < ActiveRecord::Migration[5.1]
  def change
    add_column :jobs, :job_type_text, :string, length: 5000, after: :original_url
  end
end
