class CreateWebsites < ActiveRecord::Migration[5.1]
  def change
    create_table :websites, id: :integer, limit: 1, unsigned: true do |t|
      t.string :name, null: false
      t.string :url, null: false
      t.string :redirect_url, null: false
      t.string :support_area, default: nil
      t.integer :version, null: false, default: 1, unsigned: true, limit: 1
      t.text :explanation, default: nil
      t.timestamps
    end
  end
end
