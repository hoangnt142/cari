class AddSalaryType < ActiveRecord::Migration[5.1]
  def change
    add_column :jobs, :salary_type, :tinyint, after: :original_url, comment: '1: 時給, 2: 日給, 3: 月給: 4: 年収'
  end
end
