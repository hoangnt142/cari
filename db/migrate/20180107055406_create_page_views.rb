class CreatePageViews < ActiveRecord::Migration[5.1]
  def change
    create_table :page_views, id: :integer, unsigned: true do |t|

      t.integer :page_id, unsigned: true, null: false
      t.date :year_month, null: false
      t.integer :page_view_count, null: false, default: 0
      t.timestamps

    end
  end
end
