class CreateContacts < ActiveRecord::Migration[5.1]
  def change
    create_table :contacts do |t|
      t.integer :contact_type, null: false
      t.string :company, null: false
      t.string :name, null: false
      t.string :email, null: false
      t.boolean :need_reply, null: false, default: true
      t.text :content, null: false

      t.timestamps
    end
  end
end
