class AddFilledToPages < ActiveRecord::Migration[5.1]
  def change
    add_column :pages, :filled, :boolean, default: false, null: false, after: :cache_fulltext
  end
end
