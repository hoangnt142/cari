class AddForeignKeys < ActiveRecord::Migration[5.1]
  def change
    add_foreign_key :job_search_keys, :jobs, name: 'fk_job_search_keys_1', on_delete: :cascade
    add_foreign_key :job_search_keys, :search_keys, name: 'fk_job_search_keys_2', on_delete: :cascade
    add_foreign_key :hospital_hospital_types, :hospitals, name: 'fk_hospital_hospital_types_1', on_delete: :cascade
  end
end
