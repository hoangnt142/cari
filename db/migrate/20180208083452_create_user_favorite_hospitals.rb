class CreateUserFavoriteHospitals < ActiveRecord::Migration[5.1]
  def change
    create_table :user_favorite_hospitals do |t|
      t.integer :user_id, unsigned: true, null: false
      t.integer :hospital_id, unsigned: true, null: false
      t.timestamps
    end

    add_foreign_key :user_favorite_hospitals, :users, on_delete: :cascade, name: 'fk_user_favorite_hospitals_1'
    add_foreign_key :user_favorite_hospitals, :hospitals, on_delete: :cascade, name: 'fk_user_favorite_hospitals_2'
  end
end
