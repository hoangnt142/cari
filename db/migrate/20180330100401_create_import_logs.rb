class CreateImportLogs < ActiveRecord::Migration[5.1]
  def change
    create_table :import_logs do |t|

      t.datetime :batch_started_at, null: false
      t.datetime :batch_ended_at, null: false
      t.datetime :range_from, null: false
      t.datetime :range_to, null: false

      t.timestamps
    end
  end
end
