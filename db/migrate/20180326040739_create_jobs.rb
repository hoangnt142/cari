class CreateJobs < ActiveRecord::Migration[5.1]
  def change
    create_table :jobs, id: :integer, unsigned: true do |t|

      t.integer :website_id, null: false, unsigned: true, limit: 1
      t.integer :hospital_id, null: false, unsigned: true
      t.string :original_url, null: false

      t.text :salary_detail, default: nil
      t.text :allowance, default: nil
      t.text :work_time, default: nil

      t.timestamps
    end

    add_foreign_key :jobs, :websites, on_delete: :cascade, name: 'fk_jobs_1'
    add_foreign_key :jobs, :hospitals, on_delete: :cascade, name: 'fk_jobs_2'

  end
end
