class CreateHospitalReviews < ActiveRecord::Migration[5.1]
  def change
    create_table :hospital_reviews, id: :integer, unsigned: true do |t|

      t.integer :user_id, unsigned: true, null: false
      t.integer :hospital_id, unsigned: true, null: false
      t.integer :gender, limit: 1, comment: '1: man, 2: woman'
      t.string :nickname, null: false
      t.string :email
      t.string :working_type
      t.integer :point_recommends, null: false, default: 0
      t.integer :point_career, null: false, default: 0
      t.integer :point_environment, null: false, default: 0
      t.text :text_recommends
      t.text :text_career
      t.text :text_environment
      t.float :average_point, null: false, default: 0
      t.integer :useful_count, null: false, default: 0

      t.string :title
      t.boolean :secret, null: false, default: 0
      t.boolean :approved, null: false, default: 0
      t.timestamps
    end

    add_foreign_key :hospital_reviews, :users, name: 'fk_hospital_reviews_1'
    add_foreign_key :hospital_reviews, :hospitals, name: 'fk_hospital_reviews_2'
  end
end
