class UpdateBlogPageStatus < ActiveRecord::Migration[5.1]
  def change
  end

  def data
    blog_page = Page.find(10)
    blog_page.update(publish_status: Page.publish_statuses[:published])
  end
end
