class AddRoleToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :role, :integer, default: 1, null: false, comment: "0: admin, 1: general"
  end
end
