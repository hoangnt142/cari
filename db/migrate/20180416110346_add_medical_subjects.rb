class AddMedicalSubjects < ActiveRecord::Migration[5.1]
  def change
    add_column :hospitals, :medical_subjects, :string, after: :access
  end
end
