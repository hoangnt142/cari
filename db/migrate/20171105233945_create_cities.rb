class CreateCities < ActiveRecord::Migration[5.1]
  def change
    create_table :cities, id: :integer, unsigned: true do |t|
      t.integer :prefecture_id, unsigned: true, limit: 1, null: false
      t.integer :area1_id, unsigned: true, limit: 2, default: nil
      t.integer :area2_id, unsigned: true, limit: 2, default: nil
      t.integer :area3_id, unsigned: true, limit: 2, default: nil
      t.string :name, null: false
      t.integer :status, limit: 1, null: false
      t.integer :total_jobs, unsigned: true, default: 0, null: false

      t.timestamps
    end

    add_foreign_key :cities, :prefectures, on_delete: :cascade, name: 'fk_cities_1'
    add_foreign_key :cities, :areas, column: :area1_id, on_delete: :cascade, name: 'fk_cities_2'
    add_foreign_key :cities, :areas, column: :area2_id, on_delete: :cascade, name: 'fk_cities_3'
    add_foreign_key :cities, :areas, column: :area3_id, on_delete: :cascade, name: 'fk_cities_4'
  end
end
