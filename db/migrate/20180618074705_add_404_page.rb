class Add404Page < ActiveRecord::Migration[5.1]
  def data
    Page.create(
      id: 16,
      title: 'ページが見つかりませんでした',
      seo_title: 'ページが見つかりませんでした',
      url: '/404',
      seo_robot_noindex: false,
      seo_robot_nofollow: false,
      predefined: true,
      publish_status: 1
    )
  end
end
