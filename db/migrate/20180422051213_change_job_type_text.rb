class ChangeJobTypeText < ActiveRecord::Migration[5.1]
  def change
    change_column :jobs, :job_type_text, :text
  end
end
