class CreateJobSearchKeys < ActiveRecord::Migration[5.1]
  def change
    create_table :job_search_keys, unsigned: true do |t|
      t.integer :job_id, unsigned: true, null: false
      t.integer :search_key_id, unsigned: true, limit: 1, null: false
      t.timestamps
    end
  end
end
