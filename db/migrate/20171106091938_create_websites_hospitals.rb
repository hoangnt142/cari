class CreateWebsitesHospitals < ActiveRecord::Migration[5.1]
  def change
    create_table :websites_hospitals, id: :integer, unsigned: true do |t|
      t.integer :website_id, null: false, limit: 1, unsigned: true
      t.integer :hospital_id, null: false, unsigned: true
      t.timestamps
    end

    add_foreign_key :websites_hospitals, :websites, on_delete: :cascade, name: 'fk_websites_hospitals_1'
    add_foreign_key :websites_hospitals, :hospitals, on_delete: :cascade, name: 'fk_websites_hospitals_2'

  end
end
