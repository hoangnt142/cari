class CreateHospitals < ActiveRecord::Migration[5.1]
  def change
    create_table :hospitals, id: :integer, unsigned: true do |t|
      t.integer :version, default: 0, null: false, limit: 3, unsigned: true
      t.string :name, null: false
      t.string :title, null: false

      t.string :number_of_beds, default: nil
      t.string :postal_code, limit: 7, default: nil
      t.integer :prefecture_id, null: false, limit: 1, unsigned: true
      t.integer :city_id, null: false, unsigned: true

      t.string :address, default: nil
      t.string :access, default: nil
      t.string :standard, default: nil
      t.string :karte, default: nil
      t.string :business_time, default: nil
      t.string :holidays, default: nil
      t.string :hospital_url, default: nil
      t.string :features, default: nil
      t.string :phone, default: nil
      t.string :fax, default: nil
      t.string :representative, default: nil

      t.date :established_date, default: nil

      t.float :longitude, default: 0, null: false
      t.float :latitude, default: 0, null: false

      t.integer :review_count, null: false, unsigned: true, default: 0
      t.float :average_point, null: false, unsigned: true, default: 0


      t.boolean :locked, default: 0, null: false, comment: '0: updated automatically, 1: updated manually'
      t.boolean :deleted, default: 0, null: false, comment: '0: not deleted, 1: deleted'

      t.timestamps
    end

    add_foreign_key :hospitals, :prefectures, on_delete: :cascade, name: 'fk_hospitals_1'
    add_foreign_key :hospitals, :cities, on_delete: :cascade, name: 'fk_hospitals_2'

  end
end
