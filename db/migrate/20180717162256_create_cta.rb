class CreateCta < ActiveRecord::Migration[5.1]
  def change
    create_table "ctas", force: :cascade do |t|
      t.string   "title",      limit: 255,   null: false
      t.text     "content",    limit: 65535
      t.datetime "created_at",               null: false
      t.datetime "updated_at",               null: false
    end
  end
end
