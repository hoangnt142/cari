class CreateSearchKeys < ActiveRecord::Migration[5.1]
  def change
    create_table :search_keys, id: :integer, unsigned: true, limit: 1 do |t|
      t.integer :type, comment: '1: Qualification, 2: JobContract, 3: JobType, 4: HospitalType, 5: JobDetail'
      t.string :name, null: false
      t.string :name_roman, null: false
      t.string :match_text, null: false
      t.text :description
      t.timestamps
    end
  end
end
