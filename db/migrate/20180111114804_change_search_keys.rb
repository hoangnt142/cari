class ChangeSearchKeys < ActiveRecord::Migration[5.1]
  def change
    change_column :search_keys, :type, :string, null: false
  end
end
