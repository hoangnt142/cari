class AddSalaryOnJob < ActiveRecord::Migration[5.1]
  def change
    add_column :jobs, :salary, :integer, default: nil, after: :original_url
  end
end
