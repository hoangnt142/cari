class AddUseFieldsOnPages < ActiveRecord::Migration[5.1]
  def change
    add_column :pages, :use_content_top, :boolean, default: 0, null: false, after: :page_date
    add_column :pages, :use_content, :boolean, default: 0, null: false, after: :use_content_top
    add_column :pages, :use_content_bottom, :boolean, default: 0, null: false, after: :use_content
  end

  def data
    # Hospital pages
    Page.where(pageable_type: 'Hospital').update_all(
      use_content_bottom: true
    )

    # Pages that user creates
    Page.where(predefined: 0).where(pageable_type: nil).update_all(
      use_content: true
    )

    Page.where(title: nil).each do |p|
      p.update!(title: p.name)
    end

    # Top page
    Page.find(1).update!(
      use_content_bottom: true
    )
  end

end
