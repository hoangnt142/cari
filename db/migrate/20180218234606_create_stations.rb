class CreateStations < ActiveRecord::Migration[5.1]
  def change
    create_table :stations do |t|
      t.string :name, null: false
      t.integer :line_id, null: false
      t.integer :prefecture_id, null: false, limit: 1, unsigned: true
      t.string :station_cd, null: false
      t.string :station_group_cd
      t.float :longigutue, null: false, limit: 53
      t.float :latitude, null: false, limit: 53
      t.timestamps
    end
  end
end
