class GeneratePageViewsForExistedArticles < ActiveRecord::Migration[5.1]
  def change
  end

  def data
    Article.all.each do |article|
      if article.page_views.blank?
        article.page.page_views.create(year_month: Time.now, page_view_count: 0)
      end
    end
  end
end
