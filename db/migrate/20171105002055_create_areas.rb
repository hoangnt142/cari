class CreateAreas < ActiveRecord::Migration[5.1]
  def change
    create_table :areas, id: :integer, limit: 2, unsigned: true do |t|
      t.integer :prefecture_id, unsigned: true, limit: 1, null: false
      t.integer :parent_id, unsigned: true, limit: 2, default: nil
      t.string :name, null: false
      t.string :name_roman, null: false
      t.integer :level, unsigned: true, limit: 1, null: false
      t.integer :total_jobs, unsigned: true, default: 0, null: false

      t.timestamps
    end

    add_foreign_key :areas, :prefectures, on_delete: :cascade, name: 'fk_areas_1'
    add_foreign_key :areas, :areas, column: :parent_id, on_delete: :cascade, name: 'fk_areas_2'


  end
end
