class SetArea2IdOnCityInTokyo < ActiveRecord::Migration[5.1]
  def data
    execute <<-SQL
UPDATE cities
INNER JOIN areas a ON a.name=cities.name AND a.prefecture_id=13
SET area2_id=a.id
WHERE cities.id BETWEEN 683 AND 708
      SQL
  end
end
