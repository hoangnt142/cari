class AddDataForSeikangoshi < ActiveRecord::Migration[5.1]

  class SearchData
    def initialize(*objects)
      objects.each do |o|
        case o.class.name
        when "Prefecture"
          @prefecture = o
        when "Area"
          @area = o
        when "Qualification"
          @qualification = o
        when "JobContract"
          @job_contract = o
        when "HospitalType"
          @hospital_type = o
        when "JobType"
          @job_type = o
        when "JobDetail"
          @job_detail = o
        end
      end
      build
    end

    def title
      @title
    end
    def url
      @url
    end

    def create
      attrs = {}
      attrs[:prefecture_id] = @prefecture.id if @prefecture
      attrs[:area_id] = @area.id if @area
      attrs[:qualification_id] = @qualification.id if @qualification
      attrs[:job_contract_id] = @job_contract.id if @job_contract
      attrs[:hospital_type_id] = @hospital_type.id if @hospital_type
      attrs[:job_type_id] = @job_type.id if @job_type
      attrs[:job_detail_id] = @job_detail.id if @job_detail

      search = Search.create(attrs)
      search.create_page(name: @title,
                         title: @title,
                         predefined: true,
                         url: @url,
                         created_at: Time.now,
                         updated_at: Time.now)
    end

    private
    def build
      @title = ""
      @url = ""

      if @prefecture
        @title += @prefecture.name
        @url += "/#{@prefecture.name_roman}"
      end
      if @area
        areas = [@area]
        a = @area.parent
        while a
          areas << a
          a = a.parent
        end
        areas.reverse.each do |area|
          @title += area.name
          @url += "/#{area.name_roman}"
        end
      end

      if @job_contract || @hospital_type || @job_type || @job_detail
        @title += 'で' if @title.length > 0
      else
        @title += 'の' if @title.length > 0
      end

      if @job_contract
        @title += "#{@job_contract.name}"
        @url += "/#{@job_contract.name_roman}"
      end
      if @hospital_type
        @title += "#{@hospital_type.name}"
        @url += "/#{@hospital_type.name_roman}"
      end
      if @job_type
        @title += "#{@job_type.name}"
        @url += "/#{@job_type.name_roman}"
      end
      if @job_detail
        @title += "#{@job_detail.name}"
        @url += "/#{@job_detail.name_roman}"
      end

      if @qualification
        @title += "#{@qualification.name}求人・転職"
        @url += "/#{@qualification.name_roman}"
      else
        @title += "看護師求人・転職"
      end

    end
  end

  def change

    record = SearchKey.find(1)

    Prefecture.all.each do |prefecture|
      #   f. /(prefecture)/(search_key)
      SearchData.new(prefecture, record).create

      prefecture.areas.each do |area1|
        #   g. /(prefecture)/(area1)/(search_key)
        SearchData.new(prefecture, area1, record).create

        area1.children.each do |area2|
          #   h. /(prefecture)/(area1)/(area2)/(search_key)
          SearchData.new(prefecture, area2, record).create

          area2.children.each do |area3|
            SearchData.new(prefecture, area3, record).create
          end
        end
      end
    end

    #   e. /(search_key)
    SearchData.new(record).create
  end

end
