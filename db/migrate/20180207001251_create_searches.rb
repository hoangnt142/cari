class CreateSearches < ActiveRecord::Migration[5.1]
  def change
    create_table :searches do |t|
      t.integer :prefecture_id, limit: 1, unsigned: true
      t.integer :area_id, limit: 2, unsigned: true
      t.integer :qualification_id, limit: 1, unsigned: true
      t.integer :job_contract_id, limit: 1, unsigned: true
      t.integer :hospital_type_id, limit: 1, unsigned: true
      t.integer :job_type_id, limit: 1, unsigned: true
      t.integer :job_detail_id, limit: 1, unsigned: true
      t.timestamps
    end
  end
end
