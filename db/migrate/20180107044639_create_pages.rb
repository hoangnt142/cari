class CreatePages < ActiveRecord::Migration[5.1]
  def change

    create_table :pages, id: :integer, unsigned: true, options: "ENGINE = Mroonga COMMENT = 'engine \"InnoDB\"' " do |t|

      t.integer :pageable_id, unsigned: true, default: nil
      t.string :pageable_type, default: nil
      t.string :name
      t.string :title
      t.boolean :predefined, null: false, default: 0
      t.string :url, null: false
      t.string :url_slug, default: nil
      t.string :seo_title, default: nil
      t.text :seo_description, default: nil
      t.string :seo_keywords, default: nil
      t.boolean :seo_robot_noindex, null: false, default: 0
      t.boolean :seo_robot_nofollow, null: false, default: 0
      t.integer :publish_status, limit: 1, null: false, default: 0, comment: '0: unpublished, 1: published, 2: deleted'
      t.datetime :publish_at, default: nil
      t.date  :page_date, default: nil
      t.text :content_top, default: nil
      t.text :content, null: false, default: ''
      t.text :content_bottom, default: nil
      t.integer :cta_id, unsigned: true, default: nil
      t.boolean :redirect, null: false, default: 0
      t.string :redirect_url, default: nil
      t.integer :last_updated_by, default: nil, unsigned: true
      t.text :cache_fulltext, default: nil
      t.timestamps
    end

    execute "CREATE FULLTEXT INDEX fi_pages_1 ON pages(cache_fulltext)"
    execute "CREATE UNIQUE INDEX ui_pages_1 ON pages(url)"

  end
end
