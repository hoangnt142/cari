class CreateRegions < ActiveRecord::Migration[5.1]
  def change
    create_table :regions, id: :integer, unsigned: true, limit: 1 do |t|
      t.string :name, null: false
      t.integer :total_jobs, unsigned: true, default: 0, null: false
      t.timestamps
    end
  end
end
