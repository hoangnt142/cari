class CreateArticles < ActiveRecord::Migration[5.1]
  def change
    create_table :articles, id: :integer, limit: 4, unsigned: true do |t|
      t.integer  :category_id, unsigned: true, limit: 2, default: nil
      t.timestamps
    end
    add_foreign_key "articles", "categories", name: "articles_fk2", on_delete: :nullify
  end
end
