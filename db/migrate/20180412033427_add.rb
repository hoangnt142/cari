class Add < ActiveRecord::Migration[5.1]
  def change
    Page.create(id: 407,
                name: '看護のお仕事',
                predefined: 1,
                url: '/website-guide/kango-no-oshigoto',
                url_slug: '/kango-no-oshigoto',
                seo_title: '',
                seo_description: '',
                seo_keywords: '',
                seo_robot_noindex: 0,
                seo_robot_nofollow: 0
               )
    Page.create(id: 408,
                name: 'マイナビ看護師',
                predefined: 1,
                url: '/website-guide/mynavi-kangoshi',
                url_slug: '/mynavi-kangoshi',
                seo_title: '',
                seo_description: '',
                seo_keywords: '',
                seo_robot_noindex: 0,
                seo_robot_nofollow: 0
               )
    Page.create(id: 409,
                name: 'ナース人材バンク',
                predefined: 1,
                url: '/website-guide/nurse-jinzai-bank',
                url_slug: '/nurse-jinzai-bank',
                seo_title: '',
                seo_description: '',
                seo_keywords: '',
                seo_robot_noindex: 0,
                seo_robot_nofollow: 0
               )
  end
end
