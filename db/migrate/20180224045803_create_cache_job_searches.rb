class CreateCacheJobSearches < ActiveRecord::Migration[5.1]
  def change
    create_table :cache_job_searches, id: :integer, unsigned: true, options: "ENGINE = Mroonga COMMENT = 'engine \"InnoDB\"' " do |t|
      t.integer :hospital_id, unsigned: true, null: false
      t.integer :job_id, unsigned: true, null: false
      t.integer :region_id, limit: 1, unsigned: true, null: false
      t.integer :prefecture_id, limit: 1, unsigned: true, null: false
      t.integer :city_id, unsigned: true, null: false
      t.integer :station_id, unsigned: true, default: nil
      t.integer :cache_job_search_keys_bits, limit: 8, unsigned: true, null: false, default: 0, comment: 'bits are used as 2^search_keys.id'
      t.text :cache_fulltext, default: nil

      t.timestamps
    end

    execute "CREATE FULLTEXT INDEX fi_cache_job_searches_1 ON cache_job_searches(cache_fulltext)"

  end
end
