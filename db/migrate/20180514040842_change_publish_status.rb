class ChangePublishStatus < ActiveRecord::Migration[5.1]
  def change
  end

  def data
    Page.where(id: [1,407,408,409,501]).update_all(publish_status: 1, publish_at: Time.now)
    Page.where(pageable_type: ['Search', 'Hospital']).update_all(publish_status: 1, publish_at: Time.now)
  end

  def rollback
    Page.where(id: [1,407,408,409,501]).update_all(publish_status: 0, publish_at: nil)
    Page.where(pageable_type: ['Search', 'Hospital']).update_all(publish_status: 0, publish_at: nil)
  end
end
