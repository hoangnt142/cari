class CreateCategories < ActiveRecord::Migration[5.1]
  def change
    create_table :categories, id: :integer, limit: 2, unsigned: true do |t|
      t.integer  :parent_id, unsigned: true, limit: 2, default: nil
      t.timestamps
    end
    add_foreign_key :categories, :categories, column: :parent_id, on_delete: :nullify, name: 'categories_fk1'
  end
end
