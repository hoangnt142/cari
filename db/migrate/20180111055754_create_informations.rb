class CreateInformations < ActiveRecord::Migration[5.1]
  def change
    create_table :informations, id: :integer, unsigned: true do |t|

      t.boolean :show_on_top_page, null: false, default: 1
      t.date :information_date
      t.string :title
      t.text :content

      t.timestamps
    end
  end
end
