class CreateHospitalHospitalTypes < ActiveRecord::Migration[5.1]
  def change
    create_table :hospital_hospital_types, id: :integer, unsigned: true do |t|
      t.integer :hospital_id, unsigned: true, null: false
      t.integer :search_key_id, unsigned: true, null: false, limit: 1
    end
  end
end
