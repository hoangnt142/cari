class Update < ActiveRecord::Migration[5.1]
  def change

    urls = [
      'https://px.a8.net/svt/ejp?a8mat=25YA4C+B2BZZ6+2JK4+62ENM',
      'https://px.a8.net/svt/ejp?a8mat=25ZV9T+FEX02Q+UBO+O2C9T',
      'https://px.a8.net/svt/ejp?a8mat=2Z8NZB+E7GCWY+3Y94+5YJRM'
    ]

    [1,2,3].each do |id|
      website = Website.find(id)
      website.redirect_url = urls[id - 1]
      website.save!
    end

  end
end
