class CreateSystemConfigs < ActiveRecord::Migration[5.1]
  def change
    create_table :system_configs, id: :integer, unsigned: true do |t|
      t.boolean :show_information, default: 0, null: false

      t.timestamps
    end
  end
end
