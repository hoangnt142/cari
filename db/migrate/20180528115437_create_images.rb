class CreateImages < ActiveRecord::Migration[5.1]
  def change
    create_table "images", force: true do |t|
      t.integer "category_type", null: false, default: 0, comment: "0: image library, 1: others"

      t.string   "title"
      t.text     "description"
      t.text     "caption"
      t.string   "alt", null: false, default: ""
      t.float    "width", null: false
      t.float    "height", null: false

      t.datetime "created_at",                       null: false
      t.datetime "updated_at",                       null: false
      t.string   "image_file_name",    limit: 255
      t.string   "image_content_type", limit: 255
      t.integer  "image_file_size",    limit: 4
      t.datetime "image_updated_at"
    end
  end
end
