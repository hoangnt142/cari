# frozen_string_literal: true

class DeviseCreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users, id: :integer, unsigned: true do |t|
      ## Database authenticatable
      t.string :email,              null: false, default: ""
      t.string :encrypted_password, null: false, default: ""

      ## Recoverable
      t.string   :reset_password_token
      t.datetime :reset_password_sent_at

      ## Rememberable
      t.datetime :remember_created_at

      ## Trackable
      t.integer  :sign_in_count, default: 0, null: false
      t.datetime :current_sign_in_at
      t.datetime :last_sign_in_at
      t.string   :current_sign_in_ip
      t.string   :last_sign_in_ip

      ## Confirmable
      # t.string   :confirmation_token
      # t.datetime :confirmed_at
      # t.datetime :confirmation_sent_at
      # t.string   :unconfirmed_email # Only if using reconfirmable

      ## Lockable
      # t.integer  :failed_attempts, default: 0, null: false # Only if lock strategy is :failed_attempts
      # t.string   :unlock_token # Only if unlock strategy is :email or :both
      # t.datetime :locked_at


      t.integer :gender, limit: 1, comment: "1: man, 2: woman"
      t.string :nickname, null: false
      t.date :birth, default: nil
      t.integer :prefecture_id, limit: 1, unsigned: true, default: nil
      t.integer :city_id, unsigned: true, default: nil
      t.integer :favorite_prefecture_id1, limit: 1, unsigned: true, default: nil
      t.integer :favorite_city_id1, unsigned: true, default: nil
      t.integer :favorite_prefecture_id2, limit: 1, unsigned: true, default: nil
      t.integer :favorite_city_id2, unsigned: true, default: nil

      t.boolean :banned, default: 0, null: false

      t.timestamps null: false
    end

    add_index :users, :email,                unique: true, name: 'ui_users_1'
    add_index :users, :reset_password_token, unique: true, name: 'ui_users_2'
    # add_index :users, :confirmation_token,   unique: true
    # add_index :users, :unlock_token,         unique: true
  end
end
