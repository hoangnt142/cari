class AddCssCustomToSystemConfigs < ActiveRecord::Migration[5.1]
  def change
    add_column :system_configs, :css_custom, :text, after: :show_information
  end
end
