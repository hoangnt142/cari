class AddNumberOfBedsText < ActiveRecord::Migration[5.1]
  def change
    add_column :hospitals, :number_of_beds_text, :string, default: nil, after: :number_of_beds
  end
end
