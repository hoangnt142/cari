class SetSeoInfoOnPages < ActiveRecord::Migration[5.1]
  def change
  end

  def data
    # Top
    id_title_description_keywords(
      1,
      '転職なら看護師求人サイト【Carriee（キャリー）】《%{import_log_created_at}》',
      "【全国対応】看護師の転職、求人を探すなら看護師の求人サイト「Carriee（キャリー）」にお任せ。人気の転職エージェントが保有している看護師求人やハローワークに掲載がある正社員・常勤・非常勤・夜勤専従の看護師求人がまとめて検索できます。各種病院求人はもちろん、クリニックや一般企業求人も検索できます。正看護師・准看護師・助産師・保健師・ケアマネジャー（介護支援専門員）求人も取り扱っています。"
    )

    # Search page
    id_title_description_keywords(
      501,
      '看護師求人' # TODO: check the text
    )

    # Search
    ActiveRecord::Base.connection.execute <<-SQL
UPDATE pages
SET seo_title = title
WHERE pageable_type='Search' AND pageable_id > 0
    SQL
  end

  def rollback
    # Top
    id_title_description_keywords(1)
    id_title_description_keywords(501)

    # Search
    ActiveRecord::Base.connection.execute <<-SQL
UPDATE pages
SET seo_title = NULL
WHERE pageable_type='Search' AND pageable_id > 0
    SQL
  end

  private
  def id_title_description_keywords(id, title=nil, description=nil, keywords=nil)
    page = Page.find(id)
    page.update!(title: title,
                 seo_title: title,
                 seo_description: description,
                 seo_keywords: keywords)
  end
end
