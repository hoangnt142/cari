class AddMeta < ActiveRecord::Migration[5.1]
  def change
  end

  def data
    page = Page.find(407)
    title = "｢看護のお仕事｣に登録する"
    page.update!(title: title, seo_title: title)

    page = Page.find(408)
    title = "｢マイナビ看護師｣に登録する"
    page.update!(title: title, seo_title: title)

    page = Page.find(409)
    title = "｢ナース人材バンク｣に登録する"
    page.update!(title: title, seo_title: title)

    page = Page.find(501)
    title = '看護師・助産師・保健師求人を検索する'
    page.update!(title: title,
                 seo_title: title,
                 seo_description: '看護師求人サイトのCarriee（キャリー）では%{count}件の看護師求人を掲載中！《%{import_log_created_at}》。人気の転職エージェントの看護師求人やハローワークの求人をまとめて検索することが出来ます。')
  end

  def rollback
    page = Page.find(407)
    page.update!(title: nil, seo_title: '')

    page = Page.find(408)
    page.update!(title: nil, seo_title: '')

    page = Page.find(409)
    page.update!(title: nil, seo_title: '')

    page = Page.find(501)
    page.update!(title: nil, seo_title: '看護師求人', seo_description: '')
  end


end
