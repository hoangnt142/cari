CREATE SERVER crawler
  FOREIGN DATA WRAPPER mysql
OPTIONS(
  HOST 'carriee-crawler',
  DATABASE 'carriee-crawler',
  USER 'root',
  PASSWORD ''
);

create database crawler_db;

create table crawler_db.hospital_versions (
  `id` int(10) unsigned NOT NULL,
  `hospital_id` int(10) unsigned NOT NULL,
  `version` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `hospital_type_id` int(10) unsigned DEFAULT NULL COMMENT 'job_keys.id and job_keys.type="HospitalType"',
  `number_of_beds` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `postal_code` varchar(7) COLLATE utf8mb4_bin DEFAULT NULL,
  `prefecture_id` tinyint(3) unsigned NOT NULL,
  `city_id` int(10) unsigned NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `access` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `medical_subjects` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `standard` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `karte` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `business_time` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `holidays` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `hospital_url` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `features` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `fax` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `representative` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `established_date` date DEFAULT NULL,
  `longitude` float NOT NULL DEFAULT 0,
  `latitude` float NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) engine=SPIDER comment='wrapper "mysql", srv "crawler"'
;

create table crawler_db.hospital_version_hospital_types (
  `id` bigint(20) unsigned NOT NULL,
  `hospital_version_id` int(10) unsigned NOT NULL,
  `search_key_id` tinyint(3) unsigned NOT NULL
) engine=SPIDER comment='wrapper "mysql", srv "crawler"'
;

create table crawler_db.jobs (
  `id` int(10) unsigned NOT NULL,
  `website_id` tinyint(3) unsigned NOT NULL,
  `hospital_id` int(10) unsigned NOT NULL,
  `original_url` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `salary_type` tinyint(4) DEFAULT NULL COMMENT '1: 時給, 2: 日給, 3: 月給: 4: 年収',
  `salary` int(11) DEFAULT NULL,
  `salary_detail` varchar(5000) COLLATE utf8mb4_bin DEFAULT NULL,
  `allowance` varchar(5000) COLLATE utf8mb4_bin DEFAULT NULL,
  `work_time` varchar(5000) COLLATE utf8mb4_bin DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) engine=SPIDER comment='wrapper "mysql", srv "crawler"'
;

create table crawler_db.job_search_keys (
  `id` bigint(20) unsigned NOT NULL,
  `job_id` int(10) unsigned NOT NULL,
  `search_key_id` tinyint(3) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) engine=SPIDER comment='wrapper "mysql", srv "crawler"'
;
