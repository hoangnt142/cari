
-- set area1_id
UPDATE cities c
INNER JOIN areas a1
ON a1.prefecture_id=c.prefecture_id
AND a1.parent_id IS NULL
AND c.name like concat(a1.name,'%')
SET c.area1_id=a1.id
;
