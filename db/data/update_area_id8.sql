-- その他東京都
update cities c
inner join areas a1 on a1.prefecture_id=c.prefecture_id
inner join areas a2 on a1.id=a2.parent_id
inner join areas a3 on a2.id=a3.parent_id and a3.name = c.name
set c.area3_id = a3.id, c.area2_id = a2.id
where a1.parent_id is null
;
