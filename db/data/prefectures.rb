require 'shell'

prefectures = [
  [1,1, '北海道'],
  [2,2, '青森県'],
  [3,2, '岩手県'],
  [4,2, '宮城県'],
  [5,2, '秋田県'],
  [6,2, '山形県'],
  [7,2, '福島県'],
  [8,3, '茨城県'],
  [9,3, '栃木県'],
  [10,3, '群馬県'],
  [11,3, '埼玉県'],
  [12,3, '千葉県'],
  [13,3, '東京都'],
  [14,3, '神奈川県'],
  [15,4, '新潟県'],
  [16,4, '富山県'],
  [17,4, '石川県'],
  [18,4, '福井県'],
  [19,4, '山梨県'],
  [20,4, '長野県'],
  [21,4, '岐阜県'],
  [22,4, '静岡県'],
  [23,4, '愛知県'],
  [24,5, '三重県'],
  [25,5, '滋賀県'],
  [26,5, '京都府'],
  [27,5, '大阪府'],
  [28,5, '兵庫県'],
  [29,5, '奈良県'],
  [30,5, '和歌山県'],
  [31,6, '鳥取県'],
  [32,6, '島根県'],
  [33,6, '岡山県'],
  [34,6, '広島県'],
  [35,6, '山口県'],
  [36,7, '徳島県'],
  [37,7, '香川県'],
  [38,7, '愛媛県'],
  [39,7, '高知県'],
  [40,8, '福岡県'],
  [41,8, '佐賀県'],
  [42,8, '長崎県'],
  [43,8, '熊本県'],
  [44,8, '大分県'],
  [45,8, '宮崎県'],
  [46,8, '鹿児島県'],
  [47,9, '沖縄県']]

sql =<<SQL
  INSERT INTO prefectures (id,region_id,name,name_roman,created_at,updated_at) VALUES
SQL
data = []

sh = Shell.new
prefectures.each do |prefecture|
  #  roman = sh.transact { system("echo", prefecture[2]) | system("nkf", "-e") | system("kakasi", "-Ja") | system("nkf", "-w") }
  roman = sh.transact { system("echo", prefecture[2]) | system("kakasi", "-Ja", "-Ha", "-Ka", "-Ea", "-i", "utf-8", "-o", "utf-8") }
  roman = roman.to_s.sub(/\n/,'')
  data << "(#{prefecture[0]},#{prefecture[1]},'#{prefecture[2]}','#{roman}',NOW(),NOW())"
end

sql += data.join(',') + ";"
#puts sql

File.open File.dirname(__FILE__)+"/prefectures.sql", "w" do |f|
  f.write sql
end
