
--
update cities c
inner join(
select a2.*, a1.name as name1, a2.name as name2 from areas a1
inner join areas a2 on a1.id=a2.parent_id
where a1.parent_id is null
order by a2.prefecture_id
) a on c.prefecture_id=a.prefecture_id and c.name like concat('%', a.name, '%') and (c.name not like '%区' and c.name not like '%市')
set c.area2_id=a.id
;
