
-- その他東京都(伊豆諸島)
update cities
set area2_id=(select parent_id from areas where name = '伊豆諸島'), area3_id=(select id from areas where name = '伊豆諸島')
where name in ('大島町','利島村','新島村','神津島村','三宅島三宅村','御蔵島村','八丈島八丈町','青ヶ島村','小笠原村');
