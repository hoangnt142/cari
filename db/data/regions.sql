insert into regions (id,name,created_at,updated_at) values
(1,'北海道',now(),now()),
(2,'東北',now(),now()),
(3,'関東',now(),now()),
(4,'中部',now(),now()),
(5,'近畿',now(),now()),
(6,'中国',now(),now()),
(7,'四国',now(),now()),
(8,'九州',now(),now()),
(9,'沖縄',now(),now());
