-- set area1_id for the remained
update cities c
inner join (
  select id,prefecture_id
  from areas
  where parent_id is null
  and name like 'その他%') a1 on c.prefecture_id=a1.prefecture_id and c.area1_id is null
set c.area1_id = a1.id
;
